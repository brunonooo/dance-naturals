<?php

    /*
      Plugin Name: Woo Multisite Product & Category Sync
      Description: Plugin syncs woocommerce/wordpress > posts/products & categories/taxonomies across multiple stores
      Version:     1.0
      Author:      smgom
      Text Domain: WMPCS
     */

    defined('ABSPATH') or die('No script kiddies please!');

    //check if multisite is enabled, wmpts is designed to work with multisites only
    function wmpcs_activate() {
        if (!defined('WP_ALLOW_MULTISITE') || !defined('MULTISITE')) {
            deactivate_plugins(plugin_basename(__FILE__));
            wp_die(__('WMPCS : Multisite network must be enabled to use multisite sync plugin!', 'WMPCS'));
        }
    }

    if(!function_exists('pr')) {
        function pr($data) {
            echo "<pre>";
            print_r($data);
            echo "</pre>";
        }
    }

    register_activation_hook(__FILE__, "wmpcs_activate");

    add_action('plugins_loaded', 'loadWMPCSPlugin');
    
    function loadWMPCSPlugin() {
        if (is_admin()) { //since plugin only works on admin side
            define('WMPCS_DIR', plugin_dir_path(__FILE__));
            define('WMPCS_URL', plugin_dir_url(__FILE__));
            define('WMPCS_OPTION_NAME', 'wmpcs_options');
            
            $options = get_option(WMPCS_OPTION_NAME);
            //load settings file
            require_once plugin_dir_path(__FILE__) . 'inc/wmpcs-settings.php';
            new WMPCS_Setting($options);
           
            //admin screens UI
            require_once plugin_dir_path(__FILE__) . 'inc/admin/admin.php';

            if (!empty($options)) {
                //load the main file
                require_once plugin_dir_path(__FILE__) . 'inc/loader.php';
                // Global for backwards compatibility.
                $GLOBALS['WMPCS_Loader'] = WMPCS_Loader($options);
            }
        } else {
            add_action('woocommerce_thankyou', 'sync_order', 10);
        }
    }

    function sync_order($order_id) {
        remove_action('woocommerce_thankyou', 'sync_order', 10); //remove hook 
        define('WMPCS_OPTION_NAME', 'wmpcs_options');
        $options = get_option(WMPCS_OPTION_NAME);

         //load settings file
        require_once plugin_dir_path(__FILE__) . 'inc/wmpcs-settings.php';
        new WMPCS_Setting($options);
        if (!empty($options)) {
            $current_blog_id = get_current_blog_id();
            $post_sync_options = $options['post_sync'][$current_blog_id];
            if (in_array('shop_order',$post_sync_options['post_types'])) {
                $sync_to_sites = $post_sync_options['dests'];
                if(!empty($sync_to_sites)) {
                    //product common functions file
                    require_once plugin_dir_path(__FILE__) . 'inc/post-sync/functions.php';

                    //sync class
                    require_once plugin_dir_path(__FILE__) . 'inc/post-sync/class-wmpcs-post-sync.php';

                    foreach ($sync_to_sites as $site_id) {
                        WMPCS_Post_Sync::sync_post_to_site($order_id, $site_id);
                    }
                }
            }
        }
    }