jQuery(function ($) {
	$(document).ready(function(){
		var required_select = $("#bulk-action-selector-top");
		var sites_data = $.parseJSON(wmpts_taxonomy_bulk_sync.other_sites_data)
		$.each( sites_data, function(site_id,site_name){
		 	jQuery("<option/>", {value: "wmpcs-selected-"+site_id, text: "Sync selected to "+site_name}).appendTo(required_select);
		});
	});
});