jQuery(function ($) {
    $(document).on('click', ".wmpcs_remove_row", function (e) {
        e.preventDefault();
        $(this).parent().parent('tr').remove();
    });

    $(document).ready(function () {
        if ($("#enable_post_sync").attr('checked')) {
            $(".post_sync_flow,.post_sync_flow_head,.post_sync_flow_footer").show();
        } else {
            $(".post_sync_flow,.post_sync_flow_head,.post_sync_flow_footer").hide();
        }

        if ($("#enable_taxonomy_sync").attr('checked')) {
            $(".taxonomy_sync_flow,.taxonomy_sync_flow_head,.taxonomy_sync_flow_footer").show();
        } else {
            $(".taxonomy_sync_flow,.taxonomy_sync_flow_head,.taxonomy_sync_flow_footer").hide();
        }

        $("#enable_post_sync").on('click', function () {
            if ($(this).attr('checked')) {
                $(".post_sync_flow,.post_sync_flow_head,.post_sync_flow_footer").show();
            } else {
                $(".post_sync_flow,.post_sync_flow_head,.post_sync_flow_footer").hide();
            }
        });

        var total_sites = $("input[name='total_sites']").val();

        $("#addMorepostSyncFlow").on('click', function (e) {
            e.preventDefault();
            var total_created_sync_flows = $(".post_sync_flow").length;
            if (total_created_sync_flows < total_sites) {
                var prev_row = $(this).parent('td').parent("tr").prev(".post_sync_flow");
                var prev_index = $(prev_row).data('index');
                var this_index = prev_index + 1;
                var already_selected_source_taxonomies = {};
                $(".post_sync_flow").each(function (index) {
                    already_selected_source_taxonomies[index] = $(this).find(".source_post_select").val();
                });

                prev_row.after(prev_row.clone());
                if(this_index == 1) {
                    $(".post_sync_flow:last").append('<td><button class="wmpcs_remove_row">Remove Flow</button></td>');
                }
                
                $(".post_sync_flow:last").data('index', this_index);
                $(".multiple_post_select_box:last").attr("name", "post_dest_sites[" + this_index + "][]");
                $(".post_auto_sync:last").attr("name", "post_auto_sync[" + this_index + "]");
                $(".post_bulk_sync:last").attr("name", "post_bulk_sync[" + this_index + "]");

                $.each(already_selected_source_taxonomies, function (i, val) {
                    $('.source_post_select:last option[value="' + val + '"]').attr('disabled', true);
                });
                
                $(".source_post_select:last option:not([disabled])").first().attr("selected", "selected");

                $(".multiple_post_select_box:last").find('input').val('');
                
            } else {
                alert("You have already defined all possible sync flows");
            }
        });

        $("#enable_taxonomy_sync").on('click', function () {
            if ($(this).attr('checked')) {
                $(".taxonomy_sync_flow,.taxonomy_sync_flow_head,.taxonomy_sync_flow_footer").show();
            } else {
                $(".taxonomy_sync_flow,.taxonomy_sync_flow_head,.taxonomy_sync_flow_footer").hide();
            }
        });

        $("#addMoretaxonomySyncFlow").on('click', function (e) {
            e.preventDefault();
            var total_created_sync_flows = $(".taxonomy_sync_flow").length;
            if (total_created_sync_flows < total_sites) {
                var prev_row = $(this).parent('td').parent("tr").prev(".taxonomy_sync_flow");
                var prev_index = $(prev_row).data('index');
                var this_index = prev_index + 1;
                var already_selected_source_taxonomies = {};
                $(".taxonomy_sync_flow").each(function (index) {
                    already_selected_source_taxonomies[index] = $(this).find(".source_taxonomy_select").val();
                });

                prev_row.after(prev_row.clone());
                if(this_index == 1) {
                    $(".taxonomy_sync_flow:last").append('<td><button class="wmpcs_remove_row">Remove Flow</button></td>');
                }
                $(".taxonomy_sync_flow:last").data('index', this_index);
                $(".multiple_taxonomy_select_box:last").attr("name", "taxonomy_dest_sites[" + this_index + "][]");
                $(".taxonomy_auto_sync:last").attr("name", "taxonomy_auto_sync[" + this_index + "]");
                $(".taxonomy_bulk_sync:last").attr("name", "taxonomy_bulk_sync[" + this_index + "]");

                $.each(already_selected_source_taxonomies, function (i, val) {
                    $('.source_taxonomy_select:last option[value="' + val + '"]').attr('disabled', true);
                });
                $(".source_taxonomy_select:last option:not([disabled])").first().attr("selected", "selected");
                
                $(".multiple_taxonomy_select_box:last").find('input').val('');
            } else {
                alert("You have already defined all possible sync flows");
            }
        });
    });
});
