<?php

    class WMPCS_Setting {

        //posts Settings
        public static $allowed_post_types = array('product', 'post', 'page', 'shop_order', 'shop_coupon');
        //posts Settings
        public static $not_allowed_default_post_types = array('revision', 'attachment','shop_webhook','customize_changeset','custom_css','nav_menu_item','shop_order_refund');

        public static $SYNC_ENABLED_POST_META_KEY_PREFIX = 'wmpcs_post_sync_enabled_for_site_';
        public static $SYNC_DISABLED_POST_META_KEY_PREFIX = 'wmpcs_post_sync_disabled_for_site_';
        public static $SYNCED_POST_ID_PREFIX = 'wmpcs_synced_post_id_on_site_';
        
        
        
        public static $SYNCED_COMMENT_ID_PREFIX = 'wmpcs_synced_comment_id_on_site_';
        //term Settings
        public static $taxonomies_to_sync = array('product_cat');
        
        public static $NOT_ALLOWED_DEFAULT_TAXONOMIES = array(
            'product_tag',
            'product_shipping_class',
            'product_type',
            'post_format',
            'link_category',
            'nav_menu'
        );
        
        public static $SYNCED_TERM_ID_PREFIX = 'wmpcs_synced_term_id_on_site_';
        public static $SYNC_ENABLED_TAXONOMY_META_KEY_PREFIX = 'wmpcs_taxonomy_sync_enabled_for_site_';
        public static $SYNC_DISABLED_TAXONOMY_META_KEY_PREFIX = 'wmpcs_taxonomy_sync_disabled_for_site_';
        public static $PLUGIN_OPTIONS = array();
        
        public function __construct($options) {
            if (!empty($options)) {
                self::$PLUGIN_OPTIONS = $options;
            }
        }

        public static function getAllowedTaxonomies() {
            
            if(isset(self::$PLUGIN_OPTIONS['taxonomy_sync'])) {
                $taxonomy_sync_options = array_values(self::$PLUGIN_OPTIONS['taxonomy_sync']);

                if(isset($taxonomy_sync_options[0]['taxonomies'])) {
                    return $taxonomy_sync_options[0]['taxonomies'];
                }
            }
            return array();
        }
        
        public static function woo_version_check( $version = '3.0' ) {
            if ( class_exists( 'WooCommerce' ) ) {
                global $woocommerce;
                if ( version_compare( $woocommerce->version, $version, ">=" ) ) {
                    return true;
                }
            }
            return false;
        }

    }
    