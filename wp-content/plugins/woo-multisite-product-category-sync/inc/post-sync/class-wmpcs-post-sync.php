<?php

class WMPCS_Post_Sync {

	static function perform_sync_for_post($post_id) {

		// get ids of sites that post needs to be synced to
		$sites = WMPCS_Post_Sync::get_site_ids_for_post($post_id);

		// if there are no sites that the post needs to be synced to return
		if (empty($sites)) {
			return;
		}

		// perform sync to each site
		foreach ($sites as $site) {
			WMPCS_Post_Sync::sync_post_to_site($post_id, $site);
		}
	}

	/**
	 * Function that retreives the site ids that the post will be sent to
	 * @param $post_id - interger id of the post
	 * @return array containing site ids to sync to if they are set for the post
	 * @return empty array if no ids have been set for the post
	 */
	static function get_site_ids_for_post($post_id) {
		$sites = array();

		$all_defined_sites = WMPCS_CONF::getOtherPostSyncSites();

		foreach ($all_defined_sites as $site_id => $site_url) {
			$meta_key = WMPCS_CONF::$post_sync_enabled_for_site_metakey . $site_id;
			$is_site_enabled = get_post_meta($post_id, $meta_key, TRUE);
			if ($is_site_enabled == 'yes') {
				$sites[] = $site_id;
			}
		}
		return $sites;
	}


    static function sync_post_to_site($post_id, $site_id) {
        $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
        $post_type = get_post_type($post_id);

        $first = true;
        $trid = null;
        $trid_language_code = null;

        foreach ($languages as $key => $language)
        {
            $id = icl_object_id($post_id, $post_type, false, $key);
            if($id != null) {
                $sync_id = WMPCS_Post_Sync::sync_post_to_site_single($id, $site_id);
                if ($first) {
                    $trid = $sync_id;
                    $trid_language_code = $key;
                    $set_language_args = array(
                        'element_id' => $sync_id,
                        'element_type' => get_post_type($sync_id),
                        'trid' => null,
                        'language_code' => $key,
                        'source_language_code' => null
                    );
                    $first = false;
                } else {
                    $set_language_args = array(
                        'element_id' => $sync_id,
                        'element_type' => get_post_type($sync_id),
                        'trid' => $trid,
                        'language_code' => $key,
                        'source_language_code' => $trid_language_code
                    );
                }
                do_action('wpml_set_element_language_details', $set_language_args);
            }
        }
    }

	/**
	 * Function to sync post to individual site
	 * @param $post_id - interger id of the post to sync
	 * @param $site_id - the id of the individual site to sync to
	 * @return id of the post that was created/updated in the multisite, if successful
	 * @return null if error or unsuccessful
	 */
	static function sync_post_to_site_single($post_id, $site_id) {
		global $wpdb;

		//get post
		$post_type = get_post_type($post_id);

		$current_site_id = get_current_blog_id();

		if (WMPCS_Setting::woo_version_check() && $post_type == 'product') {
			//get post
			$post = get_post($post_id, ARRAY_A); // get the original post

			$terms = get_the_terms($post_id, 'product_type');

			$woo_product_type = sanitize_title(current($terms)->name);

			if ($woo_product_type == 'variable') {

				//get variations
				$defaults = array(
					'numberposts' => -1,
					'post_type' => 'product_variation',
					'post_status' => 'any',
					'post_parent' => $post_id,
				);

				$post_variations = get_posts($defaults, ARRAY_A);
				if (!empty($post_variations)) {
					foreach ($post_variations as $key => $post_variation) {
						//preserve the ID for later mapping
						$post_variations[$key]->actual_id = $post_variation->ID; //needed for resync
						//get variation metadata
						$post_variations[$key]->meta_data = get_post_custom($post_variation->ID);
						$variation_thumbnail_image_id = get_post_meta($post_variation->ID, '_thumbnail_id', true);
						$variation_thumbnail_image_data = "";
						if (!empty($variation_thumbnail_image_id)) {
							$variation_thumbnail_image_data = wmpcs_get_attachment_data($variation_thumbnail_image_id);
						}

						$post_variations[$key]->variation_thumbnail_image_data = $variation_thumbnail_image_data;

						//VARIATION ADD OR UPDATE
						$check_if_already_synced = get_post_meta($post_variation->ID, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);

						if (empty($check_if_already_synced)) {
							$post_variations[$key]->ID = ''; // ADD POST REQUEST
						} else {
							$post_variations[$key]->ID = $check_if_already_synced; //UPDATE REQUEST
						}
					}
				}
			} elseif ($woo_product_type == 'grouped') {
				$grouped_post = new WC_Product_Grouped($post_id);
				if ($grouped_post->has_child()) {
					$child_grouped_posts = array();
					foreach ($grouped_post->get_children() as $child) {
						$group_child_id = WMPCS_Post_Sync::sync_post_to_site($child, $site_id);
						if (!empty($group_child_id)) {
							WMPCS_Post_Sync::save_synced_site_and_id_to_post($child, $site_id, $group_child_id);
							$child_grouped_posts[] = $group_child_id;
						}
					}
				}
			} elseif ($woo_product_type == 'bundle') {
				$bundle_posts = get_post_meta($post_id, '_bundle_data', TRUE);
				$dest_bundle_posts = array();
				if (!empty($bundle_posts)) {
					foreach ($bundle_posts as $key => $bundle_post) {
						$bundle_child_id = WMPCS_Post_Sync::sync_post_to_site($bundle_post['post_id'], $site_id);
						if ($bundle_child_id) {
							WMPCS_Post_Sync::save_synced_site_and_id_to_post($bundle_post['post_id'], $site_id, $bundle_child_id);
							$dest_bundle_posts[$bundle_child_id] = $bundle_post;
							$dest_bundle_posts[$bundle_child_id]['post_id'] = $bundle_child_id;
						} else {
							unset($bundle_posts[$key]);
						}
					}
				}
			}

			//get acf banner image
			$source_banner_image = get_post_meta($post_id, 'banner_image', true);

			if (!empty($source_banner_image)) {
				$source_banner_image_data = wmpcs_get_attachment_data($source_banner_image);
			}

			//get post terms according to defined taxonomies
			$taxonomies_to_sync = WMPCS_Setting::getAllowedTaxonomies();
			if (!empty($taxonomies_to_sync)) {
				$i = 0;
				$source_terms = array();

				foreach ($taxonomies_to_sync as $taxonomy) {
					$post_terms = wp_get_post_terms($post_id, $taxonomy, array("fields" => "ids"));

					if (!empty($post_terms)) {
						foreach ($post_terms as $post_term_id) {
							$synced_cat_id = get_term_meta($post_term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
							if (!empty($synced_cat_id)) {
								$source_terms[$i]['term_id'] = $synced_cat_id;
								$source_terms[$i]['term_taxonomy'] = $taxonomy;
								$i++;
							}
						}
						unset($post_terms);
					}
				}
			}

			//UPDATE OR ADD CHECK
			$check_if_already_synced = get_post_meta($post_id, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);

			if (empty($check_if_already_synced)) {
				$post['ID'] = ''; // empty id field, to tell wordpress that this will be a new post
			} else {
				$post['ID'] = $check_if_already_synced;
			}

			$sourcetags = get_the_terms($post_id, 'product_tag');
			//Get product attached images
			$source_gallery_attachment_ids = get_post_meta($post_id, '_product_image_gallery', true);

			$source_gallery_images = array();
			if (!empty($source_gallery_attachment_ids)) {

				$source_gallery_attachment_ids = explode(',', $source_gallery_attachment_ids);

				foreach ($source_gallery_attachment_ids as $source_gallery_attachment_id) {
					$image = wp_get_attachment_image_src($source_gallery_attachment_id, 'full');

					if ($image) {
						$source_gallery_images[] = array(
							'url' => $image[0],
							'alt' => get_post_meta($source_gallery_attachment_id, '_wp_attachment_image_alt', true),
							'post_title' => get_post_field('post_title', $source_gallery_attachment_id),
							'description' => get_post_field('post_content', $source_gallery_attachment_id),
							'caption' => get_post_field('post_excerpt', $source_gallery_attachment_id),
							'post_name' => get_post_field('post_name', $source_gallery_attachment_id),
							'path' => get_attached_file($source_gallery_attachment_id)
						);
					}
				}
			}

			//get featured image
			$source_featued_image_id = get_post_thumbnail_id($post_id);
			if (!empty($source_featued_image_id)) {
				$dest_featued_image_id = get_post_meta($source_featued_image_id, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);

				if (empty($dest_featued_image_id)) {
					$featured_image = wmpcs_get_featured_image_from_source($source_featued_image_id);
				}
			}

			//get all meta for the post
			$meta_data = get_post_custom($post_id);

			$source_attributes = get_post_meta($post_id, '_product_attributes', true);

			if (!empty($source_attributes)) {
				$source_product_attributes = array();
				foreach ($source_attributes as $key => $source_attribute) {
					if ($source_attribute['is_taxonomy']) {
						$new_options = array();
						$source_product_attributes[$key] = $source_attribute;
						$product_attribute_terms = get_the_terms($post_id, $source_attribute['name']);
						if (!empty($product_attribute_terms)) {
							foreach ($product_attribute_terms as $product_attribute_term) {

								$inserted_term_id = get_term_meta($product_attribute_term->term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);

								if ($inserted_term_id) {
									$product_attribute_term->term_id = $inserted_term_id;
								} else {
									$product_attribute_term->site_term_id = $product_attribute_term->term_id;
									$product_attribute_term->term_id = 0;
								}

								$source_product_attributes[$key]['values'][] = $product_attribute_term;
							}
						}
					} else {
						$source_product_attributes[$key] = $source_attribute;
					}
				}
			}

			//SWITCH TO TARGET SITE
			switch_to_blog($site_id);

			$inserted_post_id = wp_insert_post($post);
			$post['ID'] = $post['ID'] == '' ? $post_id : $post['ID'];
			sync_element_language($post, $inserted_post_id);

			if (!empty($source_terms)) {
				foreach ($source_terms as $source_term) {
					if (!is_object_in_term($inserted_post_id, $source_term['term_taxonomy'], [$source_term['term_id']])) {
						wp_set_post_terms($inserted_post_id, [$source_term['term_id']], $source_term['term_taxonomy'], TRUE);
					}
				}
			}

			//Add the source post meta to the destination post
			foreach ($meta_data as $key => $meta_values) {
				if ($key == '_thumbnail_id' || $key == '_product_attributes') {
					continue;
				}

				foreach ($meta_values as $meta_value) {
					$meta_value = maybe_unserialize($meta_value);
					update_post_meta($inserted_post_id, $key, $meta_value);
				}
			}

			if (isset($source_banner_image_data)) {
				$attach_id = wmpcs_set_gallery_images_to_destination($inserted_post_id, $source_banner_image_data);

				if (!empty($attach_id)) {
					update_post_meta($inserted_post_id, 'banner_image', $attach_id);
				}
			}

			if (!empty($source_featued_image_id)) {
				//If there was a featured image in the source post then copy it over
				if (!empty($dest_featued_image_id)) {
					// And finally assign featured image to post
					set_post_thumbnail($inserted_post_id, $dest_featued_image_id);
				} else {

					$new_dest_featued_image_id = wmpcs_set_featured_image_to_destination($inserted_post_id, $featured_image);

					WMPCS_Post_Sync::save_synced_site_and_id_to_post($new_dest_featued_image_id, $current_site_id, $source_featued_image_id);

					// And finally assign featured image to post
					set_post_thumbnail($inserted_post_id, $new_dest_featued_image_id);
				}
			}

			$dest_product_attributes = array();

			if (isset($source_product_attributes) && !empty($source_product_attributes)) {
				foreach ($source_product_attributes as $attribute_key => $source_product_attribute) {
					$value = '';
					if ($source_product_attribute['is_taxonomy']) {

						if (taxonomy_exists($attribute_key)) {

							$attribute_values = $source_product_attribute['values'];
							$tag_list = array();

							foreach ($attribute_values as $attribute_value) {
								if ($attribute_value->id) {
									$tag_list[] = $attribute_value->id;
								} else {
									$term_exists = term_exists($attribute_value->name, $attribute_value->taxonomy);

									if (!$term_exists) {

										$inserted_tag = wp_insert_term($attribute_value->name, $attribute_value->taxonomy, array('description' => $attribute_value->description));

										if (!is_wp_error($inserted_tag)) {
											$tag_list[] = $attribute_value->name;
										}
									} else {
										$tag_list[] = $attribute_value->name;
									}
								}
							}

							wp_set_post_terms($inserted_post_id, $tag_list, $attribute_key);

							unset($source_product_attribute['values']);

							$dest_product_attributes[$attribute_key] = $source_product_attribute;
						}
					} else {
						$dest_product_attributes[$attribute_key] = $source_product_attribute;
					}
				}

				update_post_meta($inserted_post_id, '_product_attributes', $dest_product_attributes);
			}

			if ($woo_product_type == 'variable') {
				$return_childrens = array();

				foreach ($post_variations as $product_variation_post) {
					$meta_data = $product_variation_post->meta_data;
					$var_main_store_id = $product_variation_post->actual_id;
					$variation_thumbnail_image_data = $product_variation_post->variation_thumbnail_image_data;
					$variation_attachment_images = $product_variation_post->variation_attachment_images;
					unset($product_variation_post->actual_id);
					unset($product_variation_post->meta_data);
					unset($product_variation_post->meta_value);
					unset($product_variation_post->variation_thumbnail_image_data);

					$product_variation_post->post_parent = $inserted_post_id;

					if (!empty($product_variation_post->ID) && FALSE === get_post_status($product_variation_post->ID)) { //post doesn't exists here
						$product_variation_post->ID = '';
					}

					$child_post_id = wp_insert_post($product_variation_post); // insert the post

					if ($child_post_id) {
						$return_childrens[$var_main_store_id] = $child_post_id;
						//Add the source post meta to the destination post
						foreach ($meta_data as $key => $values) {
							if ($key == '_max_price_variation_id' || $key == '_max_regular_price_variation_id' || $key == '_min_price_variation_id' || $key == '_min_regular_price_variation_id' || $key == '_min_sale_price_variation_id') {
								continue;
							}
							foreach ($values as $value) {
								update_post_meta($child_post_id, $key, $value);
							}
						}
						if (isset($variation_thumbnail_image_data)) {

							//delete existing images
							$product_varition_current_thumbnail_image = get_post_meta($child_post_id, '_thumbnail_id', true);

							if (!empty($product_varition_current_thumbnail_image)) {
								wp_delete_attachment($product_varition_current_thumbnail_image, TRUE);
							}
							$attach_id = wmpcs_set_gallery_images_to_destination($child_post_id, $variation_thumbnail_image_data);
							if (!empty($attach_id)) {
								update_post_meta($child_post_id, '_thumbnail_id', $attach_id);
							}
						}
					}
				}


				wp_set_object_terms($inserted_post_id, 'variable', 'product_type');

				WC_Product_Variable::sync($inserted_post_id);
			} elseif ($woo_product_type == 'grouped' && isset($child_grouped_posts) && !empty($child_grouped_posts)) {


				foreach ($child_grouped_posts as $child_grouped_post) {
					wp_update_post(
						array(
							'ID' => $child_grouped_post,
							'post_parent' => $inserted_post_id
						)
					);
				}

				wp_set_object_terms($inserted_post_id, 'grouped', 'product_type');
			} elseif ($woo_product_type == 'bundle' && !empty($dest_bundle_posts)) {

				update_post_meta($inserted_post_id, '_bundle_data', $dest_bundle_posts);
				wp_set_object_terms($inserted_post_id, 'bundle', 'product_type');
			}

			if ($sourcetags) {
				$tag_list = array();
				foreach ($sourcetags as $sourcetag) {
					if (!empty($sourcetag->name)) {
						$term_exists = term_exists($sourcetag->name, 'product_tag');
						if (!$term_exists) {
							$inserted_tag = wp_insert_term($sourcetag->name, 'product_tag');
							if (!is_wp_error($inserted_tag)) {
								$tag_list[] = $inserted_tag['term_id'];
							}
						} else {
							$tag_list[] = $term_exists['term_id'];
						}
					}
				}

				if (!empty($tag_list)) {
					wp_set_post_tags($inserted_post_id, $tag_list);
				}
			}

			if (isset($source_gallery_images)) {
				$created_gallery_images = array();

				//delete existing images
				$product_current_images = get_post_meta($inserted_post_id, '_product_image_gallery', true);

				if (!empty($product_current_images)) {
					$current_product_images_array = explode(',', $product_current_images);
					foreach ($current_product_images_array as $current_product_image_id) {
						wp_delete_attachment($current_product_image_id, TRUE);
					}
				}

				if (!empty($current_product_images_array)) {
					foreach ($current_product_images_array as $current_product_image_id) {
						wp_delete_attachment($current_product_image_id, TRUE);
					}
				}

				foreach ($source_gallery_images as $source_gallery_image) {
					$attach_id = wmpcs_set_gallery_images_to_destination($inserted_post_id, $source_gallery_image);
					if (!empty($attach_id)) {
						$created_gallery_images[] = $attach_id;
					}
				}

				if (!empty($created_gallery_images)) {
					update_post_meta($inserted_post_id, '_product_image_gallery', implode(",", $created_gallery_images));
				}
			}

			WMPCS_Post_Sync::save_synced_site_and_id_to_post($inserted_post_id, $current_site_id, $post_id);

			restore_current_blog(); //return to original blog

			if (isset($return_childrens) && !empty($return_childrens)) {
				$return['post_id'] = $inserted_post_id;
				$return['childs'] = $return_childrens;
				if (!empty($return_childrens)) {
					foreach ($return_childrens as $var_old => $var_new) {
						WMPCS_Post_Sync::save_synced_site_and_id_to_post($var_old, $site_id, $var_new);
					}
				}
			}

			WMPCS_Post_Sync::save_synced_site_and_id_to_post($post_id, $site_id, $inserted_post_id);

			return $inserted_post_id;
		} else {
			return self::sync_post_to_site_old($post_id, $site_id);
		}
	}

	static function sync_post_to_site_old($post_id, $site_id) {
		global $wpdb;
		$current_site_id = get_current_blog_id();

		$source_site_url = get_site_url($current_site_id);

		add_action('wmpcs_before_post_sync', $post_id);

		//get post
		$post = get_post($post_id, ARRAY_A); // get the original post

		$post_type = get_post_type($post);

		$is_wc_product = $is_wc_order = $is_wc_coupon = false;

		//get all meta for the post
		$meta_data = get_post_custom($post_id);

		//UPDATE OR ADD CHECK
		$check_if_already_synced = get_post_meta($post_id, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);

		if (empty($check_if_already_synced)) {
			$post['ID'] = ''; // empty id field, to tell wordpress that this will be a new post
		} else {
			$post['ID'] = $check_if_already_synced;
		}

		//get acf banner image
		$source_banner_image = get_post_meta($post_id, 'banner_image', true);

		if (!empty($source_banner_image)) {
			$source_banner_image_data = wmpcs_get_attachment_data($source_banner_image);
		}

		//get featured image
		$source_featued_image_id = get_post_thumbnail_id($post_id);
		if (!empty($source_featued_image_id)) {
			$dest_featued_image_id = get_post_meta($source_featued_image_id, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
			if (empty($dest_featued_image_id)) {
				$featured_image = wmpcs_get_featured_image_from_source($source_featued_image_id);
			}
		}

		$sourcetags = get_the_terms($post_id, 'post_tag');

		//get post terms according to defined taxonomies
		$taxonomies_to_sync = WMPCS_Setting::getAllowedTaxonomies();


		if (!empty($taxonomies_to_sync)) {
			$i = 0;
			$source_terms = array();

			foreach ($taxonomies_to_sync as $taxonomy) {
				$post_terms = wp_get_post_terms($post_id, $taxonomy, array("fields" => "ids"));

				if (!empty($post_terms)) {
					foreach ($post_terms as $post_term_id) {
						$synced_cat_id = get_term_meta($post_term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
						if (!empty($synced_cat_id)) {
							$source_terms[$i]['term_id'] = $synced_cat_id;
							$source_terms[$i]['term_taxonomy'] = $taxonomy;
							$i++;
						}
					}
					unset($post_terms);
				}
			}
		}

		if (WMPCS_Setting::woo_version_check('2.6')) {
			if (function_exists('get_current_screen')) {
				$current_screen = get_current_screen(); //determine
			} else {
				$current_screen = new \stdClass();
				$current_screen->id = '';
			}

			if ($current_screen->id == 'product') {
				$is_wc_product = true;
				$product = new WC_Product($post_id);
				//Get product attached images
				$source_gallery_attachment_ids = $product->get_gallery_attachment_ids();

				$source_gallery_images = array();

				foreach ($source_gallery_attachment_ids as $source_gallery_attachment_id) {
					$image = wp_get_attachment_image_src($source_gallery_attachment_id, 'full');

					if ($image) {
						$source_gallery_images[] = array(
							'url' => $image[0],
							'alt' => get_post_meta($source_gallery_attachment_id, '_wp_attachment_image_alt', true),
							'post_title' => get_post_field('post_title', $source_gallery_attachment_id),
							'description' => get_post_field('post_content', $source_gallery_attachment_id),
							'caption' => get_post_field('post_excerpt', $source_gallery_attachment_id),
							'post_name' => get_post_field('post_name', $source_gallery_attachment_id)
						);
					}
				}

				$terms = get_the_terms($post_id, 'product_type');

				$woo_product_type = sanitize_title(current($terms)->name);

				if ($woo_product_type == 'variable') {
					//get variations
					$defaults = array(
						'numberposts' => -1,
						'post_type' => 'product_variation',
						'post_status' => 'any',
						'post_parent' => $post_id,
					);

					$post_variations = get_posts($defaults, ARRAY_A);

					if (!empty($post_variations)) {

						foreach ($post_variations as $key => $post_variation) {
							//preserve the ID for later mapping
							$post_variations[$key]->actual_id = $post_variation->ID; //needed for resync
							//get variation metadata
							$post_variations[$key]->meta_data = get_post_custom($post_variation->ID);
							//VARIATION ADD OR UPDATE
							$check_if_already_synced = get_post_meta($post_variation->ID, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
							if (empty($check_if_already_synced)) {
								$post_variations[$key]->ID = ''; // ADD POST REQUEST
							} else {
								$post_variations[$key]->ID = $check_if_already_synced; //UPDATE REQUEST
							}
						}


						$post_attributes = $product->get_attributes();
						if (!empty($post_attributes)) {
							$source_post_attributes = array();
							foreach ($post_attributes as $tax_id => $source_post_attribute) {
								$attribute_values = wp_get_post_terms($post_id, $tax_id);
								$source_post_attributes[$tax_id] = $source_post_attribute;
								$attr_taxonomy = esc_sql(str_replace("pa_", "", $source_post_attribute['name']));

								$attribute_taxonomy_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '$attr_taxonomy'", ARRAY_A);

								$source_post_attributes[$tax_id]['taxonomy_data'] = $attribute_taxonomy_data;
								$source_post_attributes[$tax_id]['values'] = $attribute_values;
							}
						}
					}
				} elseif ($woo_product_type == 'grouped') {
					$grouped_post = new WC_Product_Grouped($post_id);
					if ($grouped_post->has_child()) {
						$child_grouped_posts = array();
						foreach ($grouped_post->get_children() as $child) {
							$group_child_id = WMPCS_Post_Sync::sync_post_to_site($child, $site_id);
							if (!empty($group_child_id)) {
								WMPCS_Post_Sync::save_synced_site_and_id_to_post($child, $site_id, $group_child_id);
								$child_grouped_posts[] = $group_child_id;
							}
						}
					}
				} elseif ($woo_product_type == 'bundle') {
					$bundle_posts = get_post_meta($post_id, '_bundle_data', TRUE);
					$dest_bundle_posts = array();
					if (!empty($bundle_posts)) {
						foreach ($bundle_posts as $key => $bundle_post) {
							$bundle_child_id = WMPCS_Post_Sync::sync_post_to_site($bundle_post['post_id'], $site_id);
							if ($bundle_child_id) {
								WMPCS_Post_Sync::save_synced_site_and_id_to_post($bundle_post['post_id'], $site_id, $bundle_child_id);
								$dest_bundle_posts[$bundle_child_id] = $bundle_post;
								$dest_bundle_posts[$bundle_child_id]['post_id'] = $bundle_child_id;
							} else {
								unset($bundle_posts[$key]);
							}
						}
					}
				} elseif ($woo_product_type == 'simple') {
					$post_attributes = $product->get_attributes();
					if (!empty($post_attributes)) {
						$source_post_attributes = array();
						foreach ($post_attributes as $tax_id => $source_post_attribute) {
							$attribute_values = wp_get_post_terms($post_id, $tax_id);
							$source_post_attributes[$tax_id] = $source_post_attribute;
							$attr_taxonomy = esc_sql(str_replace("pa_", "", $source_post_attribute['name']));

							$attribute_taxonomy_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '$attr_taxonomy'", ARRAY_A);

							$source_post_attributes[$tax_id]['taxonomy_data'] = $attribute_taxonomy_data;
							$source_post_attributes[$tax_id]['values'] = $attribute_values;
						}
					}
				}
			}


			if ($current_screen->id == 'shop_order' || current_filter() == 'woocommerce_thankyou') {
				$is_wc_order = true;
				$order = new WC_Order($post_id);
				$order_items = $order->get_items();

				$source_order_items = array();
				foreach ($order_items as $order_item_key => $order_item) {

					if ($order_item['variation_id']) {

						$dest_product_id = get_post_meta($order_item['variation_id'], WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
						if (empty($dest_product_id)) {
							$dest_product_id = get_post_meta($order_item['product_id'], WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
						}
					} else {
						$dest_product_id = get_post_meta($order_item['product_id'], WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
					}

					if ($dest_product_id) {
						if (WMPCS_Setting::woo_version_check()) {
							$source_order_items[$order_item_key]['qty'] = $order_item['quantity'];
							$source_order_items[$order_item_key]['item_id'] = $dest_product_id;
						} else {
//                            $source_order_items[$order_item_key]['qty'] = $order_item['quantity'];

							foreach ($order_item['item_meta'] as $order_item_meta_key => $order_item_meta) {
								if ($order_item_meta_key == '_product_id' && empty($order_item['variation_id'])) {
									$source_order_items[$order_item_key]['item_meta'][$order_item_meta_key][0] = $dest_product_id;
								} elseif ($order_item_meta_key == '_variation_id' && !empty($order_item['variation_id'])) {
									$source_order_items[$order_item_key]['item_meta'][$order_item_meta_key][0] = $dest_product_id;
								} else {
									$source_order_items[$order_item_key]['item_meta'][$order_item_meta_key][0] = $order_item_meta[0];
								}
							}
						}
					}
				}
			}

			if ($current_screen->id == 'shop_coupon') {
				$is_wc_coupon = true;
				$coupon = new WC_Coupon($post_id);

				if (isset($meta_data['product_ids']) && !empty($meta_data['product_ids'][0])) {
					$coupon_allowed_on_products = explode(",", $meta_data['product_ids'][0]);
					$dest_coupon_products = array();
					foreach ($coupon_allowed_on_products as $coupon_allowed_on_product) {
						$dest_product_id = get_post_meta($coupon_allowed_on_product, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
						if ($dest_product_id) {
							$dest_coupon_products[] = $dest_product_id;
						}
					}

					if (!empty($dest_coupon_products)) {
						$meta_data['product_ids'][0] = implode(",", $dest_coupon_products);
					} else {
						$meta_data['product_ids'][0] = "";
					}
				}

				if (isset($meta_data['exclude_product_ids']) && !empty($meta_data['exclude_product_ids'][0])) {
					$coupon_not_allowed_on_products = explode(",", $meta_data['exclude_product_ids'][0]);
					$dest_not_allowed_coupon_products = array();
					foreach ($coupon_not_allowed_on_products as $coupon_not_allowed_on_product) {
						$dest_product_id = get_post_meta($coupon_not_allowed_on_product, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
						if ($dest_product_id) {
							$dest_not_allowed_coupon_products[] = $dest_product_id;
						}
					}

					if (!empty($dest_not_allowed_coupon_products)) {
						$meta_data['exclude_product_ids'][0] = implode(",", $dest_not_allowed_coupon_products);
					} else {
						$meta_data['exclude_product_ids'][0] = "";
					}
				}
				if (isset($meta_data['product_categories']) && !empty($meta_data['product_categories'][0])) {
					$coupon_allowed_on_categories = explode(",", $meta_data['product_categories'][0]);
					$dest_coupon_allowed_on_categories = array();
					foreach ($coupon_allowed_on_categories as $coupon_allowed_on_category) {
						$dest_term_id = get_term_meta($coupon_allowed_on_category, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
						if ($dest_term_id) {
							$dest_coupon_allowed_on_categories[] = $dest_term_id;
						}
					}

					if (!empty($coupon_allowed_on_categories)) {
						$meta_data['product_categories'][0] = implode(",", $coupon_allowed_on_categories);
					} else {
						$meta_data['product_categories'][0] = "";
					}
				}

				if (isset($meta_data['exclude_product_categories']) && !empty($meta_data['exclude_product_categories'][0])) {
					$coupon_not_allowed_on_categories = explode(",", $meta_data['exclude_product_categories'][0]);
					$dest_coupon_not_allowed_on_categories = array();
					foreach ($coupon_not_allowed_on_categories as $coupon_not_allowed_on_category) {
						$dest_term_id = get_term_meta($coupon_not_allowed_on_category, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
						if ($dest_term_id) {
							$dest_coupon_not_allowed_on_categories[] = $dest_term_id;
						}
					}

					if (!empty($dest_coupon_not_allowed_on_categories)) {
						$meta_data['exclude_product_categories'][0] = implode(",", $dest_coupon_not_allowed_on_categories);
					} else {
						$meta_data['exclude_product_categories'][0] = "";
					}
				}
			}
		}
//        pr($source_order_items);
//        die;
		//get post format
		$source_post_format = get_post_format($post_id);

		//get comments
		$source_post_comments = get_comments(array('post_id' => $post_id));

		if (!empty($source_post_comments)) {
			foreach ($source_post_comments as $source_post_comment_key => $source_post_comment) {
				$comment_already_synced = get_comment_meta($source_post_comment->comment_ID, WMPCS_Setting::$SYNCED_COMMENT_ID_PREFIX . $site_id, TRUE);

				if ($comment_already_synced) {
					$source_post_comments[$source_post_comment_key]->already_comment_ID = $comment_already_synced;
				}
			}
		}

		//SWITCH TO TARGET SITE
		switch_to_blog($site_id);

		if (!empty($post['ID']) && FALSE === get_post_status($post['ID'])) { //post doesn't exists here
			$post['ID'] = '';
		}
		if ($post['post_parent'] != 0) {
			$post['post_parent'] = 0;
		}

		$inserted_post_id = wp_insert_post($post); // insert the post
		//Add the source post meta to the destination post
		foreach ($meta_data as $key => $meta_values) {
			if ($is_wc_product && $key == '_product_attributes') {
				continue;
			}

			if ($key == '_thumbnail_id') {
				continue;
			}

			foreach ($meta_values as $meta_value) {
				$meta_value = maybe_unserialize($meta_value);
				update_post_meta($inserted_post_id, $key, $meta_value);
			}
		}

		if (isset($source_banner_image_data)) {
			$attach_id = wmpcs_set_gallery_images_to_destination($inserted_post_id, $source_banner_image_data);

			if (!empty($attach_id)) {
				update_post_meta($inserted_post_id, 'banner_image', $attach_id);
			}
		}

		//If there was a featured image in the sourse post then copy it over
		if (!empty($source_featued_image_id)) {
			//If there was a featured image in the sourse post then copy it over
			if (!empty($dest_featued_image_id)) {
				// And finally assign featured image to post
				set_post_thumbnail($inserted_post_id, $dest_featued_image_id);
			} else {

				$new_dest_featued_image_id = wmpcs_set_featured_image_to_destination($inserted_post_id, $featured_image);

				WMPCS_Post_Sync::save_synced_site_and_id_to_post($new_dest_featued_image_id, $current_site_id, $source_featued_image_id);

				// And finally assign featured image to post
				set_post_thumbnail($inserted_post_id, $new_dest_featued_image_id);
			}
		}


		if (!empty($source_terms)) {
			foreach ($source_terms as $source_term) {
				if (!is_object_in_term($inserted_post_id, $source_term['term_taxonomy'], [$source_term['term_id']])) {
					$term_response = wp_set_post_terms($inserted_post_id, [$source_term['term_id']], $source_term['term_taxonomy'], TRUE);
				}
			}
		}

		//If there were tags in the sourse post then copy them over
		if ($sourcetags) {
			$tag_list = array();
			foreach ($sourcetags as $sourcetag) {
				$tag_list[] = $sourcetag->name;
			}
			//Check that the users plugin settings actually want this process to happen
			wp_set_post_tags($inserted_post_id, $tag_list);
		}


		if (WMPCS_Setting::woo_version_check()) {
			if (isset($source_gallery_images)) {
				$created_gallery_images = array();

				foreach ($source_gallery_images as $source_gallery_image) {
					$attach_id = wmpcs_set_gallery_images_to_destination($inserted_post_id, $source_gallery_image);
					if (!empty($attach_id)) {
						$created_gallery_images[] = $attach_id;
					}
				}


				if (!empty($created_gallery_images)) {
					//delete existing images
					$product_current_images = get_post_meta($inserted_post_id, '_product_image_gallery', true);

					if (!empty($product_current_images)) {
						$current_product_images_array = explode(',', $product_current_images);
						foreach ($current_product_images_array as $current_product_image_id) {
							wp_delete_attachment($current_product_image_id, TRUE);
						}
					}

					update_post_meta($inserted_post_id, '_product_image_gallery', implode(',', $created_gallery_images));
				}
			}

			if ($is_wc_product) {
				if ($woo_product_type == 'simple') {
					//set post attributes
					if (!empty($source_post_attributes)) {
						$attr_meta_data = array();
						$woo_taxonomy_table = "{$wpdb->prefix}woocommerce_attribute_taxonomies";
						foreach ($source_post_attributes as $attr_taxonomy => $attribute) {
							$attribute_values = array();
							foreach ($attribute['values'] as $attribute_value) {
								$attribute_values[] = $attribute_value->name;
								$attr_meta_data[$attr_taxonomy] = [
									'name' => $attr_taxonomy,
									'value' => '',
									'is_visible' => $attribute['is_visible'],
									'is_variation' => $attribute['is_variation'],
									'is_taxonomy' => $attribute['is_taxonomy'],
									'position' => $attribute['position']
								];
							}


							if (!empty($attribute['taxonomy_data'])) {
								foreach ($attribute['taxonomy_data'] as $taxonomy_data) {
									unset($taxonomy_data['attribute_id']);
									$wpdb->insert($woo_taxonomy_table, $taxonomy_data);
								}
							}

							wp_set_object_terms($inserted_post_id, $attribute_values, $attr_taxonomy, TRUE);
						}

						update_post_meta($inserted_post_id, '_product_attributes', $attr_meta_data);
					}
				} elseif ($woo_product_type == 'variable') {

					//set post attributes
					if (!empty($source_post_attributes)) {
						$attr_meta_data = array();
						$woo_taxonomy_table = "{$wpdb->prefix}woocommerce_attribute_taxonomies";
						foreach ($source_post_attributes as $attr_taxonomy => $attribute) {
							$attribute_values = array();
							foreach ($attribute['values'] as $attribute_value) {
								$attribute_values[] = $attribute_value->name;
								$attr_meta_data[$attr_taxonomy] = [
									'name' => $attr_taxonomy,
									'value' => '',
									'is_visible' => $attribute['is_visible'],
									'is_variation' => $attribute['is_variation'],
									'is_taxonomy' => $attribute['is_taxonomy'],
									'position' => $attribute['position']
								];
							}
							if (!empty($attribute['taxonomy_data'])) {
								foreach ($attribute['taxonomy_data'] as $taxonomy_data) {
									unset($taxonomy_data['attribute_id']);
									$wpdb->insert($woo_taxonomy_table, $taxonomy_data);
								}
							}

							wp_set_object_terms($inserted_post_id, $attribute_values, $attr_taxonomy, TRUE);
						}

						update_post_meta($inserted_post_id, '_product_attributes', $attr_meta_data);
					}

					$return_childrens = array();

					foreach ($post_variations as $product_variation_post) {
						$meta_data = $product_variation_post->meta_data;

						$var_main_store_id = $product_variation_post->actual_id;

						unset($product_variation_post->actual_id);
						unset($product_variation_post->meta_data);
						unset($product_variation_post->meta_value);

						$product_variation_post->post_parent = $inserted_post_id;

						if (!empty($product_variation_post->ID) && FALSE === get_post_status($product_variation_post->ID)) { //post doesn't exists here
							$product_variation_post->ID = '';
						}

						$child_post_id = wp_insert_post($product_variation_post); // insert the post

						if ($child_post_id) {
							$return_childrens[$var_main_store_id] = $child_post_id;
							//Add the source post meta to the destination post
							foreach ($meta_data as $key => $values) {
								if ($key == '_max_price_variation_id' || $key == '_max_regular_price_variation_id' || $key == '_min_price_variation_id' || $key == '_min_regular_price_variation_id' || $key == '_min_sale_price_variation_id') {
									continue;
								}
								foreach ($values as $value) {
									update_post_meta($child_post_id, $key, $value);
								}
							}
						}
					}
					wp_set_object_terms($inserted_post_id, 'Variable', 'product_type');
					WC_Product_Variable::sync($inserted_post_id);
				} elseif ($woo_product_type == 'grouped' && isset($child_grouped_posts) && !empty($child_grouped_posts)) {

					wp_set_object_terms($inserted_post_id, 'Grouped', 'product_type');

					foreach ($child_grouped_posts as $child_grouped_post) {
						wp_update_post(
							[
								'ID' => $child_grouped_post,
								'post_parent' => $inserted_post_id
							]
						);
					}
				} elseif ($woo_product_type == 'bundle' && !empty($dest_bundle_posts)) {
					update_post_meta($inserted_post_id, '_bundle_data', $dest_bundle_posts);
					wp_set_object_terms($inserted_post_id, 'bundle', 'product_type');
				}
			} elseif ($is_wc_order) {
				$order = new WC_Order($inserted_post_id);
				$order_items = $order->get_items();
				foreach ($order_items as $order_item_key => $order_item) {
					wc_delete_order_item($order_item->get_id());
				}
				if (isset($source_order_items) && !empty($source_order_items)) {
					foreach ($source_order_items as $source_order_item) {
						$order_item_product = new WC_Product($source_order_item['item_id']);
						if (WMPCS_Setting::woo_version_check()) {
							$order->add_product($order_item_product, $source_order_item['qty']);
						} else {
							$order->add_product($order_item_product, $source_order_item['item_meta']['_qty'][0]);
						}
						if (function_exists('wc_reduce_stock_levels')) {
							wc_reduce_stock_levels($inserted_post_id);
						} else {
							$order->reduce_order_stock();
						}
					}

					$order->calculate_totals(true);

					update_post_meta($inserted_post_id, 'wmpcs_order_placed_on', $source_site_url);
				}
			}
		}

		//SYNC POST FORMAT
		if ($source_post_format !== FALSE) {
			set_post_format($inserted_post_id, $source_post_format);
		}

		//SYNC COMMENTS
		if (!empty($source_post_comments)) {
			foreach ($source_post_comments as $source_post_comment) {

				$commentarr = array(
					'comment_post_ID' => $inserted_post_id,
					'comment_author' => $source_post_comment->comment_author,
					'comment_author_email' => $source_post_comment->comment_author_email,
					'comment_author_url' => $source_post_comment->comment_author_url,
					'comment_content' => $source_post_comment->comment_content,
					'comment_type' => $source_post_comment->comment_type,
					'comment_agent' => $source_post_comment->comment_agent,
					'comment_date' => $source_post_comment->comment_date,
					'comment_date_gmt' => $source_post_comment->comment_date_gmt,
					'comment_approved' => $source_post_comment->comment_approved,
				);

				if (isset($source_post_comment->already_comment_ID)) {
					$wp_inserted_comment_id = $source_post_comment->already_comment_ID;
					$commentarr['comment_ID'] = $wp_inserted_comment_id;
					wp_update_comment($commentarr);
				} else {
					$wp_inserted_comment_id = wp_insert_comment($commentarr);
				}

				if ($wp_inserted_comment_id) {
					WMPCS_Post_Sync::save_synced_site_and_id_to_comment($wp_inserted_comment_id, $current_site_id, $source_post_comment->comment_ID);
					$comments_mapping[$source_post_comment->comment_ID] = $wp_inserted_comment_id;
				}
			}
		}

		WMPCS_Post_Sync::save_synced_site_and_id_to_post($inserted_post_id, $current_site_id, $post_id);

		restore_current_blog(); // return to original blog

		if (isset($comments_mapping) && !empty($comments_mapping)) {
			foreach ($comments_mapping as $comment_id => $synced_comment_id) {
				WMPCS_Post_Sync::save_synced_site_and_id_to_comment($comment_id, $site_id, $synced_comment_id);
			}
		}

		if (isset($return_childrens) && !empty($return_childrens)) {
			$return['post_id'] = $inserted_post_id;
			$return['childs'] = $return_childrens;
			if (!empty($return_childrens)) {
				foreach ($return_childrens as $var_old => $var_new) {
					WMPCS_Post_Sync::save_synced_site_and_id_to_post($var_old, $site_id, $var_new);
				}
			}
		}

		WMPCS_Post_Sync::save_synced_site_and_id_to_post($post_id, $site_id, $inserted_post_id);

		$return = $inserted_post_id;


		add_action('wmpcs_after_post_sync_complete', $post_id);

		return $return;
	}

	/**
	 * function that takes the id of the post that was created/updated in one of the multisites,
	 * and along with the multisite site id, saves them to the original post as post meta
	 * @param $post_id - interger id of the post that was synced to another site on the multisite
	 * @param $site_id - the id of the individual site to the post was synced to
	 * @param $post_id_to_save - the id of the post on the site it was synced to
	 */
	static function save_synced_site_and_id_to_post($post_id, $site_id, $post_id_to_save) {
		update_post_meta($post_id, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, $post_id_to_save);
	}

	static function save_synced_site_and_id_to_comment($comment_id, $site_id, $comment_id_to_save) {
		update_comment_meta($comment_id, WMPCS_Setting::$SYNCED_COMMENT_ID_PREFIX . $site_id, $comment_id_to_save);
	}

}
