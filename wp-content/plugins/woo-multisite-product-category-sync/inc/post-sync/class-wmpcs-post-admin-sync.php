<?php

class WMPCS_Post_Admin_Sync {

    private $wmpcs_options = array();

    public function __construct($wmpcs_options = array()) {
        if (!empty($wmpcs_options)) {
            $this->wmpcs_options = $wmpcs_options;
            add_action('add_meta_boxes', array($this, 'add_meta_box'));
            if (empty($_GET['action']) || $_GET['action'] != 'wmpcs-selected-2') {
                add_action('wp_insert_post', array($this, 'wmpcs_process_post'), 10, 3);
            }
        }
    }

    function add_meta_box() {
        add_meta_box('wmpcs_meta_box', 'WMPCS Sync Box', array($this, 'wmpcs_admin_post_page_widget'), $this->wmpcs_options['post_types'], 'side', 'high');
    }

    function wmpcs_admin_post_page_widget() {
        global $post;
        $post_type = get_post_type($post);
        if (in_array($post_type, $this->wmpcs_options['post_types'])) {
            $sync_to_sites = $this->wmpcs_options['dests'];

            echo '<div class="options_group">';
            echo '<h3>WMPCS Post Sync</h3>';

            $is_auto_sync = false;

            if (isset($this->wmpcs_options['auto_sync']) && $this->wmpcs_options['auto_sync'] == 'on') {
                echo '<h4>Auto Sync Enabled</h4>';
                $is_auto_sync = true;
            }

            foreach ($sync_to_sites as $key => $site_id) {
                $blogname = get_site_url($site_id);
                $is_already_synced = get_post_meta($post->ID, WMPCS_Setting::$SYNCED_POST_ID_PREFIX . $site_id, TRUE);
                if (!empty($is_already_synced)) {
                    echo "<br />Synced on $blogname , post id : " . $is_already_synced;
                }

                $checked = "";

                if ($is_auto_sync) {
                    $sync_disabled_key = WMPCS_Setting::$SYNC_DISABLED_POST_META_KEY_PREFIX . $site_id;

                    $sync_disabled = get_post_meta($post->ID, $sync_disabled_key, TRUE);

                    if ($sync_disabled == 'yes') {
                        $checked .= "checked";
                    }

                    echo '<p class="form-field checkbox_class"><label for="' . $sync_disabled_key . '">Disable sync to ' . $blogname . '<span class="woocommerce-help-tip" data-tip="This will disable auto sync for this post on this site."></span></label><input type="checkbox" class="checkbox" style="" name="' . $sync_disabled_key . '" id="' . $sync_disabled_key . '" ' . $checked . '></p>';
                } else {
                    $sync_enabled_key = WMPCS_Setting::$SYNC_ENABLED_POST_META_KEY_PREFIX . $site_id;

                    $sync_enabled = get_post_meta($post->ID, $sync_enabled_key, TRUE);

                    if ($sync_enabled == 'yes') {
                        $checked .= "checked";
                    }

                    echo '<p class="form-field checkbox_class"><label for="' . $sync_enabled_key . '">Enable sync to ' . $blogname . '<span class="woocommerce-help-tip" data-tip="This will enable auto sync for this post on this site."></span></label><input type="checkbox" class="checkbox" style="" name="' . $sync_enabled_key . '" id="' . $sync_enabled_key . '" ' . $checked . '> </p>';
                }
            }


            echo "</div>";
        }
    }

    /**
     * This action hook function save meta field data
     */
    function wmpcs_process_post($post_id, $post, $update) {

        if (!empty($_GET['action']) && $_GET['action'] == 'trash') {
            return;
        }
        if ($post->post_status == 'inherit' && !empty($post->post_parent)) {
            return;
        }

        remove_action('wp_insert_post', array($this, 'wmpcs_process_post'), 10, 3); //remove hook to prevent loop

        if ($post->post_status == 'auto-draft') {
            return;
        }

        //NOT PROCEED WITH SYNC IF REQUESTED VIA AJAX
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return;
        }

        $post_type = get_post_type($post);
        //add revision posttype sync
        $this->wmpts_options['post_types'][] = 'revision';
        $post_type = get_post_type($post);
        if (in_array($post_type, $this->wmpcs_options['post_types'])) {

            $sync_to_sites = $this->wmpcs_options['dests'];

            if (!empty($sync_to_sites)) {
                foreach ($sync_to_sites as $site_id) {
                    if (isset($this->wmpcs_options['auto_sync']) && $this->wmpcs_options['auto_sync'] == 'on') {

                        $current_site_meta_key = WMPCS_Setting::$SYNC_DISABLED_POST_META_KEY_PREFIX . $site_id;
                        if (!empty($_POST[$current_site_meta_key])) {
                            update_post_meta($post_id, $current_site_meta_key, "yes");
                        } else {
                            delete_post_meta($post_id, $current_site_meta_key);
                            WMPCS_Post_Sync::sync_post_to_site($post_id, $site_id);
                        }
                    } else {
                        $current_site_meta_key = WMPCS_Setting::$SYNC_ENABLED_POST_META_KEY_PREFIX . $site_id;
                        if (!empty($_POST[$current_site_meta_key])) {
                            WMPCS_Post_Sync::sync_post_to_site($post_id, $site_id);
                            update_post_meta($post_id, $current_site_meta_key, "yes");
                        } else {
                            delete_post_meta($post_id, $current_site_meta_key);
                        }
                    }
                }
            }
        }
    }

}
