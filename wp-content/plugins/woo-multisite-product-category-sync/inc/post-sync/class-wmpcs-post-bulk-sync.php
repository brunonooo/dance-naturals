<?php

class WMPCS_Post_Bulk_Sync {

    public $wmpcs_options = array();

    public function __construct($options = []) {

        if (!empty($options)) {
            $this->wmpcs_options = $options;

            add_action('load-edit.php', [$this, 'custom_bulk_admin_footer'], 10);

            add_action('load-edit.php', [$this, 'custom_bulk_action'], 10);

            add_action('admin_notices', [$this, 'custom_bulk_admin_notices']);
        }
    }

    function custom_bulk_admin_notices() {
        global $post_type, $pagenow;

        if ($pagenow == 'edit-tags.php' && in_array($post_type, $this->wmpcs_options['post_types']) && isset($_REQUEST['ms_synced'])) {
            if ($_REQUEST['ms_synced'] == "selected") {
                $message = "Selected terms synced successfully.";
            } elseif ($_REQUEST['ms_synced'] == "all") {
                $message = "All terms synced successfully.";
            }
            echo "<div class='update'><p>{$message}</p></div>";
        }
    }

    function custom_bulk_action() {

        if (empty($_GET['post_type']) || empty($_GET['action'])) {
            return;
        }
        // 1. get the action
        $action = $_GET['action'];

        if (strpos($action, 'wmpcs') === FALSE) {
            return;
        }

        $allowed_post_types = $this->wmpcs_options['post_types'];

        if (!in_array($_GET['post_type'], $allowed_post_types)) {
            return;
        }
        $post_type = !empty($_GET['post_type']) ? $_GET['post_type'] : '';

        list($prefix, $main_action, $site_id) = explode("-", $action);

        if ($prefix != 'wmpcs') {
            return;
        }

        if (empty($site_id)) {
            return;
        }
        // this is based on wp-admin/edit.phps
        $sendback = remove_query_arg(array('exported', 'untrashed', 'deleted', 'ids', 'action'), wp_get_referer());
        if (!$sendback)
            $sendback = admin_url("edit.php?post_type=$post_type");


        // ...
        switch ($main_action) {
            // 3. Perform the action
            case 'selected':
                // if we set up user permissions/capabilities, the code might look like:

                $p_ids = $_GET['post'];

                if (!empty($p_ids)) {
                    foreach ($p_ids as $p_id) {
                        WMPCS_Post_Sync::sync_post_to_site($p_id, $site_id);
                    }
                }
                $paged = !empty($_GET['paged']) ? $_GET['paged'] : 1;
                $post_status = !empty($_GET['post_status']) ? $_GET['post_status'] : 'publish';

                //build the redirect url

                $query_args = array(
                    'post_type' => $post_type,
                    'ms_synced' => 'selected',
                    'selected_ids' => join(',', $p_ids)
                );

                if (!empty($_GET['paged'])) {
                    $query_args['paged'] = $_GET['paged'];
                }

                if (!empty($_GET['post_status'])) {
                    $query_args['post_status'] = $_GET['post_status'];
                }

                if (!empty($_GET['post_type'])) {
                    $query_args['post_type'] = $_GET['post_type'];
                }

                if (!empty($_GET['post_cat'])) {
                    $query_args['post_cat'] = $_GET['post_cat'];
                }

                if (!empty($_GET['filter_action'])) {
                    $query_args['filter_action'] = $_GET['filter_action'];
                }

                if (!empty($_GET['m'])) {
                    $query_args['m'] = $_GET['m'];
                }


                $sendback = add_query_arg($query_args, $sendback);

                break;
            // 3. Perform the action
            case 'all':

                break;
            default: return;
        }

        // ...
        // 4. Redirect client
        wp_redirect($sendback);

        exit();
    }

    function custom_bulk_admin_footer() {

        if (!empty($_POST)) {
            return;
        }

        if (empty($_GET['post_type'])) {
            global $post_type;

            if (empty($post_type)) {
                $post_type = $GLOBALS['typenow'];
            }
        } else {
            $post_type = $_GET['post_type'];
        }


        if (!empty($_GET['action']) && $_GET['action'] != -1) {
            return;
        }

        $allowed_post_types = $this->wmpcs_options['post_types'];

        $other_sites = $this->wmpcs_options['dests'];

        if (in_array($post_type, $allowed_post_types) && !empty($other_sites)) {
            $js_file_data = array();
            foreach ($other_sites as $site_id) {
                $site_url = get_site_url($site_id);
                $js_file_data[$site_id] = $site_url;
            }
            wp_enqueue_script('wmpts-post-bulk-sync', WMPCS_URL . "/assets/js/admin-post-bulk-sync.js");

            wp_localize_script('wmpts-post-bulk-sync', 'wmpts_post_bulk_sync', array('other_sites_data' => json_encode($js_file_data)));
        }
    }

}
