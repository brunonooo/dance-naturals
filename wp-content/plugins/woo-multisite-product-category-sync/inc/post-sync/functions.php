<?php

function wmpcs_get_attachment_data($thumbnail_id) {
	$image = wp_get_attachment_image_src($thumbnail_id, 'full');

	if ($image) {
		$image_details = array(
			'url' => $image[0],
			'alt' => get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true),
			'post_title' => get_post_field('post_title', $thumbnail_id),
			'description' => get_post_field('post_content', $thumbnail_id),
			'caption' => get_post_field('post_excerpt', $thumbnail_id),
			'post_name' => get_post_field('post_name', $thumbnail_id),
			'path' => get_attached_file( $thumbnail_id )
		);

		$image_details = apply_filters('mpd_featured_image', $image_details);

		return $image_details;
	}
}

if(!function_exists('wmpcs_get_featured_image_from_source')) {
	function wmpcs_get_featured_image_from_source($thumbnail_id) {
		$image = wp_get_attachment_image_src($thumbnail_id, 'full');
		if ($image) {
			$image_details = array(
				'url' => $image[0],
				'alt' => get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true),
				'post_title' => get_post_field('post_title', $thumbnail_id),
				'description' => get_post_field('post_content', $thumbnail_id),
				'caption' => get_post_field('post_excerpt', $thumbnail_id),
				'post_name' => get_post_field('post_name', $thumbnail_id),
				'path' => get_attached_file( $thumbnail_id )
			);

			$image_details = apply_filters('mpd_featured_image', $image_details);

			return $image_details;
		}
	}
}

if(!function_exists('wmpcs_set_featured_image_to_destination')) {
	function wmpcs_set_featured_image_to_destination($destination_id, $image_details) {

		// Get the upload directory for the current site
		$upload_dir = wp_upload_dir();
		// Get all the data inside a file and attach it to a variable
		$image_data = file_get_contents($image_details['path']);

		// Get the file name of the source file
		$filename = apply_filters('wmpcs_featured_image_filename', basename($image_details['url']), $image_details);

		// Make the path to the desired path to the new file we are about to create
		if (wp_mkdir_p($upload_dir['path'])) {

			$file = $upload_dir['path'] . '/' . $filename;
		} else {

			$file = $upload_dir['basedir'] . '/' . $filename;
		}

		// Add the file contents to the new path with the new filename
		file_put_contents($file, $image_data);

		// Get the mime type of the new file extension
		$wp_filetype = wp_check_filetype($filename, null);
		// Get the URL (not the URI) of the new file
		$new_file_url = $upload_dir['url'] . '/' . $filename;

		// Create the database information for this new image
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => $image_details['post_title'],
			'post_content' => $image_details['description'],
			'post_status' => 'inherit',
			'post_excerpt' => $image_details['caption'],
			'post_name' => $image_details['post_name']
		);

		// Attach the new file and its information to the database
		$attach_id = wp_insert_attachment($attachment, $file, $destination_id);

		// Add alt text from the destination image
		if ($image_details['alt']) {
			update_post_meta($attach_id, '_wp_attachment_image_alt', $image_details['alt']);
		}

		// Include code to process functions below:
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata($attach_id, $file);

		// Assign metadata to attachment
		wp_update_attachment_metadata($attach_id, $attach_data);
		return $attach_id;
	}
}

if(!function_exists('wmpcs_set_gallery_images_to_destination')) {
	function wmpcs_set_gallery_images_to_destination($destination_id, $image_details) {
		// Get the upload directory for the current site
		$upload_dir = wp_upload_dir();
		// Get all the data inside a file and attach it to a variable
		$image_data = file_get_contents($image_details['path']);
		// Get the file name of the source file
		$filename = apply_filters('wmpcs_featured_image_filename', basename($image_details['url']), $image_details);

		// Make the path to the desired path to the new file we are about to create
		if (wp_mkdir_p($upload_dir['path'])) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}

		// Add the file contents to the new path with the new filename
		file_put_contents($file, $image_data);
		// Get the mime type of the new file extension
		$wp_filetype = wp_check_filetype($filename, null);
		// Get the URL (not the URI) of the new file
		$new_file_url = $upload_dir['url'] . '/' . $filename;
		// Create the database information for this new image
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => $image_details['post_title'],
			'post_content' => $image_details['description'],
			'post_status' => 'inherit',
			'post_excerpt' => $image_details['caption'],
			'post_name' => $image_details['post_name']
		);

		// Attach the new file and its information to the database
		$attach_id = wp_insert_attachment($attachment, $file, $destination_id);

		// Add alt text from the destination image
		if ($image_details['alt']) {
			update_post_meta($attach_id, '_wp_attachment_image_alt', $image_details['alt']);
		}

		// Include code to process functions below:
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata($attach_id, $file);

		// Assign metadata to attachment
		wp_update_attachment_metadata($attach_id, $attach_data);
		return $attach_id;
	}
}

if(!function_exists('wmpcs_fix_wordpress_urls')) {
	function wmpcs_fix_wordpress_urls($url_input) {
		// Wordpress can have URLs missing the protocol (ssl website), so we have to check if the protocol is set
		$protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
		$url = preg_replace("/(^\/\/)/", $protocol, $url_input);
		return $url;
	}
}

// WPML COMPATIBILITY
function sync_element_language($el, $synched_id) {
	//BAIL EARLY IF WPML IS NOT INSTALLED AND ACTIVE
	if (!function_exists('icl_object_id')){
		return;
	}

	// https://wpml.org/wpml-hook/wpml_element_type/
	$wpml_element_type = apply_filters( 'wpml_element_type', $el['post_type'] );

	// get the language info of the original post
	// https://wpml.org/wpml-hook/wpml_element_language_details/
	$get_language_args = array('element_id' => $el['ID'], 'element_type' => $el['post_type'] );
	$original_post_language_info = apply_filters( 'wpml_element_language_details', null, $get_language_args );

	$set_language_args = array(
		'element_id'    => $synched_id,
		'element_type'  => $wpml_element_type,
		'trid'   => $original_post_language_info->trid,
		'language_code'   => $original_post_language_info->language_code,
		'source_language_code' => $original_post_language_info->language_code
	);

	do_action( 'wpml_set_element_language_details', $set_language_args );
}