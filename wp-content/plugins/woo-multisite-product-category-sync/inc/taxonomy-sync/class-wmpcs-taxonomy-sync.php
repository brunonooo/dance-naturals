<?php

    /**
     * WMPCS_Taxonomy_Sync class
     * Contains function that perform actual sychronization with retail and wholesale sites
     *
     */
    class WMPCS_Taxonomy_Sync {

        static function perform_sync_for_taxonomy($term_id) {

            add_action('wmpcs_before_taxonomy_sync', $term_id);

            // get ids of sites that taxonomy needs to be synced to
            $sites = WMPCS_Taxonomy_Sync::get_site_ids_for_taxonomy($term_id);

            // if there are no sites that the taxonomy needs to be synced to return
            if (empty($sites)) {
                return;
            }
            // perform sync to each site
            foreach ($sites as $site) {
                WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($term_id, $site);
            }

            add_action('wmpcs_after_taxonomy_sync_complete', $term_id);

            if (wp_get_referer()) {
                wp_safe_redirect(wp_get_referer());
            } else {
                wp_safe_redirect(get_home_url());
            }
        }

        static function save_synced_site_and_id_to_term($term_id, $site_id, $taxonomy_in_site_id) {
            update_term_meta($term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, $taxonomy_in_site_id);
        }

        static function sync_taxonomy_to_site($term_id, $site_id, $site_parent_cat_id = 0) {
            global $wpdb;
            $term = get_term($term_id, "", ARRAY_A);

            $return['status'] = 0;      
            if (!is_wp_error($term)) {

                $current_site_id = get_current_blog_id();

                $taxonomy = $term['taxonomy'];

                $term_meta = get_term_meta($term_id);

                if (!empty($term_meta['thumbnail_id'][0])) {
                    $source_term_attachment = wmpcs_taxonomy_get_featured_image_from_source($term_meta['thumbnail_id'][0]);
                }

                //UPDATE OR ADD CHECK
                $inserted_term_id = get_term_meta($term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);

                switch_to_blog($site_id); // switch to target blog
                // print_r($site_id); die('ad');

                if (!empty($inserted_term_id)) {
                    $term_exists = get_term($inserted_term_id, $taxonomy, ARRAY_A);
                    if (is_wp_error($term_exists)) {
                        $return['message'] = $term_exists->get_error_message();
                        return $return;
                    }
                    if ($term_exists['taxonomy'] !== $taxonomy) { //if need to sync to different taxonomy
                        $inserted_term_id = "";
                    }
                } else {
                    if ($site_parent_cat_id !== 0) {
                        $term_exists = term_exists($term['name'], $taxonomy, $site_parent_cat_id);
                    } else {
                        $term_exists = term_exists($term['name'], $taxonomy, 0);
                    }

                    if (!empty($term_exists['term_id'])) {
                        $inserted_term_id = $term_exists['term_id'];
                        $inserted_term_taxonomy_id = $term_exists['term_taxonomy_id'];
                    }
                }

                

                $term = (array) $term;
                unset($term['count']);
                unset($term['filter']);
                unset($term['parent']);

                if ($site_parent_cat_id !== 0) {
                    $term['parent'] = $site_parent_cat_id;
                }

                if (!empty($inserted_term_id)) {
                    $term['term_id'] = (int) $inserted_term_id; //overwrite id
                    wp_update_term($inserted_term_id, $taxonomy, $term);
                } else {
                    unset($term['term_id']);
                    unset($term['term_taxonomy_id']);
                    
                    $inserted_term_data = wp_insert_term($term['name'], $taxonomy, $term); // insert term

                    if (is_wp_error($inserted_term_data)) {
                        $return['message'] = $inserted_term_data->get_error_message();
                        return $return;
                    } elseif (isset($inserted_term_data['term_id'])) {
                        $inserted_term_id = $inserted_term_data['term_id'];
                    } else {
                        $error = "Unable to create term " . $term['name'];
                        $return['message'] = $error;
                        return $return;
                    }
                }

                if (isset($source_term_attachment)) {
                    
                    $category_current_image = get_term($inserted_term_id,'thumbnail_id',true);

                    if(!empty($category_current_image)) {
                        wp_delete_attachment($category_current_image, TRUE);
                    }

                    $term_meta['thumbnail_id'][0] = wmpcs_taxonomy_set_featured_image_to_destination($source_term_attachment);
                }

                //Add the source post meta to the destination post
                foreach ($term_meta as $key => $meta_values) {
                    foreach ($meta_values as $meta_value) {
                        update_term_meta($inserted_term_id, $key, $meta_value);
                    }
                }

                if ($inserted_term_id) {
                    WMPCS_Taxonomy_Sync::save_synced_site_and_id_to_term($inserted_term_id, $current_site_id, $term_id);
                }

                restore_current_blog(); // return to original blog

                if (!empty($inserted_term_id)) {
                    WMPCS_Taxonomy_Sync::save_synced_site_and_id_to_term($term_id, $site_id, $inserted_term_id);
                }

                $return['status'] = 1;
                $return['inserted_term_id'] = $inserted_term_id;

                return $return;
            } else {
                $return['message'] = $term->get_error_message();
            }
            return $return;
        }

        static function bulk_site_taxonomy_sync($site_id, $taxonomy) {
            $parent_terms = get_terms(array(
                'taxonomy' => $taxonomy,
                'parent' => 0,
                'hide_empty' => false
            ));

            if (!empty($parent_terms)) {
                foreach ($parent_terms as $parent_term) {
                    //sync parent terms
                    $synced_parent_cat = WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($parent_term->term_id, $site_id);

                    if ($synced_parent_cat['status'] == 1) {
                        WMPCS_Taxonomy_Sync::sync_children_terms($parent_term->term_id, $synced_parent_cat['inserted_term_id'], $site_id, $taxonomy);
                    }
                }
            }
        }

        static function sync_term_hirerachy($site_id, $term) {

            //get term ancestors
            $ancestors = get_ancestors($term->term_id, $term->taxonomy);
            //get toppest parent
            $top_parent = end($ancestors);
            //parent term data
            $parent_term = get_term($top_parent);

            //check if parent was synced before
            $synced_parent_id = WMPCS_Taxonomy_Sync::checkIfTermSyncedToSite($site_id, $parent_term->term_id);
            //sync parent if not synced
            if (!$synced_parent_id) {
                //sync parent terms
                $synced_parent_cat = WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($parent_term->term_id, $site_id);
                //if parent synced successfully
                if ($synced_parent_cat['status']) {
                    $synced_parent_id = $synced_parent_cat['inserted_term_id'];
                } else {
                    return;
                }
            }
            
            //if parent is synced 
            if ($synced_parent_id) {
                WMPCS_Taxonomy_Sync::sync_children_terms($parent_term->term_id, $synced_parent_id, $site_id, $term->taxonomy, $term->term_id);
            }
        }

        static function sync_children_terms($parent_term_source_id, $parent_term_target_id, $site_id, $taxonomy, $sync_term_id = 0) {

            $child_terms = get_term_children($parent_term_source_id, $taxonomy);

            //sometime when adding a new category, the newly added category is not returned by "get_term_children" function, so add it manually from here
            if($sync_term_id > 0 && !in_array($sync_term_id, $child_terms)) {
                $child_terms[] = $sync_term_id;
            }
            if (!is_wp_error($child_terms) && !empty($child_terms)) {
                
                foreach ($child_terms as $child_term_id) {
                   
                    $child_term_data = get_term($child_term_id, $taxonomy, ARRAY_A);
                    
                    if ($child_term_data['parent'] != $parent_term_source_id) {
                        $parent_term_target_id = get_term_meta($child_term_data['parent'], WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);

                        if (empty($parent_term_target_id)) {
                            $synced_level_1_encestor = WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($child_term_id, $site_id, $parent_term_target_id);
                            if ($synced_level_1_encestor['status'] == 1) {
                                $parent_term_target_id = $synced_level_1_encestor['inserted_term_id'];
                            } else {
                                continue;
                            }
                        }
                    }

                    if (!empty($parent_term_target_id)) {
                        $synced_child_cat = WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($child_term_id, $site_id, $parent_term_target_id);
                    }
                }
            }
        }

        static function checkIfTermSyncedToSite($site_id, $term_id) {
            //UPDATE OR ADD CHECK
            return get_term_meta($term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
        }

    }
    