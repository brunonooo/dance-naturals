<?php

    class WMPCS_Taxonomy_Admin_Sync {

        private $wmpcs_options = array();

        public function __construct($wmpcs_options) {
            if (!empty($wmpcs_options)) {
                $this->wmpcs_options = $wmpcs_options;

                foreach ($this->wmpcs_options['taxonomies'] as $taxonomy) {
                    // Add the fields to the product cateogry taxonomy 
                    add_action($taxonomy . '_edit_form_fields', array($this, 'WMPCS_taxonomy_fields'), 30);
                    add_action($taxonomy . '_add_form_fields', array($this, 'WMPCS_taxonomy_fields'), 30);
                    // Save the changes made on the product taxonomy taxonomy
                    add_action('edited_' . $taxonomy, array($this, 'WMPCS_taxonomy_fields_save'), 10, 2);
                    add_action('create_' . $taxonomy, array($this, 'WMPCS_taxonomy_fields_save'), 10, 2);
                }

                 // Save the changes made on the product taxonomy taxonomy 
                add_action('woocommerce_attribute_updated', array($this, 'wmpcs_woo_taxonomy_added_updated'), 10, 2);
                add_action('woocommerce_attribute_added', array($this, 'wmpcs_woo_taxonomy_added_updated'), 10, 2);
                add_action('woocommerce_attribute_deleted', array($this, 'wmpcs_woo_taxonomy_deleted'), 10, 3);
            }
        }

        function wmpcs_woo_taxonomy_deleted($attribute_id, $attribute_name, $taxonomy) {

            $other_sites = $this->wmpcs_options['dests'];

            if(!empty($other_sites)) {
                global $wpdb;
                foreach ($other_sites as $site_id) {
                    switch_to_blog($site_id); // switch to target blog
                    $taxonomy_exists    = taxonomy_exists( $taxonomy );

                    if($taxonomy_exists) {
                         $attribute_data = $wpdb->get_row("SELECT `attribute_id` FROM ".$wpdb->prefix . "woocommerce_attribute_taxonomies  WHERE attribute_name = '".$attribute_name."'", ARRAY_A);

                         if(isset($attribute_data['attribute_id']) &&  !empty($attribute_data['attribute_id'])) {
                            $attribute_id = $attribute_data['attribute_id'];

                            $wpdb->query( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_id =  $attribute_id");

                            if ( taxonomy_exists( $taxonomy ) ) {
                                $terms = get_terms( $taxonomy, 'orderby=name&hide_empty=0' );
                                foreach ( $terms as $term ) {
                                    wp_delete_term( $term->term_id, $taxonomy );
                                }
                            }
                        }
                        wp_schedule_single_event( time(), 'woocommerce_flush_rewrite_rules' );
                        delete_transient( 'wc_attribute_taxonomies' );
                    }

                    restore_current_blog(); // return to original blog
                }
            }

            return false;
        }

        function wmpcs_woo_taxonomy_added_updated($attribute_id, $attribute) {

            $other_sites = $this->wmpcs_options['dests'];

            $taxonomy =  wc_attribute_taxonomy_name( $attribute['attribute_name'] );

            if(!empty($other_sites)) {
                global $wpdb;
                foreach ($other_sites as $site_id) {
                    switch_to_blog($site_id); // switch to target blog

                    $taxonomy_exists = taxonomy_exists( wc_attribute_taxonomy_name( $attribute['attribute_name'] ) );

                    if(!$taxonomy_exists) {
                        $wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );
                    } else {
                        $attribute_data = $wpdb->get_row("SELECT `attribute_id` FROM ".$wpdb->prefix . "woocommerce_attribute_taxonomies  WHERE attribute_name = '".$attribute['attribute_name']."'", ARRAY_A);

                        if(isset($attribute_data['attribute_id']) &&  !empty($attribute_data['attribute_id'])) {
                            $wpdb->update( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute, array( 'attribute_id' => $attribute_data['attribute_id'] ) );
                        }
                    }
                    wp_schedule_single_event( time(), 'woocommerce_flush_rewrite_rules' );
                    delete_transient( 'wc_attribute_taxonomies' );
                    restore_current_blog(); // return to original blog
                }
            }
            //add newly added taxonomy to plugin options
            if(!in_array($taxonomy, $this->wmpcs_options['taxonomies'])) {
                $taxonomy_source_site = get_current_blog_id();

                $option_data = get_option(WMPCS_OPTION_NAME);
                $option_data['taxonomy_sync'][$taxonomy_source_site]['taxonomies'][] = $taxonomy;
                update_option(WMPCS_OPTION_NAME, $option_data, FALSE);
            }

            return false;
        }


        function WMPCS_taxonomy_fields($tag) {

            $current_filter = current_filter();
            if (isset($tag->term_id)) {
                $term_id = $tag->term_id;
            }
            $other_sites = $this->wmpcs_options['dests'];
            $is_auto_sync = false;
            if (strpos($current_filter, 'add_form') === FALSE) {
                echo '<tr class="form-field"><th scope="row" valign="top">WMPCS Sync Box</th></tr>';
                if (isset($this->wmpcs_options['auto_sync']) && $this->wmpcs_options['auto_sync'] == 'on') {
                    echo '<tr class="form-field"><th scope="row" valign="top">Auto Sync Enabled</th></tr>';
                    $is_auto_sync = true;
                }

                foreach ($other_sites as $site_id) {
                    $blogname = get_site_url($site_id);
                    $is_already_synced = FALSE;

                    if (isset($term_id)) {
                        $is_already_synced = get_term_meta($term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
                    }
                    if ($is_already_synced) {
                        echo "<tr class='form-field'><th scope='row' valign='top'>Synced on $blogname</th><td>Term id : $is_already_synced</td></tr>";
                    }

                    $checked = "";

                    if ($is_auto_sync) {

                        $sync_disabled_key = WMPCS_Setting::$SYNC_DISABLED_TAXONOMY_META_KEY_PREFIX . $site_id;

                        if (isset($term_id)) {
                            $sync_disabled = get_term_meta($term_id, $sync_disabled_key, TRUE);
                        }

                        if (isset($sync_disabled) && $sync_disabled == 'yes') {
                            $checked.="checked";
                        }

                        echo "<tr class='form-field'><th scope='row' valign='top'>Disable sync to $blogname<span class='woocommerce-help-tip' data-tip='This will disable auto sync for this term on this site.'></span></th><td><input type='checkbox' class='checkbox' name='$sync_disabled_key' id='$sync_disabled_key' $checked . '></td></tr>";
                    } else {
                        $sync_enabled_key = WMPCS_Setting::$SYNC_ENABLED_TAXONOMY_META_KEY_PREFIX . $site_id;

                        if (isset($term_id)) {
                            $sync_enabled = get_term_meta($term_id, $sync_enabled_key, TRUE);
                        }

                        if (isset($sync_enabled) && $sync_enabled == 'yes') {
                            $checked.="checked";
                        }

                        echo "<tr class='form-field'><th scope='row' valign='top'>Enable sync to $blogname<span class='woocommerce-help-tip' data-tip='This will enable auto sync for this term on this site.'></span></th><td><input type='checkbox' class='checkbox' name='$sync_enabled_key' id='$sync_enabled_key' $checked . '></td></tr>";
                    }
                }
            } else {


                echo '<h3>WMPCS Multisite Sync</h3>';

                if (isset($this->wmpcs_options['auto_sync']) && $this->wmpcs_options['auto_sync'] == 'on') {
                    echo '<h4>Auto Sync Enabled</h2>';
                    $is_auto_sync = true;
                }

                foreach ($other_sites as $site_id) {
                    
                    $blogname = get_site_url($site_id);
                    $is_already_synced = FALSE;

                    if (isset($term_id)) {
                        $is_already_synced = get_term_meta($term_id, WMPCS_Setting::$SYNCED_TERM_ID_PREFIX . $site_id, TRUE);
                    }
                    if ($is_already_synced) {
                        echo "<h4>Synced on $blogname</th><td>Term id : $is_already_synced</h4>";
                    }

                    $checked = "";

                    if ($is_auto_sync) {

                        $sync_disabled_key = WMPCS_Setting::$SYNC_DISABLED_TAXONOMY_META_KEY_PREFIX . $site_id;

                        if (isset($term_id)) {
                            $sync_disabled = get_term_meta($term_id, $sync_disabled_key, TRUE);
                        }

                        if (isset($sync_disabled) && $sync_disabled == 'yes') {
                            $checked.="checked";
                        }

                        echo "<div class='form-field'>Disable sync to $blogname<span class='woocommerce-help-tip' data-tip='This will disable auto sync for this term on this site.'></span><input type='checkbox' class='checkbox' name='$sync_disabled_key' id='$sync_disabled_key' $checked . '></div>";
                    } else {
                        $sync_enabled_key = WMPCS_Setting::$SYNC_ENABLED_TAXONOMY_META_KEY_PREFIX . $site_id;

                        if (isset($term_id)) {
                            $sync_enabled = get_term_meta($term_id, $sync_enabled_key, TRUE);
                        }

                        if (isset($sync_enabled) && $sync_enabled == 'yes') {
                            $checked.="checked";
                        }

                        echo "<div class='form-field'>Enable sync to $blogname<span class='woocommerce-help-tip' data-tip='This will disable auto sync for this term on this site.'></span><input type='checkbox' class='checkbox' name='$sync_enabled_key' id='$sync_enabled_key' $checked . '></div>";
                    }
                }
            }
        }

        function WMPCS_taxonomy_fields_save($term_id) {
            remove_action(current_filter(), array($this, 'WMPCS_taxonomy_fields_save'), 10, 2);
            $sync_to_sites = $this->wmpcs_options['dests'];
            if (!empty($sync_to_sites)) {

                $term_data = get_term($term_id);
                foreach ($sync_to_sites as $site_id) {

                    if (isset($this->wmpcs_options['auto_sync']) && $this->wmpcs_options['auto_sync'] == 'on') {
                        $current_site_meta_key = WMPCS_Setting::$SYNC_DISABLED_TAXONOMY_META_KEY_PREFIX . $site_id;
                        if (!empty($_POST[$current_site_meta_key])) {
                            update_term_meta($term_id, $current_site_meta_key, "yes");
                        } else {
                            delete_term_meta($term_id, $current_site_meta_key);

                            if ($term_data->parent != 0) {
                                WMPCS_Taxonomy_Sync::sync_term_hirerachy($site_id, $term_data);
                            } else {
                                WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($term_id, $site_id);
                            }
                            
                        }
                    } else {
                        $current_site_meta_key = WMPCS_Setting::$SYNC_ENABLED_TAXONOMY_META_KEY_PREFIX . $site_id;
                        if (!empty($_POST[$current_site_meta_key])) {
                            update_term_meta($term_id, $current_site_meta_key, "yes");

                            if ($term_data->parent != 0) {
                                WMPCS_Taxonomy_Sync::sync_term_hirerachy($site_id, $term_data);
                            } else {
                                WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($term_id, $site_id);
                            }
                            
                        } else {
                            delete_term_meta($term_id, $current_site_meta_key);
                        }
                    }
                }
            }
        }

    }
    