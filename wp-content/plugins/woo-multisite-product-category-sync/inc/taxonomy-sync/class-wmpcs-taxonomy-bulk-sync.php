<?php

    class WMPCS_Taxonomy_Bulk_Sync {

        public $plugin_options = array();

        public function __construct($plugin_options) {
            if (!empty($plugin_options)) {
                $this->plugin_options = $plugin_options;
                add_action('load-edit-tags.php', [$this, 'custom_bulk_admin_footer']);
                add_action('load-edit-tags.php', [$this, 'custom_bulk_action']);
                add_action('admin_notices', [$this, 'custom_bulk_admin_notices']);
            }
        }

        function custom_bulk_admin_footer() {
            if (empty($_GET['taxonomy'])) {
                return;
            }
            if(!empty($_POST)) {
                return;
            }
            
            $taxonomy = $_GET['taxonomy'];
            
            $allowed_taxonomies = $this->plugin_options['taxonomies'];
            if (in_array($taxonomy, $allowed_taxonomies)) {
                $other_sites = $this->plugin_options['dests'];

                 $js_file_data = array();
                foreach ($other_sites as $site_id) {
                    $site_url = get_site_url($site_id);
                    $js_file_data[$site_id] = $site_url;
                }
                wp_enqueue_script('wmpts-taxonomy-bulk-sync', WMPCS_URL . "/assets/js/admin-taxonomy-bulk-sync.js");
                
                wp_localize_script('wmpts-taxonomy-bulk-sync', 'wmpts_taxonomy_bulk_sync', array('other_sites_data' => json_encode($js_file_data)));
            }
        }

        function custom_bulk_action() {
           
            if (empty($_GET['taxonomy']) || empty($_POST['action'])) {
                return;
            }

            $taxonomy = $_GET['taxonomy'];

            
            $allowed_taxonomy_types = WMPCS_Setting::getAllowedTaxonomies();

            if (!in_array($taxonomy, $allowed_taxonomy_types)) {
                return;
            }
            
            $post_type = !empty($_GET['post_type'])?$_GET['post_type']:'';

            // get the action
            $action = $_POST['action'];

            if (strpos($action, 'wmpcs') === FALSE) {
                return;
            }

            list($prefix, $main_action, $site_id) = explode("-", $action);
            if ($prefix != 'wmpcs' || empty($site_id)) {
                return;
            }

            $paged = !empty($_GET['paged']) ? $_GET['paged'] : 1;
            // this is based on wp-admin/edit.phps
            $sendback = remove_query_arg( array('exported', 'untrashed', 'deleted', 'ids','action'), wp_get_referer() );
            if ( ! $sendback )
                $sendback = admin_url( "edit-tags.php?taxonomy=$taxonomy" );
            
            // ...
            switch ($main_action) {
                case 'selected':

                    $term_ids = $_POST['delete_tags'];
                    if (!empty($term_ids)) {
                        foreach ($term_ids as $term_id) {
                            $term = get_term($term_id);

                            if ($term->parent == 0) {
                                $synced_parent_cat = WMPCS_Taxonomy_Sync::sync_taxonomy_to_site($term_id, $site_id);
                                if ($synced_parent_cat['status'] == 1) {
                                    WMPCS_Taxonomy_Sync::sync_children_terms($term_id, $synced_parent_cat['inserted_term_id'], $site_id, $taxonomy);
                                }
                            }
                        }
                    }
                    // build the redirect url
                    $args_arr = array('taxonomy' => $taxonomy, 'paged' => $paged, 'post_type' => $post_type, 'wmpcs_synced' => 'selected', 'selected_ids' => join(',', $term_ids));
                     if(empty($args_arr['post_type'])) {
                        unset($args_arr['post_type']);
                     }
                    $sendback = add_query_arg($args_arr);

                    break;
                case 'all':

                    WMPCS_Taxonomy_Sync::bulk_site_taxonomy_sync($site_id, $taxonomy);
                    $args_arr = array('taxonomy' => $taxonomy, 'paged' => $paged, 'post_type' => $post_type, 'wmpcs_synced' => 'all');
                    if(empty($args_arr['post_type'])) {
                        unset($args_arr['post_type']);
                     }
                    $sendback = add_query_arg($args_arr, $sendback);

                    break;
                default: return;
            }

            // Redirect client
            wp_redirect($sendback);

            exit();
        }

        function custom_bulk_admin_notices() {
            global  $pagenow;
            if ($pagenow == 'edit-tags.php' &&
                    isset($_REQUEST['wmpcs_synced'])) {
                if ($_REQUEST['wmpcs_synced'] == "selected") {
                    $message = "Selected terms synced successfully.";
                } elseif ($_REQUEST['wmpcs_synced'] == "all") {
                    $message = "All terms synced successfully.";
                }

                echo "<div class='update'><p>{$message}</p></div>";
            }
        }

    }
    