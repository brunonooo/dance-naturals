<?php

class wmpcs_admin {

    function __construct() {
        // this will create the admin menu page
        add_action('admin_menu', array($this, 'wmpcs_admin_menu'));

        add_action('admin_post_save_wmpcs_settings', array($this, 'wmpcs_save_settings'));
    }

    //add setting page link to menu
    function wmpcs_admin_menu() {
        $page = add_menu_page("MultiSite Product/Category Sync", "WMPCS", "manage_options", "wmpcs", array($this, 'wmpcs_admin_page'));

        add_action('load-' . $page, array($this, 'wmpcs_load_js_css'));
    }

    //add setting page HTML
    function wmpcs_admin_page() {
        $sites = get_sites();

        $options = get_option(WMPCS_OPTION_NAME);
        $current_site_id = get_current_blog_id();
        //get all post types
        $post_types = get_post_types();
        //filter unrequired default post types
        foreach ($post_types as $post_type_key => $post_type) {
            if (in_array($post_type, WMPCS_Setting::$not_allowed_default_post_types)) {
                unset($post_types[$post_type_key]);
            }
        }

        //get all registered taxonomies
        $taxonomies = get_taxonomies();
        //filter unrequired default taxonomies
        foreach ($taxonomies as $taxonomy_key => $taxonomy) {
            if (in_array($taxonomy, WMPCS_Setting::$NOT_ALLOWED_DEFAULT_TAXONOMIES)) {
                unset($taxonomies[$taxonomy_key]);
            }
        }
        require_once('admin-template.php');
    }

    //load script and stylesheets
    function wmpcs_load_js_css() {
        add_action('admin_enqueue_scripts', array($this, 'wmpcs_enqueue_admin_scripts'));
    }

    function wmpcs_enqueue_admin_scripts() {
        $asset_dir = WMPCS_URL . 'assets/';
        wp_enqueue_style('wmpcs_admin_page_css', $asset_dir . "css/admin.css");
        wp_enqueue_script('wmpcs_admin_page_js', $asset_dir . "js/admin.js");
    }

    function wmpcs_save_settings() {
        $posted_data = $_POST;
        if (!empty($posted_data['action']) && $posted_data['action'] == 'save_wmpcs_settings') {

            $option_data = [];

            if (!empty($posted_data['post_dest_sites']) && isset($posted_data['enable_post_sync']) && $posted_data['enable_post_sync'] == 'on') {

                $post_dest_sites = $posted_data['post_dest_sites'];

                $post_source_sites = $posted_data['post_source_sites'];

                $post_auto_sync = isset($posted_data['post_auto_sync']) ? $posted_data['post_auto_sync'] : '';
                $product_stock_sync = isset($posted_data['enable_stock_sync']) ? $posted_data['enable_stock_sync'] : '';

                $post_bulk_sync = isset($posted_data['post_bulk_sync']) ? $posted_data['post_bulk_sync'] : '';

                foreach ($post_source_sites as $key => $post_source_site) {

                    $option_data['post_sync'][$post_source_site]['dests'] = $post_dest_sites[$key];

                    if (isset($post_auto_sync[$key])) {
                        $option_data['post_sync'][$post_source_site]['auto_sync'] = $post_auto_sync[$key];
                    }
                    
                    if (isset($product_stock_sync[$key])) {
                        $option_data['post_sync'][$post_source_site]['enable_stock_sync'] = $product_stock_sync[$key];
                    }

                    if (isset($post_bulk_sync[$key])) {
                        $option_data['post_sync'][$post_source_site]['bulk_sync'] = $post_bulk_sync[$key];
                    }
                    if (isset($posted_data['post_types'][$key])) {
                        $option_data['post_sync'][$post_source_site]['post_types'] = $posted_data['post_types'][$key];
                    }
                }
            } else {
                $option_data['post_sync'] = [];
            }

            if (isset($posted_data['enable_taxonomy_sync']) && $posted_data['enable_taxonomy_sync'] == 'on' && !empty($posted_data['taxonomy_dest_sites']) && !empty($posted_data['taxonomy_source_sites'])
            ) {
                $taxonomy_dest_sites = $posted_data['taxonomy_dest_sites'];
                $taxonomy_source_sites = $posted_data['taxonomy_source_sites'];

                $taxonomy_auto_sync = isset($posted_data['taxonomy_auto_sync']) ? $posted_data['taxonomy_auto_sync'] : '';
                $taxonomy_bulk_sync = isset($posted_data['taxonomy_bulk_sync']) ? $posted_data['taxonomy_bulk_sync'] : '';

                foreach ($taxonomy_source_sites as $key => $taxonomy_source_site) {
                    if (!empty($taxonomy_dest_sites[$key])) {
                        $option_data['taxonomy_sync'][$taxonomy_source_site]['dests'] = $taxonomy_dest_sites[$key];
                        if (isset($taxonomy_auto_sync[$key])) {
                            $option_data['taxonomy_sync'][$taxonomy_source_site]['auto_sync'] = $taxonomy_auto_sync[$key];
                        }
                        if (isset($taxonomy_bulk_sync[$key])) {
                            $option_data['taxonomy_sync'][$taxonomy_source_site]['bulk_sync'] = $taxonomy_bulk_sync[$key];
                        }
                        if (isset($posted_data['taxonomies'][$key])) {
                            $option_data['taxonomy_sync'][$taxonomy_source_site]['taxonomies'] = $posted_data['taxonomies'][$key];
                        }
                    }
                }
            } else {
                $option_data['taxonomy_sync'] = [];
            }
        }

        update_option(WMPCS_OPTION_NAME, $option_data, FALSE);
        if (wp_get_referer()) {
            wp_safe_redirect(wp_get_referer());
        } else {
            wp_safe_redirect(get_home_url());
        }
    }

}

new wmpcs_admin();
