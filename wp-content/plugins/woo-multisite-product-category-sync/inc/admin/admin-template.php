<div class="wrap">
    <h2>WooCommerce/Wordpress MultiSite Posts/Products & Categories Sync</h2>
    <form method="post" action="admin-post.php" novalidate="novalidate">
        <input type="hidden" name="action" value="save_wmpcs_settings">
        <input type="hidden" name="total_sites" value="<?php echo count($sites); ?>">
        <table class="form-table">
            <tbody>
                <tr>
                    <th><label for="enable_post_sync">Enable Post Sync</label></th>
                    <td><input <?php
                            if (isset($options['post_sync']) && !empty($options['post_sync'])) {
                                echo 'checked';
                            }
                        ?> type="checkbox" name="enable_post_sync" id="enable_post_sync"></td>
                    <td>
                    </td>
                </tr>

                <tr class="post_sync_flow_head">
                    <th></th>
                    <th>
                        Sync From
                        <span class="dashicons dashicons-editor-help" title="Site to sync from"></span>
                    </th>
                    <th>
                        Sync To
                        <span class="dashicons dashicons-editor-help" title="Sync posts to these sites">  </span>
                    </th>
                    <th>
                        Sync these post types
                        <span class="dashicons dashicons-editor-help" title="Sync these post types"></span>
                    </th>
                    <th>
                        Enable Auto Sync
                        <span class="dashicons dashicons-editor-help" title="This will enable new/existing posts to sync automatically to destination sites when created/updated. If not enabled each post will have to manually marked for sycn. Note : Crons are still not integrated."></span>
                    </th>
                    <th>
                        Enable Bulk Sync
                        <span class="dashicons dashicons-editor-help" title="This will display bulk sync option in action select box on posts list page"></span>
                    </th>
                   
                </tr>

                <?php
                    $index = 0;
                    if (isset($options['post_sync']) && !empty($options['post_sync'])) {
                        foreach ($options['post_sync'] as $saved_site_id => $post_sync_data) {
                            ?>
                            <tr class="post_sync_flow" data-index="<?php echo $index; ?>">
                                <th></th>
                                <td>
                                    <select class="source_post_select" name="post_source_sites[]">
                                        <?php
                                        foreach ($sites as $site) {
                                            if ($current_site_id != $site->blog_id) {
                                                continue;
                                            }
                                            ?>
                                            <option <?php
                                            if ($saved_site_id == $site->blog_id) {
                                                echo 'selected';
                                            }
                                            ?> value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
            <?php } ?>
                                    </select>
                                </td>
                                <td>
                                <select required class="post_dest_sites_select" multiple name="post_dest_sites[<?php echo $index; ?>][]">
                                        <?php
                                        foreach ($sites as $site) {
                                            if ($current_site_id == $site->blog_id) {
                                                continue;
                                            }
                                            ?>
                                            <option <?php
                                            if (in_array($site->blog_id, $post_sync_data['dests'])) {
                                                echo 'selected';
                                            }
                                            ?> value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
            <?php } ?>
                                    </select>
                                </td>
                                
                                <td>
                                    <select class="multiple_post_select_box" multiple name="post_types[<?php echo $index; ?>][]">
                                        <?php foreach ($post_types as $post_type) { ?>
                                        <option <?php if (isset($post_sync_data['post_types']) && in_array($post_type, $post_sync_data['post_types'])) {
                                            echo "selected";
                                        } ?> value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
            <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <input <?php
                                    if (isset($post_sync_data['auto_sync']) && $post_sync_data['auto_sync'] == 'on') {
                                        echo 'checked';
                                    }
                                    ?> type="checkbox" class="post_auto_sync" name="post_auto_sync[<?php echo $index; ?>]">
                                </td>
                                <td>
                                    <input <?php
                                    if (isset($post_sync_data['bulk_sync']) && $post_sync_data['bulk_sync'] == 'on') {
                                        echo 'checked';
                                    }
                                    ?> type="checkbox" class="post_bulk_sync" name="post_bulk_sync[<?php echo $index; ?>]">
                                </td>
                               
            <?php if ($index > 0) { ?>
                <!--                                <td>
                                        <button class="wmpcs_remove_row">Remove Flow</button>
                                    </td>-->
                            <?php } ?>
                            </tr>
                            <?php
                            $index++;
                        }
                    } else {
                        ?>
                        <tr class="post_sync_flow" data-index="<?php echo $index; ?>">
                            <th></th>
                            <td>
                                <select class="source_post_select" name="post_source_sites[]">
                                    <?php
                                    foreach ($sites as $site) {
                                        if ($current_site_id != $site->blog_id) {
                                            continue;
                                        }
                                        ?>
                                        <option value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
        <?php } ?>
                                </select>
                            </td>
                            <td>
                            <select required class="post_dest_sites_select" multiple name="post_dest_sites[<?php echo $index; ?>][]">
                                    <?php
                                    foreach ($sites as $site) {
                                        if ($current_site_id == $site->blog_id) {
                                            continue;
                                        }
                                        ?>
                                        <option value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
        <?php } ?>
                                </select>
                            </td>

                            <td>
                                <select class="multiple_post_select_box" multiple name="post_types[<?php echo $index; ?>][]">
        <?php foreach ($post_types as $post_type) { ?>
                                        <option value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
        <?php } ?>
                                </select>
                            </td>
                            <td>
                                <input type="checkbox" class="post_auto_sync" name="post_auto_sync[<?php echo $index; ?>]">
                            </td>
                            <td>
                                <input type="checkbox" class="post_bulk_sync" name="post_bulk_sync[<?php echo $index; ?>]">
                            </td>

                        </tr>
        <?php
    }
?>

<!--                <tr class ="post_sync_flow_footer">
                    <th>

                    </th>
                    <td>
                        <button id = "addMorepostSyncFlow">+ Add more sync flows</button>
                    </td>
                    <td></td>

                <tr>-->
                <tr>
                    <th><label for = "enable_taxonomy_sync">Enable Taxonomy Sync</label></th>
                    <td><input <?php
                                if (isset($options['taxonomy_sync']) && !empty($options['taxonomy_sync'])) {
                                    echo 'checked';
                                }
                            ?>  type="checkbox" name="enable_taxonomy_sync" id="enable_taxonomy_sync"></td>
                    <td>
                    </td>
                </tr>
                <tr class="taxonomy_sync_flow_head">
                    <th></th>
                    <th>
                        Sync From
                        <span class="dashicons dashicons-editor-help" title="Site to sync from"></span>
                    </th>
                    <th>
                        Sync To
                        <span class="dashicons dashicons-editor-help" title="Sync posts to these sites"></span>
                    </th>
                    <th>
                        Sync these taxonomies
                        <span class="dashicons dashicons-editor-help" title="Sync these taxonomies types"></span>
                    </th>
                    <th>
                        Enable Auto Sync
                        <span class="dashicons dashicons-editor-help" title="This will enable new/existing terms to sync automatically to destination sites when created/updated. If not enabled each term will have to manually marked for sycn. Note : Crons are still not integrated."></span>
                    </th>
                    <th>
                        Enable Bulk Sync
                        <span class="dashicons dashicons-editor-help" title="This will display bulk sync option in action select box on term list page"></span>
                    </th>
                </tr>

                <?php
                    $index = 0;
                    if (isset($options['taxonomy_sync']) && !empty($options['taxonomy_sync'])) {
                        foreach ($options['taxonomy_sync'] as $saved_site_id => $taxonomy_sync_data) {
                            ?>
                            <tr class="taxonomy_sync_flow" data-index="<?php echo $index; ?>">
                                <th></th>
                                <td>
                                    <select class="source_taxonomy_select" name="taxonomy_source_sites[]">
                                        <?php
                                        foreach ($sites as $site) {
                                            if ($current_site_id != $site->blog_id) {
                                                continue;
                                            }
                                            ?>
                                            <option <?php
                                                if ($saved_site_id == $site->blog_id) {
                                                    echo 'selected';
                                                }
                                                ?> value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                <select required class="taxonomy_dest_sites_select" multiple name="taxonomy_dest_sites[<?php echo $index; ?>][]">
                                        <?php
                                        foreach ($sites as $site) {
                                            if ($current_site_id == $site->blog_id) {
                                                continue;
                                            }
                                            ?>
                                            <option <?php
                                if (in_array($site->blog_id, $taxonomy_sync_data['dests'])) {
                                    echo 'selected';
                                }
                                ?> value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="multiple_taxonomy_select_box" multiple name="taxonomies[<?php echo $index; ?>][]">
                                        <?php foreach ($taxonomies as $taxonomy) { ?>
                                        <option <?php if (isset($taxonomy_sync_data['taxonomies']) && in_array($taxonomy, $taxonomy_sync_data['taxonomies'])) {
                                            echo "selected";
                                        } ?> value="<?php echo $taxonomy; ?>"><?php echo $taxonomy; ?></option>
            <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <input <?php
                        if (isset($taxonomy_sync_data['auto_sync']) && $taxonomy_sync_data['auto_sync'] == 'on') {
                            echo 'checked';
                        }
                                    ?> type="checkbox" class="taxonomy_auto_sync" name="taxonomy_auto_sync[<?php echo $index; ?>]">
                                </td>
                                <td>
                                    <input <?php
                        if (isset($taxonomy_sync_data['bulk_sync']) && $taxonomy_sync_data['bulk_sync'] == 'on') {
                            echo 'checked';
                        }
                                    ?> type="checkbox" class="taxonomy_bulk_sync" name="taxonomy_bulk_sync[<?php echo $index; ?>]">
                                </td>
                            </tr>
            <?php
            $index++;
        }
    } else {
        ?>
                        <tr class="taxonomy_sync_flow" data-index="<?php echo $index; ?>">
                            <th></th>
                            <td>
                                <select class="source_taxonomy_select" name="taxonomy_source_sites[]">
                                    <?php
                                    foreach ($sites as $site) {
                                        if ($current_site_id != $site->blog_id) {
                                            continue;
                                        }
                                        ?>
                                        <option value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>
                            <select required class="taxonomy_dest_sites_select" multiple name="taxonomy_dest_sites[<?php echo $index; ?>][]">
                                    <?php
                                    foreach ($sites as $site) {
                                        if ($current_site_id == $site->blog_id) {
                                            continue;
                                        }
                                        ?>
                                        <option value="<?php echo $site->blog_id; ?>"><?php echo get_site_url($site->blog_id); ?></option>
        <?php } ?>
                                </select>
                            </td>
                            <td>
                                    <select class="multiple_taxonomy_select_box" multiple name="taxonomies[<?php echo $index; ?>][]">
                                        <?php foreach ($taxonomies as $taxonomy) { ?>
                                        <option value="<?php echo $taxonomy; ?>"><?php echo $taxonomy; ?></option>
            <?php } ?>
                                    </select>
                                </td>
                            
                            <td>
                                <input type="checkbox" class="taxonomy_auto_sync" name="taxonomy_auto_sync[<?php echo $index; ?>]">
                            </td>
                            <td>
                                <input type="checkbox" class="taxonomy_bulk_sync" name="taxonomy_bulk_sync[<?php echo $index; ?>]">
                            </td>

                        </tr>
        <?php
    }
?>
                <!--
                                <tr class="taxonomy_sync_flow_footer">
                                    <th>
                
                                    </th>
                                    <td> 
                                        <button id="addMoretaxonomySyncFlow">+ Add more sync flows</button>
                                    </td>
                                    <td></td>
                
                                <tr>-->

            </tbody>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p></form>
</div>