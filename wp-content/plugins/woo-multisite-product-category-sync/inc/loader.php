<?php

    //THIS CLASS LOADS SYNC BASED ON ADMIN SETTINGS
    class WMPCS_Loader {

        private $wmpcs_options = array();
        private static $instance = NULL;

        static public function getInstance($options) {
            if (self::$instance === NULL)
                self::$instance = new WMPCS_Loader($options);
            return self::$instance;
        }

        public function __construct($options) {
            $this->wmpcs_options = $options;

            if (!empty($this->wmpcs_options)) {
                $current_blog_id = get_current_blog_id();
                $this->wmpcs_options['current_blog_id'] = $current_blog_id;
               
                //CHECK IF TAXONOMY SYNC IS ENABLED FOR CURRENT SITE
                if (!empty($this->wmpcs_options['taxonomy_sync']) && array_key_exists($current_blog_id, $this->wmpcs_options['taxonomy_sync'])) {
                    //taxonomy common functions file
                    require_once plugin_dir_path(__FILE__) . 'taxonomy-sync/functions.php';

                    $this->wmpcs_options['taxonomy_sync'] = $this->wmpcs_options['taxonomy_sync'][$current_blog_id];

                    //load sync functions file
                    require_once plugin_dir_path(__FILE__) . 'taxonomy-sync/class-wmpcs-taxonomy-sync.php';

                    //admin sync options on product edit/add page
                    require_once plugin_dir_path(__FILE__) . 'taxonomy-sync/class-wmpcs-taxonomy-admin-sync.php';
                    new WMPCS_Taxonomy_Admin_Sync($this->wmpcs_options['taxonomy_sync']);

                    //include bulk sync
                    if (isset($this->wmpcs_options['taxonomy_sync']['bulk_sync']) && $this->wmpcs_options['taxonomy_sync']['bulk_sync'] == 'on') {
                        require_once plugin_dir_path(__FILE__) . 'taxonomy-sync/class-wmpcs-taxonomy-bulk-sync.php';
                        new WMPCS_Taxonomy_Bulk_Sync($this->wmpcs_options['taxonomy_sync']);
                    }
                } else {
                    //load sync functions file
                    require_once plugin_dir_path(__FILE__) . 'taxonomy-sync/class-wmpcs-taxonomy-sync.php';
                }

                //CHECK IF PRODUCT SYNC IS ENABLED FOR CURRENT SITE
                if (!empty($this->wmpcs_options['post_sync']) && array_key_exists($current_blog_id, $this->wmpcs_options['post_sync'])) {

                    //product common functions file
                    require_once plugin_dir_path(__FILE__) . 'post-sync/functions.php';

                    //load sync class
                    $this->wmpcs_options['post_sync'] = $this->wmpcs_options['post_sync'][$current_blog_id];


                    //admin sync options on product edit/add page
                    require_once plugin_dir_path(__FILE__) . 'post-sync/class-wmpcs-post-admin-sync.php';

                    

                    $GLOBALS['WMPCS_Post_Admin_Sync'] = new WMPCS_Post_Admin_Sync($this->wmpcs_options['post_sync']);

                    //sync class
                    require_once plugin_dir_path(__FILE__) . 'post-sync/class-wmpcs-post-sync.php';

                    //include and load product bulk sync
                    if (isset($this->wmpcs_options['post_sync']['bulk_sync']) && $this->wmpcs_options['post_sync']['bulk_sync'] == 'on') {

                        require_once plugin_dir_path(__FILE__) . 'post-sync/class-wmpcs-post-bulk-sync.php';
                        new WMPCS_Post_Bulk_Sync($this->wmpcs_options['post_sync']);
                    }

                }
            }
        }

    }

    function WMPCS_Loader($options) {
        return WMPCS_Loader::getInstance($options);
    }
    