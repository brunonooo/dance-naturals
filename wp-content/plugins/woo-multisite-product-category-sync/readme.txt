=== Plugin Name ===
Contributors: smgom
Tags: Wordpress Network,Woocommerce multisite Network multisite, Woocommerce Multiste Sync, Product categories sync,  Post Sync,Pages sync, Custom Post types, Custom Taxonomies
Requires at least: 4.6
Tested up to: 4.7

Synchronize all Woo product types,posts, pages and all other custom registered post types seamlessly across multiple sites on a same network.
Synchronize product categories,post categories, post tags and all other custom registered taxonomies
User friendly interface

== Description ==
Sync all product types i.e simple,variable, grouped,virtual and bundled
Sync all wordpress default post types i.e. posts, pages 
Supports all custom registered post types
Sync product categories & attributes
Supports all custom registered taxonomies
Sync any media in post content
Sync post featured image, Sync gallery images for woocommerce product
Sync all post/product comments
Porduct/Posts/Categories are automatically synced on create/update.
Options to sync products/taxonomies in bulk.
Admin User can set the flow of taxonomies/products syncs.
Support taxonomies sync plus syncing of all custom taxonomies across multiple sites.
Sync all relative post data including all custom fields,commenets,image,tags,attributes.
Sync all taxonomy data across sites
Sync works automatically and in real time
Option to bulk sync posts and taxonomies data

== Installation ==
1) Maunal Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2) Activate the plugin through the 'Plugins' screen in WordPress
3) On main WP admin menu -> Woo MultiSite Product Sync screen to configure the plugin
4) Select Sync flow for products and taxonomies
5) Define custom taxonomies to sync across sites

== Frequently Asked Questions ==

Q.1) Can this plugin sync data bidirectional?

A) Yes, according to admin selected settings plugin can sync data between various sites. 
Admin cans select how data flows across the sites.

Q.2) Will this work for terms with custom registered taxonomies?

A) Yes, this plugin will sync all terms under any custom taxonomies.

Q.3) Can admin disable some of the posts/terms for not syncing?

A) Yes admin can disable/enable syncs for specific posts/terms.



== Screenshots ==
https://drive.google.com/file/d/0B0xmwiW8LoZ_bFJQMVRTUHFPcXc/view?usp=drivesdk


== A brief Info ==
1. Sync all product/post/taxonomies data
2. Support all product types
2. Support all custom registered post types and taxonomies
3. Data syncs in real time
4. Options for bulk data sync

