// Object assign Polyfill
if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function(target, firstSource) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);

                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}

(function(window, $){
    $(document).ready(function(){

        var is_admin = $('.wp-admin').length,
          $preview_area = !is_admin ? $('.vpc-preview') : $('.vpcos-preview'),
          view_type_selector = !is_admin ? 'view-name' : 'view-type';
        previews_generated = false,
          $saveButtons = $('.vpcos-save-loader'),
          $saveAllButton = $('#vpcos-save-all-previews'),
          $downloadAllButton = $('#vpcos-download-previews');

        if(is_admin) {// CHECK IF WE ARE DEALING WITH BACKEND
            $saveAllButton.hide()
            $downloadAllButton.hide();
        }

        function ProgressBar($el) {
            var bar = {};
            bar.$el = $el;
            bar.status = 0;
            bar.update = function (color, progress){
                bar.$el.css({ 'background-color': color, 'transform':'scaleX('+progress+')'});
            };
            bar.reset = function (color){
                bar.$el.css({ 'background-color': color, 'transform':'scaleX(0)'});
            };
            bar.error = function (){
                bar.$el.css({ 'background-color': '#d32f2f', 'transform':'scaleX(1)'});
                window.setTimeout(function () {
                    bar.$el.css({ 'background-color': '#d32f2f', 'transform':'scaleX(0)'})
                }, 2000);
            };
            bar.complete = function (color){
                bar.$el.css({ 'background-color': color, 'transform':'scaleX(1)'});
                window.setTimeout(function () {
                    bar.$el.css({ 'background-color': '#66bb6a', 'transform':'scaleX(0)'})
                }, 2000);
            };
            return bar;
        }

        // REGISTER GENERATE PREVIEW BUTTON
        $preview_area.find('#vpcos-generate-previews').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            $preview_area.addClass('loading-previews');
            var $default_configurations = $('.default-config[checked]'),
              views = {};

            // THIS IS REALLY UNSPECIFIC CODE BUT IT WORKS DUE TO LACK OF OTHER POSSIBILITIES - Manuel

            // LOOP THROUGH EACH CONFIGURATION
            $default_configurations.each(function(){
                var $config =$(this);
                // BACK UP TO THE ROWS
                $config.parents('.vpc-option-row').each(function(){
                    // THROUGH THE DEFAULT VIEWS
                    let $views = $(this).find('.repeatable-fields-table .vpc-option-row');
                    $views.each(function(){
                        var $view = $(this);
                        var src_piece = $view.find('input[type="hidden"]').val();
                        var view_name = $view.find('option[selected]').text().toLowerCase().replace(/ /g, '');
                        //!! REMEMBER TO USE NAME VIEW VALUE!!!

                        if (!views || !views[view_name]) {
                            views[view_name] = [];
                        }

                        views[view_name].push(src_piece)
                    });
                });
            });

            // BUILD UP PREVIEWS
            var k = 0,
              $preview_areas = !is_admin ? $('.vpc-preview') : $('.vpcos-preview-inner');

            for (var view in views){
                if(views.hasOwnProperty(view)){
                    $($preview_areas[k]).attr('data-'+view_type_selector, view);
                    var single_view_markup = '';
                    single_view_markup += views[view].reduce(function(a, b){
                        var c = '<img src="'+window.vpcos_localize.siteurl+'/' + b +'">';
                        if(b.includes("|"))
                            c = b.split("|")[2];
                        return a ? a + c : c ;
                    }, 0);

                    $($preview_areas[k]).html(single_view_markup);
                    $($preview_areas[k]).find('img').on('load', function(e) {
                        let preview_area_padding =  100 * e.target.naturalHeight / e.target.naturalWidth;
                        $(e.target).parent('.vpcos-preview-inner').css('padding-top', preview_area_padding+'%');
                    });

                    ++k;
                }
            }

            // SHOW MARKUP AFTER GENERATION AND UNLOCK SAVING
            $preview_area.removeClass('loading-previews').addClass('previews-ready');
            $saveAllButton.show()
            $downloadAllButton.show();
            previews_generated = true;
        });

        function getPreviewData ($previewsWrappers) {
            var views = {};
            $previewsWrappers.each(function() {
                var viewType = !is_admin ? $(this).data(view_type_selector) : $(this).find('[data-'+view_type_selector+']').data(view_type_selector);
                views[viewType] = []
                $(this).find('img').each(function(){
                    var src = $(this).attr('src');
                    if (src !== 'undefined'){
                        const regex = /(.*)-\d+x\d+\.(\w+)/gm;
                        const subst = `$1\.$2`;
                        src = src.replace(regex, subst);

                        src = isURL(src) ? src : location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') + src;
                        views[viewType].push(src);
                    }
                });
            });

            return views;
        }

        function isURL(str) {
            var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
            return pattern.test(str);
        }

        function savePreviews(extraData, cb = false){
            var title = !is_admin ? $('.post-type-page').attr('id') : $('input[name="post_title"]').val()
            var configuration_name = title.replace(/ /g, '');

            var data = {};
            data.action = 'vpcos_create_configured_images';
            data.configuration_name = configuration_name;
            Object.assign(data, extraData);
            data.with_download = true;
            console.log(data);

            $.ajax({
                url: vpcos_localize.admin_ajax,
                method: 'POST',
                data: data,
                success: function (response) {
                    console.log(response, 'You can sleep well tonight!');

                    response = JSON.parse(response);
                    if(response.images.length) {
                        response.images.map(function(image) {
                            saveAs(image.url, image.name);
                        });
                    }

                    if(typeof cb === 'function'){cb()}
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    pb.error();
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        }

        // REGISTER SAVE PREVIEW BUTTON
        $saveButtons.on('click', function(e) {
              e.preventDefault();
              e.stopPropagation();

              // BAIL EARLY IF PREVIEWS ARE NOT READY
              if (!previews_generated) {
                  return
              }

              var $view = $(this).parents('.vpcos-preview-wrapper');

              // START LOADER
              $view.addClass('saving-preview');
              var pb = ProgressBar($view.find('.vpcos-progress-bar'));

              var previewData = getPreviewData($view);

              pb.update('#37474f', .33);

              savePreviews({views: previewData}, function() {
                  pb.complete('#66bb6a');
                  // STOP LOADER
                  $view.removeClass('saving-preview');
              });
          }
        );

        $saveAllButton.on('click', function (e){
            e.preventDefault();
            e.stopPropagation();

            // BAIL EARLY IF PREVIEWS ARE NOT READY
            if (!previews_generated) {
                return
            }

            var $views = $('.vpcos-preview-wrapper');
            var $blockUi = $('#vpcos-create-configuration-previews .inside');
            $blockUi.addClass('saving-preview');

            var previewData = getPreviewData($views);

            savePreviews({views: previewData}, function() {
                // STOP LOADER
                $blockUi.removeClass('saving-preview');
            });
        });

        $downloadAllButton.on('click', function (e){
            e.preventDefault();
            e.stopPropagation();

            // BAIL EARLY IF PREVIEWS ARE NOT READY
            if (is_admin && !previews_generated) {
                return
            }

            var $views = !is_admin ? $('.vpc-preview') : $('.vpcos-preview-wrapper');
            var $blockUi = !is_admin ? $('#vpc-container') : $('#vpcos-create-configuration-previews .inside');
            $blockUi.addClass('saving-preview');

            var previewData = getPreviewData($views);

            savePreviews({views: previewData, with_download: true}, function() {
                // STOP LOADER
                $blockUi.removeClass('saving-preview');
            });
        });

        /*
         REGISTER REINDEX BUTTON
          */
        // (to optimize!)
        let $reindex_btn = $('#vpcos-reindex-config'),
          $notice = $('.vpcos-reindex-buttons small'),
          $loader = $('.vpcos-reindex .loader'),
          reindex = function(){
              $loader.show(400);
              let i = 0;
              $('#vpc-config-components-table > tbody > .ui-sortable-handle').each(function(){
                  let current_component_meta = '[components]['+i+']',
                    $current_component = $(this);

                  let $components = $current_component.find('[name*="[components]"]');

                  $components.each(function(){
                      var new_name = this.name.replace(/\[components\]\[\d+\]/, current_component_meta);
                      $(this).attr('name', new_name);
                      console.log(new_name);
                  });

                  let $options = $current_component.find('.omodal-body > table > tbody > .ui-sortable-handle');
                  if ($options.length) {
                      let k = 0;
                      $options.each(function () {
                          let $options_els = $(this).find('[name*="[options]"]'),
                            current_option_meta = '[options]['+k+']';

                          $options_els.each(function(){
                              let new_name = this.name.replace(/\[options\]\[\d+\]/, current_option_meta);
                              $(this).attr('name', new_name);
                              console.log(new_name);
                          });

                          let $views = $(this).find('.views .vpc-option-row');
                          if ($views.length){
                              let v = 0;
                              $views.each(function(){
                                  $views_els = $(this).find('[name*="[view_options]"]'),
                                    current_view_meta = '[view_options]['+v+']';

                                  $views_els.each(function(){
                                      let new_name = this.name.replace(/\[view_options\]\[\d+\]/, current_view_meta);
                                      $(this).attr('name', new_name);
                                      console.log(new_name);
                                  });
                                  ++v;
                              });
                          }
                          ++k;
                      });
                  }
                  ++i;
              });
              $loader.hide(400);
              $notice.show(400);
          };

        if ($reindex_btn.length){
            $notice.hide();
            $loader.hide();
            $reindex_btn.on('click', reindex);
        }

        /*
         * WORDSHUNTER
         */

        var $wh = $('<div class="vpcos-wordshunder">\n' +
          '            <p>WORDS HUNTER</p>\n' +
          '            <div class="vpcos-wh-wrapper">\n' +
          '                <div class="vpcos-wh-single-input">\n' +
          '                <label for="wh-find">Trova:</label>\n' +
          '                <input type="text" id="wh-find" name="wh-find">\n' +
          '                </div>\n' +
          '                <div class="vpcos-wh-single-input">\n' +
          '                    <label for="wh-replace">Sostituisci con:</label>\n' +
          '                    <input type="text" id="wh-replace" name="wh-replace">\n' +
          '                </div>\n' +
          '                <button id="wh-hunt" class="button button-primary button-large">Sostituisci</button>\n' +
          '            </div>\n' +
          '            <div class="vpcos-wh-results"></div>\n' +
          '            </div>\n' +
          '        </div>'),
          $showBtn = $('#vpcos-show-wordshunt'),
          $hideBtn = $('#vpcos-hide-wordshunt'),
          $resetBtn = $('#vpcos-reset-defaults-wordshunt');

        $hideBtn.hide();
        $('body').append($wh);

        $wh = $('.vpcos-wordshunder');
        $wh.hide();

        function whInit() {
            var $targets = $('.vpc-option-name, .vpc-option-group, textarea[name*="[desc]"]'),
              $resultsBox = $('.vpcos-wh-results');

            function getFindReplace() {
                var find = $('#wh-find').val();
                var replace = $('#wh-replace').val();

                return {
                    find: find.trim(),
                    replace: replace.trim()
                }
            }

            function printOccurrences(occ, tot, prefix='Trovate') {
                if(occ){
                    $resultsBox.html($('<small>'+prefix+' <strong>'+occ+'</strong> occorrenze su <strong>'+tot+'</strong> campi.</small>'));
                } else {
                    whMessage('Nulla da sostituire per la ricerca corrente.');
                }
            }

            function whMessage (message) {
                $resultsBox.html($('<small>'+message+'</small>'));
            }

            function whFind(replace = false) {
                var wh = getFindReplace(),
                  actionPrefix = replace ? 'Sostituite' : 'Trovate';

                if (replace) {
                    if (wh.find === '') {
                        whMessage('Stai cercando una stringa vuota... Inserisci del testo e riprova');
                        return;
                    }

                    if (wh.replace === '' && replace) {
                        whMessage('Non puoi sostituire con una stringa vuota... Hai idea della confusione che si creerebbe?');
                        return;
                    }

                    if (wh.find === wh.replace && replace){
                        whMessage('Le due stringhe sono uguali... Facciamo gli spiritosi?');
                        return;
                    }
                }

                var i = 0,
                  k = 0;

                $targets.each(function () {
                    var $current = $(this),
                      string = $current.val();
                    if (string === wh.find) {
                        if(replace){
                            $current.val(wh.replace);
                        }
                        ++k;
                    }
                    ++i;
                });

                printOccurrences(k, i, actionPrefix);
            }

            $('#wh-replace').focus(function(e) {
                e.preventDefault();
                whFind();
            });

            $('#wh-hunt').click(function(e) {
                e.preventDefault();
                whFind(true);
            });

            $showBtn.click(function(e){
                e.preventDefault();
                $wh.show();
                $hideBtn.show();
                $showBtn.hide();
            });

            $hideBtn.click(function(e){
                e.preventDefault();
                $('.vpcos-wordshunder').hide();
                $hideBtn.hide();
                $showBtn.show();
            });

            $resetBtn.click(function(e){
                e.preventDefault();
                var i = 0,
                  k = 0;
                $('.omodal').each(function(){
                    var $def = $(this).find('input[name="vpc-config[components]['+i+'][options][0][default]"]');
                    if($def){
                        $def.prop("checked", true);
                        ++k;
                    }
                    ++i;
                });

                $resetBtn.after($('<small>Resettati '+k+' default su '+i+' materiali!</small>'))
            });
        }

        whInit();

        function overrideOldValueWithNewOne(oldValue, newValue) {
            var $input = $('[value="'+oldValue+'"]');

            if($input.length > 1) {
                console.warn('Duplicate found for \''+oldValue+ '\'!')
            }

            $input.val(newValue);
            console.log('Successfully updated '+oldValue)
        }

        function requestNotificationPermission() {
            // Let's check if the browser supports notifications
            if (!("Notification" in window)) {
                window.alert("Spiacenti, il browser non supporta le notifiche. Per ricevere la notifica di completata migrazione usa un browser piu moderno, altrimenti tieni d'occhio la console per vedere lo stato della migrazione.");
            }

            if (Notification.permission === "granted") {
                new Notification("Iniziata la migrazione della configurazione " + $('input[name="post_title"]').val() + "!");
            } else {
                Notification.requestPermission().then(function(result) {
                    if (result === "granted") {
                        new Notification("Grazie, per aver attivato le notifiche! Verrai notificato al termine di ogni migrazione!");
                    } else {
                        window.alert('Notifiche non attivate. Controlla la console per vedere lo stato della migrazione.')
                    }
                });
            }
        }

        function notifyMigrationCompletion() {
            new Notification("🎉 Congratulazioni, la migrazione della configurazione" + $('input[name="post_title"]').val() + " è stata terminata con successo! 🎉");
        }

        function recursivelyMigrateImagesValue(images, currentIndex) {
            const migrationComplete = currentIndex >= images.length
            if (migrationComplete) {
                console.log('Congratulations! You migrated all the images values!');
                return
            }

            return new Promise(function(resolve) {
                // Case migration is complete
                if (migrationComplete) {
                    notifyMigrationCompletion();
                }

                var $currentImage = $(images[currentIndex])
                var isIcon = $currentImage.attr('name').indexOf('[icon]') >= 0
                var imageValue = $currentImage.val()

                // Bail Early if no need to migrate
                var test = imageValue.indexOf('|')
                if ( test >= 0){
                    console.warn(imageValue + ' does not need migration!');
                    resolve()
                } else {
                    console.log('Migrating image '+(currentIndex+1)+ ' of '+images.length);

                    $.ajax({
                        url: vpcos_localize.admin_ajax,
                        method: 'POST',
                        data: {
                            img_url: imageValue,
                            is_icon: isIcon,
                            action: 'ajax_convert_to_new_image_management'
                        },
                        success: function (response) {
                            var newValue = JSON.parse(response);
                            overrideOldValueWithNewOne(imageValue, newValue);
                            resolve();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                            resolve();
                        }
                    })
                }
            }).then(function() {recursivelyMigrateImagesValue(images, currentIndex + 1)})
        }

        $('#vpcos-migrate-images').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            requestNotificationPermission();

            var $icons = $('[name*="[icon]"]');
            var $images = $('[name*="[view_options]"]').filter(function(){
                return $(this).attr('name').indexOf('[image]') >= 0
            });

            var $allImages = $icons.add($images);

            var i = 0;
            recursivelyMigrateImagesValue($allImages, i);
        });

        window['NOOO_DEV'] = {migrate: null}
        window.NOOO_DEV.migrate = recursivelyMigrateImagesValue;
    });
})(window, jQuery);
