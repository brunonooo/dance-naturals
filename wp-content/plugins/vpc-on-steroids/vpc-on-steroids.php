<?php
/*
Plugin Name: Vpc on Steroids
Plugin URI: http://nooo.it
Description: Nooo extension of vpc plugin
Version: 0.1.0
Author: Nooo Agency
Author URI: http://nooo.it
Text Domain: vpc-on-steroids
Domain Path: /languages
*/

defined('ABSPATH' ) or die( 'Hey! You\'re not going to cheat man, right?' );

class VPC_on_steroids
{

	private $plugin_name;

	function __construct($plugin_name) {
		$this->plugin_name = $plugin_name;

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_resources' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_resources' ) );
		add_action( 'add_meta_boxes', array( $this, 'vpcos_add_create_previews_metabox' ) );
		add_action( 'add_meta_boxes', array( $this, 'vpcos_add_create_reindex_metabox' ) );
		add_action( 'add_meta_boxes', array( $this, 'vpcos_add_create_wordshunter_metabox' ) );
		add_action( 'add_meta_boxes', array( $this, 'vpcos_migrate_images_values' ) );
		add_action( 'wp_ajax_vpcos_upload_media', array($this, 'vpcos_upload_media'));
		add_action( 'wp_ajax_vpcos_create_configured_images', array($this, 'vpcos_create_configured_images'));
	}

	function enqueue_resources(){

		if(is_user_logged_in()) {
		    // Bail early if page is not Backend or configurator page
		    if (!is_admin() && !is_page_template('configurator.php') ) {
		        return;
            }
			// REG STYLES
			wp_register_style( 'vpcos_admin_styles', plugins_url( '/assets/css/vpcos-admin.css', __FILE__ ), false, null );

			// REG SCRIPTS
			wp_register_script( 'filesaver', plugins_url( '/assets/vendors/FileSaver.js-master/dist/FileSaver.min.js', __FILE__ ), array(), null, true );
			wp_register_script( 'vpcos_admin_script', plugins_url( '/assets/js/vpcos-admin.js', __FILE__ ), array(), null, true );
			wp_localize_script( 'vpcos_admin_script', 'vpcos_localize', array( 'siteurl'    => get_option( 'siteurl' ),
			                                                                   'admin_ajax' => admin_url( 'admin-ajax.php' )
			) );


			wp_enqueue_script( 'filesaver' );
			wp_enqueue_style( 'vpcos_admin_styles' );
			wp_enqueue_script( 'vpcos_admin_script' );
		}
	}

	function vpcos_add_create_previews_metabox ( ) {
		add_meta_box('vpcos-create-configuration-previews', __('Creazione anteprime', 'NOOO'), array($this, 'build_create_previews_metabox_markup'), 'vpc-config', 'normal', 'high');
	}

	function vpcos_add_create_reindex_metabox ( ) {
		add_meta_box('vpcos-create-configuration-reindex', __('Aggiorna indicizzazione configurazione', 'NOOO'), array($this, 'build_create_reindex_metabox_markup'), 'vpc-config', 'normal', 'high');
	}

	function vpcos_add_create_wordshunter_metabox () {
		add_meta_box('vpcos-create-wordshunter', __('Trova e sostituisci - Wordshunter', 'NOOO'), array($this, 'build_wordshunter_markup'), 'vpc-config', 'normal', 'high');
	}

	function vpcos_migrate_images_values () {
		add_meta_box('vpcos-migrate-images', __('Migrazione campi immagini alla nuova versione del configuratore', 'NOOO'), array($this, 'build_img_migration_button'), 'vpc-config', 'normal', 'high');
	}

	// MERGE COMMON PARTS WITH CONFIGURATOR VERSION FOR DRYIER CODE
	function vpcos_create_configured_images() {
		$views = $_POST['views'];
		$i = 0;
		$saved_images = [];

		foreach ( $views as $viewType => $view ) {
			$sizes = getimagesize(filter_var($view[0],FILTER_VALIDATE_URL) ? $view[0] : __DIR__ . "/../../.." . $view[0]);
			$mergedCanvas = imagecreatetruecolor($sizes[0], $sizes[1]);
			$transparent = imagecolorallocatealpha($mergedCanvas, 0,0,0,127);
			imagefill($mergedCanvas, 0, 0, $transparent);
			imagesavealpha($mergedCanvas, true);
			foreach ($view as $src) {
				$curr = imagecreatefrompng(filter_var($src,FILTER_VALIDATE_URL) ? $src : __DIR__ . "/../../.." . $src);
				imagealphablending($curr, true);
				imagesavealpha($curr, true);
				imagecopy($mergedCanvas, $curr, 0, 0, 0, 0, $sizes[0], $sizes[1]);
				imagedestroy($curr);
			}

			$media_info = $this->vpcos_create_media_info(array(
				'config_name' => $_POST['configuration_name'],
				'view_type' => $viewType
			));

			$media_path = $media_info['img_location']['file_path'];

			imagepng($mergedCanvas, $media_path);
			imagedestroy($mergedCanvas);

			$this->vpcos_upload_media($media_info);

			if ($_POST['with_download']) {
				$saved_images[] = array(
					'name' => $media_info['filename'],
					'url' => $media_info['img_location']['file_url']
				);
			}

			++$i;
		}

		if ($saved_images) {
			echo json_encode(array('images' => $saved_images));
		}
		die();
	}

	function vpcos_create_media_info ($m_info = array()) {
		$date = current_time('Y/m');

		$default_media_info = [
			'id' => uniqid(),
			'upload_dir' => wp_upload_dir($date),
			'config_name' => '',
			'image_location' => '',
			'image_data' =>  $_POST['img_data'],
			'filename' => null,
			'view_type' => '',
		];

		$media_info = array_merge($default_media_info, $m_info);

		$media_info['filename'] = $media_info['config_name'] .'_' . $media_info['view_type']. '_' . $media_info['id'] . '.png';

		$media_info['img_location']  = $this->get_preview_locations($media_info);

		return $media_info;
	}

	function vpcos_upload_media ($media_info) {

		$wp_filetype = wp_check_filetype($media_info['img_location']['file_path']);

		$attachment = array(
			'guid'           => $media_info['upload_dir']['url'] . '/' . basename( $media_info['filename'] ),
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => $media_info['filename'],
			'post_content' => '',
			'post_status' => 'inherit'
		);

		$attachment_id = wp_insert_attachment( $attachment, $media_info['img_location']['file_path'] );

		// GENERATING ATTACHMENT METADATA
		// DO NOT DELETE THIS (wp_generate_attachment_metadata depends on it!)
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		$attachment_metadata = wp_generate_attachment_metadata( $attachment_id, $media_info['img_location']['file_path'] );
		wp_update_attachment_metadata( $attachment_id, $attachment_metadata );
	}

	function get_preview_locations($media_info){
		$upload_dir = $media_info['upload_dir'];
		$generation_path = $upload_dir["path"];
		$generation_url = $upload_dir["url"];
		$final_file_url="";
		if (wp_mkdir_p($generation_path)) {
			$final_file_path = $generation_path . '/'. $media_info['filename'];
			$final_file_url = $generation_url . '/'. $media_info['filename'];
		}

		return array(
			'file_path' => $final_file_path,
			'file_url' => $final_file_url
		);
	}

	function build_create_previews_metabox_markup() {
		wp_nonce_field(basename(__FILE__), "vpcos-create-configuration-previews-nonce");

		?>
        <div class="vpcos-preview">
            <p>Clicca sul pulsante per creare le anteprime delle viste della configurazione.</p>
            <div class="vpcos-preview-wrapper">
                <div class="vpcos-preview-mask">
                    <div class="vpcos-preview-inner">
                    </div>
                    <span class="vpcos-save-loader"></span>
                    <span class="vpcos-progress-bar"></span>
                </div>
            </div>
            <div class="vpcos-preview-wrapper">
                <div class="vpcos-preview-mask">
                    <div class="vpcos-preview-inner">
                    </div>
                    <span class="vpcos-save-loader"></span>
                    <span class="vpcos-progress-bar"></span>
                </div>
            </div>
            <div class="vpcos-preview-wrapper">
                <div class="vpcos-preview-mask">
                    <div class="vpcos-preview-inner">
                    </div>
                    <span class="vpcos-save-loader"></span>
                    <span class="vpcos-progress-bar"></span>
                </div>
            </div>
            <div class="vpcos-buttons">
                <button id="vpcos-generate-previews" class="button button-secondary button-large"><span class="dashicons dashicons-images-alt2"></span><?php _e('Genera le anteprime', 'NOOO'); ?></button>
                <button id="vpcos-save-all-previews" class="button button-secondary button-large"><span class="dashicons dashicons-yes"></span><?php _e('Salva le anteprime', 'NOOO'); ?></button>
                <button id="vpcos-download-previews" class="button button-secondary button-large"><span class="dashicons dashicons-download"></span><?php _e('Salva e scarica le anteprime', 'NOOO'); ?></button>
                <small>Felice del risultato? Clicca sulla singola immagine per salvarla nella libreria.</small>
            </div>
        </div>
		<?php
	}

	function build_create_reindex_metabox_markup() {
		?>
        <div class="vpcos-reindex">
            <p>Clicca sul pulsante per reindicizzare la configurazione.</p>
            <div class="vpcos-reindex-buttons">
                <button id="vpcos-reindex-config" class="button button-secondary button-large"><?php _e('Reindicizza voci di configurazione', 'NOOO'); ?></button>
                <span class="loader"></span>
                <small><strong>Configurazione reindicizzata! Ora puoi tirare un sospiro di sollievo e salvare!</strong></small>
            </div>
        </div>
		<?php
	}

	function build_img_migration_button() {
		?>
        <div class="vpcos-img-migration">
            <button id="
" class="button button-secondary button-large"><?php _e('Migrazione immagini', 'NOOO'); ?></button>
            <small style="display:none">Controlla la console per il debugging e attendi che lo script finisca prima di chiudere la pagina.</small>
        </div>
		<?php
	}

	function build_wordshunter_markup() {
		?>
        <div class="vpcos-toggle-wh">
            <button id="vpcos-show-wordshunt" class="button button-secondary button-large"><?php _e('Mostra Wordshunter', 'NOOO'); ?></button>
            <button id="vpcos-hide-wordshunt" class="button button-secondary button-large"><?php _e('Nascondi Wordshunter', 'NOOO'); ?></button>
            <button id="vpcos-reset-defaults-wordshunt" class="button button-primary button-large"><?php _e('Resetta materiali di default', 'NOOO'); ?></button>
        </div>
		<?php
	}
}

if ( class_exists('VPC_on_steroids') ) {
	$VPC_on_steroids = new VPC_on_steroids('vpcos');
}

// Activation
register_activation_hook( __FILE__, array( $VPC_on_steroids, 'activate' ) );
// Dectivation
register_activation_hook( __FILE__, array( $VPC_on_steroids, 'dectivate' ) );
// Uninstall
