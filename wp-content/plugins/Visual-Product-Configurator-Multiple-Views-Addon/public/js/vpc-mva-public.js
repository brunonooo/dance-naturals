(function( $ ) {
    'use strict';
    $(document).ready(function () {

        wp.hooks.addFilter('vpc.default_preview_builder_process', function () {
            if(vpc.views)
                return false;
            else
                return true;
        });

        wp.hooks.addAction('vpc.default_preview_builder_process', function (items_selected) {
            if(typeof active_views!="undefined")
                get_finish_image_by_view(items_selected);
        });
        wp.hooks.addAction('vpc.ajax_loading_complete', function (){
            var items = vpc.vpc_selected_items_selector;
            if(typeof active_views!="undefined"){
                create_vpc_preview_zone();
                get_finish_image_by_view(items);
            }
            vpc_mva_get_preview_height();
        });

        if(typeof active_views!="undefined")
            create_vpc_preview_zone();

        function create_vpc_preview_zone(){
            var activeViews=JSON.parse(active_views);
            var preview_html="";

            $.each(activeViews, function (index, value) {
                var view_id=value;
                view_id.replace(" ", "");
                preview_html+='<li class="vpc-preview" id="preview_'+view_id+'" data-view="'+index+'" data-view-name="'+view_id+'"><div id="vpc-preview'+index+'" ></div></li>';
            });
            $('.bxslider').html(preview_html);
            window.mvaSlider = $('.bxslider').bxSlider({
                infiniteLoop: true,
                adaptiveHeight:true,
                pagerCustom: '#mva-bx-pager'
            });
        }
        $('a.pager-prev').click(function () {
            var current = slider.getCurrentSlide();
            slider.goToPrevSlide(current) - 1;
        });
        $('a.pager-next').click(function () {
            var current = slider.getCurrentSlide();
            slider.goToNextSlide(current) + 1;
        });

        var current_imgs_by_view;
        function get_finish_image_by_view(items) {

            var decoded_active_views = JSON.parse(active_views);
            var imgs_by_view = [];
            var i = 0;

            var recap = $('#vpc-container').find(':input').serializeJSON();

            $.each(decoded_active_views, function (index, value) {
                var items_view_selected = [];
                var id = "#vpc-preview" + i;
                //$(id).html("");
                $('.vpc-preview').not('.bx-clone').find(id).html("");
                $(items).each(function () {
                    if($(this).attr("data-" + value))
                    {
                        //console.log(id);
                        $('.vpc-preview').not('.bx-clone').find(id).append("<img src='" + $(this).attr("data-" + value) + "'>");
                        items_view_selected.push($(this).attr("data-" + value));
                    }
                });
                var items_view = [i, items_view_selected];
                imgs_by_view.push(items_view);
                i++;
            });
            current_imgs_by_view=imgs_by_view;
            var base_price = 0;
            if ($("#vpc-add-to-cart").length)
                base_price = $("#vpc-add-to-cart").data("price");

            if (vpc.decimal_separator = ',')
                var price = parseFloat(base_price.toString().replace(',', '.'));
            else
                var price = parseFloat(base_price);
            if (!price)
                price = 0;

            $(vpc.vpc_selected_items_selector).each(function ()
            {
                var option_price = $(this).data("price");
                if (option_price)
                    price += parseFloat(option_price);
            });
            price = wp.hooks.applyFilters('vpc.total_price', price);
            $("#vpc-price").html(accounting.formatMoney(price));
        }

        wp.hooks.addAction('vpc.option_change',function(elt,e){
            vpc_mva_get_preview_height();
        } );

        $(window).load(function(){
            //vpc_mva_view_name();
            vpc_mva_get_preview_height();
        });
        function vpc_mva_get_preview_height(){
            setTimeout(function () {
                var maxHeight = Math.max.apply(null, $(".bx-viewport .vpc-preview ").map(function ()
                {
                    return $(this).height();
                }).get());
                $('.bx-viewport').css({height: maxHeight});

            }, 200);
        }
        function vpc_mva_view_name(){
            $('.bx-pager a.bx-pager-link').each(function(){
                var index = $(this).data("slide-index");
                var viewName = $("li.vpc-preview[data-view='" + index + "']").data("view-name");
                $(this).text(viewName);
            });
        }
    });

})( jQuery );
