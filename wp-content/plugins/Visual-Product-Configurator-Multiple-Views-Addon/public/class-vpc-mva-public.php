<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/public
 * @author     orion <help@orionorigin.com>
 */
class Vpc_Mva_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	public $views;
	public $active_views;
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->views = get_option('vpc-mva-views');
		$this->active_views = array();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vpc_Mva_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vpc_Mva_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/vpc-mva-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'vpc-mva-bxslider-css', plugin_dir_url( __FILE__ ) . 'css/jquery.bxslider.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vpc_Mva_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vpc_Mva_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		/*wp_register_script( $this->plugin_name, get_template_directory_uri() . '/visual-product-configurator-extension/js/nooo-mva-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name );*/

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/vpc-mva-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'vpc-mva-bxslider.mini', plugin_dir_url( __FILE__ ) . 'js/jquery.bxslider.js', array( 'jquery' ), $this->version, false );

	}

	function get_vpc_preview_html($preview_html,$prod_id,$config_id){
		$config=$this->get_vpc_mva_config_data($prod_id);
		if(isset($config['multi-views']) && $config['multi-views']=="Yes"){
			$preview_html="<ul class='bxslider'>";
			foreach ($this->active_views as $key => $view) {
				$view_id = str_replace(' ', '', $view);
				$preview_html.='<li class="vpc-preview" id="preview_'.$view_id.'" data-view="'.$key.'" data-view-name="'.$view_id.'"><div id="vpc-preview'.$key.'" ></div></li>';
			}
			$preview_html.="</ul>";
			$preview_html.='<div id="mva-bx-pager" class="bx-wrapper">
                        <div class="bx-pager">';
			foreach ($this->active_views as $key => $view) {
				$view_name=$this->get_view_name($view);
				$view_name=apply_filters('vpc-mva-view-name',$view_name,$key,$view);
				$preview_html.=' <div class="bx-pager-item"><a data-slide-index="'.$key.'" class="bx-pager-link" >'.$view_name.'</a></div>';
			}
			$preview_html.='</ul>
                    </div>';
		}
		return $preview_html;
	}

	function get_vpc_customs_datas($customs_datas,$option,$component){
		$customs_datas=$this->get_active_views($option);
		return $customs_datas;
	}

	private function get_active_views($option) {
		//$i = 1;
		$all_imgs = "";
		foreach ($this->views as $key=>$view) {
			$sanitized_view_name = sanitize_title($view);
			if(isset($option['view_options'])){
				$views_options=$option['view_options'];
				foreach($views_options as $views_option){
					if($views_option['view']==$key){
						if (!in_array($sanitized_view_name, $this->active_views)) {
							$this->active_views[] = $sanitized_view_name;
						}
						if(isset($views_option['image'])){
							$conf_img = parse_img_value($views_option['image']);
							$o_image = $conf_img['img_tag'];

							$all_imgs.=" data-$sanitized_view_name='$o_image'";
						}
					}
				}
			}

		}
		?>
        <script>
          var active_views = '<?php echo json_encode($this->active_views); ?>';
        </script>
		<?php
		return $all_imgs;
	}

	private function get_view_name($sanitized_view_name){
		foreach ($this->views as $key=>$view) {
			if(sanitize_title($view)==$sanitized_view_name)
				return $view;
		}
	}

	function add_vpc_data($datas){
		$config=$this->get_vpc_mva_config_data($datas["product"]);
		if(isset($config['multi-views']) && $config['multi-views']=="Yes")
			$datas['views']=true;
		else
			$datas['views']=false;
		return $datas;
	}



	private function get_vpc_mva_config_data($prod_id){
		$ids=get_product_root_and_variations_ids($prod_id);
		$config_meta = get_post_meta($ids['product-id'], "vpc-config", true);
		$configs = get_proper_value($config_meta, $prod_id, array());
		$config_id = get_proper_value($configs, "config-id", false);
		$config=get_post_meta($config_id,'vpc-config',true);
		return $config;
	}

	function get_vpc_mva_config_image($output, $recap, $config, $item){
		$output="";
		$all_views=get_option('vpc-mva-views');
		$array_keys=array_keys($all_views);
		$field=$array_keys[0];
		if (is_array($recap)) {
			foreach ($recap as $component => $raw_options) {
				if (is_array($raw_options)) {
					foreach ($raw_options as $options)
						$image = $this->vpc_mva_extract_option_field_from_config($options, $component, $config, $field);

				}
				else{
					$image = $this->vpc_mva_extract_option_field_from_config($raw_options, $component, $config, $field);
				}

				$img_src = o_get_proper_image_url($image);
				$title = $raw_options;
				if (is_array($raw_options))
					$title = implode(", ", $raw_options);
				if ($img_src) {
					$img_code = "<img src='$img_src' data-tooltip-title='$title'>";
					$output.=$img_code;
				}
			}
		}
		return $output;
	}

	private function vpc_mva_extract_option_field_from_config($searched_option, $searched_component, $config, $field) {
		$unslashed_searched_option = stripslashes($searched_option);
		$unslashed_searched_component = stripslashes($searched_component);
		if(!is_array($config))
			$config=  unserialize ($config);
		foreach ($config["components"] as $i => $component) {
			if (stripslashes($component["cname"]) == $unslashed_searched_component) {
				foreach ($component["options"] as $component_option) {
					if (stripslashes($component_option["name"]) == $unslashed_searched_option) {
						if(isset($component_option['view_options'])){
							$views=$component_option['view_options'];
							foreach($views as $view ){
								if($view["view"]==$field)
									return $view["image"];
							}
						}
					}
				}
			}
		}
		return false;
	}

}
