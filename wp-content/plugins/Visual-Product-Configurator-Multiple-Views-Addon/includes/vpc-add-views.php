<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function woocommerce_vpc_mva_add_views() {
        if (isset( $_GET['error'] )) {
		echo $_GET['error'];
	} 
	// Action to perform: add, edit, delete or none
	$action = '';
	if ( ! empty( $_POST['add_new_view'] ) ) {
		$action = 'add';
	} elseif ( ! empty( $_POST['save_view'] ) && ! empty( $_GET['edit'] ) ) {
		$action = 'edit';
	} elseif ( ! empty( $_GET['delete'] ) ) {
		$action = 'delete';
	}
	// Add or edit an attribute
	if ( 'add' === $action || 'edit' === $action ) {
		// Security check
		if ( 'add' === $action ) {
			check_admin_referer( 'woocommerce-add-new_view' );
		}
		if ( 'edit' === $action ) {
			$view_key = absint( $_GET['edit'] );
			check_admin_referer( 'woocommerce-save-view_'.$view_key );
		}
		// Grab the submitted data
		$view_label   = ( isset( $_POST['view_label'] ) )   ? (string) stripslashes( $_POST['view_label'] ) : '';
                if($view_label)
                {
                    if ('add' === $action ) {   
                        $views=get_option('vpc-mva-views');
                        if(empty($views))
                        {
                            $i=1;
                            $views[$i]=$view_label;
                        }
                        else
                        {
                            if(in_array($view_label, $views))
                                $error='<div class=error>This view exist !</div>';
                            else
                                $views[]=$view_label;
                        }            
                        update_option('vpc-mva-views',$views);
                        $action_completed = true;
                    }
                    // Edit existing attribute
                    if ( 'edit' === $action ) {
                            $views=get_option('vpc-mva-views');
                            $edit=$_GET['edit'];
                            $views[$edit]=$view_label;
                            update_option('vpc-mva-views',$views);
                            $action_completed = true;
                    }
    //                flush_rewrite_rules();
                }
                else
                {
                    $error='<div class=error>Missing view name.</div>';
                    $action_completed = true;
                }
         }

	// Delete an attribute
	if ( 'delete' === $action ) {
		// Security check
		$view_id = absint( $_GET['delete'] );
                $views=get_option('vpc-mva-views');
                unset($views[$view_id]);
                update_option('vpc-mva-views',$views);
	}

	// If an attribute was added, edited or deleted: clear cache and redirect
	if ( ! empty( $action_completed ) ) {
		//delete_transient( 'wc_attribute_taxonomies' );
                if(!empty($error))
                    wp_safe_redirect( get_admin_url().'admin.php?page=vpc-mva-add-views&error='.urlencode($error));
                else{
                    wp_safe_redirect( get_admin_url().'admin.php?page=vpc-mva-add-views');
                }
		exit;
	}
	// Show 
        // admin interface
	if (!empty($_GET['edit']))
		woocommerce_edit_view();
	else
		woocommerce_add_view();
}

function woocommerce_edit_view() {
    $edit  = absint( $_GET['edit'] );
    $views = get_option('vpc-mva-views');
    $view_label=$views[$edit];
?>
	<div class="wrap woocommerce">
		<div class="icon32 icon32-attributes" id="icon-woocommerce"><br/></div>
                <h2><?php _e( 'Edit View', 'vpc-ha' ) ?></h2>
		<form action="admin.php?page=vpc-mva-add-views&amp;edit=<?php echo absint( $edit ); ?>&amp;noheader=true" method="post">
			<table class="form-table">
				<tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row" valign="top">
                                                <label for="view_label"><?php _e( 'Name', 'vpc-mva' ); ?></label>
                                        </th>
                                        <td>
                                                <input name="view_label" id="view_label" type="text" value="<?php echo esc_attr($view_label); ?>" />
                                                <p class="description"><?php _e( 'Name for the attribute (shown on the front-end).', 'vpc-mva' ); ?></p>
                                        </td>
                                        <?php do_action('vpc-mva-edit-fields',"");?>
                                    </tr>
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="save_view" id="submit" class="button-primary" value="<?php _e( 'Update', 'vpc-mva' ); ?>"></p>
			<?php wp_nonce_field( 'woocommerce-save-view_'.$edit ); ?>
		</form>
	</div>
	<?php
}

 function woocommerce_add_view() {
	global $woocommerce;
	?>
	<div class="wrap woocommerce">
            <div class="icon32 icon32-attributes" id="icon-woocommerce"><br/></div>
	    <h2><?php _e( 'Add Views', 'vpc-mva' ) ?></h2>
	    <br class="clear" />
	    <div id="col-container">
	    	<div id="col-right">
	    		<div class="col-wrap">
		    		<table class="widefat fixed" style="width:100%">
				        <thead>
				            <tr>
				                <th scope="col"><?php _e( 'Name', 'vpc-mva' ) ?></th>
                                                 <?php do_action('vpc-mva-field-title',""); ?>
				            </tr>
				        </thead>
				        <tbody>
				        	<?php
                                                    $views=get_option('vpc-mva-views');
                                                    if($views) :
                                                            foreach ($views as $key=>$view) :   
                                                                    ?><tr>
                                                                        <td>
                                                                            <a href="<?php echo esc_url( add_query_arg('edit', $key, 'admin.php?page=vpc-mva-add-views') ); ?>"><?php echo esc_html( $view); ?></a>
                                                                            <div class="row-actions"><span class="edit"><a href="<?php echo esc_url( add_query_arg('edit', $key, 'admin.php?page=vpc-mva-add-views') ); ?>"><?php _e( 'Edit', 'vpc-ha' ); ?></a> | </span><span class="delete"><a class="delete" href="<?php echo esc_url( wp_nonce_url( add_query_arg('delete', $key, 'admin.php?page=vpc-mva-add-views'), 'woocommerce-delete-attribute_' . $key ) ); ?>"><?php _e( 'Delete', 'vpc-ha' ); ?></a></span></div>
                                                                        </td>
                                                                         <?php do_action('vpc-mva-field-container',$key,$view); ?>
                                                                      </tr><?php
                                                            endforeach;
                                                    else :
				        			?><tr><td><?php _e( 'No views currently exist.', 'vpc-mva' ) ?></td></tr><?php
                                                    endif;
				        	?>
				        </tbody>
                                </table>
	    		</div>
	    	</div>
	    	<div id="col-left">
                    <div class="col-wrap">
                        <div class="form-wrap">
                            <h3><?php _e( 'Add New View', 'vpc-mva' ) ?></h3>
                            <form action="admin.php?page=vpc-mva-add-views&amp;noheader=true" method="post">
                                <div class="form-field">
                                        <label for="view_label"><?php _e( 'Name', 'vpc-mva' ); ?></label>
                                        <input name="view_label" id="view_label" type="text" value="" />
                                        <p class="description"><?php _e( 'Name for the view (shown on the front-end).', 'vpc-mva' ); ?></p>
                                </div>
                                <?php do_action('vpc-mva-add-fields',""); ?>
                                <p class="submit"><input type="submit" name="add_new_view" id="submit" class="button" value="<?php _e( 'Add View', 'vpc-mva' ); ?>"></p>
                                <?php wp_nonce_field( 'woocommerce-add-new_view' ); ?>
                            </form>
                        </div>
	    		</div>
	    	</div>
	    </div>
	    <script type="text/javascript">
			jQuery('a.delete').click(function(){
	    		var answer = confirm ("<?php _e( 'Are you sure you want to delete this view?', 'vpc-mva' ); ?>");
				if (answer) return true;
				return false;
	    	});
            </script>
	</div>
	<?php
}