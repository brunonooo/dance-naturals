<?php

/**
 * Fired during plugin deactivation
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/includes
 * @author     orion <help@orionorigin.com>
 */
class Vpc_Mva_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
