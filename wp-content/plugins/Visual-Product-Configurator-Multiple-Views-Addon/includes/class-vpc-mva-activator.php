<?php

/**
 * Fired during plugin activation
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/includes
 * @author     orion <help@orionorigin.com>
 */
class Vpc_Mva_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
