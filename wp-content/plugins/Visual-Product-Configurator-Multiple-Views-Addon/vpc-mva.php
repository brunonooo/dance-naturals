<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.woocommerceproductconfigurator.com
 * @since             1.0.0
 * @package           Vpc_Mva
 *
 * @wordpress-plugin
 * Plugin Name:       Visual Product Configurator Multiple Views Addon
 * Plugin URI:        https://www.woocommerceproductconfigurator.com/demo/multi-views-configuration/
 * Description:       Visual Product Configurator Multiple Views Addon allows to you to have multiple views of your configurator on a slide.
 * Version:           2.2
 * Author:            orion
 * Author URI:        https://www.woocommerceproductconfigurator.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       vpc-mva
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
define( 'VPC_MVA_URL', plugins_url('/', __FILE__) );
define( 'VPC_MVA_DIR', dirname(__FILE__) );
define('VPC_MVA_FILE', 'vpc-mutliple-views-addon/vpc.php' );
define('VPC_MVA_VERSION', '2.2' );
define('ORION_MVA_ADDON_NAME', 'Visual Products Configurator Multiviews Add-on' );
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-vpc-mva-activator.php
 */
function activate_vpc_mva() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-vpc-mva-activator.php';
	Vpc_Mva_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-vpc-mva-deactivator.php
 */
function deactivate_vpc_mva() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-vpc-mva-deactivator.php';
	Vpc_Mva_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_vpc_mva' );
register_deactivation_hook( __FILE__, 'deactivate_vpc_mva' );



/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-vpc-mva.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_vpc_mva() {

	$plugin = new Vpc_Mva();
	$plugin->run();

}
run_vpc_mva();
