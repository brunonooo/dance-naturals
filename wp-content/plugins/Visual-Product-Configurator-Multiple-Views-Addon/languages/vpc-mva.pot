#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: vpc\n"
"POT-Creation-Date: 2018-04-25 18:44+0100\n"
"PO-Revision-Date: 2016-11-07 11:31+0100\n"
"Last-Translator: ORION TEAM <support@orionorigin.com>\n"
"Language-Team: ORION <support@orionorigin.com>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.5\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../admin/class-vpc-mva-admin.php:104 ../admin/class-vpc-mva-admin.php:140
msgid "Views"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:116
msgid "View"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:123
msgid "Image"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:133
msgid "ID"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:145
msgid "views"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:147
msgid "Add view"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:160
msgid "Activate Multi views"
msgstr ""

#: ../admin/class-vpc-mva-admin.php:166
msgid "Activate multi views or not."
msgstr ""

#: ../includes/vpc-add-views.php:100
msgid "Edit View"
msgstr ""

#: ../includes/vpc-add-views.php:106 ../includes/vpc-add-views.php:136
#: ../includes/vpc-add-views.php:167
msgid "Name"
msgstr ""

#: ../includes/vpc-add-views.php:110
msgid "Name for the attribute (shown on the front-end)."
msgstr ""

#: ../includes/vpc-add-views.php:116
msgid "Update"
msgstr ""

#: ../includes/vpc-add-views.php:128
msgid "Add Views"
msgstr ""

#: ../includes/vpc-add-views.php:148
msgid "Edit"
msgstr ""

#: ../includes/vpc-add-views.php:148
msgid "Delete"
msgstr ""

#: ../includes/vpc-add-views.php:154
msgid "No views currently exist."
msgstr ""

#: ../includes/vpc-add-views.php:164
msgid "Add New View"
msgstr ""

#: ../includes/vpc-add-views.php:169
msgid "Name for the view (shown on the front-end)."
msgstr ""

#: ../includes/vpc-add-views.php:172
msgid "Add View"
msgstr ""

#: ../includes/vpc-add-views.php:181
msgid "Are you sure you want to delete this view?"
msgstr ""
