(function ($) {
	'use strict';
	$(document).ready(function () {
		$(document).on('click','.o-modal .table-fixed-layout tbody tr td .add-rf-row',function () {
			    var par = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().index();
			    var child = $(this).parent().parent().index();
			    var replacement = "vpc-config[components][" + par + "][options][" + child + "]";
			    $(this).prev().children().each(function (index,a) {
			      if (index === 1) {
							setTimeout(function () {
								$($(a).find("tr")).find(":input[name*='[options]']").each(function (i2, e2)
								{
									var new_name = this.name.replace(/vpc-config\[components\]\[\d+\]\[options\]\[\d+\]/, replacement);
									$(this).attr("name", new_name);
								});
							},1000);
			      }
			    })
		});
	});

})(jQuery);
