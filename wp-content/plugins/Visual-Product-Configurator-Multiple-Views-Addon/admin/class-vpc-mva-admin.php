<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       orionorigin.com
 * @since      1.0.0
 *
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Vpc_Mva
 * @subpackage Vpc_Mva/admin
 * @author     orion <help@orionorigin.com>
 */
class Vpc_Mva_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vpc_Mva_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vpc_Mva_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/vpc-mva-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vpc_Mva_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vpc_Mva_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/vpc-mva-admin.js', array( 'jquery' ), $this->version, false );

	}
        function add_vpc_mva_submenu() {
            $parent_slug = "edit.php?post_type=vpc-config";
            add_submenu_page($parent_slug, __('Views', 'vpc-mva'), __('Views', 'vpc-mva'), 'manage_product_terms', 'vpc-mva-add-views', array($this, 'vpc_mva_add_views'));
        }

        function vpc_mva_add_views() {
            include_once( VPC_MVA_DIR . '/includes/vpc-add-views.php' );
            woocommerce_vpc_mva_add_views();
        }
        function vpc_mva_components_options_fields($options_fields) {
            $views = get_option('vpc-mva-views');
            if(is_array($views)){

                $view_field=array(
                    'title'=>__('View', 'vpc-mva'),
                    'type' => 'select',
                    'name' => 'view',
                    'options' => $views,
                );

                $img_file=array(
                        'title' => __('Image', 'vpc-mva'),
                        'name' =>'image',
                        'url_name' => "image_url",
                        'type' => 'image',
                        'set' => 'Set',
                        'remove' => 'Remove',
        //                'lazyload'=>true,
                    );

                $view_field_id=array(
                    'title' => __('ID', 'vpc-mva'),
                    'name' => 'view_id',
                    'type' => 'text',
    //                'custom_attributes' => array('disabled' => 'disabled')
                );

                $options_fields['fields'][]=array(
                    'title' => __('Views', 'vpc-mva'),
                    'name' => 'view_options',
                    'type' => 'repeatable-fields',
                    'class' => 'striped views',
                    'fields' => array( $view_field_id,$view_field,$img_file),
                    'desc' => __('views', 'vpc-mva'),
                    'row_class'=>'vpc-option-row',
                    'add_btn_label'=> __("Add view", "vpc-mva")
                );
            }
            return $options_fields;
        }

        function add_multiview_settings($skin_settings){
            $skin_settings[] = array(
                        'type' => 'sectionbegin',
                        'id' => 'vpc-multi-container',
//                        'table' => 'options',
                    );
            $skin_settings[] = array(
                        'title' => __('Activate Multi views', 'vpc-mva'),
                        'name' => 'vpc-config[multi-views]',
                        'type' => 'radio',
                        'options' => array("Yes" => "Yes", "No" => "No"),
                        'default' => 'No',
                        'class' => 'chosen_select_nostd view_option',
                        'desc' => __('Activate multi views or not.', 'vpc'),
                    );
            $skin_settings[] =array('type' => 'sectionend');
            return $skin_settings;
        }
}
