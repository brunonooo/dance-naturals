<?php

defined( 'ABSPATH' ) || exit;

$context            = Timber::get_context();
$queried_object = get_queried_object();
$pt = $queried_object->name;
$context['post'] = new TimberPost(get_editing_page_id($pt, 'archive'));
$context['posts'] = Timber::get_posts(array(
    'post_type' => $pt,
    'posts_per_page' => -1,
    'orderby'   => 'menu_order',
    'order'     => 'ASC'

));

Timber::render( array( 'templates/archive-'.$pt.'.twig' ), $context );
