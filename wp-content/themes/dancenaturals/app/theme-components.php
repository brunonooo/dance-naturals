<?php

/**
 * CUSTOM POST TYPES
 **/
if(!function_exists('nooo_post_types')){
	function nooo_post_types() {
// ---------------------- Businesses
		$labelsTestimonials = array(
			'name'               => __('Testimonials', THEME_CONTEXT),
			'singular_name'      => __('Testimonial', THEME_CONTEXT),
			'menu_name'          => __('Testimonials', THEME_CONTEXT),
			'name_admin_bar'     => __('Testimonials', THEME_CONTEXT),
		);

		register_post_type( 'testimonial', array(
			'public'             => true,
			'labels'             => $labelsTestimonials,
			'show_ui'            => true,
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => 5,
			'menu_icon'          =>   'dashicons-star-filled',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'revisions'),
		));

	}
}

add_action( 'init', 'nooo_post_types' );