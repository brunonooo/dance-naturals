<?php

if(!defined('THEME_NAME'))define('THEME_NAME', 'Dance Naturals');
if(!defined('THEME_CONTEXT'))define('THEME_CONTEXT', 'dancenaturals');
if(!defined('THEME_SLUG'))define('THEME_SLUG', 'dancenaturals');

/**
 *  DISABLING WORDPRESS XML-RPC for security reasons
 **/
add_filter('xmlrpc_enabled', '__return_false');

/**
 * DISABLING META GENERATOR
 **/
remove_action('wp_head', 'wp_generator');

/**
 * DISABLING QUERY STRINGS FROM ASSETS
 **/
if (!function_exists('remove_query_string_from_assets')){
	function remove_query_string_from_assets ( $url ) {
		return remove_query_arg( 'ver', $url );
	}
}

add_filter( 'style_loader_src', 'remove_query_string_from_assets' );

/**
 * SUPPORT FOR TITLE AND METAS
 **/
if (!function_exists('mytheme_supports')) {
	function mytheme_supports() {
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails', array( 'post', 'product', 'testimonial' ) );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-zoom' );
	}
}
add_action( 'after_setup_theme', 'mytheme_supports' );

/**
 * SUPPORT FOR EXCERPT FOR PAGES
 **/
add_post_type_support( 'page', 'excerpt');

/**
 * LAZY LOAD PLACEHOLDER
 */
if(!defined('PLACEHOLDER_SIZE'))define( 'PLACEHOLDER_SIZE' ,'ll-placeholder');
add_image_size( PLACEHOLDER_SIZE, 50, false );

/**
 * CUSOM IMG SIZES
 */
add_image_size( THEME_SLUG.'_thumb', 150, false );
add_image_size( THEME_SLUG.'_medium', 300, false );
add_image_size( THEME_SLUG.'_medium_large', 768, false );
add_image_size( THEME_SLUG.'_large', 1024, false );
add_image_size( THEME_SLUG.'_xlarge', 1440, false );

/**
 * SET MAX SRCSET IMG WIDTH AND REMOVE FULLSIZE
 */
if(!defined('IMG_MAX_SIZE'))define('IMG_MAX_SIZE', 2048 );
add_image_size( 'max-size', IMG_MAX_SIZE );

add_filter( 'wp_calculate_image_srcset', 'dq_add_custom_image_srcset', 10, 5 );

if (!function_exists('dq_add_custom_image_srcset')) {
	function dq_add_custom_image_srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ) {

		// image base name
		$image_basename = wp_basename( $image_meta['file'] );
		// upload directory info array
		$upload_dir_info_arr = wp_get_upload_dir();
		// base url of upload directory
		$baseurl = $upload_dir_info_arr['baseurl'];

		// Uploads are (or have been) in year/month sub-directories.
		if ( $image_basename !== $image_meta['file'] ) {
			$dirname = dirname( $image_meta['file'] );

			if ( $dirname !== '.' ) {
				$image_baseurl = trailingslashit( $baseurl ) . $dirname;
			}
		}

		$image_baseurl = trailingslashit( $image_baseurl );

		foreach ( $sources as $sourceKey => $sourceValue ) {
			$key = intval( $sourceKey );

			if ( $key > IMG_MAX_SIZE ) {
				unset( $sources[ $sourceKey ] );
			}
		}

		//return sources with new srcset value
		return $sources;
	}
}