<?php

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
	wp_enqueue_style('dancenaturals-sage/vendor.css', asset_path('styles/vendor.css', __dir__), array('sage/vendor.css'), null);
	wp_enqueue_style('dancenaturals-sage/main.css', asset_path('styles/main.css', __dir__), array('sage/main.css'), null);
	wp_enqueue_script('dancenaturals-sage/main.js', asset_path('scripts/main.js', __dir__), array('sage/main.js'), null, true);
}, PHP_INT_MAX);
