<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$context            = Timber::get_context();

if ( is_product_category() ) {
	$queried_object = get_queried_object();
	$term_id = $queried_object->term_id;
	$context['category'] = get_term( $term_id, 'product_cat' );
	$context['title'] = single_term_title( '', false );
}

$shopPage = get_option( 'woocommerce_shop_page_id' );
$context['post'] = new TimberPost($shopPage);

$context['filters_sidebar'] = Timber::get_widgets( 'shop-sidebar' );
$context['categories'] = get_terms(array(
	'taxonomy' => 'product_cat',
	'slug' => array(__('donna', THEME_CONTEXT.'_slugs'), __('uomo', THEME_CONTEXT.'_slugs'))
));

$configs = array();

$context['posts_per_category'] = get_configurable_products_per_category($context['categories'], __('crea-la-tua-scarpa', THEME_SLUG.'_slugs'));

// REMOVING UNNECESSARY DATA FROM PAGE
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

/**
 * REMOVING PRICE FROM CARDs
 */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

Timber::render( array( 'templates/woo/archive-product.twig' ), $context );
