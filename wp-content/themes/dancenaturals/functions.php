<?php

function child_add_to_context($data){
	$data['parent_link'] = get_template_directory_uri();
	$data['asset_dir']  = __dir__.'/assets';
	return $data;
}

add_filter('timber/context', 'child_add_to_context');

require_once 'app/theme-config.php';
require_once 'app/resources.php';
require_once 'app/theme-components.php';
