'use strict';
/* eslint-disable */
export default {
    init() {

        let slider_global_options  ={
            interval : 3000,
        };
        // The Slide (Product) class.
        class Slide {
            constructor(el) {
                this.DOM = {el: el};
                // The slide´s container.
                this.DOM.wrap = this.DOM.el.querySelector('.slide__wrap');
                // The img container.
                this.DOM.imgWrap = this.DOM.el.querySelector('.img-wrapper');
                // The image element.
                this.DOM.img = this.DOM.wrap.querySelector('.slide__img');
                // The title container.
                this.DOM.titleWrap = this.DOM.wrap.querySelector('.slide__title-wrap');
                // Some config values.
                this.config = {
                    animation: {
                        duration: 1.2,
                        ease: 'Expo.easeInOut',
                    },
                };
            }
            // Sets the current class.
            setCurrent(isCurrent = true) {
                this.DOM.el.classList[isCurrent ? 'add' : 'remove']('slide--current');
            }
            setAnimating(isCurrent = true) {
                this.DOM.el.classList[isCurrent ? 'add' : 'remove']('slide--animating');
            }
            // Hide the slide.
            hide(direction) {
                return this.toggle('hide', direction);
            }
            // Show the slide.
            show(direction) {
                this.DOM.el.style.zIndex = 3;
                return this.toggle('show', direction);
            }

            zoomAction(){
                TweenLite.fromTo(this.DOM.imgWrap, ((slider_global_options.interval+500)/1000), {'scale' : 1, ease: 'Power0.easeNone'}, {'scale' : 1.15, ease: 'Power0.easeNone'});
            }
            // Show/Hide the slide.
            toggle(action, direction) {
                return new Promise((resolve) => {
                    // When showing a slide, the slide´s container will move 100% from the right or left depending on the direction.
                    // At the same time, both title wrap and the image will move the other way around thus creating the unreveal effect.
                    // Also, when showing or hiding a slide, we scale it from or to a value of 1.1.
                    if ( action === 'show' ) {
                        TweenLite.to(this.DOM.wrap, this.config.animation.duration, {
                            ease: this.config.animation.ease,
                            startAt: {x: direction === 'right' ? '100%' : '-100%'},
                            x: '0%',
                        });
                        TweenLite.to(this.DOM.titleWrap, this.config.animation.duration, {
                            ease: this.config.animation.ease,
                            startAt: {x: direction === 'right' ? '-100%' : '100%'},
                            x: '0%',
                        });
                    }

                    TweenLite.to(this.DOM.img, this.config.animation.duration, {
                        ease: this.config.animation.ease,
                        startAt: action === 'hide' ? {} : {x: direction === 'right' ? '-100%' : '100%', scale: 1},
                        x: '0%',
                        scale: action === 'hide' ? 1.1 : 1,
                        onStart: () => {
                            this.DOM.img.style.transformOrigin = action === 'hide' ?
                                direction === 'right' ? '100% 50%' : '0% 50%':
                                direction === 'right' ? '0% 50%' : '100% 50%';
                            this.DOM.el.style.opacity = 1;
                        },
                        onComplete: () => {
                            this.DOM.el.style.zIndex = 2;
                            this.DOM.el.style.opacity = action === 'hide' ? 0 : 1;
                            resolve();
                        },
                    });
                });
            }
        }

        // The Slideshow class.
        class Slideshow {
            constructor(el) {
                this.DOM = {el: el};
                // The slides.
                this.slides = [];
                // Initialize/Create the slides instances.
                Array.from(this.DOM.el.querySelectorAll('.slide')).forEach((slideEl) => this.slides.push(new Slide(slideEl)));
                // The total number of slides.
                this.slidesTotal = this.slides.length;
                // At least 2 slides to continue...
                if ( this.slidesTotal < 2 ) {
                    return false;
                }
                // Current slide position.
                this.current = 0;
                // Initialize the slideshow.
                this.init();
            }
            // Set the current slide and initialize some events.
            init() {
                this.slides[this.current].setCurrent();
                window.setInterval(() => {this.navigate('right')}, slider_global_options.interval);
            }
            // Navigate the slideshow.
            navigate(direction) {
                // If animating return.
                if ( this.isAnimating ) return;
                this.isAnimating = true;

                // The next/prev slide´s position.
                const nextSlidePos = direction === 'right' ?
                    this.current < this.slidesTotal-1 ? this.current+1 : 0 :
                    this.current > 0 ? this.current-1 : this.slidesTotal-1;

                this.slides[nextSlidePos].zoomAction();
                this.slides[nextSlidePos].setAnimating();


                Promise.all([ this.slides[this.current].hide(direction), this.slides[nextSlidePos].show(direction)])
                    .then(() => {
                        // Update current.
                        this.slides[this.current].setCurrent(false);
                        this.slides[this.current].setAnimating(false);
                        this.current = nextSlidePos;
                        this.slides[this.current].setCurrent();
                        this.isAnimating = false;
                    })
            }
        }

        // Initialize the slideshow
        new Slideshow(document.querySelector('.slideshow'));
    },
    finalize() {
    },
};
/* eslint-enable */
