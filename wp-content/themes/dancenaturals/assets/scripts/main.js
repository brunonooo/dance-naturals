/** import local dependencies */
import Router from './util/Router';
import home from './routes/home';

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
    /** Home */
    home,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
