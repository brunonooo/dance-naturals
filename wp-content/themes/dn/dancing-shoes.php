<?php
/**
 * Template Name: Scarpe da ballo
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();

Timber::render('dancing-shoes.twig' , $context );
