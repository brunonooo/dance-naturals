<?php
/*
 * ADDING CLASSES TO QUANTITY INPUT
 */

function custom_classes_woocommerce_cart_item_quantity( $inputDiv, $cart_item_key, $cart_item = null ){
    $input = str_replace('<div class="quantity', '<div class="quantity d-flex align-items-center justify-content-end', $inputDiv);
    return $input;
}

add_filter( 'woocommerce_cart_item_quantity', 'custom_classes_woocommerce_cart_item_quantity', 11, 3 );
/**
 * WOOCOMMERCE SPECIFIC FUNCTIONS AND OVERRIDES
 */
function get_upsells($id){
    $upsells = get_post_meta( get_the_ID(), '_upsell_ids',true);

    if(!empty($upsells)){
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => 4,
            'post__in' => $upsells
        );
        $ups = new WP_Query( $args );
        wp_reset_postdata();

        return $ups->posts;
    } else {
        return false;
    }
}

/*
 * REMOVING DASHBOARD FROM MY ACCOUNT MENU
 */

add_filter ( 'woocommerce_account_menu_items', 'remove_my_account_links' );
function remove_my_account_links( $menu_links ){

    unset( $menu_links['dashboard'] ); // Dashboard

    return $menu_links;
}

if ( ! function_exists( 'custom_woocommerce_form_field' ) ) {

    /**
     * Outputs a checkout/address form field.
     *
     * @param string $key Key.
     * @param mixed  $args Arguments.
     * @param string $value (default: null).
     * @return string
     */
    function custom_woocommerce_form_field( $key, $args, $value = null ) {
        $defaults = array(
            'type'              => 'text',
            'label'             => '',
            'description'       => '',
            'placeholder'       => '',
            'maxlength'         => false,
            'required'          => false,
            'autocomplete'      => false,
            'id'                => $key,
            'class'             => array(),
            'label_class'       => array(),
            'input_class'       => array(),
            'return'            => false,
            'options'           => array(),
            'custom_attributes' => array(),
            'validate'          => array(),
            'default'           => '',
            'autofocus'         => '',
            'priority'          => '',
        );

        $args = wp_parse_args( $args, $defaults );
        $args = apply_filters( 'woocommerce_form_field_args', $args, $key, $value );

        if ( $args['required'] ) {
            $args['class'][] = 'validate-required';
            $required        = '&nbsp;<abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';
        } else {
            $required        = '';
            /*$required = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';*/
        }

        if ( is_string( $args['label_class'] ) ) {
            $args['label_class'] = array( $args['label_class'] );
        }

        if ( is_null( $value ) ) {
            $value = $args['default'];
        }

        // Custom attribute handling.
        $custom_attributes         = array();
        $args['custom_attributes'] = array_filter( (array) $args['custom_attributes'], 'strlen' );

        if ( $args['maxlength'] ) {
            $args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );
        }

        if ( ! empty( $args['autocomplete'] ) ) {
            $args['custom_attributes']['autocomplete'] = $args['autocomplete'];
        }

        if ( true === $args['autofocus'] ) {
            $args['custom_attributes']['autofocus'] = 'autofocus';
        }

        if ( $args['description'] ) {
            $args['custom_attributes']['aria-describedby'] = $args['id'] . '-description';
        }

        if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {
            foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {
                $custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
            }
        }

        if ( ! empty( $args['validate'] ) ) {
            foreach ( $args['validate'] as $validate ) {
                $args['class'][] = 'validate-' . $validate;
            }
        }

        $field           = '';
        $label_id        = $args['id'];
        $sort            = $args['priority'] ? $args['priority'] : '';
        $field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</p>';

        switch ( $args['type'] ) {
            case 'country':
                $countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();

                if ( 1 === count( $countries ) ) {

                    $field .= '<strong>' . current( array_values( $countries ) ) . '</strong>';

                    $field .= '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . current( array_keys( $countries ) ) . '" ' . implode( ' ', $custom_attributes ) . ' class="country_to_state" readonly="readonly" />';

                } else {

                    $field = '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="country_to_state country_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . '><option value="">' . esc_html__( 'Select a country&hellip;', 'woocommerce' ) . '</option>';

                    foreach ( $countries as $ckey => $cvalue ) {
                        $field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
                    }

                    $field .= '</select>';

                    $field .= '<noscript><button type="submit" name="woocommerce_checkout_update_totals" value="' . esc_attr__( 'Update country', 'woocommerce' ) . '">' . esc_html__( 'Update country', 'woocommerce' ) . '</button></noscript>';

                }

                break;
            case 'state':
                /* Get country this state field is representing */
                $for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'billing_state' === $key ? 'billing_country' : 'shipping_country' );
                $states      = WC()->countries->get_states( $for_country );

                if ( is_array( $states ) && empty( $states ) ) {

                    $field_container = '<p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p>';

                    $field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" />';

                } elseif ( ! is_null( $for_country ) && is_array( $states ) ) {

                    $field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
						<option value="">' . esc_html__( 'Select a state&hellip;', 'woocommerce' ) . '</option>';

                    foreach ( $states as $ckey => $cvalue ) {
                        $field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
                    }

                    $field .= '</select>';

                } else {

                    $field .= '<input type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

                }

                break;
            case 'textarea':
                $field .= '<textarea name="' . esc_attr( $key ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';

                break;
            case 'checkbox':
                $field = '<label class="checkbox ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '>
						<input type="' . esc_attr( $args['type'] ) . '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="1" ' . checked( $value, 1, false ) . ' /> ' . $args['label'] . $required . '</label>';

                break;
            case 'text':
            case 'password':
            case 'datetime':
            case 'datetime-local':
            case 'date':
            case 'month':
            case 'time':
            case 'week':
            case 'number':
            case 'email':
            case 'url':
            case 'tel':
                $field .= '<input type="' . esc_attr( $args['type'] ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

                break;
            case 'select':
                $field   = '';
                $options = '';

                if ( ! empty( $args['options'] ) ) {
                    foreach ( $args['options'] as $option_key => $option_text ) {
                        if ( '' === $option_key ) {
                            // If we have a blank option, select2 needs a placeholder.
                            if ( empty( $args['placeholder'] ) ) {
                                $args['placeholder'] = $option_text ? $option_text : __( 'Choose an option', 'woocommerce' );
                            }
                            $custom_attributes[] = 'data-allow_clear="true"';
                        }
                        $options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_attr( $option_text ) . '</option>';
                    }

                    $field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
							' . $options . '
						</select>';
                }

                break;
            case 'radio':
                $label_id = current( array_keys( $args['options'] ) );

                if ( ! empty( $args['options'] ) ) {
                    foreach ( $args['options'] as $option_key => $option_text ) {
                        $field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';
                        $field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio ' . implode( ' ', $args['label_class'] ) . '">' . $option_text . '</label>';
                    }
                }

                break;
        }

        if ( ! empty( $field ) ) {
            $field_html = '';

            if ( $args['label'] && 'checkbox' !== $args['type'] ) {
                $field_html .= '<label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label'] . $required . '</label>';
            }

            $field_html .= '<span class="woocommerce-input-wrapper">' . $field;

            if ( $args['description'] ) {
                $field_html .= '<span class="description" id="' . esc_attr( $args['id'] ) . '-description" aria-hidden="true">' . wp_kses_post( $args['description'] ) . '</span>';
            }

            if ('checkbox' === $args['type']) {
                $field_html .= '<span class="svg-wrapper">'. file_get_contents( get_stylesheet_directory_uri().'/assets/vectors/tick.svg') .'</span>';
            }

            $field_html .= '</span>';

            // ADDING INPUT TYPE TO CLASSES
            $args['class'][] = 'input-type-'.$args['type'];

            $container_class = esc_attr( implode( ' ', $args['class'] ) );
            $container_id    = esc_attr( $args['id'] ) . '_field';
            $field           = sprintf( $field_container, $container_class, $container_id, $field_html );
        }

        /**
         * Filter by type.
         */
        $field = apply_filters( 'woocommerce_form_field_' . $args['type'], $field, $key, $args, $value );

        /**
         * General filter on form fields.
         *
         * @since 3.4.0
         */
        $field = apply_filters( 'woocommerce_form_field', $field, $key, $args, $value );

        if ( $args['return'] ) {
            return $field;
        } else {
            echo $field; // WPCS: XSS ok.
        }
    }
};

if ( ! function_exists( 'wc_custom_dropdown_variation_attribute_options' ) ) {

    /**
     * Output a list of variation attributes for use in the cart forms.
     *
     * @param array $args Arguments.
     * @since 2.4.0
     */
    function wc_custom_dropdown_variation_attribute_options( $args = array() ) {
        $args = wp_parse_args( apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ), array(
            'options'          => false,
            'attribute'        => false,
            'product'          => false,
            'selected'         => false,
            'name'             => '',
            'id'               => '',
            'class'            => '',
            'show_option_none' => __( 'Choose an option', 'woocommerce' ),
        ) );

        // Get selected value.
        if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
            $selected_key     = 'attribute_' . sanitize_title( $args['attribute'] );
            $args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] ); // WPCS: input var ok, CSRF ok, sanitization ok.
        }

        $options               = $args['options'];
        $product               = $args['product'];
        $attribute             = $args['attribute'];
        $name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
        $id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
        $class                 = $args['class'];
        $show_option_none      = (bool) $args['show_option_none'];
        $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

        if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
            $attributes = $product->get_variation_attributes();
            $options    = $attributes[ $attribute ];
        }

        $html  = '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
        $html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

        if ( ! empty( $options ) ) {
            if ( $product && taxonomy_exists( $attribute ) ) {
                // Get terms if this is a taxonomy - ordered. We need the names too.
                $terms = wc_get_product_terms( $product->get_id(), $attribute, array(
                    'fields' => 'all',
                ) );

                foreach ( $terms as $term ) {
                    if ( in_array( $term->slug, $options, true ) ) {
                        $html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</option>';
                    }
                }
            } else {
                foreach ( $options as $option ) {
                    // This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
                    $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
                    $html    .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>';
                }
            }
        }

        $html .= '</select>';

        echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args ); // WPCS: XSS ok.
    }
}

function validate_cart_item_variations($cart_item, $cart_item_variations) {
    $compulsory_variations = $cart_item['data']->get_variation_attributes();
    $current_variations = $cart_item_variations ? array_merge($compulsory_variations, $cart_item_variations) : $compulsory_variations;
    $invalid_variations = array_filter($current_variations, function($value) {
        return empty($value);
    });

    return $invalid_variations;
}

function get_invalid_variations_alert ($invalid_variations = false) {
    $invalid_variations_notice = '<div class="invalid-variations"><h4>'.__('ATTENZIONE! Alcune variazioni non sono state selezionate e non è possibile procedere con l\'ordine:', THEME_CONTEXT).'</h4>';
    if(!empty($invalid_variations)){
        $invalid_variations_notice .= '<ul>';
        foreach ($invalid_variations as $key => $value){
            $attr_slug = str_replace('attribute_', '', $key);
            $attr_name = wc_attribute_label($attr_slug);
            $invalid_variations_notice .= '<li>'.$attr_name.' '.__(' é obbligatorio!', THEME_CONTEXT).'</li>';
        }
        $invalid_variations_notice .= '</ul>';
    }

    $invalid_variations_notice .= '</div>';

    return $invalid_variations_notice;
}



/**
 * CUSTOM GET FORMATTED CART ITEM DATA
 * Gets and formats a list of cart item data + variations for display on the frontend.
 *
 * @since 3.3.0
 * @param array $cart_item Cart item object.
 * @param bool  $flat Should the data be returned flat or in a list.
 * @return string
 */
function custom_get_formatted_cart_item_data( $cart_item, $flat = false ) {
    if ('WC_Product_Simple' == get_class($cart_item['data'])) {
        return;
    }

    if('WC_Product_Variation' == get_class($cart_item)){
        $cart_variations = $cart_item->get_variation_attributes();
    } else {
        $cart_variations = $cart_item['variation'];
    }

    if( $cart_variations == "" && isset($cart_item['vpc-custom-vars'])) {
        $cart_variations = $cart_item['attributes'];
    }

    $invalid_variations = validate_cart_item_variations($cart_item, $cart_variations);

    if (empty($invalid_variations)){
        return get_formatted_data($cart_item, $cart_variations, $flat);
    } else {
        $invalid_variations_alert = get_invalid_variations_alert($invalid_variations);
        $prod_link = '<a class="btn btn--text btn--back text--error mb-5" href="'.$cart_item['data']->get_permalink().'">'.__('Seleziona gli attributi per procedere', THEME_CONTEXT).'</a>';
        return $invalid_variations_alert.$prod_link;
    }

}

function get_formatted_data($item, $variations, $flat = false) {
    $item_data = array();

    if ( isset( $variations ) ) {
        foreach ( $variations as $name => $value ) {
            $taxonomy = wc_attribute_taxonomy_name( str_replace( 'attribute_pa_', '', urldecode( $name ) ) );

            if ( taxonomy_exists( $taxonomy ) ) {
                // If this is a term slug, get the term's nice name.
                $term = get_term_by( 'slug', $value, $taxonomy );
                if ( ! is_wp_error( $term ) && $term && $term->name ) {
                    $value = $term->name;
                }
                $label = wc_attribute_label( $taxonomy );
            } else {
                // If this is a custom option slug, get the options name.
                $value = apply_filters( 'woocommerce_variation_option_name', $value );
                $label = wc_attribute_label( str_replace( 'attribute_', '', $name ), $item );
            }

            $item_data[] = array(
                'key'   => $label,
                'value' => $value,
            );
        }
    }

// Filter item data to allow 3rd parties to add more to the array.
    $item_data = apply_filters( 'woocommerce_get_item_data', $item_data, $item );

// Format item data ready to display.
    foreach ( $item_data as $key => $data ) {
        // Set hidden to true to not display meta on cart.
        if ( ! empty( $data['hidden'] ) ) {
            unset( $item_data[ $key ] );
            continue;
        }
        $item_data[ $key ]['key']     = ! empty( $data['key'] ) ? $data['key'] : $data['name'];
        $item_data[ $key ]['display'] = ! empty( $data['display'] ) ? $data['display'] : $data['value'];
    }

// Output flat or in list format.
    if ( count( $item_data ) > 0 ) {
        ob_start();

        if ( $flat ) {
            foreach ( $item_data as $data ) {
                echo esc_html( $data['key'] ) . ': ' . wp_kses_post( $data['display'] ) . "\n";
            }
        } else {
            wc_get_template( 'cart/cart-item-data.php', array( 'item_data' => $item_data , 'note' => $item['prod_note']) );
        }

        return ob_get_clean();
    }

}

// IF CART ITEM HAS BEEN CONFIGURED ADD CLASSES TO CART
function configurated_product_classes ($classes, $cart_item, $cart_item_key) {
    global $woocommerce;
    $cart_content=$woocommerce->cart->cart_contents;
    $item_content=$cart_content[$cart_item_key];

    if(isset($item_content['visual-product-configuration'])){
        $classes.=' cart_item_configured';
    }
    return $classes;
}

add_filter('woocommerce_cart_item_class', 'configurated_product_classes', 10, 3);

// CLEAN UP NAME FROM VARIATIONS

function clean_name_from_variations ($name, $cart_item, $cart_item_key) {
    $name = $cart_item['data']->get_title();
    return $name;
}

add_filter('woocommerce_cart_item_name', 'clean_name_from_variations', 10, 3);

/*
 * AJAX ADD TO CART
 */
add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');

function woocommerce_ajax_add_to_cart() {

    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
    $variation_id = absint($_POST['variation_id']);
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status = get_post_status($product_id);

    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

        do_action('woocommerce_ajax_added_to_cart', $product_id);

        if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
            wc_add_to_cart_message(array($product_id => $quantity), true);
        }

        ob_start();
        woocommerce_mini_cart();
        $mini_cart = ob_get_clean();

        $data = array(
            'fragments' => apply_filters(
                'woocommerce_add_to_cart_fragments', array(
                    'div.widget_shopping_cart_content' => $mini_cart,
                )
            ),
            'cart_hash' => apply_filters( 'woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5( json_encode( WC()->cart->get_cart_for_session() ) ) : '', WC()->cart->get_cart_for_session() ),
        );

        wp_send_json( $data );
    } else {

        $data = array(
            'error' => true,
            'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
        );

        echo wp_send_json($data);
    }

    wp_die();
}


/*
 * AJAX REMOVE FROM CART
 */
function woocommerce_ajax_remove_from_cart() {
    // Get mini cart
    ob_start();

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item)
    {
        if($cart_item['product_id'] == $_POST['product_id'] && $cart_item_key == $_POST['cart_item_key'] )
        {
            WC()->cart->remove_cart_item($cart_item_key);
        }
    }

    WC()->cart->calculate_totals();
    WC()->cart->maybe_set_cart_cookies();
    woocommerce_mini_cart();
    $mini_cart = ob_get_clean();

    // Fragments and mini cart are returned
    $data = array(
        'fragments' => apply_filters( 'woocommerce_add_to_cart_fragments', array(
                'div.widget_shopping_cart_content' => $mini_cart,
            )
        ),
        'cart_hash' => apply_filters( 'woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5( json_encode( WC()->cart->get_cart_for_session() ) ) : '', WC()->cart->get_cart_for_session() )
    );

    wp_send_json( $data );

    wp_die();
}

add_action( 'wp_ajax_woocommerce_ajax_remove_from_cart', 'woocommerce_ajax_remove_from_cart' );
add_action( 'wp_ajax_nopriv_woocommerce_ajax_remove_from_cart', 'woocommerce_ajax_remove_from_cart' );

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_btn_cart_fragment', 999 , 1 );

if( !function_exists( 'woocommerce_btn_cart_fragment' ) ) {
    function woocommerce_btn_cart_fragment($fragments)
    {
        ob_start();
        wc_get_template('cart/cart-button.php');
        $fragments['#btn--cart'] = ob_get_clean();

        return $fragments;
    }
}


/*add_action( 'wp_loaded', array( __CLASS__, 'process_login' ), 20 );*/
add_action( 'init', 'nooo_process_registration', PHP_INT_MAX);
/**
 * OVERRIDING REGISTRATION FORM VALIDATION
 * (Process the registration form.)
 */
function nooo_process_registration() {
    $nonce_value = isset( $_POST['_wpnonce'] ) ? $_POST['_wpnonce'] : '';
    $nonce_value = isset( $_POST['woocommerce-register-nonce'] ) ? $_POST['woocommerce-register-nonce'] : $nonce_value;

    if ( ! empty( $_POST['register'] ) && wp_verify_nonce( $nonce_value, 'woocommerce-register' ) ) {
        $username = 'no' === get_option( 'woocommerce_registration_generate_username' ) ? $_POST['username'] : '';
        $password = 'no' === get_option( 'woocommerce_registration_generate_password' ) ? $_POST['password'] : '';
        $email    = $_POST['email'];

        // AVOID CALLING ALSO WC HOOK PROCESSING REGISTRATION FORM
        unset($_POST['register']);

        try {
            $validation_error = new WP_Error();
            $validation_error = apply_filters( 'woocommerce_process_registration_errors', $validation_error, $username, $password, $email );

            $error_code = $validation_error->get_error_code();

            if ( $error_code ) {
                throw new Exception( $validation_error->get_error_message() );
            }

            $new_customer = wc_create_new_customer( sanitize_email( $email ), wc_clean( $username ), $password );

            if ( is_wp_error( $new_customer ) ) {
                throw new Exception( $new_customer->get_error_message() );
            }

            if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $new_customer ) ) {
                wc_set_customer_auth_cookie( $new_customer );
            }

            if ( ! empty( $_POST['redirect'] ) ) {
                $redirect = wp_sanitize_redirect( $_POST['redirect'] );
            } elseif ( wc_get_raw_referer() ) {
                $redirect = wc_get_raw_referer();
            } else {
                $redirect = wc_get_page_permalink( 'myaccount' );
            }

            wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_registration_redirect', $redirect ), wc_get_page_permalink( 'myaccount' ) ) );
            exit;

        } catch ( Exception $e ) {
            wc_add_notice( '<strong>' . __( 'Ooops...', 'woocommerce' ) . '</strong> ' . $e->getMessage(), 'error' );
        }
    }
}

add_filter( 'woocommerce_process_registration_errors', 'custom_register_validation', 9, 4 );
function custom_register_validation( $validation_error, $username, $password, $email ) {
    $not_autogenerated_password = 'no' === get_option( 'woocommerce_registration_generate_password' );
    $password_confirmation = $not_autogenerated_password ? $_POST['confirm_password'] : '';
    $privacy  =  $_POST['privacy-acceptance'];
    $mailchimp = $_POST['mailchimp-acceptance'];

    if(empty($privacy) || empty($mailchimp)) {
        $validation_error->add( 'acceptance_error', __( 'devi accettare i campi relativi alla privacy per continuare.', THEME_CONTEXT . '_validation_messages' ) );
    }

    if ($not_autogenerated_password) {
        // CUSTOM VALIDATION FOR EXTRA FIELDS
        if (empty($password_confirmation)) {
            $validation_error->add( 'confirm_password_error', __( 'devi confermare la password per continuare.', THEME_CONTEXT . '_validation_messages' ) );
        }
        if ($password_confirmation !== $password) {
            $validation_error->add( 'confirm_password_error', __( 'le due password non combaciano.', THEME_CONTEXT . '_validation_messages' ) );
        }
    }

    return $validation_error;
}

/*
 * ADDING LOGO URL TO EMAIL SETTINGS
 */

add_filter( 'woocommerce_email_settings', 'extra_email_settings');
if(!function_exists('extra_email_settings')){
    function extra_email_settings($settings){
        array_splice( $settings, 10, 0, array( array(
            'title'       => __( 'Header Logo', 'woocommerce' ),
            'desc'        => __( 'URL to a logo you want to show in the email header. Upload images using the media uploader (Admin > Media).', 'woocommerce' ),
            'id'          => 'woocommerce_email_header_logo',
            'type'        => 'text',
            'css'         => 'min-width:150px;',
            'placeholder' => __( 'N/A', 'woocommerce' ),
            'default'     => '',
            'autoload'    => false,
            'desc_tip'    => true,
        )));
        return $settings;
    }
}

// GET PRICE FOR TWIG
if (!function_exists('custom_get_prod_price')){
    function custom_get_prod_price($id, $formatted=true) {
        $product = wc_get_product($id);
        $price = 0;
        if ( vpc_woocommerce_version_check() ) {
            $product_type = $product->product_type;
        } else {
            $product_type = $product->get_type();
        }
        if ( $product_type == "variable" ) {
            $prod_id = get_field('default_variant_id', $id);
            if($prod_id === false || !is_numeric($prod_id)) {
                $variations = $product->get_available_variations();

                $i = 0;
                $min = 100000;
                $found = 0;
                foreach ($variations as $variation) {
                    $var = wc_get_product($variation['variation_id']);
                    if ($var->is_in_stock() && $var->get_price() < $min) {
                        $found = $i;
                        $min = $var->get_price();
                    }
                    ++$i;
                }
                $prod_id = $variations[$found]['variation_id'];
            }
            $variable_product= new WC_Product_Variation( $prod_id );
            $regular_price = $variable_product->regular_price;
            $sales_price = $variable_product->sale_price;
            $price = $regular_price + $sales_price;
        }
        else
        {
            $price = $product->get_price();
        }

        $thePrice = $formatted ? wc_price($price) : $price;
        return $thePrice;
    }
}

// REMOVING "ITEM REMOVED" NOTICES
if (!function_exists('delete_remove_product_notice')) {
    function delete_remove_product_notice(){
        $notices = WC()->session->get( 'wc_notices', array() );
        if(isset($notices['success'])){
            for($i = 0; $i < count($notices['success']); $i++){
                if (strpos($notices['success'][$i], __('removed','woocommerce')) !== false) {
                    array_splice($notices['success'],$i,1);
                }
            }
            WC()->session->set( 'wc_notices', $notices['success'] );
        }
    }

    add_action( 'woocommerce_before_shop_loop', 'delete_remove_product_notice', 5 );
    add_action( 'woocommerce_shortcode_before_product_cat_loop', 'delete_remove_product_notice', 5 );
    add_action( 'woocommerce_before_single_product', 'delete_remove_product_notice', 5 );
}

function add_accessories_to_cart_item( $cart_item_data, $product_id, $variation_id ) {

    if ( empty( $available_accessories ) || isset($cart_item_data['accessories']) ) {
        return $cart_item_data;
    }

    $accessories = array();
    foreach ( $available_accessories as $accessory) {
        $accessories[] = $accessory->ID;
    }

    $cart_item_data['accessories'] = $accessories;
    return $cart_item_data;
}

add_filter( 'woocommerce_add_cart_item_data', 'add_accessories_to_cart_item', 10, 3 );

if( !function_exists('add_note_to_cart_item')) {
    function add_note_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
        $note = filter_input( INPUT_POST, 'product_note' );
        if ( empty( $note ) || isset($cart_item_data['product_note']) ) {
            return $cart_item_data;
        }

        $cart_item_data['prod_note'] = $note;
        return $cart_item_data;
    }
}

add_filter( 'woocommerce_add_cart_item_data', 'add_note_to_cart_item', 10, 3 );


function add_gift_data_to_cart_item( $cart_item_data, $product_id ) {
    if ( isset( $_REQUEST['gift_name'] ) && ! empty( $_REQUEST['gift_name'] ) ) {
        $cart_item_data['gift_name'] = sanitize_text_field( $_REQUEST['gift_name'] );
    }
    if ( isset( $_REQUEST['gift_email'] ) && ! empty( $_REQUEST['gift_email'] ) ) {
        $cart_item_data['gift_email'] = sanitize_text_field( $_REQUEST['gift_email'] );
    }
    if ( isset( $_REQUEST['gift_message'] ) && ! empty( $_REQUEST['gift_message'] ) ) {
        $cart_item_data['gift_message'] = sanitize_text_field( $_REQUEST['gift_message'] );
    }

    return $cart_item_data;
}

function add_gift_data_to_all( $item, $cart_item_key, $values, $order ) {
    if ( isset( $values['gift_name'] ) ) {
        $item->add_meta_data( 'gift_name', $values['gift_name'] );
    }
    if ( isset( $values['gift_email'] ) ) {
        $item->add_meta_data( 'gift_email', $values['gift_email'] );
    }
    if ( isset( $values['gift_message'] ) ) {
        $item->add_meta_data( 'gift_message', $values['gift_message'] );
    }
}

add_filter( 'woocommerce_add_cart_item_data', 'add_gift_data_to_cart_item', 10, 2 );
add_action( 'woocommerce_checkout_create_order_line_item', 'add_gift_data_to_all', 10, 4 );



function display_custom_item_data( $cart_item_data, $cart_item ) {
    if ( isset( $cart_item['gift_name'] ) ) {
        $cart_item_data[] = array(
            'key'   => __( 'Nome destinatario', 'dancenaturals' ),
            'value' => wc_clean( $cart_item['gift_name'] )
        );
    }
    if ( isset( $cart_item['gift_email'] ) ) {
        $cart_item_data[] = array(
            'key'   => __( 'Indirizzo E-mail destinatario', 'dancenaturals' ),
            'value' => wc_clean( $cart_item['gift_email'] )
        );
    }
    if ( isset( $cart_item['gift_message'] ) ) {
        $cart_item_data[] = array(
            'key'   => __( 'Messaggio', 'dancenaturals' ),
            'value' => wc_clean( $cart_item['gift_message'] )
        );
    }

    return $cart_item_data;
}
add_filter( 'woocommerce_get_item_data', 'display_custom_item_data', 10, 2 );


// REDUCING MIN PASSWORD STRENGTH
function nooo_min_password_strength( $strength ) {
    return 3;
}
add_filter( 'woocommerce_min_password_strength', 'nooo_min_password_strength', 10, 1 );

// CHANGING PASSWORD HINT
add_filter( 'password_hint', function( $hint ) {
    return __( 'The password should be at least height characters long and use upper, lower case letters, numbers, and symbols like ! " ? $ % ^ & ).', THEME_CONTEXT.'_forms' );
} );

if(!function_exists('add_image_to_order_email')){
    function add_image_to_order_email ($item_id, $item, $order, $plain_text){
        if(!isset($item['vpc-custom-data'])) {
            $prod = $item->get_product();
            echo wp_get_attachment_image($prod->get_image_id());
        }
    }
}
add_action( 'woocommerce_order_item_meta_start', 'add_image_to_order_email', 10, 4 );

/*if(!function_exists('reorder_meta_recap')){
    function reorder_meta_recap ($item_id, $item, $order, $plain_text){
        $wantedOrder = array('pa_modifica-materiale','pa_tacco', 'pa_design-tacco', 'pa_taglia', 'pa_calzata', 'Note:');
        $orderedMeta = array();
        foreach ($wantedOrder as $index){
            $meta = $item->get_meta($index);

            if(!empty($meta))
                $orderedMeta[] = array($index => $meta);
        }

        $item->set_meta_data($orderedMeta);
        var_dump($item);
    }
}
add_action( 'woocommerce_order_item_meta_start', 'reorder_meta_recap', 10, 5 );*/


if ( ! function_exists( 'display_item_meta_in_custom_order' ) ) {
    /**
     * Display item meta data CUSTOM ORDER.
     *
     * @param  WC_Order_Item $item Order Item.
     * @param  array         $args Arguments.
     * @return string|void
     */
    function display_item_meta_in_custom_order( $item, $args = array() ) {
        $html    = '';
        $args    = wp_parse_args( $args, array(
            'before'    => '<ul class="wc-item-meta"><li>',
            'after'     => '</li></ul>',
            'separator' => '</li><li>',
            'echo'      => true,
            'autop'     => false,
        ) );

        $wantedOrder = array('pa_modifica-materiale','pa_tacco', 'pa_design-tacco', 'pa_taglia', 'pa_calzata', 'Note:');

        $strings = array_filter($item->get_formatted_meta_data(), function($item){
            return $item->key != "billing_cf";
        });

        $strings = array_map(function($item){
            return '<strong class="wc-item-meta-label">' . $item->display_key . ':</strong> ' . $item->display_value;
        }, $strings);

        if ( $strings ) {
            $html = $args['before'] . implode( $args['separator'], $strings) . $args['after'];
        }

        $html = apply_filters( 'woocommerce_display_item_meta', $html, $item, $args );

        if ( $args['echo'] ) {
            echo $html; // WPCS: XSS ok.
        } else {
            return $html;
        }
    }
}

// Add images as email attachments instead of inside email
add_filter( 'woocommerce_email_attachments', 'nooo_woocommerce_attachments', 10, 3 );
function nooo_woocommerce_attachments($attachments, $email_id, $email_object){

    if( $email_id === 'new_order'){
        $orderItems = $email_object->get_items();
        foreach ($orderItems as $item) {
            $customData = $item->get_meta('vpc-custom-data');
            $images = explode('|', $customData['preview_saved']);

            foreach ($images as $src){
                $switched = false;

                if(strpos($src, '/sites/2/') === false){
                    $switched = true;
                    switch_to_blog(1);
                }
                $uploadDir = wp_upload_dir();
                $attachments[] = get_attached_file(get_image_id_by_url($src));

                if($switched) {
                    switch_to_blog(2);
                }
            }
        }
    }
    return $attachments;
}

if(!function_exists('add_notes_to_order')) {
    function add_notes_to_order( $item, $cart_item_key, $values, $order ) {
        if ( ! isset( $values['prod_note'] ) ) {
            return;
        }

        if ( ! empty( $values['prod_note'] ) ) {
            $item->add_meta_data( __( 'Nota:', THEME_CONTEXT ), $values['prod_note'] );
        }
    }
}

if(!function_exists('add_cf_to_order')) {
    function add_cf_to_order( $item, $cart_item_key, $values, $order ) {
        global $_POST;
        if ( ! isset( $_POST['billing_cf'] ) ) {
            return;
        }

        if ( ! empty( $_POST['billing_cf']  ) ) {
            $item->add_meta_data( 'billing_cf', $_POST['billing_cf'] );
        }
    }
}
add_action( 'woocommerce_checkout_create_order_line_item', 'add_notes_to_order', 10, 4);
add_action( 'woocommerce_checkout_create_order_line_item', 'add_cf_to_order', 10, 5);


add_action('woocommerce_checkout_fields', 'end_custom_checkout_fields');

function end_custom_checkout_fields($fields) {

    if (ICL_LANGUAGE_CODE === 'it'){
        $fields['billing']['billing_cf'] = array(
            'type' => 'text',
            'class' => array( 'form-row form-row-wide cf-field validate-required input-type-text' ) ,
            'label' => __('CODICE FISCALE', THEME_CONTEX),
            'placeholder' => __('Inserisci il codice fiscale', THEME_CONTEX) ,
            'required' => true
        );
    }

    return $fields;
}


/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Codice Fiscale').':</strong> ' . get_post_meta( $order->id, 'billing_cf', true ) . '</p>';
}


// Redirect after save address
function action_woocommerce_customer_save_address( $user_id, $load_address ) {
    wp_safe_redirect(wc_get_page_permalink('myaccount'));
    exit;
};
add_action( 'woocommerce_customer_save_address', 'action_woocommerce_customer_save_address', 99, 2 );
