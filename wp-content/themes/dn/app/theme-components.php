<?php

/**
 * CUSTOM POST TYPES
 **/
if(!function_exists('nooo_post_types')){
	function nooo_post_types() {

// ---------------------- LOCATIONS
		$labelsLocations = array(
			'name'               => __('Rivenditori', THEME_CONTEXT ),
			'singular_name'      => __('Rivenditore', THEME_CONTEXT),
			'menu_name'          => __('Rivenditori', THEME_CONTEXT),
			'name_admin_bar'     => __('Rivenditori', THEME_CONTEXT),
		);

		register_post_type( 'reseller', array(
			'public'             => true,
			'labels'             => $labelsLocations,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 5.5,
			'menu_icon'          => 'dashicons-location-alt',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'revisions'),
			'map_meta_cap'       => true,
		));
	}
}

add_action( 'init', 'nooo_post_types' );

// ---------------------- CUSTOM TAXONOMIES
if(!function_exists('nooo_taxonomies')){
	function nooo_taxonomies() {
		$showcaseLabels = array(
			'name'                       => __('Vetrine', THEME_SLUG),
			'singular_name'              =>  __('Vetrina', THEME_SLUG),
			'menu_name'                  =>  __('Vetrine', THEME_SLUG),
			'all_items'                  =>  __('Tutte le vetrine', THEME_SLUG),
			'new_item_name'              =>  __('Nome Nuova Vetrina', THEME_SLUG),
			'add_new_item'               =>  __('Aggiungi nuova Vetrina', THEME_SLUG),
			'edit_item'                  =>  __('Modifica Vetrina', THEME_SLUG),
			'update_item'                =>  __('Aggiorna Vetrina', THEME_SLUG),
			'search_items'               =>  __('Cerca Vetrina', THEME_SLUG),
			'add_or_remove_items'        =>  __('Aggiungi o rimuovi vetrine', THEME_SLUG),
		);
		$showcaseArgs = array(
			'labels'                     => $showcaseLabels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'showcase', 'product', $showcaseArgs );
	}
}

add_action( 'init', 'nooo_taxonomies');
