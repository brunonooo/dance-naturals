<?php

/**
 * Theme assets
 */

add_action('wp_enqueue_scripts', function () {
	// CLEANING LOADED RESOURCES FIRST TO IMPROVE WEBSITE PERFORMANCE
	clean_resources();

	if(is_singular('product')) {
        wp_enqueue_style('lightslider', get_template_directory_uri().'/node_modules/lightslider/dist/css/lightslider.min.css', false, null);
    }

	wp_register_style('sage/vendor.css', asset_path('styles/vendor.css'), false, null);
	wp_register_style('sage/main.css', asset_path('styles/main.css'), false, null);
	wp_register_script('custom_woo_country_select', get_template_directory_uri().'/assets/scripts/woocommerce/country-select.js', array(), null, true);
	wp_register_script( 'html5shiv', get_template_directory_uri() . "/node_modules/html5shiv/dist/html5shiv.min.js", array(), null, true);
	wp_register_script( 'respond', get_template_directory_uri() . "/node_modules/respond.js/dest/respond.min.js", array(), null, true);
	wp_register_script( 'typedarray-polyfill', get_template_directory_uri() . "/node_modules/js-polyfills/typedarray.js", array(), null, true);
	wp_register_script( 'matchmedia-polyfill', get_stylesheet_directory_uri() . "/node_modules/matchmedia-polyfill/matchMedia.addListener.js", array(), null, true);
	wp_register_script('sage/vendor.js', asset_path('scripts/vendor.js'), array('jquery'), null, true);
	wp_register_script('sage/main.js', asset_path('scripts/main.js'), array(), null, true);

	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Arapey:400,400i|Roboto+Mono:400,400i,700,700i');
	wp_enqueue_style('sage/vendor.css');
	wp_enqueue_style('sage/main.css');

	wp_dequeue_script('wc_country_select');
	wp_enqueue_script('custom_woo_country_select');

	wp_enqueue_script( 'html5shiv');
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9');

	wp_enqueue_script( 'respond');
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9');

	wp_enqueue_script( 'typedarray-polyfill');
	wp_script_add_data( 'typedarray-polyfill', 'conditional', 'lt IE 10');

	wp_enqueue_script( 'matchmedia-polyfill');
	wp_script_add_data( 'matchmedia-polyfill', 'conditional', 'IE');

	if(is_page_template('resellers.php') || is_page_template('contact-us.php')){
		$currLang = ICL_LANGUAGE_CODE;
		wp_register_script('gmaps', "https://maps.googleapis.com/maps/api/js?key=AIzaSyAcUF1RxhsIkoAH-_KN8uQeyaosxw9SQvk&callback=mapsManager.gmapsCheck&language=".$currLang, array(), null, true);
		wp_enqueue_script('gmaps');
	}

	wp_register_script('gsap-ce', get_template_directory_uri().'/assets/scripts/vendors/CustomEase.js', array(), null, true);
	wp_enqueue_script('gsap-ce');

	if(is_page_template('configurator.php')){
		wp_deregister_script('vpc-mva');
		wp_enqueue_script( 'vpc-mva', get_template_directory_uri().'/visual-product-configurator-extension/js/nooo-mva-public.js', array( 'jquery' ), false, false );
	}

	if(is_singular('product')) {
		wp_deregister_script( 'wc-single-product');
		wp_register_script('wc-single-product', get_template_directory_uri().'/assets/scripts/woocommerce/single-product.js', array(), null, true);
		wp_enqueue_script('wc-single-product');

		wp_deregister_script( 'wc-add-to-cart-variation' );
		wp_register_script('wc-add-to-cart-variation', get_template_directory_uri().'/assets/scripts/woocommerce/add-to-cart-variation.js', array( 'jquery', 'wp-util' ), null, true);
		wp_enqueue_script('wc-add-to-cart-variation');
	}

	if(!is_tax('product_cat')){
		wp_deregister_script('history-js');
		wp_deregister_script('ajaxify-js');
	}

	// SCRIPTS
	wp_enqueue_script('sage/vendor.js');
	wp_enqueue_script('sage/main.js');
	wp_localize_script( 'sage/main.js', 'ajax', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' )
	));

}, PHP_INT_MAX);

/**
 * ADDING CUSTOM STYLES TO LOGIN PAGE
 */
if(!function_exists('my_login_stylesheet')) {
	function my_login_stylesheet() {
		wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Muli:700,800,900');
		wp_enqueue_style( 'custom-login', asset_path('styles/backend.css') );
	}
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

if(!function_exists('clean_resources')){
	function clean_resources(){
		wp_deregister_style('bodhi-svgs-attachment');
		if (!is_page_template('configurator.php') && !is_singular('product')) {
			wp_deregister_style('vpc');
			wp_deregister_style('vpc-mva');
			wp_deregister_style('vpc-mva-bxslider-css');
			wp_deregister_style('o-flexgrid');

			wp_deregister_script('vpc-mva-bxslider.mini');
			wp_deregister_script('vpc-mva');
			wp_deregister_script('vpc-accounting');
			wp_deregister_script('wp-js-hooks');
			wp_deregister_script('serializejson');
			wp_deregister_script('htmltocanvas');
		}

		// WPML
		wp_deregister_style('wcml-dropdown-0-css');

		// WOOCOMMERCE STYLES
		if (!is_singular('product')){
			wp_deregister_style('woocommerce-general');
			wp_deregister_style('woocommerce-layout');
			wp_deregister_style('woocommerce-smallscreen');
			wp_deregister_style('woocommerce-general-css');
			wp_deregister_style('woocommerce-inline-inline');
			wp_deregister_style('wc-gateway-ppec-frontend-cart');
			wp_deregister_style('wcml-dropdown-0');

			// WOOCOMMERCE SCRIPTS
			wp_deregister_script('wcml-front-scripts');
			wp_deregister_script('cart-widget');
			wp_deregister_script('wcml-multi-currency');
		}
	}
}
