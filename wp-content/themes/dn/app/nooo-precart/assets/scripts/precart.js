(function($){
    $(document).ready(function () {

        function get_selected_variations_attributes() {
            var options = $("select[name^='attribute_']");
            var new_options = {};
            $.each(options, function () {
                var option_name = $(this).attr("name");
                new_options[option_name] = $(this).find("option:selected").val();
            });
            return new_options;
        }

        $('.single_add_to_cart_button').click(function(e){
            e.preventDefault();
            $(this).attr('disabled', true);
            $(this).addClass('loading');
            var old_key = $('input[name="old_key"]').val(),
                product_id = $('input[name="product_id"]').val(),
                variation_id = $('input[name="variation_id"]').val(),
                product_note = $('.mdl-content textarea[name="product_note"]').val(),
                new_variations_attributes = get_selected_variations_attributes(),
                url = window.precart_ajax.ajax_url;

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    action: "precart_update_cart",
                    old_key: old_key,
                    product_id: product_id,
                    variation_id: variation_id,
                    new_variations_attributes: new_variations_attributes,
                    product_note: product_note,
                },
                error: function(error){
                    alert('Ops, something went wrong!');
                    console.log('Ups, something went wrong with the request: \n' + error );
                },
                success : function(response) {
                    var data = JSON.parse(response);
                    $(this).attr('disabled', false);
                    $(this).removeClass('loading');
                    window.location.href = data.cart_url;
                },
            });
        })
    });
}(jQuery));
