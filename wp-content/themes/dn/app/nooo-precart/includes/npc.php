<?php
class Nooo_precart {

    private static $_instance = null;
    private $product = null;
    private $cart = null;

    function __construct() {
    }

    public static function instance() {
        if ( Nooo_precart::$_instance == null ) {
            Nooo_precart::$_instance = new Nooo_precart();
        }

        return Nooo_precart::$_instance;
    }

    public function init() {
        if ($this->is_active()){
            add_action('wp_enqueue_scripts', array($this, 'precart_assets'), PHP_INT_MAX);
        }
        add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'add_precart_buttons' ) );
        /*add_action('woocommerce_add_to_cart', array($this, 'replace_product_variations'), 20, 2);*/
    }

    public function get_session_data() {
        $session_data = null;
        if ( WC()->session ) {
            $session_data = WC()->session->get( 'config_views' );
        }

        return $session_data;
    }

    public function get_precart_image_info($index) {
        $views = $this->get_config_views();
        return get_custom_image_info($views[$index]);
    }

    public function precart_gallery_image_html($alias_id, $image_num, $attr = array()) {
        $full_src = $this->get_precart_image_info($image_num);
        if ( empty( $attr ) ) {
            $attr = array(
                'title'                   => get_post_field( 'post_title', $alias_id ),
                'data-caption'            => get_post_field( 'post_excerpt', $alias_id ),
                'data-src'                => $full_src[0],
                'data-large_image'        => $full_src[0],
                'data-large_image_width'  => $full_src[1],
                'data-large_image_height' => $full_src[2],
                'class'                   => $image_num === 0 ? 'w-100 wp-post-image' : ''
            );
        }

        $image = '<img src="' . $full_src[ 0 ] .'" ';

        foreach ($attr as $name => $value){
            $image .= $name.'="'.$value.'" ';
        }

        $image .= '/>';
        return $image;
    }

    public function delete_session_data() {
        return WC()->session->set( 'config_views', null );
    }

    public function set_session_data( $views ) {
        return WC()->session->set( 'config_views', $views );
    }

    public function is_active() {
        $has_session_data = ! empty($this->get_session_data());
        $prod = $this->get_product();
        $is_last_prod_added_to_cart = $prod['product_id'] === get_the_id();
        return ($has_session_data && $is_last_prod_added_to_cart);
    }

    public function is_prod_in_precart() {
        $precart_prod    = $this->get_wc_product();
        $precart_prod_id = $precart_prod->get_id();
        $post_id         = get_the_id();
        $id_matches      = $precart_prod_id === $post_id;

        return $this->is_active() && $id_matches;
    }

    public function get_cart() {
        if ( ! $this->cart  && WC()->cart !== null ) {
            $this->cart = WC()->cart->get_cart();
        }

        return $this->cart;
    }

    public function get_product() {
        if ( ! $this->product ) {
            $cart          = $this->get_cart();
            $this->product = end( $cart );
        }

        return $this->product;
    }

    public function get_wc_product() {
        $product = $this->get_product();

        return wc_get_product( $product['product_id'] );
    }

    public function get_selected_variation() {
        $cart_item = $this->get_product();

        return $cart_item['data'];
    }

    public function get_key() {
        $product = $this->get_product();

        return $product['key'];
    }

    public function get_config_price() {
        return WC()->session->get( 'config_price' );
    }

    public function get_config_views() {
        return explode( '|', WC()->session->get( 'config_views' ) );
    }

    public static function parse_config_views( $views_string ) {
        return explode( '|', $views_string );
    }

    public function get_prod_permalink( $_product, $cart_item, $cart_item_key ) {
        $item_configured = isset( $cart_item['visual-product-configuration'] );

        if ( $item_configured ) {
            $config_url = vpc_get_configuration_url( $cart_item['variation_id'] );
            if ( get_option( 'permalink_structure' ) ) {
                $edit_url = $config_url . "?edit=$cart_item_key&qty=" . $cart_item['quantity'];
            } else {
                $edit_url = $config_url . "&edit=$cart_item_key&qty=" . $cart_item['quantity'];
            }
            $this->product_permalink = $edit_url;
        }
    }

    public function activate() {
        add_action( 'admin_notices', array( $this, 'add_activation_notices' ) );
    }

    public function add_precart_buttons() {
        if ( $this->is_active() ) {
            ?>
            <button id="reset-configuration" type="submit" class="btn btn__outlined alt"
                    name="precart_reset"><?php _e( 'Azzera configurazione', THEME_CONTEXT . '_precart' ); ?></button
            <?php
        }
    }

    public function precart_assets() {
        wp_register_style( 'nooo-precart-style', get_template_directory_uri() . '/app/nooo-precart/assets/styles/precart.css', false, null );
        wp_register_script( 'nooo-precart-js', get_template_directory_uri() . '/app/nooo-precart/assets/scripts/precart.js', array(), null, true );
        wp_localize_script( 'nooo-precart-js', 'precart_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        wp_enqueue_style( 'nooo-precart-style' );
        wp_enqueue_script( 'nooo-precart-js' );
    }

    public function add_activation_notices() {
        if ( ! class_exists( 'Woocommerce' ) ) {
            ?>
            <div class="error notice">
                <p><?php _e( 'Nooo Precart is active but it needs Woocommerce to be active to work. We disabled it until Woocommerce is activated.', $this->plugin_name.'_notices' ); ?></p>
            </div>
            <?php
        } else {
            ?>
            <div class="updated notice is-dismissable">
                <p><?php _e( 'YAY! Nooo Precart is active and running!', $this->plugin_name.'_notices' ); ?></p>
            </div>
            <?php
        }
    }
}

if(!function_exists('precart_update_cart')){
    function precart_update_cart() {
        if (!empty($_POST['old_key'])){
            $cart = Precart()->get_cart();
            $old_prod = $cart[$_POST['old_key']];
            $old_prod_key = $_POST['old_key'];
            $old_prod_config = $old_prod['vpc-custom-vars'];
            $old_prod_config['attributes'] = $_POST['new_variations_attributes']; // UPDATING VPC ATTRIBUTES
            $old_prod_visual_config = $old_prod['visual-product-configuration'];
            WC()->cart->remove_cart_item($old_prod_key);
            $prod_id = $_POST['product_id'];
            $prod_variation_id = $_POST['variation_id'];
            $prod_variation = $_POST['new_variations_attributes'];
            $cart_item_data = array(
                'vpc-custom-vars' => $old_prod_config,
                'visual-product-configuration' => $old_prod_visual_config,
            );
            if (isset($_POST['product_note'])) { $cart_item_data['prod_note'] = $_POST['product_note']; }
            WC()->cart->remove_cart_item($old_prod_key);
            WC()->cart->add_to_cart($prod_id, 1, $prod_variation_id, $prod_variation, $cart_item_data);
            Precart()->delete_session_data();
        }

        echo json_encode(array('cart_url' => wc_get_cart_url()));
        die();
    }
}

add_action( 'wp_ajax_nopriv_precart_update_cart', 'precart_update_cart' );
add_action( 'wp_ajax_precart_update_cart', 'precart_update_cart' );

if(!function_exists('precart_get_selected_attributes')) {
    function precart_get_selected_attributes($cart_item = false) {
        if ($cart_item == false && !Precart()->is_active()){
            return false;
        }

        $cart_item = !empty($cart_item) ? 	$cart_item : Precart()->get_product();

        if (isset($cart_item['vpc-custom-vars']) && isset($cart_item['vpc-custom-vars']['attributes'])){
            return $cart_item['vpc-custom-vars']['attributes'];
        } else {
            $variation = wc_get_product($cart_item['variation_id']);
            return $variation->get_variation_attributes();
        }
    }
}

if(!function_exists('is_attribute_selected')) {
    function is_attribute_selected($option, $attribute_name) {
        // SELECT ONLY IF WE ARE IN PRECART
        if(Precart()->is_active()){
            $attributes = precart_get_selected_attributes();
            $vpc_attr = 'attribute_'.$attribute_name;

            if(array_key_exists($vpc_attr, $attributes) || array_key_exists($attribute_name, $attributes)){
                return $attributes[$vpc_attr] === $option || $attributes[$attribute_name] === $option;
            }
        }
    };
}

if(!function_exists('get_selected_attribute')) {
    function get_selected_attribute($attribute_name) {
        if(Precart()->is_active()){
            $attributes = precart_get_selected_attributes();
            $vpc_attr = 'attribute_'.$attribute_name;
            return isset($attributes[$vpc_attr]) ? $attributes[$vpc_attr] : $attributes[$attribute_name];
        } else {
            return false;
        }
    };
}
