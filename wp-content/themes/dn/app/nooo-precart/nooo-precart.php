<?php
/*
Plugin Name: Nooo Precart
Plugin URI: http://nooo.it
Description: Nooo extension of vpc plugin
Version: 0.1.0
Author: Nooo Agency, Manuel Pettenò
Author URI: http://nooo.it
Text Domain: nooo-precart
Domain Path: /languages
*/

defined('ABSPATH' ) or die( 'Hey! You\'re not going to cheat man, right?' );


if (!class_exists('Nooo_precart') ) {
	include_once dirname( __FILE__ ) . '/includes/npc.php';
}

function Precart(){
	if (class_exists('Nooo_precart')) {
		return Nooo_precart::instance();
	}
}

add_action('wp', array(Precart(), 'init'));
