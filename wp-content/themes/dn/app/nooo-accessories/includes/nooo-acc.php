<?php
class Nooo_accessories {

    private static $_instance = null;
    private $accessories = null;

    function __construct() {
        $this->accessories = null;
        add_filter('woocommerce_update_cart_action_cart_updated', array($this, 'add_accessories_to_cart'));
        add_filter('woocommerce_before_calculate_totals', array($this, 'update_cart_prices'));
        add_action('woocommerce_cart_item_removed', array($this, 'remove_accessories_from_cart'), 10, 2);
        add_action( 'woocommerce_checkout_create_order_line_item', array( $this,'add_accessories_to_order' ), 10, 4);
    }

    public static function instance() {
        if ( Nooo_accessories::$_instance == null ) {
            Nooo_accessories::$_instance = new Nooo_accessories();
        }

        return Nooo_accessories::$_instance;
    }

    public function get_session_data() {
        $session_data = null;
        if ( WC()->session ) {
            $session_data = WC()->session->get( 'active_accessories' );
        }

        return $session_data;
    }

    public function delete_session_data($cart_item_key = null) {
        if ( WC()->session ) {
            if (!$cart_item_key)
                WC()->session->set('active_accessories', null);
            else
            {
                $accessories = $this->get_session_data();
                unset($accessories[$cart_item_key]);
                WC()->session->set( 'active_accessories', $accessories );
            }
        }
    }

    public function build_session_data () {
        $selected_accessories = array();
        foreach ($_POST['cart'] as $item_key => $item_data) {
            if (isset($item_data['selected_accessories'])) {
                $cart_item = WC()->cart->get_cart_item($item_key);
                $sel_acc_list = array();
                foreach ($item_data['selected_accessories'] as $accID) {
                    $acc = wc_get_product($accID);
                    $sel_acc_list[$accID] = array(
                        'name' => $acc->get_name(),
                        'price' => $acc->get_price()
                    );
                }
                $selected_accessories[$item_key] = $sel_acc_list;
            }
        }
        WC()->session->set( 'active_accessories', $selected_accessories );
    }

    public function remove_accessories_from_cart($cart_item_key, $instance) {
        $active_accessories = $this->get_session_data();
        if (isset($active_accessories[$cart_item_key])) {
            unset($active_accessories[$cart_item_key]);
            WC()->session->set( 'active_accessories', null );
            WC()->session->set( 'active_accessories', $active_accessories );
        }
    }

    public function update_cart_prices() {
        $active_accessories = $this->get_session_data();
        $items = WC()->cart->get_cart();

        foreach($items as $item_key => $cart_item)
        {
            $base_price = $cart_item['data']->get_regular_price();

            $accessories = array_key_exists($item_key,$active_accessories) ?  $active_accessories[$item_key] : [];

            $accessories_total = array_reduce($accessories,function($total, $accessory){
                return $total + floatval($accessory['price']);
            },0);

            $new_price = $base_price + $accessories_total;
            $cart_item['data']->set_price($new_price);
        }
    }

    public function add_accessories_to_cart ($cart) {
        $this->build_session_data();
        return $cart;
    }

    public function add_accessories_to_order( $item, $cart_item_key, $values, $order ) {
        $accessories = $this->get_session_data();
        if(!isset($accessories[$cart_item_key])) return;

        if( !empty($accessories[$cart_item_key]) ) {
            $accessoriesMarkup = '';
            $item_count = count($accessories[$cart_item_key]);
            $i = 0;
            foreach ( $accessories[$cart_item_key] as $accessory ) {
                $accessoriesMarkup .= $accessory['name'].($i < $item_count-1 ? ', ' : '.');
                ++$i;
            }
            $item->add_meta_data( __('Accessori', THEME_CONTEXT), $accessoriesMarkup );
            $this->delete_session_data($cart_item_key);
        }
    }

    public function get_accessories_query_args() {
        return array(
            'post_type'      => 'product',
            'posts_per_page' => -1,
            'tax_query'      => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => __('accessori', THEME_CONTEXT.'_slugs'),
                ),
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => __('accessori-sandali', THEME_CONTEXT.'_slugs'),
                    'operator' => 'NOT IN',
                ),
            )
        );
    }

// TO REFACTOR WITH DYNAMIC CROSS SELLS
// (No time to link all cross sells)
    public function get_available_accessories () {
        if (!$this->accessories ) {
            $this->accessories = get_posts($this->get_accessories_query_args());
        }
        return $this->accessories;
    }

    public function get_available_accessories_timber () {
        return Timber::get_posts($this->get_accessories_query_args());
    }
}
