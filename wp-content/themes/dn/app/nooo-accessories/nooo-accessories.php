<?php
/*
Plugin Name: Nooo Accessories
Plugin URI: http://nooo.it
Description: Nooo extension of woocommerce to have accessories in cart
Version: 0.1.0
Author: Nooo Agency, Manuel Pettenò
Author URI: http://nooo.it
Text Domain: nooo-accessories
Domain Path: /languages
*/

defined('ABSPATH' ) or die( 'Hey! You\'re not going to cheat man, right?' );


if (!class_exists('Nooo_accessories') ) {
	include_once dirname( __FILE__ ) . '/includes/nooo-acc.php';
}

function Accessories(){
	if (class_exists('Nooo_accessories')) {
		return Nooo_accessories::instance();
	}
}

add_action('wp', array(Accessories(), 'init'));
