<?php

require_once __DIR__ . '/NoooCache.php';

/**
 * sage paths helper
 */
function asset_path($asset, $childThemePath = null ) {

    if(isset($childThemePath)) {
        $themeurl = get_stylesheet_directory_uri();
        $dirstylesheet = $childThemePath . '/../dist/assets.json';
    } else {
        $themeurl = get_template_directory_uri();
        $dirstylesheet = __dir__ . '/../dist/assets.json';
    }

    $uristylesheet = $themeurl . '/dist';

    $manifest = new man_jsonmanifest($dirstylesheet, $uristylesheet);
    return $manifest->geturi($asset);
}


/**
 * ------ add async and defer attributes
 */
function add_defer_attribute($tag, $handle)
{
    $scripts_to_defer = array('gmaps');

    foreach ($scripts_to_defer as $defer_script) {
        if ($defer_script === $handle) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
    }
    return $tag;
}

function add_async_attribute($tag, $handle)
{
    $scripts_to_async = array('gmaps');

    foreach ($scripts_to_async as $async_script) {
        if ($async_script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }
    return $tag;
}

add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


/**
 * ----------------------  images management helpers
 */


function get_cache_id($opts)
{
    $cache_id = $opts['id'];
    $cache_id .= $opts['size'];
    $cache_id .= $opts['extra_classes'];
    $cache_id .= $opts["lazy"] ? "1" : "0";
    $cache_id .= $opts["onscroll"] ? "1" : "0";
    $cache_id .= $opts['cover'] ? "1" : "0";
    $cache_id .= $opts['sizes_attr'];
    $cache_id .= $opts['path'];
    $cache_id .= $opts['placeholder'];

    return md5($cache_id);
}

/**
 * --- GET IMAGES FOR IMG WRAPPERS
 *
 */
function get_img($args = array()) {

    //SETTING DEFAULTS
    $defaults = array(
        'id' => false,
        'size' => 'full',
        'extra_classes' => '',
        'lazy' => true,
        'onscroll' => true,
        'cover' => true,
        'sizes_attr' => '',
        'path' => '',
        'placeholder' => '',
    );

    // CASE A NUMBER IS PROVIDED
    if(is_numeric($args)){
        $id = (int)$args;
        $args = array(
            'id' => $id,
        );
    }

    if (ENVIROMENT == 'DEV'){
        $opts['size'] = 'nec_thumb';
    }

    // MERGING WITH ARGS PROVIDED
    $opts = !empty($args) ? array_merge($defaults, $args) : $defaults;

    if (!empty($opts['id'])) {
        $currentPost = get_post($opts['id']);
    } else {
        $currentPost = get_post(get_the_ID());
    }

    // CASE ITS A MEDIA
    if ($currentPost->post_type != 'attachment') {
        $currentPost = get_post(get_post_thumbnail_id($currentPost->ID));
    }

    $src = wp_get_attachment_image_url($currentPost->ID, $opts['size']);
    $srcset = wp_get_attachment_image_srcset($currentPost->ID, $opts['size']);
    $placeholder = wp_get_attachment_image_url($currentPost->ID, PLACEHOLDER_SIZE);

    if (!empty($opts['path']) && empty($opts['id'])){
        $src = $opts['path'];
        $srcset = '';
        $placeholder = $opts['placeholder'];
    }

    if (!empty($opts['sizes_attr'])) {
        $sizes = $opts['sizes_attr'];
    } else {
        $sizes = wp_get_attachment_image_sizes($currentPost->ID, $opts['size']);
    }

    $alt = get_post_meta($currentPost->ID, '_wp_attachment_image_alt', true);
    $alt = !empty($alt) ? $alt : $currentPost->post_title;

    if ($opts['lazy']) {
        $imgMarkup = sprintf('<img class="lazy-placeholder" src="%6$s" alt="%4$s">
									<img class="lazy-image %5$s" src="" alt="%4$s" srcset="" sizes="%3$s"> 
                                    <noscript data-magic-src="%1$s" data-magic-srcset="%2$s" %7$s>
                                   <img class="%5$s" src="%1$s" alt="%4$s" srcset="%2$s" sizes="%3$s">
                                   </noscript>',
            $src,
            $srcset,
            $sizes,
            $alt,
            ($opts['cover'] ? $opts['extra_classes'].' cover' : $opts['extra_classes']),
            $placeholder,
            ($opts['onscroll'] ? 'data-lazy-onscroll' : '' )
        );
    } else {
        $imgMarkup = sprintf('<img class="%5$s" src="%1$s" sizes="%3$s" srcset="%2$s" alt="%4$s">',
            $src,
            $srcset,
            $sizes,
            $alt,
            ($opts['cover'] ? $opts['extra_classes'].' cover' : $opts['extra_classes']));
    }

    return $imgMarkup;
}

if(!function_exists('get_prod_img')) {
    function get_prod_img($cart_item, $args = array()) {
        $customization_data = $cart_item['vpc-custom-vars'];
        if(isset($customization_data)){
            $args['path'] = Precart()->parse_config_views($customization_data['preview_saved'])[0];
        } else {
            $args['id'] = $cart_item['product_id'];
        }

        $img_markup = get_img($args);
        return $img_markup;
    }
}

/**
 * get image from url
 */
if(!function_exists('get_image_id_by_url')){
    function get_image_id_by_url( $attachment_url = '' ) {

        global $wpdb;
        $attachment_id = false;

        // if there is no url, return.
        if ( '' == $attachment_url )
            return;

        // get the upload directory paths
        $upload_dir_paths = wp_upload_dir();

        // make sure the upload path base directory exists in the attachment url, to verify that we're working with a media library image
        if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

            // if this is the url of an auto-generated thumbnail, get the url of the original image
            $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

            // remove the upload path base directory from the attachment url
            $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

            // finally, run a custom database query to get the attachment id from the modified attachment url
            $attachment_id = $wpdb->get_var( $wpdb->prepare( "select wposts.id from $wpdb->posts wposts, $wpdb->postmeta wpostmeta where wposts.id = wpostmeta.post_id and wpostmeta.meta_key = '_wp_attached_file' and wpostmeta.meta_value = '%s' and wposts.post_type = 'attachment'", $attachment_url ) );

        }

        return $attachment_id;
    }
}


function removefileextension($filename)
{
    $ext = getfileextension($filename);
    return str_replace($ext, '', $filename);
}

function getfileextension($filename)
{
    return substr($filename, strrpos($filename, '.') + 1);
}

function get_alt($id)
{
    return get_post_meta(intval($id), '_wp_attachment_image_alt', true);
}

/**
 * ------ get static image markup
 * @deprecated
 */
function static_media($name, $alt = false, $classes = false, $highdensity = false, $optimized = true)
{
    $ext = getfileextension($name);
    $alt = ($alt ? $alt : removefileextension($name));
    $dir = ($optimized ? '/dist' : '/resources/assets');

    if ($ext == 'svg') {
        $assetspath = get_bloginfo('stylesheet_directory') . $dir . '/vectors/';
    } else {
        $assetspath = get_bloginfo('stylesheet_directory') . $dir . '/images/';
    }

    echo sprintf('<img %1$s src="%2$s%3$s" alt="%4$s" %5$s>',
        ($classes ? 'class="' . $classes . '"' : ''),
        $assetspath,
        $name,
        $alt,
        ($highdensity ? 'srcset="' . $assetspath . $name . ' 1x,' . $assetspath . '@2x/' . $name . ' 2x"' : '')
    );
}

/**
 * ------ slugify text
 */
function slugify($text, $spacer = '-') {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', $spacer, $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

/**
 * ------ listify text
 */
function listify($text)
{
    $newtext = slugify($text);

    // remove duplicate -
    $list = preg_replace('[-]', ',', $newtext);

    return $list;
}


/**
 * ------ returns info select options
 *
 *  requires list of wp objects
 *  ( to refactor !)
 */

function get_select_options($list)
{
    $options = '';

    foreach ($list as $listelement) {

        $options .= '<option value="' . $listelement->slug . '">' . ucfirst($listelement->name) . '</option>';
    }


    return $options;
}


/**
 *  ----------- get current term
 *
 * get the current term which archive is being displayed
 * good for categories, tags and custom taxonomies
 *
 */
function getcurrentterm()
{
    if (!is_category() && !is_tag() && !is_tax()) {
        return false;
    }

    $term_slug = get_query_var('term');
    $taxonomyname = get_query_var('taxonomy');
    return get_term_by('slug', $term_slug, $taxonomyname);
}


/**
 *  ----------- find numbers - format numeric values
 *
 * separates numeric values from text values putting them
 * into span tags with the class 'num'
 *
 */

function findnumbers($str)
{
    $newstring = $str;

    preg_match_all('!\d+!', $str, $matches);

    $count = 0;

    foreach ($matches[0] as $num) {
        if ($count <= 0) {
            $formattednum = '<span class="num">' . $num . '</span>';
            $newstring = str_replace($num, $formattednum, $newstring, $count);
        }
    }

    return $newstring;
}

/**
 *
 *
 * language switcher for wpml
 *
 *
 */

function get_language_switcher() {
	$langswitcher = '<ul class="lang-switcher list">';
	$langs = apply_filters('wpml_active_languages', null, array());

    foreach ($langs as $lang) {
        $langswitcher .= '<li class="lang-switch ' . ($lang['active'] !== 0 ? 'lang-current' : '') . '">';
        $langswitcher .= $lang['active'] !== 0 ? $lang['language_code'] : '<a href="' . $lang['url'] . '">' . $lang['language_code'] . '</a>';
        $langswitcher .= '</li>';
    }

    $langswitcher .= '</ul>';
    return $langswitcher;
}

if (!function_exists('has_translation')){
    function has_translation() {
        $langs = apply_filters('wpml_active_languages', null, array());
        return count($langs) > 1;
    }
}

/**
 * pager hack
 */

function start_pager_hack($queryargs)
{
    global $wp_query;
    global $temp_query;

    $paged = (get_query_var('paged') ? get_query_var('paged') : 1);
    $queryargs['paged'] = $paged;

    $currentquery = new wp_query($queryargs);
    $temp_query = $wp_query;
    $wp_query = null;
    $wp_query = $currentquery;
}

function end_pager_hack()
{
    global $wp_query;
    global $temp_query;

    $wp_query = null;
    $wp_query = $temp_query;
}

/**
 * function to order posts by terms order
 * (not working with hierarchical terms or multiple terms assignment... yet)
 */
function match_terms_sort($postlist, $termslist)
{
    $reorderedposts = array();
    $newlist = $postlist;

    foreach ($termslist as $term) {
        foreach ($newlist as $key => $value) {
            if (has_term($term->term_id, 'category', $value)) {
                $reorderedposts[] = $value;
                unset($newlist[$key]);
            };
        }
    }

    return $reorderedposts;
}

/*
 * function to format phone numbers
 */
function format_phone($phone_num){
    return preg_replace('/[^0-9,]|,[0-9]*$/','',$phone_num);
}


/*
 * function to format urls to urns
 */
function format_urn($url){
    if (!empty($url)) {
        return str_replace( array( 'http://', 'https://' ), '', $url );
    }
}

/*
 * functions to get editing page
 */
function parse_editing_page_args($entity_type, $queried_obj) {

    switch($entity_type) {
        case 'category':
            $key = 'editing_page';
            $value = (string)$queried_obj->term_id;
            break;
        case 'archive':
            $key = 'editing_page_pt';
            $value = $queried_obj;
            break;
        default:
            $key = 'editing_page';
            $value = 0;
    }
    return array(
        'post_type' => 'page',
        'meta_key' => $key,
        'meta_value' => $value,
        'suppress_filters' => 0
    );
}

function get_editing_page_id($queried_obj, $entity_type) {
    $args = parse_editing_page_args($entity_type, $queried_obj);
    $page = get_posts($args);
    return $page[0];
}

/*
 * TRANSLATE ACF FIELD
 * Needs WPML and ACF to work
 */
if(!function_exists('get_field_name_translation')) {
    function get_field_name_translation($field_name, $prepend = false) {
        global $sitepress;
        if (ICL_LANGUAGE_CODE === $sitepress->get_default_language()){
            return $field_name;
        } else {
            return $prepend ? ICL_LANGUAGE_CODE.'_'.$field_name : $field_name.'_'.ICL_LANGUAGE_CODE;
        }
    }
}

if(!function_exists('get_translated_field')) {
    function get_translated_field( $selector, $post_id = false, $format_value = true ) {
        $field_name = get_field_name_translation($selector);
        return get_field($field_name, $post_id, $format_value);
    }
}

if(!function_exists('the_translated_field')) {
    function the_translated_field( $selector, $post_id = false, $format_value = true ) {
        $field_name = get_field_name_translation($selector);
        return the_field($field_name, $post_id, $format_value);
    }
}

if (!function_exists('get_custom_image_info')) {
    function get_custom_image_info($src) {
        $image_info = array($src);
        @list( $width, $height ) = getimagesize($src);

        if ( $src && $width && $height ) {
            $image_info = array( $src, $width, $height );
        }
        return $image_info;
    }
}

/**
 * CALL IN DIFFERENT LANGUAGE
 * (hack for gettext calls in specific language)
 */
if(!function_exists('call_in_different_language')) {
    function call_in_different_language($callback, $lang){
        global $sitepress;
        $current_language = $sitepress->get_current_language();
        $sitepress->switch_lang($lang);
        call_user_func($callback);
        $sitepress->switch_lang($current_language);
    }
}
