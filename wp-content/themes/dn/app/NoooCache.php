<?php
if(!class_exists('NoooCache')) {
	class NoooCache
	{
		private static $_instance = null;

		private function getCacheFile($key)
		{
			return $this->get_cache_path()."/" . $key;
		}

		private function get_cache_path()
		{
			return get_template_directory(). "/cache";
		}

		/**
		 * @return NoooCache
		 */
		public static function getInstance()
		{
			if(NoooCache::$_instance == null)
				NoooCache::$_instance = new NoooCache();
			return NoooCache::$_instance;
		}

		/**
		 * Cache constructor.
		 */
		private function __construct()
		{}

		public function saveString($key, $value)
		{
			file_put_contents($this->getCacheFile($key),$value);
		}

		public function getString($key)
		{
			$file = $this->getCacheFile($key);
			if(file_exists($file))
				return file_get_contents($file);
			else
				return null;
		}
	}
}