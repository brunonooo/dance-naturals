<?php
/**
 * Template Name: Configuratore
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();

if (is_user_logged_in()) {
    echo '<div class="admin-actions-wrapper"><button id="vpcos-download-previews" class="button button-secondary button-large" style="position: fixed; bottom: 5rem; left: 5rem; z-index:99;"><span class="dashicons dashicons-download"></span>'. __('Salva e scarica le anteprime', 'NOOO') .'</button>';
}

Timber::render( array( 'configurator.twig' ), $context );
