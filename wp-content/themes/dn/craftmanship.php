<?php
/**
 * Template Name: Artigianalita
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();

Timber::render('craftmanship.twig' , $context );
