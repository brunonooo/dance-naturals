<?php
$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['scrolling_text'] = __('The shape of style is yours', THEME_CONTEXT);

$context['woman_shoes'] = Timber::get_posts(array(
	'post_type' => 'product',
	'posts_per_page' => 8,
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'slug',
			'terms' =>  array('scarpe-da-donna'),
		),
		array(
			'taxonomy' => 'showcase',
			'field' => 'slug',
			'terms' => __('homepage', THEME_SLUG.'_slugs'),
		)
	),
	'suppress_filters' => false,
));

$context['woman_shoes_link'] = get_term_link(__('scarpe-da-donna', THEME_SLUG.'_slugs'), 'product_cat');

$context['man_shoes'] = Timber::get_posts(array(
	'post_type' => 'product',
	'posts_per_page' => 8,
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'slug',
			'terms'    => array('scarpe-da-uomo'),
		),
		array(
			'taxonomy' => 'showcase',
			'field' => 'slug',
			'terms' => __('homepage', THEME_SLUG.'_slugs'),
		)
	),
	'suppress_filters' => false,
));

$context['man_shoes_link'] = get_term_link(__('scarpe-da-uomo', THEME_SLUG.'_slugs'), 'product_cat');

$context['is_front_page'] = true;
/**
 * REMOVING PRICE FROM CARDS
 */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

Timber::render( array( 'front-page.twig' ), $context );
