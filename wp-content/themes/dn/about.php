<?php
/**
 * Template Name: About
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['craftmanship_url'] = get_the_permalink(twig_translate_id(2672, 'page'));
$context['dance_shoes_url'] = "https://www.dancenaturals.it/";

Timber::render('about.twig' , $context );
