<?php

$context = Timber::get_context();
$context['post'] = new Timber\Post(twig_translate_id(15402, 'page'));
$context['posts'] = Timber::get_posts();

Timber::render('blog.twig' , $context );
