<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_template_directory() . '/static/no-timber.html';
	});

	return;
}

Timber::$dirname = array('templates', 'views');

require_once 'app/ManifestInterface.php';
require_once 'app/JsonManifest.php';
require_once 'app/helpers.php';
require_once 'app/theme-config.php';
require_once 'app/resources.php';
require_once 'app/theme-components.php';
require_once 'app/nooo-precart/nooo-precart.php';
require_once 'app/nooo-accessories/nooo-accessories.php';
require_once 'app/woo-functions.php';

// CONFIGURATORE
require_once 'visual-product-configurator-extension/vpc-extension.php';
/**
 * REGISTERING CUSTOM SKIN FOR CONFIGURATOR
 */
if (!function_exists('nooo_skins_register')) {
	function nooo_skins_register(){
		require_once get_template_directory().'/visual-product-configurator-extension/nooo-custom-skin.php';
	}
}

add_action('init', 'nooo_skins_register');

/**
 * ADDING TO TIMBER's CONTEXT
 */
add_filter('timber/context', 'add_to_context');

function add_to_context($data){
	// CUSTOM HELPERS
	$data['current_year'] = current_time('Y');
	$data['lang'] = ICL_LANGUAGE_CODE;
	$data['isDev'] = defined( 'ENVIROMENT' ) && ENVIROMENT == 'DEV' ? 'true' : 'false';
	$data['options'] = get_fields('options');
	$data['THEME_CONTEXT'] = THEME_CONTEXT;
	$data['THEME_SLUG'] = THEME_SLUG;
	$data['configurator_link'] = get_post_type_archive_link( 'product' );
	$data['cart_link'] = wc_get_cart_url();
	$data['cart_items_count'] = WC()->cart->get_cart_contents_count();
	$data['is_checkout'] = is_checkout();
	$data['is_cart'] = is_cart();
	$data['login_link'] = get_permalink( wc_get_page_id( 'myaccount' ) );
	$data['user_logged_in'] = is_user_logged_in();
	$data['user_is_shop_manager'] = current_user_can('manage_woocommerce');
	$data['parent_link'] = get_template_directory_uri();

	// COMPONENT'
	$data['main_menu'] = new TimberMenu('main-menu');
	$data['secondary_menu'] = new TimberMenu('secondary-menu');
	$data['logo'] = file_get_contents( get_template_directory().'/assets/vectors/logo-dn.svg');
	$data['cart_icon'] = file_get_contents( get_template_directory().'/assets/vectors/cart.svg');
	$data['edit_icon'] = file_get_contents( get_template_directory().'/assets/vectors/edit.svg');
	$data['close_icon'] = file_get_contents( get_template_directory().'/assets/vectors/close.svg');
	$data['info_icon'] = file_get_contents( get_template_directory().'/assets/vectors/info.svg');
	$data['tick'] = file_get_contents( get_template_directory().'/assets/vectors/tick.svg');
	$data['contact_form'] = do_shortcode(__('[contact-form-7 id="20595" title="Form di Contatti"]', THEME_CONTEXT.'_forms'));
	$data['newsletter_form'] = do_shortcode(__('[contact-form-7 id="20556" title="Newsletter Form"]', THEME_CONTEXT.'_forms'));
	$data['breadcrumbs'] = do_shortcode('[wpseo_breadcrumb]');

	return $data;
}

/**
 *
 * Customizing Timber
 *
 */

add_filter('timber/twig', 'add_to_twig');

function twig_translate_id($id, $postType, $lang = null){
	return apply_filters('wpml_object_id', $id, $postType, false, $lang);
};

function add_to_twig($twig) {
	// WORDPRESS CORE
	$twig->addFunction( new Twig_SimpleFunction('absint', 'absint'));
	$twig->addFunction( new Twig_SimpleFunction('wp_nonce_field', 'wp_nonce_field'));
	$twig->addFunction( new Twig_SimpleFunction('get_term_link', 'get_term_link') );
	$twig->addFunction( new Twig_SimpleFunction('get_page_link', 'get_page_link') );

	// WOOCOMMERCE
	$twig->addFunction(new Twig_SimpleFunction('wc_print_notices', 'wc_print_notices'));
	$twig->addFunction(new Twig_SimpleFunction('custom_get_prod_price', 'custom_get_prod_price'));

	// VPC HELPERS
	$twig->addFunction(new Twig_SimpleFunction('get_configuration_url', 'get_configuration_url'));

	// CUSTOM HELPERS
	$twig->addFunction( new Twig_SimpleFunction('id_translate', 'twig_translate_id'));
	$twig->addFunction( new Twig_SimpleFunction('get_translated_field', 'get_translated_field'));
	$twig->addFunction( new Twig_SimpleFunction('get_cover_img', 'get_cover_img'));
	$twig->addFunction( new Twig_SimpleFunction('get_img', 'get_img'));
	$twig->addFunction( new Twig_SimpleFunction('language_switcher', 'get_language_switcher'));
	$twig->addFunction( new Twig_SimpleFunction('has_translation', 'has_translation'));
	$twig->addFunction( new Twig_SimpleFunction('asset_path', 'asset_path'));

	$twig->addFilter( new Twig_SimpleFilter('alt', 'get_alt'));
	$twig->addFilter( new Twig_SimpleFilter('slugify', 'slugify'));
	$twig->addFilter( new Twig_SimpleFilter('listify', 'listify'));
	$twig->addFilter( new Twig_SimpleFilter('bool', 'to_boolean'));
	$twig->addFilter( new Twig_SimpleFilter('telephone', 'format_phone'));
	$twig->addFilter( new Twig_SimpleFilter('urn', 'format_urn'));
	$twig->addFilter( new Twig_SimpleFilter('translate_field', 'get_field_name_translation'));
    $twig->addFilter( new Twig_SimpleFilter('hash', 'md5'));

	return $twig;
}

/**
 * WOOCOMMERCE - TIMBER INTEGRATION
 */
function timber_set_product( $post ) {
	global $product;

	if ( is_woocommerce() ) {
		$product = wc_get_product( $post->ID );
	}
}

/**
 *
 * REGISTERING MENUS
 *
 */
register_nav_menus( array(
	'main-menu' =>  __('Menu Principale', 'nooo'),
	'secondary-menu' =>  __('Menu Secondario', 'nooo'),
));


/*
 * REGISTERING SIDEBARS
 */
add_action( 'widgets_init', 'dn_widgets_init' );

function dn_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar - Filtri Ecommerce', THEME_CONTEXT ),
		'id' => 'shop-sidebar',
		'description' => __( 'I widgets in quest\'area verranno visualizzati nella pagina dell\'ecommerce', THEME_CONTEXT ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	));
}

/**
 *
 *  REMOVING TITLE ATTR
 *
 */
function removeTitleAttr( $menu ){
	return $menu = preg_replace('/ title=\"(.*?)\"/', '', $menu );
}

/**
 *
 * ADDING THEME SUPPORT
 *
 */

add_theme_support( 'post-thumbnails', array('post', 'page', 'locations', 'sector', 'service') );
add_theme_support( 'title-tag' );
add_theme_support( 'html5' );
add_theme_support( 'yoast-seo-breadcrumbs' );

/**
 *
 * REGISTERING GMAPS API KEY FOR ACF
 *
 */

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyAcUF1RxhsIkoAH-_KN8uQeyaosxw9SQvk');
}

add_action('acf/init', 'my_acf_init');

// EXTENDING CF7 WITH CUSTOM FORM TAGS
add_action( 'wpcf7_init', 'wpcf7_add_custom_checkbox' );

function wpcf7_add_custom_checkbox() {
	wpcf7_add_form_tag(
		array( 'custom_acceptance'),
		'custom_acceptance_form_tag_handler', array( 'name-attr' => true ) );
}

function custom_acceptance_form_tag_handler ( $tag){
	if ( empty( $tag->name ) ) {
		return '';
	}

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type );

	if ( $validation_error ) {
		$class .= ' wpcf7-not-valid';
	}

	if ( $tag->has_option( 'invert' ) ) {
		$class .= ' invert';
	}

	if ( $tag->has_option( 'optional' ) ) {
		$class .= ' optional';
	}

	$atts = array(
		'class' => trim( $class ),
	);

	$item_atts = array();

	$item_atts['type'] = 'checkbox';
	$item_atts['name'] = $tag->name;
	$item_atts['value'] = '1';
	$item_atts['tabindex'] = $tag->get_option( 'tabindex', 'signed_int', true );
	$item_atts['aria-invalid'] = $validation_error ? 'true' : 'false';

	if ( $tag->has_option( 'default:on' ) ) {
		$item_atts['checked'] = 'checked';
	}

	$item_atts['class'] = $tag->get_class_option();
	$item_atts['id'] = $tag->get_id_option();

	$item_atts = wpcf7_format_atts( $item_atts );

	$content = empty( $tag->content )
		? (string) reset( $tag->values )
		: $tag->content;

	$content = trim( $content );

	$tick = file_get_contents( get_template_directory().'/assets/vectors/tick.svg');

	if ( $content ) {
		$html = sprintf(
			'<label class="wpcf7-list-item woocommerce-form__label-for-checkbox"><input %1$s /><span class="svg-wrapper">'.$tick.'</span><span class="wpcf7-list-item-label">%2$s</span></label>',
			$item_atts, $content );
	} else {
		$html = sprintf(
			'<span class="wpcf7-list-item woocommerce-form__label-for-checkbox"><input %1$s /><span class="svg-wrapper">'.$tick.'</span></span>',
			$item_atts );
	}

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><span %2$s>%3$s</span>%4$s</span>',
		sanitize_html_class( $tag->name ), $atts, $html, $validation_error );

	return $html;
}

add_filter('wpcf7_validate_custom_acceptance', 'custom_acceptance_validation_filter', 20, 2);

function custom_acceptance_validation_filter( $result, $tag ) {
	$value = $_POST[$tag->name];
	if ( !(int)$value ) {
		$result->invalidate( $tag, "You need to accept to subscribe" );
	}

	return $result;
}

function get_materials_images(){
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		$scheme = $_POST['scheme'];

		for($i = 0; $i < count($scheme); ++$i ){
			$item = $scheme[$i];
			$id = get_image_id_by_url($item['oldAttr']);
			$item['newAttr'] = wp_get_attachment_image_url($id, THEME_SLUG.'_thumb');
			$scheme[$i] = $item;
		}
		echo json_encode($scheme);
	}
	die();
}

add_action( 'wp_ajax_nopriv_get_materials_images', 'get_materials_images' );
add_action( 'wp_ajax_get_materials_images', 'get_materials_images' );

/**
 * SORT PER CATEGORY AND FILTER OUT NON CONFIGURABLES
 */

if (!function_exists('get_configurable_products_per_category')){
	function get_configurable_products_per_category($categories, $showcase_slug){
		$results = array();
		foreach ($categories as $cat) {
			// SKIP UNCATEGORIZED
			if ($cat->term_id != twig_translate_id(16, 'product_cat')) {

				$current_category_products = Timber::get_posts( array(
						'posts_per_page' => -1,
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'product_cat',
								'field' => 'slug',
								'terms' => $cat->slug,
							),
							array(
								'taxonomy' => 'showcase',
								'field' => 'slug',
								'terms' => $showcase_slug,
							)
						),
					)
				);

				$configurable_products = array();

				foreach ($current_category_products as $single_prod){
					$configurable = get_field('global_config', $single_prod->ID);
					if($configurable) {
						$configurable_products[] = $single_prod;
					};
				}

				$results[$cat->name] = $configurable_products;
			}
		}
		return $results;
	}
}

/*
 * GENERATES A BLURRY PLACEHOLDER
 */
add_filter('wp_generate_attachment_metadata','blur_me_out');
function blur_me_out($meta) {
	$file = wp_upload_dir();
	$file = trailingslashit($file['path']).$meta['sizes'][PLACEHOLDER_SIZE]['file'];
	list($orig_w, $orig_h, $orig_type) = @getimagesize($file);
	// TO REFACTOR
	$image = wp_load_image($file);

	//BLACK AND WHITE
	/*imagefilter($image, IMG_FILTER_GRAYSCALE);*/
	//BLUR
	for ($x=1; $x <=40; $x++){
		imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR, 999);
	}

	imagefilter($image, IMG_FILTER_SMOOTH,99);
	imagefilter($image, IMG_FILTER_BRIGHTNESS, 10);
	imagefilter($image, IMG_FILTER_COLORIZE, 'red');

	switch ($orig_type) {
		case IMAGETYPE_GIF:
			imagegif( $image, $file );
			break;
		case IMAGETYPE_PNG:
			imagepng( $image, $file );
			break;
		case IMAGETYPE_JPEG:
			imagejpeg( $image, $file );
			break;
	}
	return $meta;
}

// FORCING ADMIN COLOR FOR DIFFERENT WEBSITES
add_filter('get_user_option_admin_color', 'change_admin_color');
function change_admin_color() {
	if ( get_current_blog_id() == 2 ) {
		return "midnight";
	} else {
		return;
	}
}

// PROD PERMALINK LINKING TO CONFIGURATION TO USE IN CART/PRECART
function get_prod_permalink($_product, $cart_item, $cart_item_key) {
	$item_configured = isset( $cart_item['visual-product-configuration'] );

	if ( $item_configured ) {
		$config_url = vpc_get_configuration_url( $cart_item['variation_id'] );
		if ( get_option( 'permalink_structure' ) ) {
			$edit_url = $config_url . "?edit=$cart_item_key&qty=" . $cart_item['quantity'];
		} else {
			$edit_url = $config_url . "&edit=$cart_item_key&qty=" . $cart_item['quantity'];
		}
		return $edit_url;
	} else {
		return apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
	}
}
// OPTIONS PAGES
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(
		array(
			'page_title' 	=> __('DN Sconti Utente', THEME_CONTEXT.'_admin'),
			'menu_title'	=> __('DN Sconti Utente', THEME_CONTEXT.'_admin'),
			'menu_slug' 	=> 'user-discounts',
			'capability'	=> 'manage_options',
			'icon_url'   => 'dashicons-tickets-alt',
			'redirect'		=> false
		)
	);

	acf_add_options_page(
		array(
			'page_title' 	=> __('DN Moduli Globali', THEME_CONTEXT.'_admin'),
			'menu_title'	=> __('DN Moduli Globali', THEME_CONTEXT.'_admin'),
			'menu_slug' 	=> 'global-modules',
			'capability'	=> 'manage_options',
			'icon_url'   => 'dashicons-welcome-widgets-menus',
			'redirect'		=> true
		)
	);

	acf_add_options_sub_page(array(
		'page_title' 	=> __('Testi Newsletter', THEME_CONTEXT.'_admin'),
		'menu_title'	=> __('Testi Newsletter', THEME_CONTEXT.'_admin'),
		'parent_slug'	=> 'global-modules',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> __('Info Prodotti', THEME_CONTEXT.'_admin'),
		'menu_title'	=> __('Info Prodotti', THEME_CONTEXT.'_admin'),
		'parent_slug'	=> 'global-modules',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> __('Soletta', THEME_CONTEXT.'_admin'),
		'menu_title'	=> __('Soletta', THEME_CONTEXT.'_admin'),
		'parent_slug'	=> 'global-modules',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> __('Documenti Legali', THEME_CONTEXT.'_admin'),
		'menu_title'	=> __('Documenti Legali', THEME_CONTEXT.'_admin'),
		'parent_slug'	=> 'global-modules',
	));
}

// CHANGING REORDER CAPABILITY TO ALLOW STORE MANAGERS TO INTERACT WITH THE PLUGIN
if(!function_exists('allow_store_managers')) {
	function allow_store_managers($cap, $sort_data_id = false){
		return 'manage_woocommerce';
	}
	add_filter('apto_reorder_capability', 'allow_store_managers');
	add_filter('apto/wp-admin/reorder-interface/sort-view-required-capability', 'allow_store_managers', 10, 2);
}
