<?php
/**
 * Template Name: Contatti
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();

Timber::render('contact-us.twig' , $context );
