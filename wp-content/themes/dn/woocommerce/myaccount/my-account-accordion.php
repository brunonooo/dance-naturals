<?php
/**
 * My Account custom accordion
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$context = Timber::get_context();
$context['menu_items'] = wc_get_account_menu_items();

Timber::render( 'templates/woo/myaccount/my-account-accordion.twig', $context );


