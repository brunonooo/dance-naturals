<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$context = Timber::get_context();
$context['page_title'] = ( 'billing' === $load_address ) ? __( 'Billing address', 'woocommerce' ) : __( 'Shipping address', 'woocommerce' );
$context['load_address'] = $load_address;
$context['address'] = $address;

if(!function_exists('loop_print_form_fields')){
    function loop_print_form_fields($address) {
        foreach ( $address as $key => $field ) {
            if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
                $field['country'] = wc_get_post_data_by_key( $field['country_field'], $address[ $field['country_field'] ]['value'] );
            }

            custom_woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
        }
    }
}

Timber::render( 'templates/woo/myaccount/form-edit-address.twig', $context );
