<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
$context            = Timber::get_context();

do_action( 'woocommerce_before_account_orders', $has_orders );
$context['has_orders'] = $has_orders;
$context['current_page'] = $current_page;
$context['columns'] = wc_get_account_orders_columns();
$context['customer_orders'] = $customer_orders;

if(!function_exists('print_total')){
    function print_total($order, $item_count){
        /* translators: 1: formatted order total 2: total order items */
        return sprintf( _n( '%1$s <span class="badge">for %2$s item</span>', '%1$s <span class="badge">for %2$s items</span>', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count );
    }
}

if(!function_exists('get_order_date')){
    function get_order_date($order) {
        return esc_attr( $order->get_date_created()->date( 'c' ));
    }
}

Timber::render( 'templates/woo/myaccount/orders.twig', $context );
