<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$context            = Timber::get_context();
$context['enable_my_account_registration'] = get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes';
$context['username'] = (! empty( $_POST['username'] ) ) ?  esc_attr( wp_unslash( $_POST['username'] ) ) : '';
$context['password'] = ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : '';
$context['lost_password_url'] = esc_url( wp_lostpassword_url() );
$context['on_registration_do_not_generate_username'] = 'no' === get_option( 'woocommerce_registration_generate_username' );
$context['on_registration_do_not_generate_password'] =  'no' === get_option( 'woocommerce_registration_generate_password' );

wc_print_notices();

Timber::render( 'templates/woo/myaccount/form-login.twig', $context );
