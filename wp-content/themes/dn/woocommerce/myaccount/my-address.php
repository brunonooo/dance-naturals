<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['customer_id'] = get_current_user_id();
$context['shipping_enabled_and_not_only_to_billing_address'] = ! wc_ship_to_billing_address_only() && wc_shipping_enabled();

if ( $context['shipping_enabled_and_not_only_to_billing_address'] ) {
    $context['addresses'] = apply_filters( 'woocommerce_my_account_get_addresses', array(
        'billing' => __( 'Billing address', 'woocommerce' ),
        'shipping' => __( 'Shipping address', 'woocommerce' ),
    ), $context['customer_id'] );
} else {
    $context['addresses'] = apply_filters( 'woocommerce_my_account_get_addresses', array(
        'billing' => __( 'Billing address', 'woocommerce' ),
    ),  $context['customer_id'] );
}

function get_customer_address($customer_id, $name){
    $user = new WC_Customer($customer_id);

    return $user->{'get_'.$name}();
}

Timber::render( 'templates/woo/myaccount/my-address.twig', $context );
