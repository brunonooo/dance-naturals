<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

wc_print_notices();

$context            = Timber::get_context();
$context['cart_url'] = esc_url( wc_get_cart_url() );
$context['cart'] = WC()->cart->get_cart();
$context['coupons_enabled'] = wc_coupons_enabled();
$context['accessories'] = Accessories()->get_available_accessories_timber();
$context['selected_accessories'] = Accessories()->get_session_data();
$context['sandal_accessories'] = Timber::get_posts(array(
	'post_type'      => 'product',
	'posts_per_page' => -1,
	'tax_query'      => array(
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'slug',
			'terms'    => __('accessori-sandali', THEME_CONTEXT.'_slugs'),
		),
	)
));

// REMOVING CROSS SELLS
// Implementing our own cross sell
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

function print_remove_link($cart_item_key, $product_id, $_product){
	return apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
		'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;<strong>%s</strong></a>',
		esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
		__( 'Remove this item', 'woocommerce' ),
		esc_attr( $product_id ),
		esc_attr( $_product->get_sku() ),
		__('Rimuovi', THEME_CONTEXT )
	), $cart_item_key );
}

function get_prod_price($_product) {
	return WC()->cart->get_product_price( $_product );
}

function get_subtotal($_product, $cart_item) {
	return WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] );
}

Timber::render( 'templates/woo/cart/cart.twig', $context );
