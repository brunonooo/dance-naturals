<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$context = Timber::get_context();

Timber::render( 'templates/woo/cart/cart-button.twig', $context );
