<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$context            = Timber::get_context();
$context['package_name'] = $package_name;
$context['available_methods'] = $available_methods;
$context['index'] = $index;
$context['chosen_method'] = $chosen_method;
$context['customer_has_calculated_shipping'] = WC()->customer->has_calculated_shipping();
$context['no_shipping_available'] = apply_filters( 'woocommerce_cart_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
$context['no_shipping_available_no_cart'] = apply_filters( 'woocommerce_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
$context['show_package_details'] = $show_package_details;
$context['package_details'] = $package_details;
$context['show_shipping_calculator'] = ! empty( $show_shipping_calculator );
Timber::render( 'templates/woo/cart/cart-shipping.twig', $context );
