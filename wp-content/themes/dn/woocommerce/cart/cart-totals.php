<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$context            = Timber::get_context();
$context['calculated_shipping'] = ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : '';
$context['cart_totals_subtotal_html'] = WC()->cart->get_cart_subtotal();
$context['coupons'] = WC()->cart->get_coupons();
$context['needs_shipping'] = WC()->cart->needs_shipping();
$context['show_shipping'] = WC()->cart->show_shipping();
$context['fees'] = WC()->cart->get_fees();
$context['tax_enabled'] = wc_tax_enabled();
$context['prices_show_taxes'] = WC()->cart->display_prices_including_tax();
$context['taxable_address'] = WC()->customer->get_taxable_address();
$context['estimated_text'] = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() ? sprintf( ' <small>' . __( '(estimated for %s)', 'woocommerce' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )  : '';
$context['tax_totals'] = WC()->cart->get_tax_totals();
$context['tax_or_vat'] = WC()->countries->tax_or_vat();

Timber::render( 'templates/woo/cart/cart-totals.twig', $context );
