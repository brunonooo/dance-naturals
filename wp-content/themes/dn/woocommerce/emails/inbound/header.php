<!DOCTYPE html>
<html <?php language_attributes(); ?> class="dance-naturals-inbound">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
	<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr id="body_row">
			<td align="center" valign="top">
				<!-- Body -->
				<table border="0" cellpadding="0" cellspacing="0" width="800" id="template_body">
					<tr>
						<td valign="top" id="body_content">
							<!-- Content -->
							<table border="0" cellpadding="20" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<div id="body_content_inner">
											<h1><?php echo $email_heading; ?></h1>
											<p><?php printf( __( 'You have received an order from %s. The order is as follows:', 'woocommerce' ), '<strong>'.$order->get_formatted_billing_full_name().'</strong>' ); ?></p>