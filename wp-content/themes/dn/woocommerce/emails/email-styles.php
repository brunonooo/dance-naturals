<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg              = get_option( 'woocommerce_email_background_color' );
$body            = get_option( 'woocommerce_email_body_background_color' );
$base            = get_option( 'woocommerce_email_base_color' );
$base_text       = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text            = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link = wc_hex_is_light( $base ) ? $base : $base_text;
if ( wc_hex_is_light( $body ) ) {
	$link = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = wc_hex_darker( $bg, 5 );
$body_darker_10  = wc_hex_darker( $body, 5 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
?>
    #wrapper {
    background-color: <?php echo esc_attr( $bg ); ?>;
    margin: 0;
    padding: 70px 0 70px 0;
    -webkit-text-size-adjust: none !important;
    width: 100%;
    }

    #template_container {
    box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important;
    background-color: <?php echo esc_attr( $body ); ?>;
    border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
    border-radius: 3px !important;
    }

    #template_header {
    width: 100% !important;
    background-color: <?php echo esc_attr( $base ); ?>;
    background-position: center;
    background-size: cover;
    border-radius: 3px 3px 0 0 !important;
    color: <?php echo esc_attr( $base_text ); ?>;
    border-bottom: 0;
    font-weight: bold;
    line-height: 100%;
    vertical-align: middle;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    }

    #template_header h1,
    #template_header h1 a {
    color: <?php echo esc_attr( $base_text ); ?>;
    }

    #template_footer td {
    padding: 0;
    -webkit-border-radius: 6px;
    }

    #template_footer #credit {
    border:0;
    color: <?php echo esc_attr( $body ); ?>;
    font-family: Arial;
    font-size:12px;
    line-height:125%;
    text-align:center;
    padding: 0 48px 48px 48px;
    }

    #body_content {
    background-color: <?php echo esc_attr( $body ); ?>;
    }

    #footer_row {
    background-color: <?php echo $base; ?>;
    }

    #footer_row a {
    color: <?php echo $bg; ?>;
    font-weight: 700;
    }

    #body_content table td td {
    padding: 12px;
    }

    #body_content table td th {
    padding: 12px;
    }

    #body_content td ul.wc-item-meta {
    font-size: small;
    margin: 1em 0 0;
    padding: 0;
    list-style: none;
    }

    #body_content td ul.wc-item-meta li {
    margin: 0.5em 0 0;
    padding: 0;
    }

    #body_content td ul.wc-item-meta li p {
    margin: 0;
    }

    #body_content p {
    margin: 0 0 16px;
    }

    #body_content_inner {
    color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 14px;
    line-height: 150%;
    text-align: center;
    padding: 15px;
    }

    .td {
    color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    border: 1px solid <?php echo esc_attr( $body_darker_10 ); ?>;
    vertical-align: middle;
    }

    #addresses {
    margin: 20px 0 !important;
    padding: 30px !important;
    border: 1px solid gainsboro;
    }

    #addresses h2 {
    text-align: left;
    }

    .address {
    color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    line-height: 1.3;
    }

    .text {
    color: <?php echo esc_attr( $text ); ?>;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    }

    .link {
    color: <?php echo esc_attr( $base ); ?>;
    }

    #header_wrapper {
    padding: 100px 48px;
    display: block;
    text-align: center;
    }

    #header_wrapper img {
    max-width: 100px;
    }

    h1 {
    color: <?php echo esc_attr( $base ); ?>;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 30px;
    font-weight: 700;
    line-height: 150%;
    margin: 15px 0;
    text-align: center;
    }

    h2 {
    color: <?php echo esc_attr( $base ); ?>;
    display: block;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 18px;
    font-weight: bold;
    line-height: 130%;
    margin: 0 0 18px;
    text-align: center;
    }

    h3 {
    color: <?php echo esc_attr( $base ); ?>;
    display: block;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 16px;
    font-weight: bold;
    line-height: 130%;
    margin: 16px 0 8px;
    text-align: center;
    }

    a {
    color: <?php echo esc_attr( $link ); ?>;
    font-weight: normal;
    text-decoration: underline;
    }

    img {
    border: none;
    display: inline-block;
    font-size: 14px;
    font-weight: bold;
    height: auto;
    outline: none;
    text-decoration: none;
    text-transform: capitalize;
    vertical-align: middle;
    margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
    }

    .product-name {
    text-align: left;
    }

    .variation {
    margin-top: 10px;
    line-height: 1.2;
    }

    .variation p {
    margin: 0;
    display: inline;
    font-size: 80%;
    }

    .variation small {
    font-weight: 700;
    }

    .single-variation {
    margin-bottom: 5px;
    }

    .vpc-cart-options__material-preview {
    font-size: 80%;
    line-height: 1.4;
    }

    .vpc-order-config-option-title {
    margin: 5px 0;
    }

    .woocommerce-bacs-bank-details {
    background-color: <?php echo $body_darker_10 ?>;
    padding: 20px;
    margin: 20px 0;
    }
    .wc-bacs-bank-details {
    padding: 0;
    list-style: none;
    }

    .dance-naturals-inbound h1 {
    font-size: 22px;
    }
    .dance-naturals-inbound #body_content_inner {
    font-size: 16px;
    }
    .vpc-cart-config-image {
    overflow: hidden;
    }
    .vpc-cart-config-image img {
    float: left;
    }

    .order_item > td {
    width: 80%;
    }

    .vpc-order-config-option-title {
    font-size: 16px;
    border-top: 1px solid #ececec;
    padding-top: 15px;
    margin-top: 15px;
    }

    .vpc-cart-options-container {
    border-bottom: 1px solid #ececec;
    padding-bottom: 15px;
    margin-bottom: 15px;
    font-size: 14px;
    }

    .vpc-cart-options-container.vpc-cart-options__material-preview > div {
    display: inline-block;
    width: 32%;
    margin: 4px 0;
    }

    .variation {
    display: none;
    }

    .wc-item-meta li {
    display: inline-block;
    margin-right: 10px !important;
    font-size: 14px;
    }

    .wc-item-meta li p {
    display: inline-block;
    }


<?php
