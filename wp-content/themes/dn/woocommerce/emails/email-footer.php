<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
</div>
</td>
</tr>
</table>
<!-- End Content -->
</td>
</tr>
</table>
<!-- End Body -->
</td>
</tr>
<tr id="footer_row">
    <td align="center" valign="top">
        <!-- Footer -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
            <tbody class="mcnFollowBlockOuter">
            <tr>
                <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td align="center" style="padding-left:9px;padding-right:9px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                                    <tbody><tr>
                                        <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tbody><tr>
                                                    <td align="center" valign="top">
                                                        <!--[if mso]>
                                                        <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        <td align="center" valign="top">
                                                        <![endif]-->


                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                            <tbody><tr>
                                                                <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                        <tbody><tr>
                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                    <tbody><tr>

                                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                            <a href="https://www.facebook.com/DanceNaturalsOfficial/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                        </td>


                                                                                    </tr>
                                                                                    </tbody></table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                        <!--[if mso]>
                                                        </td>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        <td align="center" valign="top">
                                                        <![endif]-->


                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                            <tbody><tr>
                                                                <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                        <tbody><tr>
                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                    <tbody><tr>

                                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                            <a href="https://www.instagram.com/dancenaturalsofficial/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-instagram-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                        </td>


                                                                                    </tr>
                                                                                    </tbody></table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                        <!--[if mso]>
                                                        </td>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        <td align="center" valign="top">
                                                        <![endif]-->


                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                            <tbody><tr>
                                                                <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                        <tbody><tr>
                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                    <tbody><tr>

                                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                            <a href="http://dnt2.noooserver.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                        </td>


                                                                                    </tr>
                                                                                    </tbody></table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>

                                                        <!--[if mso]>
                                                        </td>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        </tbody></table>

                </td>
            </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
            <tbody class="mcnDividerBlockOuter">
            <tr>
                <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                    <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="70%" style="min-width: 70%;margin: 0 auto;border-top: 2px solid #FFFFFF;">
                        <tbody><tr>
                            <td>
                                <span></span>
                            </td>
                        </tr>
                        </tbody></table>
                    <!--
									<td class="mcnDividerBlockInner" style="padding: 18px;">
									<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
					-->
                </td>
            </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
            <tr>
                <td valign="top">
                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2" valign="middle" id="credit">
                                <em>Copyright Dance Naturals® 2018 di Calzaturificio Lady Laura srl<br>
                                    P.iva: 03707920280 -&nbsp;</em>Email:&nbsp;<a href="mailto:info@dancenaturals.it">info@dancenaturals.it</a>&nbsp;- Tel.<a href="tel:+390498935140">049 893 5140</a><br>
                                <br>
								<?php echo wpautop( wp_kses_post( wptexturize( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) ) ) ); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- End Footer -->
    </td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</body>
</html>
