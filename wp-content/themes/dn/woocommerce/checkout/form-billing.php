<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$context            = Timber::get_context();
$context['ship_to_billing_address_only'] = wc_ship_to_billing_address_only() && WC()->cart->needs_shipping();
$context['checkout'] = $checkout;
$context['registration_on_checkout'] = ! is_user_logged_in() && $checkout->is_registration_enabled();
$context['checked'] = checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true );

Timber::render( 'templates/woo/checkout/form-billing.twig', $context );
