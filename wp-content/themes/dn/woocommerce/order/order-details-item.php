<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

$context            = Timber::get_context();
$context['order']           = $order;
$context['item']           = $item;
$context['item_id']           = $item_id;
$context['product']           = $product;
$context['is_visible'] = $product && $product->is_visible();
$context['product_permalink'] = apply_filters( 'woocommerce_order_item_permalink', $context['is_visible'] ? $product->get_permalink( $item ) : '', $item, $order );
$context['order_item_name'] = apply_filters( 'woocommerce_order_item_name', $context['product_permalink'] ? sprintf( '<a href="%s">%s</a>', $context['product_permalink'], $item->get_name() ) : $item->get_name(), $item, $context['is_visible'] );
$context['order_item_quantity'] = apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item->get_quantity() ) . '</strong>', $item );
$context['formatted_subtotal'] = $order->get_formatted_line_subtotal( $item );
$context['show_purchase_note']           = $show_purchase_note;
$context['purchase_note']           = $purchase_note;
$context['formatted_purchase_note'] = wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) );

Timber::render( 'templates/woo/order/order-details-item.twig', $context );
