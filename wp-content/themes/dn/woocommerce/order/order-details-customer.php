<?php
/**
 * Order Customer Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-customer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$context            = Timber::get_context();
$context['order'] = $order;
$context['show_shipping'] = ! wc_ship_to_billing_address_only() && $order->needs_shipping_address();
$context['billing_address'] = wp_kses_post($order->get_address('billing'));
$context['shipping_address'] = wp_kses_post($order->get_address('shipping'));

Timber::render( 'templates/woo/order/order-details-customer.twig', $context );
