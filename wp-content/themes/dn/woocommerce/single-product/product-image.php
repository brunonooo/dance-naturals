<?php
/**
 * Single Product Image
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$context            = Timber::get_context();
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$context['wrapper_columns'] = esc_attr( $columns );

$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . ( has_post_thumbnail() ? 'with-images' : 'without-images' ),
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
));

$context['wrapper_classes'] = esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) );

/**
 * MAIN VIEW MARKUP
 */
$post_thumbnail_id = $product->get_image_id();

/**
 * CUSTOMIZED wc_get_gallery_image_html from "dance-naturals/wp-content/plugins/woocommerce/includes/wc-template-functions.php"
 * Get HTML for a gallery image.
 */
function nooo_get_gallery_image_html( $attachment_id, $main_image = false, $image_num = 0 ) {
	$is_precart = Precart()->is_active();
	$flexslider        = (bool) apply_filters( 'woocommerce_single_product_flexslider_enabled', get_theme_support( 'wc-product-gallery-slider' ) );
	$gallery_thumbnail = wc_get_image_size( 'gallery_thumbnail' );
	$thumbnail_size    = apply_filters( 'woocommerce_gallery_thumbnail_size', array( $gallery_thumbnail['width'], $gallery_thumbnail['height'] ) );
	$image_size        = apply_filters( 'woocommerce_gallery_image_size', $flexslider || $main_image ? 'woocommerce_single' : $thumbnail_size );
	$full_size         = apply_filters( 'woocommerce_gallery_full_size', apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' ) );
	$thumbnail_src     = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
	$full_src          = $is_precart ? Precart()->get_precart_image_info($image_num) : wp_get_attachment_image_src( $attachment_id, $full_size );
	$image             = $is_precart ? Precart()->precart_gallery_image_html($attachment_id, $image_num) : wp_get_attachment_image( $attachment_id, $image_size, false, array(
		'title'                   => get_post_field( 'post_title', $attachment_id ),
		'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
		'data-src'                => $full_src[0],
		'data-large_image'        => $full_src[0],
		'data-large_image_width'  => $full_src[1],
		'data-large_image_height' => $full_src[2],
		'class'                   => $main_image ? 'w-100 wp-post-image' : '',
	) );

	if($is_precart){
		$html = '<div data-thumb="' . esc_url( $full_src[0] ) . '" class="woocommerce-product-gallery__image col-xs-12 mb-4 pr-md-0 pl-md-0"><a href="' . esc_url( $full_src[0] ) . '">' . $image . '</a></div>';
	}else{
		$html = '<div data-thumb="' . esc_url( $thumbnail_src[0] ) . '" class="woocommerce-product-gallery__image col-xs-12 mb-4 pr-md-0 pl-md-0"><a href="' . esc_url( $full_src[0] ) . '">' . $image . '</a></div>';
	}

	return $html;
}

if ( has_post_thumbnail() ) {
	$html  = nooo_get_gallery_image_html( $post_thumbnail_id, true );
} else {
	$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
	$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
	$html .= '</div>';
}

$context['main_view'] = apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );

/**
 * OTHER VIEWS MARKUP
 */
$extraViewsHtml = '';

$attachment_ids = $product->get_gallery_image_ids();

// USED IN PRECART CONTENT
$loops = Precart()->is_active() ?  1 : 0;

if ( $attachment_ids && has_post_thumbnail() ) {
	foreach ( $attachment_ids as $attachment_id ) {
		$extraViewsHtml .= apply_filters( 'woocommerce_single_product_image_thumbnail_html', nooo_get_gallery_image_html( $attachment_id, true, $loops ), $attachment_id);
		if (Precart()->is_active()){++$loops;};
	}
}

$context['extra_views'] = $extraViewsHtml;

Timber::render( 'templates/woo/single-product/product-image.twig', $context );


