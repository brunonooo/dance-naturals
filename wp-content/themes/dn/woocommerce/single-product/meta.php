<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
$context            = Timber::get_context();
$context['post']    = Timber::get_post();

$sku = $product->get_sku();
$context['sku_enabled'] = wc_product_sku_enabled() && ( $sku || $product->is_type( 'variable' ) );
$context['sku'] =  $sku ? $sku : esc_html__( 'N/A', 'woocommerce' );
$context['category_list'] = wc_get_product_category_list( $product->get_id(), ', ', '', '</span>' );
$context['n_category_text'] = _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' );
$context['tag_list'] = wc_get_product_tag_list( $product->get_id(), ', ', '', '' );
$context['n_tag_text'] = _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' );

Timber::render( 'templates/woo/single-product/meta.twig', $context );