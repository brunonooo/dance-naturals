<?php
/**
 * Simple product add to cart
 *
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
    return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

/**
 * GET CURRENT PRODUCT QUANTITY
 */
function get_quantity_input($min = false, $max = false){
    global $product;

    woocommerce_quantity_input( array(
        'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $min ? $min : $product->get_min_purchase_quantity(), $product ),
        'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $max ? $max : $product->get_max_purchase_quantity(), $product ),
        'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
    ));
}

if ( $product->is_in_stock() ) {
    $context            = Timber::get_context();
    /*$context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );*/
    $context['post']    = Timber::get_post();
    $context['add_to_cart_text'] = $product->get_price_html();

    Timber::render( 'templates/woo/single-product/add-to-cart/simple.twig', $context );
}
