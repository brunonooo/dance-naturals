<?php
/**
 * Variable product add to cart
 */

defined( 'ABSPATH' ) || exit;

global $product;

$context            = Timber::get_context();
/*$context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );*/
$context['post']    = Timber::get_post();
$context['add_to_cart'] = $product->single_add_to_cart_text();
ksort($attributes);

// SUPER STATIC SORT TO MAKE IT LOOK BEAUTIFUL ON FRONTEND
if(isset($attributes['pa_calzata'])){
    $calzata = $attributes['pa_calzata'];
    unset($attributes['pa_calzata']);
    $attributes['pa_calzata'] = $calzata;
};
$context['attributes'] = $attributes;

$keys = array_keys( $attributes );
$context['last_attr_name'] = end( $keys );
$context['available_variations'] = htmlspecialchars( wp_json_encode( $available_variations ) );

function get_quantity_input($min = false, $max = false){
    global $product;

    woocommerce_quantity_input( array(
        'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $min ? $min : $product->get_min_purchase_quantity(), $product ),
        'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $max ? $max : $product->get_max_purchase_quantity(), $product ),
        'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
    ));
}

function get_attr_info($option, $attribute_name, $i) {
    $attr = get_term_by('slug', $option, $attribute_name);
    $style = 'style="background-image: url(\''.get_field('material_image', $attribute_name.'_'.$attr->term_id).'\')"';
    $check_first = $i === 0 ? 'checked' : '';
    $checked = is_attribute_selected($option, $attribute_name) ? 'checked' : $check_first;

    return array($attr, $style, $checked);
}

function dropdown_variation($attribute_name, $options, $class = ''){
    global $product;
    $selected = get_selected_attribute($attribute_name);
    wc_custom_dropdown_variation_attribute_options ( array(
        'class' => $class,
        'options'   => $options,
        'attribute' => $attribute_name,
        'product'   => $product,
        'selected'  => $selected,
    ) );
}

function radio_variation($attribute_name, $options){
    $radio = '<div class="d-flex flex-row flex-wrap">';
    $i = 0;
    foreach ($options as $option) {
        list($attr, $style, $checked) = get_attr_info($option, $attribute_name, $i);
        $radio .= '<div class="radio-wrapper mr-3 mb-3 '.$option.'-radio-wrapper '.$checked.'">';
        $radio .= '<div class="radio-info-popover flex-row mt-4"><div class="image-'.$attribute_name.'" '.$style.'></div><div class="radio-info-popover__description p-4"><h4 class="font-weight-bold mb-2">'.$attr->name.'</h4><p>'.$attr->description.'</p></div></div>';
        $radio .= '<button class="image-'.$attribute_name.'" '.$style.' data-variation-value="'.$option.'">'.$option.'</button>';
        $radio .= '</div>';
        ++$i;
    }
    $radio .= '</div>';
    return $radio;
}

function modal_variation($attribute_name, $options) {
    $modal_content = '<div class="variations-modal-content d-flex flex-column pb-3"><h4 class="pre-title mb-3">'.wc_attribute_label($attribute_name).'</h4><div class="variations-modal-inner d-flex flex-row flex-wrap">';
    $i = 0;
    foreach ($options as $option) {
        list($attr, $style, $checked) = get_attr_info($option, $attribute_name, $i);
        $modal_content .= '<div class="modal-wrapper p-3 my-2 '.call_in_different_language(__($option, THEME_CONTEXT.'_attributi'), 'it').'-modal-wrapper '.$checked.' flex flex-column align-items-center" data-variation-value="'.$option.'" data-variation-reference="'.$attribute_name.'">';
        $modal_content .= '<div class="image-color-el" '.$style.'></div><h4 class="my-2 text-center">'.$attr->name.'</h4>';
        $modal_content .= '</div>';// closing modal-wrapper
        ++$i;
    }
    $modal_content .= '</div></div>'; // closing variation-modal-content and variation-modal-inner
    return $modal_content;
}

// TO REFACTOR
function get_attribute_img($attribute_name) {
    $images = array(
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('rocchetto', THEME_CONTEXT.'_attributi')                                                     => 'images/tacchi/rocchetto.png',
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('spillo', THEME_CONTEXT.'_attributi')                                                        => 'images/tacchi/spillo.png',
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('squadrato', THEME_CONTEXT.'_attributi')                                                     => 'images/tacchi/squadrato.png',
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('metallo', THEME_CONTEXT.'_attributi')                                                       => 'images/tacchi/metallo.png',
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('moderno', THEME_CONTEXT.'_attributi')                                                       => 'images/tacchi/grosso.png',
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('oro', THEME_CONTEXT.'_attributi')                                                           => 'images/tacchi/oro.png',
        'pa_'.__('design-tacco', THEME_CONTEXT.'_attributi').'/'.__('antracite', THEME_CONTEXT.'_attributi')                                                     => 'images/tacchi/antracite.png',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('nero-2', THEME_CONTEXT.'_attributi')               => 'images/colori-materiali/raso/nero.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('bronze-2', THEME_CONTEXT.'_attributi')             => 'images/colori-materiali/raso/bronze.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('flesh-2', THEME_CONTEXT.'_attributi')              => 'images/colori-materiali/raso/flesh.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('brown', THEME_CONTEXT.'_attributi')                => 'images/colori-materiali/raso/brown.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('bianco-en', THEME_CONTEXT.'_attributi')               => 'images/colori-materiali/raso/bianco.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('bianco', THEME_CONTEXT.'_attributi')               => 'images/colori-materiali/raso/bianco.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('laminato-argento', THEME_CONTEXT.'_attributi')     => 'images/colori-materiali/nappa/laminato-argento.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('laminato-platino', THEME_CONTEXT.'_attributi')     => 'images/colori-materiali/nappa/laminato-platino.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('leopardato', THEME_CONTEXT.'_attributi')           => 'images/colori-materiali/camoscio/leopardato.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('fish-argento', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/camoscio/fish-argento.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('fish-nero', THEME_CONTEXT.'_attributi')            => 'images/colori-materiali/camoscio/fish-nero.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('fish-platino', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/camoscio/fish-platino.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('camoscio-nero', THEME_CONTEXT.'_attributi')        => 'images/colori-materiali/camoscio/nero.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('casmoscio-tan', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/camoscio/tan.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('capretto-tan', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/capretto/tan.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('vernice-nera', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/vernici/vernice-nera.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('nappa-nera', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/nappa/nappa-nera.jpg',
        'pa_'.__('modifica-materiale', THEME_CONTEXT.'_attributi').'/'.__('nero', THEME_CONTEXT.'_attributi')         => 'images/colori-materiali/nappa/nero.jpg',
    );
    return asset_path($images[$attribute_name]);
}

if(isset( $_SESSION['product_note'])) {
    $context['note_value'] = $_SESSION['product_note']['note_id'] === (string)$context['post']->ID  ? $_SESSION['product_note']['content'] : '';
    unset($_SESSION['product_note']);
} else {
    $context['note_value'] = '';
}

if (Precart()->is_active()) {
    Timber::render( 'templates/woo/single-product/add-to-cart/variable-precart.twig', $context );
} else {
    Timber::render( 'templates/woo/single-product/add-to-cart/variable.twig', $context );
}
