<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

$context            = Timber::get_context();
/*$context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );*/
$context['post']    = Timber::get_post();
$context['button_text'] = __('Aggiungi al carrello', THEME_CONTEXT );

if (Precart()->is_active()){
	$context['button_text'] = __('Acquista', THEME_CONTEXT );
	$context['old_key'] = Precart()->get_key();
}

Timber::render( 'templates/woo/single-product/add-to-cart/variation-add-to-cart-button.twig', $context );
