<?php
function remove_unnecessary_from_summary(){
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	if(Precart()->is_active()){
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	}
}

add_action('woocommerce_before_single_product_summary', 'remove_unnecessary_from_summary');

$context            = Timber::get_context();
$context['post']    = $context['current_prod'] = Timber::get_post();
$product            = wc_get_product( $context['post']->ID );
$context['product'] = $product;
$context['upsells'] = get_upsells($product->get_id());

$terms = get_the_terms( $context['post']->ID, 'product_cat' );
$context["product_terms"] = array_map(function($item){
    return $item->term_id;
}, $terms);

// GETTING VALUE FROM SESSION FOR PRECART (if available)
if (isset($_POST['precart_reset'])) {
	Precart()->delete_session_data();
}

if (Precart()->is_active()) {
	$context['cart'] = Precart()->get_cart();
	$context['current_prod'] = Precart()->get_product();
	$context['views'] = Precart()->get_config_views();
	$context['config_price'] = Precart()->get_config_price();
	Timber::render( 'templates/precart.twig', $context );
} else {
	Timber::render( 'templates/woo/single-product.twig', $context );
}

