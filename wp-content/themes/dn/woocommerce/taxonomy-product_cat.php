<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$context            = Timber::get_context();

if ( is_product_category() ) {
	$queried_object = get_queried_object();
	$term_id = $queried_object->term_id;
	$context['category'] = get_term( $term_id, 'product_cat' );
	$context['current_cat'] = $context['category']->name;
	$context['title'] = single_term_title( '', false );

	remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

	remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
	remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

	remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

	remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
}

if(empty($context['category']->parent)){
	$context['cats'][$queried_object->name] = get_term_link($queried_object->term_id);
} else {
	$context['children'] = get_terms( array(
		'include' => get_term_children($context['category']->parent, 'product_cat')
	));

	foreach ($context['children'] as $cat) {
		// SKIP UNCATEGORIZED
		if ($cat->term_id != twig_translate_id(16, 'product_cat')) {
			$context['cats'][$cat->name] = get_term_link($cat->term_id);
		}
	}
}

$context['post'] = new TimberPost(get_editing_page_id($context['category'], 'category'));
$context['filters_sidebar'] = Timber::get_widgets( 'shop-sidebar' );
$posts = Timber::get_posts(array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => $context['category']->slug
		),
		array(
			'taxonomy' => 'showcase',
			'field' => 'slug',
			'terms' => __('ecommerce', THEME_SLUG.'_slugs'),
		)
	)
));
$context['products'] = $posts;

/**
 * REMOVING PRICE FROM CARDs
 */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

Timber::render( array( 'templates/woo/taxonomy-product_cat.twig' ), $context );
