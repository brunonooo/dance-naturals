<?php
$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['blog_url'] = get_the_permalink(9359); //check blog id

Timber::render('single.twig' , $context );
