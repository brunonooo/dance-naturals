<?php
/**
 * Template Name: User Related
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['customer_id'] = get_current_user_id();

if ( $context['user_logged_in'] ){  // RICORDA CHECK SU ENDPOINT
    if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
        $context['addresses'] = apply_filters( 'woocommerce_my_account_get_addresses', array(
            'billing',
            'shipping',
        ), $context['customer_id'] );
    } else {
        $context['addresses'] = apply_filters( 'woocommerce_my_account_get_addresses', array(
            'billing',
        ),  $context['customer_id'] );
    }
}

Timber::render( array( 'user-related-page.twig' ), $context );
