<?php
/**
 * CONFIGURATOR OVERRIDES
 */

require_once __DIR__ . '/../app/NoooCache.php';
if(!function_exists('nooo_price_button')){
    function nooo_price_button() {
        if (is_admin() && !is_ajax())
            return;

        $price_container_html='
<div id="vpc-price-container">
    <span id="vpc-price"></span>
</div>';
        $price_container_html=  apply_filters("vpc_config_price_container",$price_container_html);

        echo $price_container_html;
    }
}

if(!function_exists('nooo_get_action_buttons')) {
    function nooo_get_action_buttons( $product_id ) {
        if ( ! $product_id ) {
            return;
        }
        $buttons = vpc_get_action_buttons_arr( $product_id );
        ob_start();
        ?>
        <div class="vpc-action-buttons">
            <div class="vpc-action-buttons__inner w-100">
                <div class="choice-actions">
                    <button id="vpc-go-back" class="vpc-go-back btn ls-1 w-100 font-weight-bold"></button>
                    <button id="vpc-update" class="vpc-update btn ls-1 w-100 font-weight-bold"></button>
                </div>
                <?php
                vpc_get_quantity_container();

                foreach ( $buttons as $button ) {
                    if ( ! isset( $button["requires_login"] ) ) {
                        $button["requires_login"] = false;
                    }
                    if ( ! isset( $button["visible_admin"] ) ) {
                        $button["visible_admin"] = true;
                    }
                    if ( ! isset( $button["attributes"] ) ) {
                        $button["attributes"] = array();
                    }

                    if ( ! is_user_logged_in() && $button["requires_login"] ) {
                        continue;
                    } else if ( is_admin() && ! is_ajax() && ! $button["visible_admin"] ) {
                        continue;
                    }
                    // Custom attribute handling
                    $custom_attributes = array();

                    foreach ( $button['attributes'] as $attribute => $attribute_value ) {
                        $custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
                    }
                    ?>
                    <button
                        id="<?php echo esc_attr( $button['id'] ); ?>"
                        class="<?php echo esc_attr( $button['class'] ); ?> single_add_to_cart_button btn btn--full ls-1 w-100"
                        <?php echo implode( ' ', $custom_attributes ); ?>
                    >
                        <?php echo nooo_price_button(); ?>
                    </button>

                    <?php
                }
                ?>
            </div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();

        return apply_filters( "vpc_action_buttons_html", $output, $product_id );
    }
}

if(!function_exists('')) {
    function find_matching_product_variation( $product, $attributes ){
        foreach( $attributes as $key => $value ) {
            if( strpos( $key, 'attribute_' ) === 0 ) {
                continue;
            }

            unset( $attributes[ $key ] );
            $attributes[ sprintf( 'attribute_%s', $key ) ] = $value;
        }

        if( class_exists('WC_Data_Store') ) {

            $data_store = WC_Data_Store::load( 'product' );
            return $data_store->find_matching_product_variation( $product, $attributes );

        } else {

            return $product->get_matching_variation( $attributes );

        }
    }
}

if(!function_exists('get_configuration_url')) {
    function get_configuration_url( $prod_id ) {

        $product = wc_get_product( $prod_id );
        $config = get_field('global_config', $prod_id); // Checks for custom global configurations field

        if ( empty($config) ) {
            $design_url = get_the_permalink( $prod_id ); // Falling back to product page if it is not configurable
        } else {
            if ( vpc_woocommerce_version_check() ) {
                $product_type = $product->product_type;
            } else {
                $product_type = $product->get_type();
            }

            /**
             * CREATING LINK FOR CONFIGURATION WITH FIRST AVAILABLE VARIATION
             * Checking for first available variation in stock to direct users to configuration page with that variation selected
             */
            if ( $product_type == "variable" ) {
                $prod_id = get_field('default_variant_id', $prod_id);
                if($prod_id === false || !is_numeric($prod_id)) {
                    $variations = $product->get_available_variations();

                    $i = 0;
                    $min = 100000;
                    $found = 0;
                    foreach ($variations as $variation) {
                        $var = wc_get_product($variation['variation_id']);
                        if ($var->is_in_stock() && $var->get_price() < $min) {
                            $found = $i;
                            $min = $var->get_price();
                        }
                        ++$i;
                    }
                    $prod_id = $variations[$found]['variation_id'];
                }
            }

            $design_url = vpc_get_configuration_url( $prod_id );
        }

        return $design_url;
    }
}
if(!function_exists('dn_fetch_img')) {
    function dn_fetch_img(){
        $views = $_REQUEST['imgList'];
        for ($i = 0; $i < count($views); ++$i){
            $images = $views[$i];
            for ($k = 0; $k < count($images); ++$k) {
                $id = get_image_id_by_url($images[$k]);
                $imgMarkup = get_img(array(
                    'id' => $id,
                    'lazy' => false,
                ));
                $images[$k] = $imgMarkup;
            }
            $views[$i] = $images;
        }

        echo json_encode($views);

        die();
    }

    add_action("wp_ajax_dn_fetch_img", "dn_fetch_img");
    add_action("wp_ajax_nopriv_dn_fetch_img", "dn_fetch_img");
}

/**
 * FUNCTION TO GET DATA FOR CART
 */
if(!function_exists('get_vpc_data')) {
    function get_vpc_data($thumbnail_code, $values, $cart_item_key) {
        global $woocommerce,$vpc_settings;
        if ($values["variation_id"])
            $product_id = $values["variation_id"];
        else
            $product_id = $values["product_id"];
        $config = get_product_config($product_id);
        //We extract the recap from the cart item key
        $recap = get_recap_from_cart_item($values);

        // CHECK FOR CONFIGURATION
        $cart_content=$woocommerce->cart->cart_contents;
        $item_content=$cart_content[$cart_item_key];
        $item_configured = isset($item_content['visual-product-configuration']);
        if($item_configured){
            $thumbnail_code .= "<div class=\"vpc-cart-config-wrapper\"><a class=\"vpc-config-trigger btn__dropdown\" role=\"button\" data-toggle=\"collapse\" href=\"#dd-".$cart_item_key."\" aria-expanded=\"false\" aria-controls=\"dd-".$cart_item_key."\">".__("Visualizza Configurazione", THEME_CONTEXT)."</a><div id=\"dd-".$cart_item_key."\" class=\"vpc-cart-config-inner collapse\">";
        }

        if (!empty($recap)) {
            if (isset($values['vpc-custom-vars']['attributes']) && !empty($values['vpc-custom-vars']['attributes'])) {
                $details = '';
                foreach ($values['vpc-custom-vars']['attributes'] as $key => $value) {
                    $name = explode("_",$key);
                    $details .= '<dt class="variation-'.ucfirst(end($name)).'">'.ucfirst(end($name)).':</dt>
        <dd class="variation-'.ucfirst(end($name)).'"><p>'.ucfirst($value).'</p></dd>';
                }
                $thumbnail_code .= '<dl class="variation">'.$details.'</dl>';
            }
            if(get_proper_value($vpc_settings, "hide-options-selected-in-cart","No")=="No"){
                $formatted_config = $this->get_formatted_config_data($recap, $config->settings, $values);
                $thumbnail_code.= "<div class='vpc-cart-config o-wrap'><div class='o-col xl-1-1'>" . $formatted_config . "</div></div>";
            }
        }

        $config_url=vpc_get_configuration_url($product_id);

        if (get_option('permalink_structure'))
            $edit_url=$config_url."?edit=$cart_item_key&qty=".$item_content['quantity'];
        else
            $edit_url=$config_url."&edit=$cart_item_key&qty=".$item_content['quantity'];
        if($item_configured)
            $thumbnail_code.='<a class="btn btn--full w-100 alt" href="' . $edit_url . '">'.__("Modifica Configurazione", THEME_CONTEXT).'</a>';

        $thumbnail_code = apply_filters("vpc_get_config_data", $thumbnail_code, $recap,$config, $values, $cart_item_key);

        //CLOSING WRAPPER
        if($item_configured) {
            $thumbnail_code .= "</div></div>";
        }

        return $thumbnail_code;
    }
}

/*
 * GLOBAL CONFIGURATION SELECT
 */
if(!function_exists('check_for_global_config')) {
    add_action( 'save_post_product', 'check_for_global_config', 10, 1 );
    add_action( 'post_updated', 'check_for_global_config', 10, 3 );
    function check_for_global_config($product_id) {
        if ( get_post_type( $product_id ) === 'product' ) {
            $global_config1 = 'field_5bb36c4f1a184';
            $global_config2 = 'field_5bd3046da6967';
            if ( ! $_POST['acf'][ $global_config1 ] && ! $_POST['acf'][ $global_config2 ] ) {
                return;
            }

            $global_config = ! $_POST['acf'][ $global_config1 ] ? $_POST['acf'][ $global_config2 ] : $_POST['acf'][ $global_config1 ];

            $args = array(
                'post_type'   => 'product_variation',
                'post_status' => array( 'private', 'publish' ),
                'numberposts' => - 1,
                'orderby'     => 'menu_order',
                'order'       => 'asc',
                'post_parent' => $product_id // get parent post-ID
            );

            $variations = get_posts( $args );
            $new_config = array();
            foreach ( $variations as $variation ) {
                $meta_key                              = "vpc-config";
                $new_config[ (string) $variation->ID ] = array(
                    'config-id' => $global_config
                );
            }

            if ( ! empty( $new_config ) && isset( $_POST[ $meta_key ] ) ) {
                $old_metas = get_post_meta( $product_id, $meta_key, true );
                if ( empty( $old_metas ) ) {
                    $old_metas = array();
                }
                $new_metas = array_replace( $old_metas, $new_config );
                update_post_meta( $product_id, $meta_key, $new_metas );
            }
        }
    }
}

if (!function_exists('add_image_sizing_extension')){
    function add_image_sizing_extension($path, $extension = '-150x150') {
        if(!empty($path)){
            $info = pathinfo($path);
            return $info['dirname'].'/'.$info['filename'].$extension.'.'.$info['extension'];
        }
    }
}

if(!function_exists('create_configured_images')) {
    function create_configured_images() {
        global $vpc;
        $saveUrl = wp_upload_dir();
        $preview_saved = '';
        $i = 0;
        foreach ($_POST['views'] as $viewType => $view ) {
            $sizes = getimagesize(filter_var($view[0],FILTER_VALIDATE_URL) ? $view[0] : __DIR__ . "/../../../.." . $view[0]);
            $mergedCanvas = imagecreatetruecolor($sizes[0], $sizes[1]);
            $transparent = imagecolorallocatealpha($mergedCanvas, 0,0,0,127);
            imagefill($mergedCanvas, 0, 0, $transparent);
            imagesavealpha($mergedCanvas, true);
            foreach ($view as $src) {
                $curr = imagecreatefrompng(filter_var($src,FILTER_VALIDATE_URL) ? $src : __DIR__ . "/../../../.." . $src);
                imagealphablending($curr, true);
                imagesavealpha($curr, true);
                imagecopy($mergedCanvas, $curr, 0, 0, 0, 0, $sizes[0], $sizes[1]);
                imagedestroy($curr);
            }

            $preview_path = $saveUrl['basedir'].'/VPC/'.time().'_'.$_POST['product_id'].'_'.$viewType.'.png';
            $preview_url = $saveUrl['baseurl'].'/VPC/'.time().'_'.$_POST['product_id'].'_'.$viewType.'.png';
            $pipe = $i === 0 ? '' : '|';
            $preview_saved .= $pipe.$preview_url;
            imagepng($mergedCanvas, $preview_path);
            imagedestroy($mergedCanvas);
            ++$i;
        }

        $_POST['custom_vars']['preview_saved'] = $preview_saved;
        $vpc->add_vpc_configuration_to_cart();
        die('finito!'); //Just in case vpc_configuration doesnt call die
    }
}

add_action( 'wp_ajax_nopriv_create_configured_images', 'create_configured_images' );
add_action( 'wp_ajax_create_configured_images', 'create_configured_images' );
