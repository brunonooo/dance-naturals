(function( $ ) {
    'use strict';
    $(document).ready(function () {

        wp.hooks.addFilter('vpc.default_preview_builder_process', function () {
            if(vpc.views)
                return false;
            else
                return true;
        });

        wp.hooks.addAction('vpc.default_preview_builder_process', function (items_selected) {
            if(typeof active_views!="undefined")
                get_finish_image_by_view(items_selected);
            window.VPC_groups_manager.setup_groups();
        });

        wp.hooks.addAction('vpc.ajax_loading_complete', function (){
            var items = vpc.vpc_selected_items_selector;
            if(typeof active_views!="undefined"){
                create_vpc_preview_zone();
                get_finish_image_by_view(items);
            }
            vpc_mva_get_preview_height();
        });

        // GROUPING AND HIERARCHY MANAGER
        window.VPC_groups_manager = {
            first_load: true,
            groups : {},
            group_switcher : {
                $el : $('#group-switcher'),
                on : function (e) {
                    this.$el.css({
                        'top': e.clientY,
                        'left': e.clientX,
                    });
                },
                off : function () {
                    this.$el.css({
                        'top': '100vh',
                        'left': '100vw',
                    });
                },
            },
            activate_all_groups : function () {
                $('[data-component_group]').removeClass('inactive-group-el');
            },
            activate_current_in_groups : function () {
                var ref = this;
                for (var group in ref.groups ){
                    if (ref.groups.hasOwnProperty(group)) {
                        console.log(group);
                        $('[data-component_group="'+group+'"]').not('#'+ref.groups[group].active).addClass('inactive-group-el')
                    }
                }
            },
            setup_groups : function () {
                if (!this.first_load) return;
                var ref = this,
                    current_group = '';
                $('[data-component_group]').each(function(){
                    var group = $(this).attr('data-component_group');

                    if (group != '' && current_group != group) {
                        if(!ref.groups[group])
                            ref.groups[group] = {
                                active: '',
                            };

                        current_group = group;
                        $('[data-component_group="'+group+'"]:not(:first)').addClass('inactive-group-el');
                        // UPDATE STATE
                        ref.groups[group].active = $('[data-component_group="'+group+'"]:first').attr('data-component_id');
                    }
                });

                this.first_load = false;
            },
            synch_materials : function (active_id, group_id) {
                var $current_material = $('#'+this.groups[group_id].active+' .vpc-single-option-wrap--selected'),
                    current_material_value = $current_material.attr('data-full-value'),
                    $new_component = $('#'+active_id),
                    $new_material = $new_component.find('[data-full-value="'+current_material_value+'"]');

                if ($new_material.length) {
                    $new_material.find('input').click();
                } else {
                    $($new_component).find('input').eq(0).click();
                }
            },
            activate_children : function ($children) {
                var ref = this;
                $children.each(function(){
                    var current_group = $(this).attr('data-component_group'),
                        current_id = $(this).attr('data-component_id');
                    ref.switch_group(current_id, current_group);
                });
            },
            activate_grouped_element: function ($el) {
                $el.removeClass('inactive-group-el');
            },
            deactivate_grouped_element: function ($el) {
                $el.addClass('inactive-group-el');
            },
            activate_in_group : function($input, $img) {
                var $component = $input.parents('.vpc-component');
                if ($component.hasClass('inactive-group-el')){
                    $img.addClass('inactive-group-el');
                }
                return $img;
            },
            switch_group : function(active_id,  group_id) {
                var ref = this;
                if (!group_id || group_id == '') {
                    return;
                }
                var $grouped_elements = $('[data-component_group="'+group_id+'"]');
                if (!$grouped_elements.length){
                    console.log('Trying to activate but there are no elements in this group!');
                    return;
                }

                $grouped_elements.each(function(){
                    var component_id = $(this).attr('data-component_id');
                    if (component_id != active_id){
                        ref.deactivate_grouped_element($(this));
                    } else {
                        ref.activate_grouped_element($(this));
                        ref.synch_materials(active_id, group_id);
                        // UPDATE STATE
                        ref.groups[group_id].active = active_id;
                    }
                });

                var $children = $('[data-component_parent="'+active_id+'"]');
                if ($children.length){
                    this.activate_children($children);
                } else {
                    wp.hooks.doAction('vpc.default_preview_builder_process',  vpc.vpc_selected_items_selector);
                }
            },
        };

        $(document).on('click', '#group-switcher button', function (e) {
            var $current = $(e.target),
                group =  $current.attr('data-group'),
                component_id = $current.attr('data-group-component_id');
            window.VPC_groups_manager.switch_group(component_id, group);
            window.VPC_groups_manager.group_switcher.off();
        });

        $(document).on('contextmenu', '.vpc-component', function (e) {
            // AVOID SHOWING CONTEXTMENU BEHAVIOUR FOR CHILDREN AND NON GROUPS
            if ($(this).attr('data-component_group') == '' || $(this).attr('data-component_parent') != 'none') return;
            e.preventDefault();
            window.VPC_groups_manager.group_switcher.on(e);
        });

        $(document).on('click', function(e){
            if ( $(e.target).closest('#group-switcher').length === 0 ) {
                window.VPC_groups_manager.group_switcher.off();
            }
        });

        if(typeof active_views!="undefined")
            create_vpc_preview_zone();

        function create_vpc_preview_zone(){
            var activeViews=JSON.parse(active_views);
            var preview_html="";

            $.each(activeViews, function (index, value) {
                var view_id=value;
                view_id.replace(" ", "");
                preview_html+='<li class="vpc-preview" id="preview_'+view_id+'" data-view="'+index+'" data-view-name="'+view_id+'"><div id="vpc-preview'+index+'" ></div></li>';
            });

            $('.bxslider').html(preview_html);
            window.mvaSlider = $('.bxslider').bxSlider({
                infiniteLoop: false,
                adaptiveHeight:true,
                hideControlOnEnd: true,
                controls: true,
                pager: true,
            });
        }

        var current_imgs_by_view;

        var appendImages = function(userData){
            var promise = new Promise(function (resolve, reject) {
                if(userData) {
                    var images_by_view = JSON.parse(userData);
                    console.log(images_by_view);
                    for (var i = 0; i < images_by_view.length; ++i) {
                        var current_view = images_by_view[i];
                        $('.vpc-preview').not('.bx-clone').find('#vpc-preview'+i).html("");
                        for (var k = 0; k < current_view.length; ++k){
                            $('.vpc-preview').not('.bx-clone').find('#vpc-preview'+i).append(current_view[k]);
                        }
                    }

                    resolve('added new images');
                } else {
                    reject();
                }
            });
            return promise;
        };

        var changePrice = function() {
            var base_price = 0;
            if ($("#vpc-add-to-cart").length)
                base_price = $("#vpc-add-to-cart").data("price");

            if (vpc.decimal_separator = ',')
                var price = parseFloat(base_price.toString().replace(',', '.'));
            else
                var price = parseFloat(base_price);
            if (!price)
                price = 0;

            $(vpc.vpc_selected_items_selector).each(function () {
                var option_price = $(this).data("price");
                if (option_price)
                    price += parseFloat(option_price);
            });
            price = wp.hooks.applyFilters('vpc.total_price', price);
            $("#vpc-price").html(accounting.formatMoney(price));
        };

        var printError = function(message){
            new Error(message);
        };

        function fetchResponsiveImages(imgsList) {
            var promise = new Promise(function (resolve, reject) {
                $.ajax({
                    type: "POST",
                    url: window.ajax.ajaxUrl,
                    data: {
                        action: "dn_fetch_img",
                        imgList: imgsList,
                    },
                    success: resolve,
                    error: reject,
                });
            });

            return promise;
        }

        var current_imgs_by_view;
        function get_finish_image_by_view(items) {

            var decoded_active_views = JSON.parse(active_views);
            var imgs_by_view = [];
            var i = 0;

            var recap = $('#vpc-container').find(':input').serializeJSON();

            $.each(decoded_active_views, function (index, value) {
                var items_view_selected = [];
                var id = "#vpc-preview" + i;
                //$(id).html("");
                $('.vpc-preview').not('.bx-clone').find(id).html("");

                $(items).each(function () {
                    //console.log(id);
                    var img_src = $(this).attr("data-" + value);

                    // TEMP
                    if(img_src){
                        img_src = img_src.indexOf('STIVALE_ESTERNO') > 0 || img_src.indexOf('STIVALE_INTERNO') > 0 ? img_src.replace('683', '1024') : img_src; // TEMP

                        var $new_image = window.VPC_groups_manager.activate_in_group($(this), $(img_src));
                        $('.vpc-preview').not('.bx-clone').find(id).append($new_image);
                        items_view_selected.push(img_src);
                    }
                });
                var items_view = [i, items_view_selected];
                imgs_by_view.push(items_view);
                i++;
            });

            current_imgs_by_view=imgs_by_view;
            var base_price = 0;
            if ($("#vpc-add-to-cart").length)
                base_price = $("#vpc-add-to-cart").data("price");

            if (vpc.decimal_separator = ',')
                var price = parseFloat(base_price.toString().replace(',', '.'));
            else
                var price = parseFloat(base_price);
            if (!price)
                price = 0;

            $(vpc.vpc_selected_items_selector).each(function ()
            {
                var option_price = $(this).data("price");
                if (option_price)
                    price += parseFloat(option_price);
            });
            price = wp.hooks.applyFilters('vpc.total_price', price);
            $("#vpc-price").html(accounting.formatMoney(price));
        }

        wp.hooks.addAction('vpc.option_change',function($elt,e){
            vpc_mva_get_preview_height();
        });

        $(window).load(function(){
            //vpc_mva_view_name();
            vpc_mva_get_preview_height();
        });

        function vpc_mva_get_preview_height(){
            setTimeout(function () {
                var maxHeight = Math.max.apply(null, $(".bx-viewport .vpc-preview ").map(function ()
                {
                    return $(this).height();
                }).get());
                $('.bx-viewport').css({height: maxHeight});
            }, 200);
        }

        function vpc_mva_view_name(){
            $('.bx-pager a.bx-pager-link').each(function(){
                var index = $(this).data("slide-index");
                var viewName = $("li.vpc-preview[data-view='" + index + "']").data("view-name");
                $(this).text(viewName);
            });
        }
    });

})( jQuery );
