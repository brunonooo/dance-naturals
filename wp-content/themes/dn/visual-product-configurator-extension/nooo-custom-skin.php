<?php

/*
 * OPEN PLUGIN FOLDER AND ADD THIS
 * "Nooo_Custom_VPC_Skin"=>__("Nooo Vista Custom", "vpc")
 * to $skins_arr in class-vpc-config.php
 */

class Nooo_Custom_VPC_Skin
{

	public $product;
	public $product_id;
	public $settings;
	public $config;

	public function __construct($product_id = false, $config = false)
	{
		if ($product_id) {
			if (vpc_woocommerce_version_check())
				$this->product = new WC_Product($product_id);
			else
				$this->product = wc_get_product($product_id);
			$this->product_id = $product_id;

			$this->config = get_product_config($product_id);
		} elseif ($config) {
			$this->config = new VPC_Config($config);
		}
	}

	public function display($config_to_load = array())
	{
		$this->enqueue_styles_scripts();

		ob_start();

		if (!$this->config || empty($this->config))
			return __("No valid configuration linked to this product.", "vpc");

		$skin_name = get_class($this);

		$config = $this->config->settings;

		$options_style = "";
		$components_aspect = get_proper_value($config, "components-aspect", "closed");
		if ("closed" == $components_aspect)
			$options_style = "display: none";
		$product_id = "";
		if (class_exists('Woocommerce')) {
			if (vpc_woocommerce_version_check())
				$product_id = $this->product->id;
			else
				$product_id = $this->product->get_id();
			do_action("vpc_before_container", $config, $product_id, $this->config->id);
		}
		$conf_desc = get_configurator_description($config);
		$conf_desc = apply_filters('vpc_configurator_description', $conf_desc, $config, $product_id);

		?>
        <script>
            var vpc_config_messages = {
                add_to_cart: '<?php _e('Sto applicando la configurazione...', THEME_CONTEXT . '-messaggi-configuratore'); ?>',
                success: '<?php _e('La configurazione è stata aggiunta al carrello!', THEME_CONTEXT . '-messaggi-configuratore'); ?>',
                go_to_cart: '<?php _e('Vai al carrello', THEME_CONTEXT . '-messaggi-configuratore'); ?>'
            }
        </script>
        <div id="vpc-container" class="row o-wrap <?php echo esc_attr($skin_name); ?>"
             data-curr="<?php echo (class_exists('Woocommerce')) ? html_entity_decode(htmlentities(get_woocommerce_currency_symbol())) : ''; ?>">
			<?php do_action("vpc_before_inside_container", $config, $this->product->get_id(), $this->config->id); ?>

            <!--<div class="o-col conf_desc"><?php /*echo html_entity_decode(htmlentities($conf_desc)); */?></div>-->

            <div class="col-xs-12 col-md-7 col-lg-8 p-sm-0 p-xs-0" id="vpc-preview-wrap" data-above-the-fold>
                <!--<div class="default-right-skin">
                    <?php /*vpc_get_price_container(); */ ?>
                </div>-->
                <!--<div class="vpc-preview-toggle">
					<i class="fa fa-eye" ></i>
				</div>-->
				<?php
				$preview_html = '<div id="vpc-preview"></div>';
				$preview_html = apply_filters("vpc_preview_container", $preview_html, $this->product->get_id(), $this->config->id);
				echo html_entity_decode(htmlentities($preview_html));
				?>

				<?php do_action("vpc_after_preview_area", $config, $this->product->get_id(), $this->config->id) ?>
            </div>

            <div class="d-xs-block d-lg-inline-block display-4 info-badge">
                <div class="d-flex align-items-center justify-content-start display-4 info-badge__container">
                    <img class="d-inline-block p-xs-4 p-xl-5" src="<?php echo get_template_directory_uri() ?>/dist/vectors/info.svg">

                    <div class="info-badge__content d-flex align-items-center">
                        <div class="info-badge__content-wrapper p-xs-4 pt-xl-4 b-xl-4 pl-xl-0 pl-0">
                            <p><?php _e('Possiamo aiutarti nella configurazione?', THEME_CONTEXT); ?></p>
                            <span
                                    class="d-block mt-3"><a class="d-xs-block d-md-inline-block font-weight-bold" href="mailto:info@dancenaturals.it">info@dancenaturals.it</a><span class="d-xs-none d-lg-inline-block">&nbsp;-&nbsp;</span><a class="d-xs-block d-lg-inline-block mt-xs-2 mt-lg-0" href="tel:+390498935140">tel.049 893 5140</a></span>
                        </div>
                        <span class="info-badge__close pr-xl-5"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-5 col-lg-4 vpc-components-col d-flex flex-column pt-5 pb-4">
            <h2 class="vpc-main-label display-4 font-weight-bold text-uppercase mb-xs-3 mb-4 ls-1"><span
                        class="vpc-main-label-prefix d-inline-block text-uppercase"><?php _e('Personalizza ', THEME_CONTEXT); ?></span><?php _e('stile ', THEME_CONTEXT); ?>
            </h2>
            <div id="vpc-components" class="flex-grow-1 pb-xs-5 pb-md-0">
				<?php
				do_action("vpc_before_components", $config);
				if (isset($config["components"])) {
					$i = 0;
					$groups_markup = '';
					foreach ($config["components"] as $component_index => $component) {
						$this->get_components_block($component, $options_style, $config_to_load, $i);
						if (!empty($component['cgroup'])) {
							$groups_markup .= '<button data-group="'.$component['cgroup'].'" data-group-component_id="'.$component['component_id'].'">'.__($component['cname'], THEME_CONTEXT.'_configurator').'</button>';
						}
						++$i;
					}
				}
				do_action("vpc_after_components", $config);
				?>
            </div>

			<?php echo html_entity_decode(htmlentities(nooo_get_action_buttons($this->product_id))); ?>
        </div>

        <div id="vpc-form-builder-wrap">
			<?php
			if (class_exists('Ofb')) {
				if (isset($config['ofb_id'])) {
					$form_builder_id = $config['ofb_id'];
					$form = display_form_builder($form_builder_id);
					echo $form;
				}
			}
			?>
        </div>

        <div id="vpc-bottom-limit"></div>
        <div class="vpc-debug">
			<?php do_action("vpc_container_end", $config) ?>
        </div>
        </div>

        <div id="debug"></div>
        <div id="group-switcher">
			<?php echo $groups_markup; ?>
        </div>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	private function get_components_block($component, $options_style, $config_to_load = array(), $index)
	{
		global $vpc_settings;
		$skin_name = get_class($this);
		$c_icon = "";
		$options = "";

		// Bail early if component index is set to 0 or 1 HIDING COMPONENT
		if ($component['c_index'] === '1') {
           return;
        }

		if (isset($component["options"]))
			$options = $component["options"];
		if ($options) {
			$options = sort_options_by_group($options);
		}
		$component_id = "component_" . sanitize_title(str_replace(' ', '', $component["cname"]));
		$component_id = get_proper_value($component, "component_id", sanitize_title($component_id));
		//We make sure we have an usable behaviour
		$handlable_behaviours = vpc_get_behaviours();
//        var_dump($handlable_behaviours);
		if (!isset($handlable_behaviours[$component["behaviour"]]))
			$component["behaviour"] = "radio";

		/*if ($component["cimage"])
			$c_icon = "<img src='" . add_image_sizing_extension(o_get_proper_image_url($component["cimage"])) . "'>";*/

		$component_group = get_proper_value($component, "cgroup", '');
		$component_parent = get_proper_value($component, "parent", '');
		$components_attributes_string = apply_filters("vpc_component_attributes", "data-component_id = '$component_id' data-component_group = '$component_group' data-component_parent = '$component_parent' data-component_number = '$index'", $this->product_id, $component);
		?>
        <div id='<?php echo $component_id; ?>' class="vpc-component<?php echo $component['c_index'] === '0' ? ' d-none' : ''; ?>" <?php echo $components_attributes_string ?>>

        <div
                    class="d-flex vpc-component-header m-md-0 p-xs-5 p-md-4">
                <div class="d-flex align-items-center">
					<?php
					echo "$c_icon<span class='d-inline-block vpc-component-header__name'><span class='vpc-component-name font-weight-bold text-uppercase ls-1 mb-4'>" . __($component['cname'], THEME_CONTEXT.'_configurator') . "</span>";
					?>

                    <span
                            class="d-block vpc-selected txt display-4 mt-xs-3 mt-md-2"><?php esc_attr_e('none', 'vpc'); ?></span></span>

					<?php /* <span class="vpc-selected-icon"><img width="24" src="" alt="..."></span> */ ?>
                </div>

                <div class="d-flex vpc-mod-label">
                    <span
                            class="vpc-modify mt-xs-5 mt-md-0 ls-1 text-uppercase"><?php _e('modifica', THEME_CONTEXT); ?></span>
                </div>
            </div>

            <div class="vpc-options" style="<?php echo $options_style; ?>" data-spy="scroll"
                 data-target="#vpc-options-nav-<?php echo slugify($component["cname"]); ?>" data-offset="0">
                <!--<div id="vpc-options-nav-<?php /*echo slugify($component["cname"]); */?>" class="d-xs-none d-md-none">
                    <ul class="nav nav-pills">
						<?php
				/*						$current_group = "";
										foreach ($options as $option_index => $option) : */?>
							<?php /*if ($current_group != $option["group"]) : */?>
                                <li class="nav-item">
                                    <a class="nav-link"
                                       href="#group-<?php /*echo slugify($component["cname"]) . '-' . slugify($option["group"]); */?>>"><?php /*echo $option["group"]; */?></a>
                                </li>
							<?php /*endif; */?>
							<?php /*$current_group = $option["group"]; */?>
						<?php /*endforeach; */?>
                    </ul>
                </div>-->
				<?php
				do_action('vpc_' . $component["behaviour"] . "_begin", $component, $skin_name);
				$current_group = "";
				if (!is_array($options) || empty($options))
					esc_attr_e("No option detected for the component. You need at least one option per component.", "vpc");
				else {
					$product_id = $this->product_id;//get_query_var("vpc-pid", false);
					//WAD compatibility
					$discount_rate = 0;
					if (function_exists("vpc_get_discount_rate"))
						$discount_rate = vpc_get_discount_rate($product_id);

					foreach ($options as $option_index => $option) {
						if ("" == $option["name"]) {
							if (intval($option_index) == intval(count($options) - 1)) {
								echo "</div>";
							}
							continue;
						}

						if (($current_group != $option["group"]) || ($option_index == 0)) {
							if ($option_index !== 0) {
								if ("dropdown" == $component["behaviour"])
									echo "</select>";
								echo "</div></div>";
							}
							echo "<div id='group-" . slugify($component["cname"]) . "-" . slugify($option["group"]) . "' class='vpc-group row'><div class='col-xs-12 vpc-group-name__container'><div class='row'><div class='col-xs-12 vpc-group-name mb-md-5 mb-lg-5'><span class='display-4 p-4 color-primary font-weight-bold d-block'>" . $option["group"] . "</span></div></div></div><div class='vpc-group__options'>"; //."</div>";// . "<br>";
							if ("dropdown" == $component["behaviour"]) {
								echo "<select name=" . $component["cname"] . " id=" . $component_id . ">";
								echo "<option value='none'></option>";
							}
						}
						$current_group = $option["group"];
						$opt_img = get_proper_value($option, "image");
						$o_image = $opt_img;
						$opt_icon_id = get_proper_value($option, "icon");
						$o_icon = $opt_icon_id;
						$o_name = $component["cname"];
						$name_tooltip = get_proper_value($vpc_settings, "view-name");
						$price_tooltip = get_proper_value($vpc_settings, "view-price");
						//                    $input_id = uniqid();
						//                    $label_id = "cb$input_id";

						$checked = "";
						if ($config_to_load && isset($config_to_load[$component["cname"]])) {
							$saved_options = $config_to_load[$component["cname"]];
							if ((is_array($saved_options) && in_array($option["name"], $saved_options)) || ($option["name"] == $saved_options)
							)
								$checked = "checked='checked'";
						} else if (isset($option["default"]) && $option["default"] == 1)
							$checked = "checked='checked' data-default='1'";

						$price = get_proper_value($option, "price", 0);
						if (strpos($price, ','))
							$price = floatval(str_replace(',', '.', $price));
						if ("" == $price)
							$price = 0;
						$price = $price - $price * $discount_rate;
						$price = vpc_apply_taxes_on_price_if_needed($price, $this->product);
						$linked_product = get_proper_value($option, "product", false);
						$formatted_price_raw = 0;
						if ($linked_product) {
							$price = 0;
							if (class_exists('Woocommerce')) {
								if (vpc_woocommerce_version_check())
									$product = new WC_Product($linked_product);
								else
									$product = wc_get_product($linked_product);
								if (!$product->is_purchasable() || ($product->managing_stock() && !$product->is_in_stock())) {
									if ($option_index == count($options) - 1) {
										echo "</div>";
									}
									continue;
								}
								$price = $product->get_price();
								$price = vpc_apply_taxes_on_price_if_needed($price, $product);
								$formatted_price_raw = wc_price($price);
							}
						} else {
							if (class_exists('Woocommerce'))
								$formatted_price_raw = wc_price($price);
						}

						$price = apply_filters("vpc_options_price", $price, $option, $component, $this);
						if (apply_filters("vpc_option_visibility", 1, $option) != 1) {
							if ($option_index == count($options) - 1) {
								echo "</div>";
							}
							continue;
						}

						$formatted_price = strip_tags($formatted_price_raw);
						$option_id = "component_" . sanitize_title(str_replace(' ', '', $component["cname"])) . "_group_" . sanitize_title(str_replace(' ', '', $option["group"])) . "_option_" . sanitize_title(str_replace(' ', '', $option["name"]));
						$option_id = get_proper_value($option, "option_id", $option_id);
						$customs_datas = apply_filters("vpc_options_customs_datas", "", $option, $component);
						switch ($component["behaviour"]) {
							case 'radio':
							case 'checkbox':
								$input_type = "radio";
								if ($component["behaviour"] == "checkbox") {
									$o_name .= "[]";
									$input_type = "checkbox";
								}

								if ($name_tooltip == "Yes")
									$tooltip = $option["name"];
								else
									$tooltip = '';
								if ($price_tooltip == "Yes") {
									if (strpos($formatted_price, '-') !== false || strpos($formatted_price, '+') !== false)
										$tooltip .= " $formatted_price";
									else
										$tooltip .= " +$formatted_price";
								}
								if (!empty($option["desc"]))
									$tooltip .= " (" . $option["desc"] . ")";

								$label_id = "cb$option_id";

								/*$tooltip =  apply_filters("vpc_options_tooltip", $tooltip, $price, $option, $component);*/
								?>
                                <div class="vpc-single-option-wrap <?php if ($option_index == 0) {
									echo 'vpc-single-option-wrap--selected';
								} ?> col-xs-12 col-md-3 text-center position-relative mb-5 sm-p-0 xs-p-0"
                                     data-oid="<?php echo $option_id; ?>"
                                     data-option-value="<?php echo slugify($option['name']); ?>" data-full-value="<?php echo slugify(__($option['group'], THEME_CONTEXT.'_configurator')); ?>-<?php echo slugify(__($option['name'], THEME_CONTEXT.'_configurator')); ?>">
                                    <input id="<?php echo $option_id; ?>" type="<?php echo $input_type; ?>"
                                           name="<?php echo esc_attr($o_name); ?>"
                                           value="<?php echo esc_attr(__($option['group'], THEME_CONTEXT.'_configurator') . ' '. $option["name"]); ?>"
                                           data-icon='<?php echo $o_icon; ?>'
                                           data-price="<?php echo $price; ?>"
                                           data-product="<?php echo isset($option["product"]) ? $option["product"] : ""; ?>"
                                           data-oid="<?php echo $option_id; ?>" <?php echo $checked . ' ' . $customs_datas; ?>>
                                    <label id="<?php echo $label_id; ?>" for="<?php echo $option_id; ?>"
                                           data-o-title="<?php echo esc_attr($tooltip); ?>" class="custom m-auto">
                                        <span class="img-wrapper overlay-100">
                                            <!--// ICON IMAGE TAG HERE-->
                                        </span>
                                    </label>
                                    <!--<style>
                                        #<?php /*echo $label_id; */
									?>:before
                                        {
                                            background-image: url("<?php /*echo $o_icon; */
									?>");
                                        }
                                    </style>-->

                                    <div class="vpc-single-option-meta flex-column">
                                        <p class="vpc-single-option-name flex-grow-1 display-4 mb-0 ls-0"><?php echo esc_html(__($option["name"], THEME_CONTEXT.'_configurator')); ?></p>
                                        <small class="vpc-single-option-price display-4 mt-1"><?php echo $formatted_price ?></small>
                                    </div>
                                </div>
								<?php
								break;

							case 'dropdown':
								$selected = "";
								if ($config_to_load && isset($config_to_load[$component["cname"]])) {
									$saved_options = $config_to_load[$component["cname"]];
									if ((is_array($saved_options) && in_array($option["name"], $saved_options)) || ($option["name"] == $saved_options)
									)
										$selected = "selected";
								} else if (isset($option["default"]) && $option["default"] == 1)
									$selected = "selected";
								?>
                                <option id="<?php echo $option_id; ?>" value="<?php echo $option["name"]; ?>"
                                        data-img="<?php echo $o_image; ?>" data-icon="<?php echo $o_icon; ?>"
                                        data-price="<?php echo $price; ?>"
                                        data-product="<?php echo isset($option["product"]) ? $option["product"] : ""; ?>"
                                        data-oid="<?php echo $option_id; ?>" <?php echo $selected . ' ' . $customs_datas; ?>>
									<?php echo $option["name"]; ?>
                                </option>
								<?php
								break;

							default:
								//  do_action('vpc_'.$skin_name.'_' . $component["behaviour"], $component);
								do_action('vpc_' . $component["behaviour"], $option, $o_image, $price, $option_id, $component, $skin_name, $config_to_load, $this->config->settings);
								break;
						}
						do_action('vpc_each_' . $component["behaviour"] . '_end', $option, $o_image, $price, $option_id, $component, $skin_name, $config_to_load);

						if ($option_index == count($options) - 1) {
							if ($component["behaviour"] == "dropdown")
								echo "</select>";
							echo "</div></div>";
						}
						$current_group = $option["group"];
					}
				}

				do_action('vpc_' . $component["behaviour"] . '_end', $component, $this->config, $skin_name);
				?>
            </div>
        </div>
		<?php
	}

	public function enqueue_styles_scripts()
	{
		if (is_admin())
			vpc_enqueue_core_scripts();
		/*wp_enqueue_style("vpc-default-skin", VPC_URL . 'public/css/vpc-default-skin.css', array(), VPC_VERSION, 'all');
		wp_enqueue_style("vpc-right-sidebar-skin", VPC_URL . 'public/css/vpc-right-sidebar-skin.css', array(), VPC_VERSION, 'all');*/
		/*wp_enqueue_style("o-flexgrid", VPC_URL . 'admin/css/flexiblegs.css', array(), VPC_VERSION, 'all');
		wp_enqueue_style("FontAwesome", VPC_URL . 'public/css/font-awesome.min.css', array(), VPC_VERSION, 'all');*/
		/*wp_enqueue_style("o-tooltip", VPC_URL . 'public/css/tooltip.min.css', array(), VPC_VERSION, 'all');

		wp_enqueue_script("o-tooltip", VPC_URL . 'public/js/tooltip.min.js', array('jquery'), VPC_VERSION, false);*/

		// OVERRIDING DEFAULT BEHAVIOUR
		// CUSTOM JS CODE IMPLEMENTATION ADDED TO PAGE TEMPLATE WITH WEBPACK ROUTER -- Template: page-template-configurator.js
		wp_deregister_script('vpc-public');
		wp_register_script('vpc-public', get_template_directory_uri() . '/visual-product-configurator-extension/js/nooo-vpc-public.js', array('jquery', 'vpc-accounting'), VPC_VERSION, false);
		wp_enqueue_script('vpc-public');
		wp_enqueue_script("vpc-nooo-skin", get_template_directory_uri() . '/visual-product-configurator-extension/js/vpc-nooo-skin.js', array('jquery', 'vpc-public'), VPC_VERSION, false);
		wp_localize_script("vpc-nooo-skin", 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));

		wp_enqueue_script("o-serializejson", VPC_URL . 'public/js/jquery.serializejson.min.js', array('jquery'), VPC_VERSION, false);


		// wp_enqueue_script("jquery-scroll-follow", VPC_URL . 'public/js/jquery.simple-scroll-follow.min.js', array('jquery'), VPC_VERSION, true);
		// wp_enqueue_script("vpc-scroll-follow", VPC_URL . 'public/js/vpc-follow-scroll.js', array('jquery', 'vpc-public'), VPC_VERSION, true);
	}

}
