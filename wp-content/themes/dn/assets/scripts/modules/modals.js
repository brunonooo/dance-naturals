import $ from "jquery";
import {TimelineLite} from "gsap";

class Modal {
    constructor($trigger, onOpen = null, onClose = null) {
        this.$trigger = $trigger;
        this.$modal = $('.dn-modal-wrapper');
        this.$modalDialog = this.$modal.find('.dn-modal');
        this.onClose = onClose;
        this.onOpen = onOpen;
        this.init();
    }

    releaseScroll = () => {
        if (window.gc.sb){
            window.gc.sb.block(false);
        } else {
            document.querySelector('body').style.overflow = 'visible';
        }
    };

    blockScroll = () => {
        if (window.gc.sb){
            window.gc.sb.block(true);
        } else {
            document.querySelector('body').style.overflow = 'hidden';
        }
    };

    modalClose  = (callback = null) => {
        let ref = this;
        if(typeof this.onClose === 'function') {this.onClose.call(ref, ref.$trigger);}
        let tl = new TimelineLite({
            onComplete : () => {
                ref.releaseScroll();
                if(typeof callback === 'function'){callback();}
            },
        });

        tl
            .to(ref.$modalDialog, 1, {scale: .9, rotation: 3, y: 50})
            .to(ref.$modal, .4, { autoAlpha: 0}, .3);
    };

    static close = (callback = null) => {
        let $modal = $('.dn-modal-wrapper'),
            $modalDialog = $modal.find('.dn-modal');

        let tl = new TimelineLite({
            onComplete : () => {
                if (window.gc.sb){
                    window.gc.sb.block(false);
                } else {
                    document.querySelector('body').style.overflow = 'visible';
                }
                if(typeof callback === 'function'){callback();}
            },
        });

        tl
            .to($modalDialog, 1, {scale: .9, rotation: 3, y: 50})
            .to($modal, .4, { autoAlpha: 0}, .3);
    };

    injectModalContent = ($trigger) => {
        // BINDING TRIGGER WITH CONTENT
        let modalHash = $trigger.data('modal-trigger-link'),
            $contentCtx = $('.mdl-content-wrapper[data-modal-content-link="'+modalHash+'"]');

        if(!$contentCtx.length){
            console.error('I couldn\'t find ' + modalHash + ' data!');
            return
        }

        let $modalContent = $('.mdl-content', $contentCtx).html(),
            $modalLabel = $('label', $contentCtx).html();

        $('#dn-modal-label').html($modalLabel);
        $('#dn-modal-body').html($modalContent);
    };

    modalOpen = (e, callback = null) => {
        let ref = this;
        this.injectModalContent($(e.target));

        if(typeof this.onOpen === 'function') {this.onOpen.call(ref, ref.$trigger);}
        let tl = new TimelineLite({
            onStart : ref.blockScroll,
            onComplete : () => {
                if(typeof callback === 'function'){callback();}
            },
        });
        tl
            .to(ref.$modal, .7, { autoAlpha: 1})
            .to(ref.$modalDialog, 1, {scale: 1, rotation: 0, y: 0, ease:'Power3.easeOut'}, 0);
    };

    init = () => {
        let ref = this;
        // Registering Events on object creation
        this.$trigger.click(ref.modalOpen);
        $(document).on('keypress', function(e){
            if ( e.keyCode === 13 ) {
                ref.modalClose();
            }
        });
        this.$modal.on('click' ,'.md-modal-background', ref.modalClose);
        this.$modal.on('click' ,'.close, .modal-close', ref.modalClose);

        // No-script safe code :D
        this.$modal.removeClass('d-none');
        this.modalClose();
    };
}

export default Modal;
