import H from "../../nooo_modules/nooo-helpers";
import $ from "jquery";

/**
 * Small Utility to pin elements in the page on scroll and set bounds to their scrolling action
 * Usage:
 * Add [data-pinned-bounds] attribute to container html and [data-pinned-el] on pin target by default it will pin it as soon as the top of the element touches the header bottom
 * you can add [data-pinned-offset] with a number to add an offset and [data-self-bottom] to make the element stop when its bottom edge reaches the bound container
 */
const PinOnScroll = function (options = {}) {
    let def = {
        target: '[data-pinned-el]', // Target selector
        bounds: '[data-pinned-bounds]', // Bounds selector
        header: 'main-navigation',
        checkHeight: false, // Check if height is more than the window height
        conditions: [],
    };

    // Shallow merging options with defaults
    let opts = {};
    Object.assign(opts, def, options);

    const { target, bounds, header, checkHeight, conditions } = opts;

    let windowHeightDuration;
    let updateDuration = function () {
        windowHeightDuration = window.innerHeight;
    };

    $(window).on("resize", H.throttle(updateDuration, 250));
    updateDuration();

    let boundsList = Array.from(document.querySelectorAll(bounds)),
        lastKnownScrollPosition = 0;

    // Checking if all conditions are true
    let isRightConditions = conditions.filter(condition => condition);

    if (boundsList.length && isRightConditions.length) {

        let headerHeight = document.getElementById(header).clientHeight;

        let bnds = [];
        let Bounds = function (bounds) {
            this.pinned = bounds.querySelector(target);
            this.top = bounds.getBoundingClientRect().top;
            this.bottom = bounds.getBoundingClientRect().bottom;
            this.pin = (offsetY) => {
                let scroll = offsetY + headerHeight,
                    scrollBottom = offsetY + windowHeightDuration;

                if (this.pinned.getAttribute('data-pinned-offset')) {
                    scroll += parseInt(this.pinned.getAttribute('data-pinned-offset'));
                }

                if (this.pinned.getAttribute('data-self-bottom')) {
                    scrollBottom = offsetY + this.top + this.pinned.clientHeight;
                }

                if (scroll >= this.top && scrollBottom <= this.bottom) {
                    this.pinned.style.transform = 'translateY(' + (scroll - this.top) + 'px)';
                }

                if (scroll <= this.top) {
                    this.pinned.style.transform = 'translateY(0)';
                }

                lastKnownScrollPosition = offsetY;
            };

            this.refresh = () => {
                this.top = bounds.getBoundingClientRect().top + lastKnownScrollPosition;
                this.bottom = bounds.getBoundingClientRect().bottom + lastKnownScrollPosition;
            };
        };

        let refreshBounds = H.throttle(() => {
            headerHeight = document.getElementById(header).clientHeight;
            bnds.forEach(function (bounds) {
                bounds.refresh();
            });
        }, 500);

        let heightCheckPassed = (bounds) => {
            if (!checkHeight){
                return true;
            } else {
                let trg = bounds.querySelector(target),
                    result = trg.offsetHeight + headerHeight < windowHeightDuration;
                return result;
            }

        };

        boundsList.forEach(function (bounds) {
            if (heightCheckPassed(bounds)) {
                let bnd = new Bounds(bounds);
                // Case we're using smooth scrollbar
                if (window.gc.sb) {
                    window.gc.sb.addListener(({offset}) => {
                        bnd.pin(offset.y);
                    });
                } else {
                    $(window).on('scroll', function () {
                        bnd.pin($(document).scrollTop());
                    });
                }

                bnds.push(bnd);
            }
        });

        window.addEventListener('resize', refreshBounds);
        $('body').on('show_variation', refreshBounds);
    }
};

export default PinOnScroll;
