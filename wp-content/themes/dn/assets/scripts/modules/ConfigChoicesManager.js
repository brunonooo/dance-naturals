class ConfigChoicesManager {
    constructor(confirm, revert) {
        this.confirm = confirm;
        this.revert = revert;
        this.cache = {
            component: null,
            lastSelection: null,
        }
    }

    getSelectionData (component) {
        this.cache.component = component;
        this.cache.lastSelection = component.querySelector('.vpc-single-option-wrap--selected input');
    }

    revertToCachedSelection () {
        this.cache.lastSelection.click();
        this.clearSelectionCache();
    }

    saveSelectionData () {
        // It's selected from the user so don't need to do anything here
        this.clearSelectionCache();
    }

    cacheCurrentSelection(component) {
        this.getSelectionData(component);
    }

    clearSelectionCache () {
        this.cache = {
            component: null,
            lastSelection: null,
        }
    }

    handleChoice (button, cb = null) {
        button.id === 'vpc-go-back' && this.cache.lastSelection ? this.revertToCachedSelection() : this.saveSelectionData();
        if (cb) cb();
    }
}

export default ConfigChoicesManager;
