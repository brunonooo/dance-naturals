/** import external dependencies */
import 'bootstrap';

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import singleProduct from './routes/single-product';
import pageTemplateUserRelatedPage from './routes/page-template-user-related-page';
import pageTemplateConfigurator from './routes/page-template-configurator';
import pageTemplateResellers from './routes/page-template-resellers';
import pageTemplateContactUs from './routes/page-template-contact-us';
import woocommerceCart from './routes/woocommerce-cart'
import woocommerceCheckout from './routes/woocommerce-checkout'

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
    /** All pages */
    common,
    /** Home page */
    home,
    /** Single Product */
    singleProduct,
    /** User Related Page */
    pageTemplateUserRelatedPage ,
    /** Page Template Configurator */
    pageTemplateConfigurator ,
    /** Page Template Resellers */
    pageTemplateResellers ,
    /** Page Template Contact Us */
    pageTemplateContactUs ,
    /** Cart */
    woocommerceCart ,
    /** Checkout */
    woocommerceCheckout ,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
