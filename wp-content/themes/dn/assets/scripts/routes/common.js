'use strict';

/*POLYFILLS*/
import '../../nooo_modules/polyfills/closest';
import '../../nooo_modules/polyfills/findPolyfill'
import '../../nooo_modules/polyfills/events-constructor';
import 'es6-symbol';
import 'promise-polyfill/src/polyfill';
import objectFitImages from 'object-fit-images';

/* Sentry
* ( needs promise polyfill to work in older browsers so keep after polyfills! )
*/
import * as Sentry from '@sentry/browser';

import $ from 'jquery';
import 'imagesloaded';
import Bowser from 'bowser';
import PinOnScroll from '../modules/PinOnScroll';
import Modal from '../modules/modals'

import H from '../../nooo_modules/nooo-helpers';
import ua from '../../nooo_modules/nooo-hound';
import ImageMage from '../../nooo_modules/imgblackmagic/nooo-image-blackmagic';
import SimLink from '../../nooo_modules/nooo-sim-links';
import Scrollbar from 'smooth-scrollbar';
import OverscrollPlugin from 'smooth-scrollbar/plugins/overscroll/index';
import BlockPlugin from '../../nooo_modules/smooth-scrollbar-plugin/smooth-scrollbar-block';
import messagesToBeRemoved from '../manifests/messages-config';

import {TweenLite, TimelineLite} from 'gsap';
import ScrollMagic from "scrollmagic";

export default {
    init() {

        if (!window.gc.isDev ) { // ENVIROMENT is set in wp-config.php
            Sentry.init({ dsn: 'https://b89bda35a9f945b7baf429fb25c39152@sentry.io/1367257' });
        }

        // More Precise User Agent Classes
        const parser = Bowser.getParser(window.navigator.userAgent);
        const bVersion = parser.getBrowserVersion();
        const toKebabCase = (str, before = '', after = '') => {
            const kebab = str.toLowerCase().replace(new RegExp(' '), '-');
            return before + kebab + after;
        };

        const version = toKebabCase(bVersion ? bVersion.substr(0, bVersion.indexOf('.')) : '0', 'v');
        let UAclasses = [
            toKebabCase(parser.getBrowserName()),
            toKebabCase(parser.getOSName()),
            toKebabCase(parser.getEngine().name),
            version,
        ].join(' ');
        $('html').addClass(UAclasses);

        if (ua.isMobile) {
            $('html').addClass('mobile');
            window.gc.isMobile = true;
        }

        let sbContainer = document.getElementById('scrollable-container'),
            scrollTarget = document.querySelector('.scroll-target');

        function setScrollbarWindow(el) {
            let wW = window.innerWidth,
                wH = window.innerHeight;

            el.style.width = wW + 'px';
            el.style.height = wH + 'px';
        }

        $("#vpc-add-to-cart[data-pid='62961']").closest("#vpc-container").find('.bxslider li[data-view-name="esterno"]').css("transform", "scaleX(-1)");
        $("#vpc-add-to-cart[data-pid='62961']").closest("#vpc-container").find('.bxslider li[data-view-name="interno"]').css("transform", "scaleX(-1)");

        // $('.postid-64695 figure>div:nth-child(1)').css("transform", "scaleX(-1)");
        // $('.postid-64695 figure>div:nth-child(3)').css("transform", "scaleX(-1)");

        if (!ua.isMobile && sbContainer && scrollTarget && !($('.woocommerce-cart').length || $('.woocommerce-checkout').length)) {
            setScrollbarWindow(sbContainer);

            Scrollbar.use(OverscrollPlugin);
            Scrollbar.use(BlockPlugin);

            window.gc.sb = Scrollbar.init(sbContainer, {
                damping: 1,
                continuousScrolling: false,
                wheelEventTarget: scrollTarget,
                renderByPixels: false,
                plugins: {
                    overscroll: {
                        effect: 'bounce',
                        damping: 1,
                        maxOverscroll: 15,
                    },
                },
                syncCallbacks: true,
            });

            $('body').addClass('sb-running');

            window.gc.sb.block = (open) => {
                window.gc.sb.updatePluginOptions('block', {open: open});
            };

            $(window).resize(function () {
                setScrollbarWindow(sbContainer);
            });
        }

        let windowHeightDuration;

        let updateDuration = function () {
            windowHeightDuration = window.innerHeight;
        };

        $(window).on("resize", H.throttle(updateDuration, 250));
        updateDuration();

        let isSingleProduct = document.querySelector('.single-product');
        PinOnScroll({
            conditions: [!ua.isMobile, false],
            checkHeight: !!isSingleProduct,
        });

        /**
         * PINNED ELEMENTS
         */

        let pinnedList = Array.from(document.querySelectorAll('[data-pinned]'));
        if (pinnedList.length && window.gc.sb) {
            pinnedList.forEach(function (pinned) {
                window.gc.sb.addListener(({offset}) => {
                    pinned.style.transform = 'translateY(' + offset.y + 'px)';
                });
            });
        }

        //START IMAGE LOADING!
        window.imageMage = ImageMage.init({
            smoothScrollBar: window.gc.sb,
        });

        /**
         * IGNITION
         */
        let pageLoadEnd = new CustomEvent('pageloadend');
        let $imagesAboveTheFold = $('[data-above-the-fold]');

        if (!$('.page-template-configurator').length) {
            if ($imagesAboveTheFold.length) {
                $imagesAboveTheFold.imagesLoaded(function () {
                    H.$body.removeClass('page-loading');

                    // DISPLAY NONE OF LOADER FOR SEO
                    window.setTimeout(function () {
                        $('#preloader').addClass('killed');

                        $('body').addClass('page-load-end');
                        window.dispatchEvent(pageLoadEnd);
                    }, 500);
                });
            } else {
                H.$body.removeClass('page-loading');
                window.setTimeout(function () {
                    $('#preloader').addClass('killed');

                    $('body').addClass('page-load-end');
                    window.dispatchEvent(pageLoadEnd);
                }, 500);
            }
        }

        /**
         * MENU TOGGLING
         */

        let menu = {
            isOpen: false,
            isAnimating: false,
            el: document.querySelector('.overlay-menu'),
            openAnim: new TimelineLite({paused: true}),
            closeAnim: new TimelineLite({paused: true}),
            $items: $('.menu-item a'),
        };

        menu.nav = document.querySelector('.main-navigation__nav');
        menu.navLogo = document.querySelectorAll('.logo');
        menu.mainWrapper = menu.el.querySelector('.overlay-menu__main-wrapper');
        menu.mainInner = menu.el.querySelector('.overlay-menu__main-inner');
        menu.mainLi = menu.mainInner.querySelectorAll('li');
        menu.ctaWrapper = menu.el.querySelector('.overlay-menu__cta-wrapper');
        menu.ctaInner = menu.el.querySelector('.overlay-menu__cta-inner');
        menu.images = menu.ctaInner.querySelectorAll('.img-wrapper');

        menu.open = function () {
            if (window.gc.sb) {
                window.gc.sb.block(true);
            }
            menu.isAnimating = true;
            menu.el.parentElement.setAttribute('data-menu-open', true);
            this.openAnim.play(0);
        };

        menu.onOpenEnd = function () {
            menu.isOpen = true;
            menu.isAnimating = false;
        };

        menu.close = function () {
            if (window.gc.sb) {
                window.gc.sb.block(false);
            }

            menu.el.parentElement.setAttribute('data-menu-open', false);
            menu.isAnimating = true;
            this.closeAnim.play(0);
        };
        menu.onCloseEnd = function () {
            menu.isOpen = false;
            menu.isAnimating = false;
        };

        TweenLite.set(menu.ctaWrapper, {x: '-101%'});
        TweenLite.set(menu.ctaInner, {x: '101%'});
        TweenLite.set(menu.mainWrapper, {x: '-101%'});
        TweenLite.set(menu.mainInner, {x: '101%'});
        TweenLite.set(menu.images, {x: '-5%'});
        TweenLite.set(menu.navLogo, {autoAlpha: 1});
        TweenLite.set(menu.mainLi, {rotation: -5, opacity: 0, y: 80});
        TweenLite.set(menu.el, {autoAlpha: 0});

        menu.openAnim
            .timeScale(2)
            .set(menu.el, {autoAlpha: 1})
            .to(menu.mainWrapper, 2, {x: '0%', ease: 'Power4.easeInOut'}, 0)
            .to(menu.navLogo, .5, {autoAlpha: 0}, '-=1.5')
            .to(menu.mainInner, 2, {x: '0%', ease: 'Power4.easeInOut'}, 0)
            .addLabel('secondPanel', '-=1')
            .staggerTo(menu.mainLi, 2, {rotation: 0, opacity: 1, y: '0%', ease: 'Power.easeOut'}, 0.15, .2)
            .to(menu.ctaWrapper, 2, {x: '0%', ease: 'Power4.easeInOut'}, 'secondPanel')
            .to(menu.ctaInner, 2, {x: '0%', ease: 'Power4.easeInOut'}, 'secondPanel')
            .to(menu.images, 2.2, {x: '0%', ease: 'Power4.easeInOut'}, 'secondPanel', '-=6');

        menu.closeAnim
            .timeScale(3.2)
            .to(menu.ctaWrapper, 2, {x: '-100%', ease: 'Power4.easeIn'}, 0)
            .to(menu.ctaInner, 2, {x: '100%', ease: 'Power4.easeIn'}, 0)
            .to(menu.mainWrapper, 2, {x: '-100%', ease: 'Power4.easeOut'})
            .to(menu.mainInner, 2, {x: '100%', ease: 'Power4.easeOut'}, '-=2')
            .to(menu.images, 1.5, {x: '-10%', ease: 'Power4.easeIn'}, '-=2')
            .to(menu.navLogo, 0, {autoAlpha: 1}, '-=2')
            .set(menu.el, {autoAlpha: 0});

        menu.openAnim.eventCallback('onComplete', menu.onOpenEnd);
        menu.closeAnim.eventCallback('onComplete', menu.onCloseEnd);

        let toggleMenu = () => {
            //BAIL EARLY IF STILL ANIMATING
            if (menu.isAnimating) {
                return;
            }

            if (!menu.isOpen) {
                menu.open();
            } else {
                menu.close();
            }
        };

        menu.$items.click(function () {
            menu.close();
        });

        // AJAX MENU SELECTION FEEDBACK
        $('#main-nav .nooo-ajax-link > a').click(function () {
            $('.current-menu-item').removeClass('current-menu-item');
            $(this).parent('.menu-item').addClass('current-menu-item');
        });

        /**
         *
         * FORM ANIMATIONS
         *
         */
        document.addEventListener('focusin', function (e) {
            let wrapper = e.target.closest('.woocommerce-input-wrapper, .wpcf7-form-control-wrap');
            if (wrapper) {
                H.extraClass(wrapper, 'in-focus');
            }
        });

        document.addEventListener('focusout', function () {
            let inFocus = document.querySelectorAll('.in-focus');
            Array.from(inFocus).forEach(function (focused) {
                H.deleteClass(focused, 'in-focus');
            });
        });


        /**
         *
         * CUSTOM CHECKBOX BEHAVIOUR
         *
         */

        window.gc.checkboxesInit = () => {
            let checkboxLabels = $('[type="checkbox"]').parents('label');

            checkboxLabels.on('click', function () {
                let $label = $(this),
                    $checkbox = $label.find('[type="checkbox"]');

                if ($checkbox.prop('checked')) {
                    $label.removeClass('checkbox-checked');
                    $checkbox.prop('checked', false);
                } else {
                    $label.addClass('checkbox-checked');
                    $checkbox.prop('checked', true);
                }
            });
        };

        window.gc.checkboxesInit();


        /**
         *
         * FORM SUBMISSION
         *
         */

        let $form = $('.form-wrapper');
        let stopAjax = function () {
            $form.removeClass('doing-ajax');
        };

        let customValidation = function () {
            $('.wpcf7-form-control-wrap').each(function () {

                if ($('.wpcf7-not-valid', this).length) {
                    $(this).addClass('not-valid');
                } else {
                    $(this).removeClass('not-valid');
                }
            });

            stopAjax();
        };

        $('.btn--submit__proxy').click(function (e) {
            let $currentForm = $(e.target).closest('.form-wrapper');
            $currentForm.find('[type="submit"]').click();
            $currentForm.addClass('doing-ajax');
        });

        $('.wpcf7').on('wpcf7:invalid', customValidation);

        $('.wpcf7').on('wpcf7:spam', stopAjax);

        $('.wpcf7').on('wpcf7:mailsent', customValidation);

        $('.wpcf7').on('wpcf7:mailfailed', stopAjax);

        $('.wpcf7').on('wpcf7:submit', stopAjax);

        $(document).on('click', '#btn-toggle, .overlay-menu__cta',toggleMenu);

        // Proceed to checkout mobile proxy
        $(document).on('click','.mobile-submit-proxy', function() {
            const $goToCheckoutBtn = $('.wc-proceed-to-checkout a');
            if($goToCheckoutBtn.length){
                window.location = $goToCheckoutBtn.attr('href');
            }
        });

        /*
         * DROPDOWN
         */
        let $dropdowns = $('.btn__dropdown');

        if ($dropdowns.length) {
            document.addEventListener('click', function (e) {
                let currentDropdown = e.target.closest('.btn__dropdown');
                if ($(currentDropdown).length) {
                    e.preventDefault();
                    let target = $(currentDropdown).attr('href') || $(currentDropdown).data('target');
                    if ($(target).length) {
                        $(target).collapse('toggle');
                    }
                }
            });
        }

        /*
         * SHY ELEMENT BEHAVIOUR ( Show and hide affixed element when only certain areas are in viewport )
         */

        let checkShyPos = (offset, shyElement, shyAreaBounds) => {
            if (offset >= shyAreaBounds.top || offset <= shyAreaBounds.bottom) {
                $(shyElement).addClass('shy-in');
            }

            if (offset >= shyAreaBounds.bottom || offset < shyAreaBounds.top) {
                $(shyElement).removeClass('shy-in');
            }
        };

        let shy = (shyElement, shyArea) => {
            let shyAreaBounds = {},
                windowsHeight = window.innerHeight;

            shyAreaBounds.top = shyArea.getBoundingClientRect().top;
            shyAreaBounds.bottom = shyAreaBounds.top + shyArea.clientHeight;

            let recalculateShy = H.throttle(() => {
                shyAreaBounds.top = shyArea.getBoundingClientRect().top;
                shyAreaBounds.bottom = shyAreaBounds.top + shyArea.clientHeight;
                windowsHeight = window.innerHeight;
            }, 250);

            $(window).resize(recalculateShy);

            checkShyPos(windowsHeight, shyElement, shyAreaBounds);

            if (window.gc.sb) {
                window.gc.sb.addListener(({offset}) => {
                    let windowBottom = offset.y + windowsHeight;
                    checkShyPos(windowBottom, shyElement, shyAreaBounds);
                });
            } else {
                window.addEventListener('scroll', function () {
                    let windowBottom = window.scrollY + windowsHeight;
                    checkShyPos(windowBottom, shyElement, shyAreaBounds);
                });
            }
        };

        let priceFilter = document.querySelector('.range-slider'),
            productsGrid = document.getElementById('products-grid');

        if (priceFilter && productsGrid) {
            shy(priceFilter, productsGrid);
        }

        // MANAGING CART BUTTON REDIRECTION
        $(document).on('click', '#btn--cart', (e) => {
            e.preventDefault();
            let $cartBtn = $('#btn--cart'),
                shopLink = $cartBtn.attr('data-link-shop'),
                cartLink = $cartBtn.attr('data-link-cart');

            if ($cartBtn.hasClass('cart-has-item')) {
                window.location = cartLink;
            } else {
                window.location = shopLink;
            }
        });

        function logErrors(jqXHR, textStatus, error) {
            console.log(jqXHR, textStatus, error);
        }

        function addToCartAnimation() {
            // ADD TO CART ANIMATION
            let cart = document.querySelector('#btn--cart .cart-icon');
            let cartJump = new TimelineLite({paused: true});

            cartJump
                .timeScale(2)
                .to(cart, 1, {y: -20, ease: 'Power2.easeOut'})
                .addLabel('jumptop')
                .to(cart, .5, {rotation: '-5deg'}, '-=0.5')
                .to(cart, 1.5, {
                    y: 0,
                    ease: window.CustomEase.create("custom", "M0,0 C0.14,0 0.246,0.098 0.344,0.55 0.38,0.718 0.434,0.963 0.442,1 0.45,0.985 0.523,0.789 0.654,0.8 0.778,0.81 0.847,0.983 0.854,1 0.877,0.968 0.884,0.948 0.928,0.948 0.966,0.948 1,1 1,1"),
                }, 'jumptop')
                .to(cart, .5, {rotation: 0}, '-=0.5');

            cartJump.play(0);
        }

        let refreshFragments = (fragments) => {
            if (fragments) {
                $.each(fragments, function (key, value) {
                    $(key).replaceWith(value)
                });

                /*$(document.body).trigger('wc_fragments_refreshed');*/
            }
        };

        $(document.body).on('added_to_cart', addToCartAnimation);

        /*
         * AJAX ADD TO CART
         */
        if (window.wc_add_to_cart_params && window.wc_add_to_cart_params.cart_redirect_after_add != 'yes') {
            $(document).on('click', '.single_add_to_cart_button', function (e) {

                e.preventDefault();
                let $btn = $(this),
                    $form = $btn.closest('form.cart'),
                    id = $btn.val(),
                    product_qty = $form.find('input[name=quantity]').val() || 1,
                    product_id = $form.find('input[name=product_id]').val() || id,
                    variation_id = $form.find('input[name=variation_id]').val() || 0;

                let data = {
                    action: 'woocommerce_ajax_add_to_cart',
                    product_id: product_id,
                    product_sku: '',
                    quantity: product_qty,
                    variation_id: variation_id,
                };

                // TRIGGER IN CASE OTHER PLUGINS ARE USING adding_to_cart EVENT
                $(document.body).trigger('adding_to_cart', [$btn, data]);

                $.ajax({
                    type: 'post',
                    url: window.ajax.ajax_url,
                    data: data,
                    beforeSend: function () {
                        $btn.removeClass('added').addClass('loading');
                    },
                    complete: function () {
                        $btn.addClass('added').removeClass('loading');
                    },
                    success: function (response) {
                        if (response.error && response.product_url) {
                            window.location = response.product_url;
                            return;
                        } else {
                            refreshFragments(response.fragments);
                            // TRIGGER IN CASE OTHER PLUGINS ARE USING added_to_cart EVENT
                            $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $btn]);
                        }
                    },
                    error: logErrors,
                });
            });
        }

        /*
         * AJAX REMOVE FROM CART
         */
        $(document).on('click', '.quick-cart-dropdown__remove', function (e) {
            e.preventDefault();

            let product_id = $(this).attr("data-product-id"),
                cart_item_key = $(this).attr("data-cart-item-key"),
                $product_container = $(this).parents('.quick-cart-dropdown__list__el');

            // Add loader
            $product_container.addClass('deleting');

            $.ajax({
                type: 'post',
                url: window.ajax.ajaxUrl,
                data: {
                    action: "woocommerce_ajax_remove_from_cart",
                    product_id: product_id,
                    cart_item_key: cart_item_key,
                },
                success: function (response) {
                    if (!response || response.error) {
                        console.log(response);
                        return;
                    } else {
                        refreshFragments(response.fragments);
                    }
                },
            });
        });

        // NOT MANY OTHER WAYS TO REMOVE AJAX NOTICES IN CART
        let removeWcMessages = () => {
            let $wc_message = $('.woocommerce-message');
            if ($wc_message.length) {
                messagesToBeRemoved.messages.forEach(message => {
                    let itMatches = (message.it !== "" && $wc_message.html().search(message.it)),
                        enMatches = (message.en !== "" && $wc_message.html().search(message.en));
                    if (itMatches || enMatches) {
                        $wc_message.hide();
                    }
                });
            }
        };

        removeWcMessages();

        $(document.body).on('updated_wc_div', function () {
            removeWcMessages();
        });

        function initAnimations() {
            let scOptions = {};

            if(!window.gc.isMobile) {
                scOptions = {
                    refreshInterval: 0,
                }
            }

            let scenes = [];
            let ctrl = new ScrollMagic.Controller(scOptions);

            /**
             ** ANIMAZIONE TEXT SCROLL
             **/
            let scrollingText = document.querySelectorAll('.text-scrolling'),
                wWidth = window.innerWidth;

            if(scrollingText){

                Array.from(scrollingText).forEach((st) => {
                    let triggerPoint = parseFloat(st.getAttribute('data-trigger-point')),
                        startingPosition = st.getAttribute('data-text-starting-point');

                    st.style.left = startingPosition;

                    let scrollTextScene = new ScrollMagic.Scene({
                        triggerHook: triggerPoint ? triggerPoint : 1,
                        triggerElement: scrollingText[0],
                        duration: windowHeightDuration,
                    })
                        .setTween(TweenLite.to(scrollingText, 1, {x: -wWidth / 8, ease: 'Circ.easeNone'}));

                    scenes.push(scrollTextScene);
                });

            }
            /**
             * ADDING ANIMATIONS
             */

            scenes.forEach(function (scene) {
                ctrl.addScene(scene);

                if (window.gc.sb) {
                    window.gc.sb.addListener(() => {
                        scene.refresh();
                    });
                }
            });
        }

        window.addEventListener('pageloadend', function(){
            initAnimations();

            if(window.gc.sb){
                $('.roll').click(function (e){
                    e.preventDefault();
                    let target = this.hash,
                        $target = $(target);

                    const destinationTop = $target.offset().top
                    console.log(window.gc.sb.offset.y, destinationTop)
                    window.gc.sb.scrollTo( 0, window.gc.sb.offset.y +destinationTop, 350)

                })
            }
        });

        // INFO BADGE TOGGLING
        let $infoBadge = $('.info-badge');

        if($infoBadge.length) {
            let $ibContent = $('.info-badge__content'),
                ibContentWidth = $ibContent.width();

            let refreshWidth = H.throttle(() => {
                ibContentWidth = 0;
                $ibContent.children().each(function(){
                    ibContentWidth += $(this).outerWidth();
                });

                if ($ibContent.width() > 0) {
                    $ibContent.width(ibContentWidth);
                }
            }, 250);

            $(document).on('click', '.info-badge__close', function(){
                TweenLite.set($ibContent, {overflow:'hidden'});
                TweenLite.to($ibContent, .5, {width: 0, ease:'Power3.easeOut'});
            });

            // info svg
            $(document).on('click', '.info-badge img', function(){
                let $children = $ibContent.children();
                TweenLite.set($children, {opacity: 0, x: -20});
                TweenLite.set($ibContent, {overflow:'visible'});
                TweenLite.to($ibContent, .5, {width: ibContentWidth, ease:'Power3.easeOut'});
                TweenLite.to($children, 1, {opacity: 1, x: 0, ease:'Power3.easeOut', delay:.2});
            });

            $(window).on('resize',refreshWidth);
        }

        $('.nav-tabs a').click(function () {
            window.location.hash = $(this).attr("href");
        });

        if($("a[href='"+ window.location.hash +"']").length)
            $("a[href='"+ window.location.hash +"']").click();

    },
    finalize() {

        /**
         *
         * WINDOW RESIZE EVENT MANAHEMENT
         *
         **/
        let rtime,
            timeout = false,
            delta = 200,
            rszend = new CustomEvent('rszend');


        $(window).resize(function () {
            rtime = new Date();
            if (timeout === false) {
                timeout = true;
                setTimeout(resizeend, delta);
            }
        });

        function resizeend() {
            if (new Date() - rtime < delta) {
                setTimeout(resizeend, delta);
            } else {
                timeout = false;
                window.dispatchEvent(rszend);
            }
        }

        /**
         *
         * SCROLL ANIM
         *
         */
        $('a[href^="#"].roll').on('click', function (e) {
            e.preventDefault();
            let target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - H.headersHeight - 15,
            }, 800)
        });

        /**
         * OBJECT FIT POLYFILL
         **/
        objectFitImages('.cover');

        /**
         ** ACTIVATE SIM LINK
         **/
        new SimLink({
            target: '[data-sim-link]',
        });

        /**
         * PSEUDO LINK
         * Link che mandano ad altri link presenti nella pagina
         * (necessari per quando si deve inserire un link con indirizzo dinamico nei WYSIWYG)
         */
        let initPseudoLinks = () => {
            $(document).on('click', '.pseudo-link', function(e){
                e.preventDefault();
                let target = $(this).attr('href'),
                    destination = $(target).attr('href');
                if ($(this).attr('target') && $(this).attr('target') === '_blank'){
                    window.open(destination, '_blank');
                } else {
                    window.location.href = destination;
                }

            });
        };

        initPseudoLinks();


        // MODALS
        let updateActiveLabel = ($newMaterial = '') => {
            let $activeLabel = $('.variation-active');

            if ($newMaterial !== '') {
                let  materialName = $newMaterial.find('h4').text(),
                    $label = $('<p class="display-4 ml-3">'+materialName+'</p>'),
                    $preview = $newMaterial.find('.image-color-el').clone();

                $activeLabel.html('');
                $activeLabel.append($preview);
                $activeLabel.append($label);
            } else {
                $activeLabel.html($activeLabel.find('p').data('base-label'));
            }
        };

        let selectMaterial = ($materialWrapper) => {
            let value = $materialWrapper.attr('data-variation-value'),
                refVariation = $materialWrapper.attr('data-variation-reference');

            $('.modal-wrapper').removeClass('checked');
            $materialWrapper.addClass('checked');
            updateActiveLabel($materialWrapper);

            if($('#'+refVariation).find('option[value="'+value+'"]').length){
                document.getElementById(refVariation).value = value;
            }
        };

        let modalVariationOnOpen = function ($trigger) {

            // MANAGING LABEL CHANGE
            let $wrapper = $trigger.parents('.variation-wrapper');

            // first selection for default value
            let $modalSelect = $wrapper.find('select'),
                $modalOptions = $wrapper.find('.modal-wrapper'),
                selected = $modalSelect.val();

            if (selected !== '') {
                selectMaterial($('.dn-modal .modal-wrapper').filter('[data-variation-value="'+selected+'"]'));
            } else {
                $modalOptions.removeClass('checked');
            }

            // Registering Close
            $('.dn-modal .modal-wrapper').on('click', function (e){
                e.preventDefault();
                let $current = $(this),
                    refVariation = $current.attr('data-variation-reference');

                selectMaterial($current);
                Modal.close(() => {
                    $('#'+refVariation).trigger('change');
                });
            });
        };

        const $triggers = $('.modal-trigger[data-modal-type="modal-variation"]');

        if($triggers.length) {
            $triggers.each(function (){
                new Modal($(this), modalVariationOnOpen);
            });

            selectMaterial($('.modal-wrapper[data-variation-value="' + $('#pa_modifica-materiale').val() + '"]'));
        }

        let modalOnNoteOpen = function ($trigger) {
            // Load saved value
            $('.dn-modal textarea').val($trigger.parents('.variation-wrapper').find('textarea').val());
            window.setTimeout(function(){$('.dn-modal textarea').focus()}, 1000); // update with callback in the future...
        };

        let modalOnNoteClose = function ($trigger) {
            // Save value
            $trigger.parents('.variation-wrapper').find('textarea').val($('.dn-modal textarea').val());
        };

        const $triggersNote = $('.modal-trigger[data-modal-type="modal-note"]');

        if($triggersNote) {
            $triggersNote.each(function () {
                new Modal($(this), modalOnNoteOpen, modalOnNoteClose);
                let $ta = $(this).parents('.variation-wrapper').find('textarea');
                // Set default if textarea has text
                $ta.val($ta.text());
            });
        }

        if(!window.gc.isMobile){
            const $miniCart = $('#quick-cart');
            if ($miniCart.length) {
                $('#btn--cart-wrapper').on( 'mouseenter', function(){
                    if($miniCart.find('.widget_shopping_cart_content').length) {
                        $miniCart.addClass('mini-cart__in');
                    }
                });

                $miniCart.on('mouseleave', function(){
                    $miniCart.removeClass('mini-cart__in');
                });
            }
        }

        /**
         * DEV HELPERS
         * from here down
         */

        if (!window.gc.isDev) {
            return;
        }

        let $badge = $('<span id="breakpoint-badge" class="badge badge-secondary">Breakpoints</span>'),
            breakpoints = {
                xs: 576,
                sm: 768,
                md: 992,
                lg: 1200,
            };

        function badgeSwitch(breakpoint) {
            $('#breakpoint-badge').text(breakpoint);
        }

// BADGE
        function checkBreakpoint() {
            let ww = window.innerWidth,
                bps = Object.keys(breakpoints);

            // IF XXS RETURN
            if (ww < breakpoints['xs']) {
                badgeSwitch('xxs');
                return;
            }

            for (let i = 0; i <= bps.length; ++i) {
                let isLast = i + 1 >= bps.length,
                    curr = bps[i],
                    next = bps[i + 1];

                if (ww > breakpoints[curr] && ww < breakpoints[next] || isLast) {
                    badgeSwitch(bps[i]);
                    return;
                }
            }
        }

        $badge.appendTo('body');

        checkBreakpoint();
        $(window).on('resize', checkBreakpoint)
// DO NOT ADD CODE INSIDE HERE

    },
};
