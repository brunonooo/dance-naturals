'use strict';

import H from '../../nooo_modules/nooo-helpers';
import charming from 'charming';
import { TweenLite, TimelineLite } from 'gsap';
import $ from "jquery";
import ConfigChoicesManager from '../modules/ConfigChoicesManager';

export default {
    init() {
        // materials flag
        /*let materialsReady = false;*/
        let isFirstLoad = true;

        /*let swapAttribute = (selector, attribute, callback) => {
            let targets = Array.from(document.querySelectorAll(selector));
            let scheme = [];

            // BUILDING TARGETS CACHE
            targets.forEach((target) => {
                let attrContent = target.getAttribute(attribute);
                if (attrContent != '' ){
                    scheme.push({
                        oldAttr: attrContent,
                        newAttr: '',
                    });
                }
            });

            $.ajax({
                url: window.ajax.ajaxUrl,
                type: 'post',
                data: {
                    action: callback,
                    scheme: scheme,
                },
                success: function( response ) {
                    if (response) {
                        let scheme = JSON.parse(response);
                        scheme.forEach((item) => {
                            let el = document.querySelector('['+attribute+'="'+item.oldAttr+'"]');
                            el.setAttribute(attribute, item.newAttr);
                        });

                        // LAZYLOAD
                        let activeTab = document.querySelector('.vpc-component__active');
                        if(activeTab) {
                            window.imageMage.lazyLoad(activeTab);
                        }

                        materialsReady = true;
                    }
                },
                error: function() {
                    alert('Qualcosa è andato storto! Controlla la rete e riprova più tardi');
                },
            });
        };*/

        // ADD TO CART ANIMATION
        let addMsg = document.getElementById('vpc-message-popup');
        charming(addMsg);
        let letters = Array.from(document.getElementById('vpc-message-popup').childNodes);

        let tl = new TimelineLite({
            paused: true,
            onComplete:function() {
                this.restart();
            },
        });

        tl
            .staggerFromTo(letters, .5, { y: 10, opacity: 0, ease: 'Power4.easeOut'}, { y:0, opacity: 1, ease: 'Power4.easeOut'}, .1)
            .staggerFromTo(letters, .5, { y: 0, opacity: 1, ease: 'Power4.easeIn'}, {y: -10, opacity: 0 ,ease: 'Power4.easeIn' }, .1);

        $("#vpc-add-to-cart").on('click', () => {
            tl.play(0);
        });

        // HELPERS
        let find_tallest_height = (tallest, el) => {
            return Math.max(tallest, el.offsetHeight);
        };

        let option_group_length = (og) => {
            let materials = og.querySelectorAll('.vpc-single-option-wrap'),
                single_width = materials[0].clientWidth,
                og_width = materials.length * single_width;
            TweenLite.set(og, { 'width' : og_width });
        };

        // SHOE PART HIGHLIGHTING
        let shoePartHighlighting = function() {
            $(document).on('mouseenter','.vpc-component-header', function(){
                let index = $('.vpc-component-header').index($(this));

                // BAIL EARLY IF ALREADY IN EDITING MODE
                if ($('.vpc-component__active').length) return;

                $('.vpc-preview').each(function(){
                    let img = $(this).find('img')[index];
                    $(img).addClass('currently-editing');
                });
            });

            $(document).on('mouseleave','.vpc-component-header', function(){
                $('.vpc-preview img').removeClass('currently-editing');
            });
        };

        /**
         * C INTERACTION AND UX
         **/
        //vpc-mva-plugin.js hack
        window.mvaSlider.redrawSlider();

        /**
         * Component Class (Keeping it organised)
         */
        class Configurator {
            constructor() {
                this.loading= false;
                this.$componentsWrapper= $('#vpc-components');
                this.componentsWrapperHeight= 0;
                this.componentsWrapperOffset= 0;
                this.$buyButton= $('#vpc-add-to-cart');
                this.$choiceActions= $('.choice-actions button');
                this.$mainLabel= $('.vpc-main-label');
                this.$mainLabelPrefix= $('.vpc-main-label-prefix');
                this.prefixWidth= 0;
                this.isOpen = false;
                this.currentComponent= null;
                this.$componentsHeaders = $('.vpc-component .vpc-component-header');
                this.lastAnimation = null;
            }

            getSelectedComponent (node){
                let component = {};
                component.$el = $(node).parent('.vpc-component');
                component.$siblings = component.$el.siblings();
                component.height = component.$el.outerHeight(true);
                component.width = component.$el.width();
                component.translate = component.$el.offset().top - C.componentsWrapperOffset.top;
                component.$options = component.$el.find('.vpc-options');
                component.$next = component.$el.next();
                component.$name = $('<span class="vpc-component-name">'+component.$el.find('.vpc-component-name').text()+'</span>');
                return component;
            }

            refreshLayoutSpacings () {
                this.componentsWrapperHeight = this.$componentsWrapper.outerHeight();
                this.componentsWrapperOffset = this.$componentsWrapper.offset();
                this.prefixWidth = this.$mainLabelPrefix.width();
            }

            addLoader () {
                this.loading = true;
                $('#preloader').removeClass('killed');
                $('body').addClass('page-loading');
            }

            removeLoader () {
                this.loading = false;
                $('body').removeClass('page-loading');
                window.setTimeout(() => {
                    $('#preloader').addClass('killed');

                    if (isFirstLoad) { // On first load dispatch pageload event
                        isFirstLoad = false;
                        window.dispatchEvent(pageLoadEnd);
                    }
                }, 100);
            }

            selectOption (target) {
                // Bail early if already selected
                if($(target).parent('.vpc-single-option-wrap').hasClass('vpc-single-option-wrap--selected') ) {
                    return;
                }

                let views_list = Array.from(document.querySelectorAll('.vpc-preview')),
                    views = views_list.map(function(view){return view.getAttribute('data-view-name')});

                // Add a class for the element selected to stylizing its focus state
                let $ctx = $(target).parents('.vpc-component');
                $('.vpc-single-option-wrap--selected', $ctx).removeClass('vpc-single-option-wrap--selected');
                $(target).parent('.vpc-single-option-wrap').addClass('vpc-single-option-wrap--selected');


                // MOVE TO BEST AVAILABLE VIEW
                let $input = $(this).find('input'),
                    active_view = window.mvaSlider.getCurrentSlide(),
                    view_type = $('.vpc-preview[data-view="'+active_view+'"]').attr('data-view-name'),
                    data_view = 'data-'+view_type,
                    input_view = $input.attr(data_view);

                if (!input_view) {
                    /* eslint-disable */
                    for (var i = 0; i < views.length; ++i) {
                        let closest_view_url = $input.attr('data-'+views[i]);
                        if (closest_view_url) {
                            window.mvaSlider.goToSlide(i);
                            return;
                        }
                    }
                    /* eslint-enable */
                }
            }

            init () {
                let ref = this;
                shoePartHighlighting();

                // Redraw Slider to fit images in view
                window.mvaSlider.redrawSlider();

                if (!window.gc.isMobile) {
                    $(window).on('resize', H.throttle(ref.refreshLayoutSpacings.bind(ref), 100));
                    this.refreshLayoutSpacings();
                }

                // Hide ChoiceActions on init
                TweenLite.set(this.$choiceActions, { yPercent: '-300%'});

                // Enable slider controls
                $('.bx-controls').removeClass('disabled');

                // Add listeners for configurator actions
                $(document).on('click', '.vpc-component-header', function () {
                    const currentlySelected = ref.getSelectedComponent(this);

                    if( !ref.component ){
                        ref.component = currentlySelected
                        animateOpen();
                        choicesManager.cacheCurrentSelection(ref.component.$el.get(0));
                    } else {
                        ref.selectOption(currentlySelected);
                        choicesManager.saveSelectionData();
                        animateClose();
                    }
                });

                $(document).on('click', '.choice-actions button', function (e) {
                    choicesManager.handleChoice(e.target, animateClose);
                });

                $(document).on('click', '.vpc-single-option-wrap', function (e) {
                    ref.selectOption(e.target);
                });
            }
        }

        // Little helper to avoid loading massive images for icons
        const addIconExtention = (url, sizes = '-150x150') => {
            const urlPieces = url.split('.');
            const ext = [...urlPieces].reverse()[0]; // Spreading array for immutability
            const urlFirstPart = urlPieces.slice(0, -1).join('.');
            return urlFirstPart + sizes + '.' + ext;
        }

        let C = new Configurator();

        // Open and Close Animations
        let animateOpen = (cb = false) => {
            C.isOpen = true;
            let openTL = new TimelineLite({paused: true}),
                handleOpenOnStart = function () {
                    C.component.$el.addClass('vpc-component__active');
                    C.$mainLabel.append(C.component.$name);
                    C.component.$name = C.$mainLabel.find('.vpc-component-name');
                    if(typeof cb === 'function'){cb()}
                },
                handleOpenCompleted = function () {
                    // REMOVE GLOW
                    $('.vpc-preview img').removeClass('currently-editing');

                    let spyon = C.component.$options.data('target');
                    C.component.$options.scrollspy({target: spyon});

                    // LAZY LOAD MATERIALS
                    const $materials = C.component.$el.find('input[type="radio"]');
                    $materials.each(function() {
                        const iconData = $(this).data('icon')
                        const iconParsedData = iconData.split('|');
                        const iconImageTag = iconParsedData[2] ? iconParsedData[2] : '<img src="/'+addIconExtention(iconParsedData[0])+'"/>';
                        $(this).siblings('label').find('.img-wrapper').html(iconImageTag);
                    })

                    if (typeof cb === 'function'){cb()}

                    // SCROLL TO BOTTOM ON TINY PHONES AND SAFARI IOS (bottom navbar hinding buy buttons)
                    if (window.innerWidth < 768){
                        $('html,body').animate({scrollTop: document.body.scrollHeight},"fast");
                    }
                };

            openTL.eventCallback('onComplete', handleOpenCompleted);
            openTL.eventCallback('onStart', handleOpenOnStart);


            if(H.isVertical()) {
                openTL
                    .timeScale(4)
                    .to(C.$componentsHeaders, .5, {
                            opacity: 0,
                            yPercent: '-8%',
                            ease: 'Expo.easeOut',
                        },
                    )
                    .addLabel('buttonStart', '-=.2')
                    .set(C.component.$el, {
                        width: '100%',
                    })
                    .set(C.component.$siblings, {
                        display: 'none',
                    })
                    .to(C.component.$options, .8, {
                        display: 'flex',
                        autoAlpha: 1,
                        y: 0,
                        ease: 'Power3.easeInOut',
                    }, '-=0.2')
                    .to(C.$mainLabelPrefix, .5, {
                        y: -30,
                        opacity: 0,
                    }, '-=0.5')
                    .to(C.$mainLabel, 1, {
                        x: -C.prefixWidth,
                        ease: 'Expo.easeInOut',
                    }, '-=0.2')
                    .to(C.component.$name, 1, {
                        x: 0,
                        opacity: 1,
                        ease: 'Expo.easeInOut',
                    }, '-=0.8')
                    .to(C.$buyButton, 1, {
                        yPercent: '200%',
                        ease: 'Expo.easeIn',
                    }, 'buttonStart')
                    .to(C.$choiceActions, 1.2, {
                        yPercent: '0%',
                        ease: 'Expo.easeOut',
                    }, '-=.5' )
                    .set(C.$componentsWrapper, {'overflow-y': 'hidden'});
            } else {
                openTL
                    .timeScale(4)
                    .set(C.component.$el, {
                        position: 'absolute',
                        y: 0,
                    })
                    .set(C.component.$next, {
                        marginTop: C.component.height,
                    })
                    .to(C.$buyButton, 1, {
                        yPercent: '200%',
                        ease: 'Expo.easeIn',
                    }, 0)
                    .to(C.$mainLabel, 1, {
                        x: -C.prefixWidth,
                        ease: 'Expo.easeInOut',
                    }, 0)
                    .to(C.component.$name, 1, {
                        x: 0,
                        opacity: 1,
                        ease: 'Expo.easeInOut',
                    }, 0)
                    .to(C.$choiceActions, 1.2, {
                        yPercent: '0%',
                        ease: 'Expo.easeOut',
                    }, '-=0.8')
                    .to(C.$mainLabelPrefix, 1, {opacity: 0}, 0)
                    .staggerTo(C.component.$siblings, .8, {
                        xPercent: '-50%',
                        opacity: 0,
                        ease: 'Power3.easeInOut',
                    }, .15, 0)
                    .to(C.component.$el, 1, {
                        height: C.componentsWrapperHeight,
                        y: -C.component.translate,
                        ease: 'Power3.easeInOut',
                    }, 0)
                    .to(C.component.$options, .8, {
                        display: 'block',
                        autoAlpha: 1,
                        scale: 1,
                        ease: 'Power3.easeInOut',
                    }, '-=0.8')
                    .set(C.$componentsWrapper, {'overflow-y': 'hidden'});
            }

            if (C.lastAnimation) {
                C.lastAnimation.pause();
            }
            openTL.invalidate().restart();
            C.lastAnimation = openTL;
        };

        let animateClose = (cb = false) => {
            let closeTL = new TimelineLite({paused: true}),
                handleCloseCompleted = function () {
                    C.component.$el.removeClass('vpc-component__active');
                    C.$mainLabel.find('.vpc-component-name').remove();
                    C.component = null;
                    if(typeof cb === 'function'){cb()}

                    C.isOpen = false;
                    // SCROLL BACK TOP ON TINY PHONES AND SAFARI IOS (bottom navbar hinding buy buttons)
                    if (window.innerWidth < 768){
                        $('html,body').animate({scrollTop: 0},"fast");
                    }
                };

            closeTL.eventCallback('onComplete', handleCloseCompleted);

            if(H.isVertical()) {
                closeTL
                    .timeScale(4)
                    .to(C.component.$options, .8, {
                        autoAlpha: 0,
                        y: 50,
                        ease: 'Power3.easeInOut',
                    })
                    .set(C.component.$siblings, {
                        display: 'block',
                    })
                    .to(C.$componentsHeaders, .5, {
                            opacity: 1,
                            yPercent: '0%',
                            ease: 'Expo.easeOut',
                        },
                    )
                    .to(C.component.$el, 1, {height: C.component.height, width: C.component.width, y: 0, ease: 'Power3.easeInOut'}, '-=1')
                    .to(C.$choiceActions, 1, {yPercent: '-300%', ease: 'Power3.easeIn'}, 0)
                    .to(C.component.$name, 1, {xPercent: '50%', opacity: 0, ease: 'Expo.easeInOut'}, 0)
                    .to(C.$mainLabel, 1, {x: 0, ease: 'Expo.easeInOut'}, '-=0.2')
                    .to(C.$mainLabelPrefix, .5, {opacity: 1, y: 0}, '-=0.2')
                    .set(C.component.$el, {position: 'relative', display: 'block'})
                    .set(C.component.$next, {marginTop: 0})
                    .to(C.$buyButton, 1.2, {yPercent: '0%', ease: 'Expo.easeOut'}, '-=.5')
                    .set(C.$componentsWrapper, {'overflow-y': 'scroll'});
            } else {
                closeTL
                    .timeScale(4)
                    .to(C.$choiceActions, 1, {yPercent: '-300%', ease: 'Power3.easeIn'}, 0)
                    .to(C.component.$name, 1, {xPercent: '50%', opacity: 0, ease: 'Expo.easeInOut'}, 0)
                    .to(C.$mainLabel, 1, {x: 0, ease: 'Expo.easeInOut'}, '-=0.2')
                    .to(C.$mainLabelPrefix, 1, {opacity: 1}, '-=1')
                    .to(C.component.$el, 1, {height: C.component.height, y: 0, ease: 'Power3.easeInOut'}, '-=1')
                    .set(C.component.$el, {position: 'relative'})
                    .set(C.component.$next, {marginTop: 0})
                    .to(C.component.$options, .8, {
                        display: 'none',
                        autoAlpha: 0,
                        scale: .85,
                        ease: 'Power3.easeInOut',
                    }, '-=0.5')
                    .staggerTo(C.component.$siblings, 1.5, {
                        xPercent: '0%',
                        opacity: 1,
                        ease: 'Power3.easeOut',
                    }, .1, '-=1')
                    .to(C.$buyButton, 1.2, {yPercent: '0%', ease: 'Expo.easeOut'}, '-=.5')
                    .set(C.$componentsWrapper, {'overflow-y': 'scroll'});
            }

            if (C.lastAnimation) {
                C.lastAnimation.pause();
            }
            closeTL.invalidate().restart();
            C.lastAnimation = closeTL;
        };

        // Initializing Choices Manager ( X or V buttons behaviour )
        const choicesManager = new ConfigChoicesManager(document.querySelector('.vpc-update button'), document.querySelector('.vpc-go-back button'));

        let recalc_operations = () => {
            if (window.VPC_groups_manager) window.VPC_groups_manager.activate_all_groups();

            H.checkOrientation();

            let options = Array.from(document.querySelectorAll('.vpc-options')),
                components = Array.from(document.querySelectorAll('.vpc-component')),
                labelPrefix = Array.from(document.querySelectorAll('.vpc-main-label-prefix')),
                componentHeaders = Array.from(document.querySelectorAll('.vpc-component-header')),
                componentsOptions = Array.from(document.querySelectorAll('.vpc-group__options'));

            // Removing all styles from affected nodes
            let mobileAffectedNodes = [
                ...options,
                ...components,
                ...labelPrefix,
                ...componentHeaders,
                ...componentsOptions,
            ];

            for (let [index, el] of mobileAffectedNodes.entries() ){
                console.log(index, el);
                el.style = '';
            }

            if (H.isVertical()) {
                TweenLite.set(options, {'display': 'flex', autoAlpha: 0, y: 50});

                let tallest_options = [];
                let name_height = components[0].querySelector('.vpc-group-name__container').offsetHeight;
                components.forEach((component) =>{
                    let options_groups = Array.from(component.querySelectorAll('.vpc-group__options'));
                    tallest_options.push(options_groups.length ? options_groups.reduce(find_tallest_height, 0) : options_groups[0].offsetHeight);
                    Array.from(component.querySelectorAll('.vpc-group__options')).forEach(option_group_length);
                });

                let tallest_option = Math.max(...tallest_options);
                TweenLite.set(components, {'height': tallest_option + name_height});

            }

            TweenLite.set(options, {'display': 'none'});

            if (window.VPC_groups_manager) window.VPC_groups_manager.activate_current_in_groups();
            window.mvaSlider.redrawSlider();

            C.refreshLayoutSpacings();

            C.removeLoader();
        };

        let calculate_mobile_sizes = () => {
            C.addLoader();

            if (C.isOpen) {
                animateClose(recalc_operations)
            } else {
                recalc_operations(false);
                C.removeLoader();
            }
        };

        if (window.gc.isMobile) {
            $(window).on('resize', H.throttle(calculate_mobile_sizes, 100));
        }

        let pageLoadEnd = new CustomEvent('pageloadend');

        window.addEventListener('pageloadend', function(){
            // Initializing Configurator
            C.init();
        });

        if (window.gc.isMobile) {
            calculate_mobile_sizes();
        } else {
            C.removeLoader();
        }
    },
    finalize() {
    },
};
