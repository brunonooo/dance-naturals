'use strict';

export default {
    init() {
        let registerAccessoriesInput = () => {
            $('.woocommerce-cart-form').on('change', '.accessories input[type="checkbox"]', () => {
                /*let $item = $(e.target).parents('.woocommerce-cart-form__cart-item'),
                    itemKey = $item.data('item-key'),
                    $selectedAccessories = $item.find('input[type="checkbox"]:checked');*/
                let $updateButton = $("button[name='update_cart'],input[name='update_cart']");

                $updateButton.removeAttr('disabled')
                    .click()
                    .prop('disabled', true);

            });
        };

        registerAccessoriesInput();

        $(document).on('updated_wc_div', () => {
            window.gc.checkboxesInit();
            registerAccessoriesInput();
        });

        /*
         * OVERRIDING TOGGLE SHIPPING CALCULATOR
         * to avoid overlapping cart totals to other elements of the ui
         */
        let $cartTotals = $('.cart-collaterals');
        $(document).on('click', '.shipping-calculator-button ', function(e){
            e.preventDefault();
            let y = $cartTotals.css('transform').split(',')[5].replace(')', '').trim();
            if (parseInt(y) > 0) {
                $cartTotals.css({
                    'transform' : 'translateY(0)',
                    'margin-top': y+'px',
                });
            } else {
                let mt  = $cartTotals.css('margin-top');
                $cartTotals.css({
                    'transform' : 'translateY('+mt+')',
                    'margin-top': '0',
                });
            }
        })

        const $invalidVariations = $('.invalid-variations');
        if ($invalidVariations.length) {
            const $checkoutButton = $('.wc-proceed-to-checkout .checkout-button')
            $checkoutButton.attr("disabled", "disabled");
            $checkoutButton.on('click', function (e) {
                e.preventDefault();
                window.alert(window.gc.labels.variations_alert);
            })
        }
    },
    finalize() {
    },
};
