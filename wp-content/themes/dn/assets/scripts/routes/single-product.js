'use strict';

import 'lightslider';
import H from '../../nooo_modules/nooo-helpers';

export default {
    init() {
        if (window.gc.isMobile){
            let slider = null;

            const initSlider = () => {
                return $('.woocommerce-product-gallery__wrapper').lightSlider({
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    slideMove: 1,
                });
            }

            if (H.isVertical()) {
                slider = initSlider();
            }

            $(window).resize(H.throttle(function () {
                if (H.isVertical()) {
                    slider.destroy();
                    slider = initSlider();
                } else {
                    if(slider) {
                        slider.destroy();
                    }
                }
            }, 500));
        }

        /**
         * MANAGING PRICE CHANGE ON ADD TO CART BUTTON
         */
        let $addToCartWrapper =  $('.woocommerce-variation-add-to-cart'),
            $addToCart = $addToCartWrapper.find('.single_add_to_cart_button'),
            addToCartText = $addToCart.text(),
            btnsEnabled = true;

        let setProdNotAvailable = function(){
            btnsEnabled = false;
            $addToCartWrapper.addClass('disabled-buttons');
        };

        let setProdAvailable = function() {
            btnsEnabled = true;
            $addToCartWrapper.removeClass('disabled-buttons');
        };

        let calc_price = (variation_price) => {
            let total = variation_price,
                $config_price_element = $('[data-config-price]');

            if($config_price_element.length) {
                total += parseInt($config_price_element.data('config-price'));
            }

            return total+'€';
        };

        // TO REFACTOR
        $('body').on('show_variation', function(e, variation){
            let price = calc_price(variation.display_price);
            if (price) {
                setProdAvailable();
            } else {
                setProdNotAvailable();
                price = 'NON DISPONIBILE';
            }

            $addToCart.html(price+' '+addToCartText);
        });

        $addToCartWrapper.click(function(e){
            if(!btnsEnabled){
                e.preventDefault();
                e.stopPropagation();
                /* eslint-disable */
                console.log('Stop clicking please... Buttons are disabled!');
                /* eslint-enable */
                return;
            }
        });

        let FakeVariationsSelects = () => {
            // TO REFACTOR! IT CAN BE IMPROVED A LOT

            // RADIO FOR HEELS
            // implemented static because its the day before go live
            let $radioWrapper = $('.radio-wrapper');

            if($radioWrapper.length){
                let $selectHeel = $('#pa_design-tacco').length > 0 ? $('#pa_design-tacco') : $('#pa_scegli-il-design-tacco');
                let $radioButtons = $radioWrapper.find('button');

                let selectHeelDesign = ($button) => {
                    let value = $button.data('variation-value');
                    $radioWrapper.removeClass('checked');
                    $button.parents('.radio-wrapper').addClass('checked');

                    if($selectHeel.find('option[value="'+value+'"]').length){
                        $selectHeel.val(value);
                    }
                };

                // first selection for default value
                let def = $selectHeel.val();
                $radioWrapper.removeClass('checked');
                $radioButtons.filter('[data-variation-value="'+def+'"]').parents('.radio-wrapper').addClass('checked');

                $radioButtons.click(function (e){
                    e.preventDefault();
                    selectHeelDesign ($(this));
                    $selectHeel.trigger('change');
                });

                let selectHeelHeight = document.getElementById('pa_tacco'),
                    $metallo = $('.metallo-radio-wrapper, .oro-radio-wrapper, .antracite-radio-wrapper, .metallo-en-radio-wrapper, .oro-en-radio-wrapper, .antracite-en-radio-wrapper');

                // Hide metal if heel height is lower than 5cm
                $('body').on('change', '.variation-wrapper select', function(){
                    let heelHeight = selectHeelHeight.value,
                        heelH = heelHeight !== '' ? parseInt(heelHeight.replace('cm', '')) : false;
                    // Bail Early if is correct Metal height
                    if(!heelH  || heelH >= 7) {
                        $metallo.css({
                            'display' : 'flex',
                        });
                    } else {
                        $metallo.hide();
                        if($metallo.hasClass('checked')) {
                            selectHeelDesign($('[data-variation-value="moderno"]'));
                            $selectHeel.trigger('change');
                        }
                    }
                });
            }

            if($('select#pa_scegli-il-design-tacco').length){
                // Hide metal if heel height is lower than 5cm
                let selectHeelHeight = document.getElementById('pa_tacco');
                let $selectHeel = $('select#pa_scegli-il-design-tacco');

                $('body').on('change', '.variation-wrapper select', function(){
                    let heelHeight = selectHeelHeight.value,
                    heelH = heelHeight !== '' ? parseInt(heelHeight.replace('cm', '')) : false;
                    if(heelH === 5)
                    {
                        $("select#pa_scegli-il-design-tacco option").filter(function(){return $(this).val().includes("metallo")}).hide();
                        $selectHeel.val("");
                    }else
                    {
                        $("select#pa_scegli-il-design-tacco option").filter(function(){return $(this).val().includes("metallo")}).show();
                    }
                });
            }
        };

        FakeVariationsSelects();
    },
    finalize() {
        Array.from(document.querySelectorAll('.variation-wrapper select')).map(select => {
            const options = Array.from(select.querySelectorAll('option'));
            if(options.length === 2){
                select.value = options[1].value;
            }
        })
    },
};
