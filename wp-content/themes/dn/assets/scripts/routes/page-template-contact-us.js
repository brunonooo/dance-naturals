'use strict';

import Maps from '../../nooo_modules/maps/man-maps';
import mapStyles from '../../nooo_modules/maps/map-style.json';

export default {
    init() {
        window.mapsManager.mapsInit = () => {
            //INIT MAPS
            window.mapsManager.map = new Maps({
                selector    : '.map-canvas',
                center : {
                    lat     : 45.486596,
                    lng     : 11.988424,
                },
                forceCenter: false,
                forceZoom: false,
                mapOptions      : {
                    center              : {
                        lat     : 45.486596,
                        lng     : 11.988424,
                    },
                    mapTypeId           : window.google.maps.MapTypeId.ROADMAP,
                    scrollwheel         : false,
                    gestureHandling     : 'cooperative',
                    maxZoom             : 10,
                    mapTypeControl      : false,
                    streetViewControl   : false,
                    rotateControl       : false,
                    styles: mapStyles,
                },
                markers     : {
                    '.marker' : {
                        icon  : '/assets/images/logo-dn.png',
                        size  : {
                            w : 50,
                            h : 27,
                        },
                    },
                },
            });

            window.mapsManager.map.init();
        };

        window.mapsManager.myMapsCheck();
    },
    finalize() {
    },
};
