'use strict';

import { TweenLite, TimelineLite } from 'gsap';

export default {
    init() {
        /**
         * ACCORDION EXTRA ACTIVATORS
         */
        let $activators = $('[data-accordion-trigger]'),
            $accordion = $('#accordion');

        $accordion.on('hide.bs.collapse', function(){
            $accordion.removeClass('accordion--open');
        });

        $accordion.on('shown.bs.collapse', function(){
            $accordion.addClass('accordion--open');
        });

        if($activators.length && $accordion.length) {
            $activators.on('click', function(){
                let id = $(this).data('accordion-trigger');
                let $target =  $accordion.find('#'+id+'-collapsible');

                if($target.length){
                    $target.collapse('toggle');
                }
            });
        }

        /**
         * ADDRESSES MODIFICATION FORMS
         */

        let $formsWrapper = $('.form-address-wrapper'),
            $formTriggers = $('[data-address-target]');

        if ($formsWrapper.length && $formTriggers.length) {

            // INIT SELECTWOO FOR SELECTS OUTSIDE OF WOOCOMMERCE CONTAINER
            $formsWrapper.find('select').each(function(){
                let parent = $(this).parent();

                $(this).selectWoo({
                    dropdownParent: parent,
                });
            });

            let main = document.querySelector('main'),
                formAnimation = new TimelineLite({paused: true}),
                $activeForm = null,
                $forms = $('.form-address'),
                $formsClose = $('.form-address-close');


            let showActiveForm = () => {
                TweenLite.set($activeForm, { autoAlpha: 1, display: 'block' });
            };

            let hideActiveForm = () => {
                TweenLite.set($forms, { autoAlpha: 0, display: 'none' });
            };

            // GET FORM WRAPPER READY TO ANIMATE IN
            TweenLite.set($formsWrapper, { yPercent: '20%', autoAlpha: 0 });
            TweenLite.set($forms, { autoAlpha: 0, display: 'none' });

            formAnimation
                .to( main, .8, { scale: .85, autoAlpha: 0, ease: 'Power4.easeInOut'})
                .to( $formsWrapper, .8, {  yPercent: '0%', autoAlpha: 1, ease: 'Power4.easeInOut'}, '-=0.4');

            formAnimation.eventCallback('onStart', showActiveForm);
            formAnimation.eventCallback('onReverseComplete', hideActiveForm);

            let formIn = () => {
                if (window.gc.sb){
                    window.gc.sb.block(true);
                } else {
                    document.querySelector('body').style.overflow = 'hidden';
                }

                formAnimation.play();
            };

            let formOut = () => {
                if (window.gc.sb){
                    window.gc.sb.block(false);
                } else {
                    document.querySelector('body').style.overflow = 'visible';
                }

                formAnimation.reverse(null , false);
            };

            $formTriggers.on('click', function(){
                let id = $(this).data('address-target');
                $activeForm =  $('#'+id);

                if($activeForm.length){
                    formIn();
                }
            });

            $formsClose.on('click', function(e){
                if (!e.target.closest('.form-address')){
                    formOut();
                }
            });
        }

        /**
         * LOGIN ANIMATIONS
         */
        let login = document.querySelector('#customer_login');

        if (login) {
            let loginImgWrapper = login.querySelector('.img-wrapper'),
                loginImg = loginImgWrapper.querySelector('img'),
                loginContent = document.querySelector('.login__content'),
                loginContentCol = loginContent.querySelector('.login__content-col'),
                loginToggle = document.querySelector('.login__col .login__toggle'),
                registerContent = document.querySelector('.register__content'),
                registerContentCol = registerContent.querySelector('.register__content-col'),
                registerToggle = document.querySelector('.register__col .register__toggle'),
                loginTL = new TimelineLite({
                    paused: true,
                });

            // GETTING READY TO ANIMATE
            TweenLite.set(registerContent, { xPercent : '100%' });
            TweenLite.set(loginToggle, { visibility: 'hidden', opacity: 0 });
            TweenLite.set(registerToggle, { visibility: 'visible', opacity: 1 });

            loginTL
                .fromTo(registerToggle, .3, { visibility: 'visible', opacity: 1 }, { visibility: 'hidden', opacity: 0 })
                .fromTo(loginContent, 1, { xPercent: '0%', ease: 'Power4.easeInOut' }, { xPercent : '-100%',  ease: 'Power4.easeInOut' }, 0)
                .fromTo(loginContentCol, 1, { xPercent: '0%', opacity: 1, ease: 'Power4.easeInOut' }, { xPercent : '100%', opacity: 0,  ease: 'Power4.easeInOut' }, 0)
                .fromTo(loginImg, 2, { xPercent: '0%', ease: 'Power4.easeInOut'  }, { xPercent: '-20%', ease: 'Power4.easeInOut' }, '-=1')
                .fromTo(registerContent, 1, { xPercent: '100%', ease: 'Power4.easeInOut'  }, { xPercent: '0%', ease: 'Power4.easeInOut' }, '-=1.5')
                .fromTo(registerContentCol, 1, { xPercent: '-100%', opacity: 0, ease: 'Power4.easeInOut' }, { xPercent : '0%', opacity: 1, ease: 'Power4.easeInOut' }, '-=1.5')
                .fromTo(loginToggle, .3, { opacity: 0, visibility: 'hidden' }, { opacity: 1, visibility: 'visible' });

            let viewToggle = (action) => {
                if (action == 'register') {
                    loginTL.play();
                    if (window.gc.isMobile || $(window).width() < 992){
                        TweenLite.set(registerContent, { zIndex: 2});
                    }
                } else {
                    loginTL.reverse();
                    if (window.gc.isMobile || $(window).width() < 992){
                        TweenLite.set(registerContent, { zIndex: 0});
                    }
                }
            };

            document.addEventListener('click', function(e){
                if( e.target.closest('.login__toggle') || e.target.closest('.register__toggle')) {
                    let action = e.target.closest('.login__toggle') ? 'login' : 'register';
                    viewToggle(action);
                }
            });
        }

        /*
         * MANAGE HASH CONTROLS IN URLs
         */
        let openOrders = () => {
            $('#orders-collapsible').collapse('show');
        };

        let registerHandler = () => {
            $('.register__toggle').click();
        };

        let loginHandler = () => {
            $('.login__toggle').click();
        };

        let hashHandlers = {
            orders: openOrders,
            register: registerHandler,
            login: loginHandler,
        };

        let handleHashUrls = () => {
            // CLOSE MENU IF IT's OPEN
            if ($('[data-menu-open="true"]').length) {
                $('.btn--menu').click();
            }

            let hash = window.location.hash.replace('#', '');

            if (hashHandlers.hasOwnProperty(hash)){
                hashHandlers[hash]();
            }
        };

        // LISTEN ALSO FOR IN PAGE DYNAMIC HASH CHANGES
        window.addEventListener('hashchange', () => {
            handleHashUrls();
        });

        if (window.location.hash) {
            handleHashUrls();
        }

        // TO REFACTOR!!!
        $('.woocommerce-error, .woocommerce-info, .woocommerce-message').click(function(){
            let $target = $(this);
            $target.animate({bottom: '-500px'}, 600, function(){
                $target.hide();
            });
        });

    },
    finalize() {
    },
};
