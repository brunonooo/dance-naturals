'use strict';

// ANIMATION LIBRARIES
import ScrollMagic from 'scrollmagic';
import { TweenLite, TimelineLite } from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import * as PIXI from 'pixi.js';

import 'imagesloaded';


export default {
    init() {
        let windowHeightDuration;

        function updateDuration () {
            windowHeightDuration = window.innerHeight;
        }

        $(window).on("resize", updateDuration);
        $(window).triggerHandler("resize");

        /**
         * STARTING UP ANIMATIONS
         */
        let scOptions = {};

        if(!window.gc.isMobile) {
            scOptions = {
                refreshInterval: 0,
            }
        }

        let scenes = [];
        let ctrl = new ScrollMagic.Controller(scOptions);

        /**
         * HEADER CLONE AND ANIMATION
         */

        let headerSwap = document.querySelector('[data-header-swapper]');

        if(headerSwap){
            let header = document.getElementById('main-navigation'),
                headerClone = header.cloneNode(true);

            headerClone.querySelector('.main-navigation__nav').classList.remove('main-navigation__nav--transparent');
            let cloneMarkup = `<div class="header-mask">${headerClone.outerHTML}</div>`;
            header.insertAdjacentHTML('afterend', cloneMarkup);

            let headerMask = document.querySelector('.header-mask'),
                headerCloneEl = headerMask.querySelector('.main-navigation'),
                headersHeight = header.offsetHeight,
                headerNav = header.querySelector('.main-navigation__nav');

            let headerMaskAnimation = new ScrollMagic.Scene({
                duration: headersHeight,
                triggerHook: 0,
                offset: -headersHeight-30,
                triggerElement: headerSwap,
            })
                .setTween(TweenLite.to( headerMask, 1, { y: '0', ease: 'Power0.easeNone' }));

            let headerCloneAnimation = new ScrollMagic.Scene({
                duration: headersHeight,
                triggerHook: 0,
                offset: -headersHeight-30,
                triggerElement: headerSwap,
            })
                .setTween(TweenLite.to( headerCloneEl, 1, { y: '0', ease: 'Power0.easeNone' }));

            headerCloneAnimation.on('end', (e) => {
                if (e.state == 'AFTER' ) {
                    headerNav.setAttribute('data-passed-welcome', true);
                } else {
                    headerNav.setAttribute('data-passed-welcome', false);
                }
            });

            scenes.push(headerMaskAnimation);
            scenes.push(headerCloneAnimation);
        }

        /**
         * WELCOME STUFF
         */
        let heroTitle = document.getElementById('hero-title'),
            hero      = document.getElementById('hero'),
            heroImg   = hero.querySelector('.cover'),
            frameTop = hero.querySelector('.hero__frame--top'),
            frameLeft = hero.querySelector('.hero__frame--left'),
            frameBottom = hero.querySelector('.hero__frame--bottom'),
            frameRight = hero.querySelector('.hero__frame--right'),
            heroCanvas = hero.querySelector('.hero__canvas'),
            navLogo = document.querySelector('#nav-logo'),
            heroTl = new TimelineLite({
                paused: true,
            });

        /**
         * WELCOME DISPLACEMENT ANIMATION
         * TEMPORARILY REMOVING ALL ANIMATIONS IF THERE IS NO CANVAS
         */
        if (heroCanvas) {
            let displaceHero = () => {
                let renderer            = new PIXI.autoDetectRenderer({width : window.innerWidth, height: window.innerHeight, view: heroCanvas});
                let stage               = new PIXI.Container();
                let slidesContainer     = new PIXI.Container();
                let displacementSprite  = new PIXI.Sprite.fromImage( window.gc.themePath+'/assets/images/ripple.jpg' );
                let displacementFilter  = new PIXI.filters.DisplacementFilter( displacementSprite );

                function initPixi(){
                    stage.addChild(slidesContainer);
                    displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
                    stage.filters = [displacementFilter];
                    stage.addChild(displacementSprite);
                }

                let src           = heroImg.getAttribute('src');

                function loadSprite(src){
                    let texture = new PIXI.Texture.fromImage(src);
                    let sprite = new PIXI.Sprite(texture);

                    // CENTER SPRITE
                    sprite.anchor.set(0.5);
                    sprite.x = renderer.width / 2;
                    sprite.y = renderer.height / 2;

                    // ADD TO RENDERER
                    slidesContainer.addChild(sprite);
                }

                let ticker = new PIXI.ticker.Ticker();
                ticker.autoStart = true;
                displacementSprite.scale.x = 2;
                displacementSprite.scale.y = 2;
                ticker.speed = .1;

                ticker.add(function( delta ) {

                    displacementSprite.x += 8 * delta;
                    displacementSprite.y += 2;

                    renderer.render( stage );

                });

                initPixi();
                loadSprite(src);
            };

            displaceHero();

            /**
             * WELCOME APPEAR ANIMATION
             */
            heroTl
                .fromTo( heroCanvas, 3, { scale : 0.98, opacity: 0 },{ scale : 1.1, opacity: 1, ease: 'Power4.easeOut' })
                .fromTo( frameTop , 2, { scaleY : 0 }, { scaleY : 1, ease: 'Power4.easeOut' }, 0)
                .fromTo( frameLeft , 2, { scaleX : 0 }, { scaleX : 1, ease: 'Power4.easeOut' }, 0)
                .fromTo( frameBottom , 2, { scaleY : 0 }, { scaleY : 1, ease: 'Power4.easeOut' }, 0)
                .fromTo( frameRight , 2, { scaleX : 0 }, { scaleX : 1, ease: 'Power4.easeOut' }, 0)
                .to( navLogo , 2, { y : 32, ease: 'Power4.easeOut' }, 0);

            /**
             * LOGO FALLING BACK AT SCROLL
             */

            let navLogoAnimation = new ScrollMagic.Scene({
                triggerHook: 0,
                triggerElement: hero,
                duration: windowHeightDuration / 1.3,
            })
                .setTween(TweenLite.to(navLogo, 1, { y: 0, ease: 'Power0.easeNone' } ));

            scenes.push(navLogoAnimation);

            /**
             * WELCOME PARALLAX
             */
            let wParallax = new ScrollMagic.Scene({
                triggerHook: .05,
                triggerElement: hero,
                duration: "100%",
            })
                .setTween(TweenLite.to( heroTitle, 1, { yPercent: '35%', opacity: 0, ease: 'Circ.easeNone' }));

            scenes.push(wParallax);

            let titleBits = Array.from(heroTitle.querySelectorAll('span')),
                pBits = Array.from(heroTitle.querySelectorAll('.lead')),
                btnBits = Array.from(heroTitle.querySelectorAll('.btn-group')),
                bits = titleBits.concat(pBits, btnBits),
                dDelta = 50,
                bitDistance = bits.length;

            bits.forEach(function(bit){

                let titleParallax = new ScrollMagic.Scene({
                    triggerHook: 0,
                    triggerElement: hero,
                    duration: "100%",
                })
                    .setTween(TweenLite.to( bit, 1, { y: '-'+bitDistance+'vh', opacity: 0, ease: 'Circ.easeNone' }));

                scenes.push(titleParallax);

                bitDistance += dDelta;
                dDelta /= 1.3;
            });
        }

        /**
         * ADDING ANIMATIONS
         */

        scenes.forEach(function (scene) {
            ctrl.addScene(scene);

            if (window.gc.sb) {
                window.gc.sb.addListener(() => {
                    scene.refresh();
                });
            }
        });

        window.addEventListener('pageloadend', function(){
            heroTl.play();
        });

        /*  function mobileMediaListener(mediaQuery) {
             if (mediaQuery.matches || window.gc.isMobile) {

             }
         }

         function smMaxMediaListener(mediaQuery) {
             if (mediaQuery.matches || window.gc.isMobile) {
             }
         }

         function mdMinMediaListener(mediaQuery) {
             if (mediaQuery.matches  && !window.gc.isMobile) {
             }
         }

         let mobileMediaCheck = window.matchMedia('(max-width: 767px)');
         mobileMediaCheck.addListener(mobileMediaListener);

         mobileMediaListener(mobileMediaCheck);

         let smMaxMediaCheck = window.matchMedia('(max-width: 991px)');
         smMaxMediaCheck.addListener(smMaxMediaListener);

         smMaxMediaListener(smMaxMediaCheck);

         let mdMinMediaCheck = window.matchMedia('(min-width: 992px)');
         mdMinMediaCheck.addListener(mdMinMediaListener);

         mdMinMediaListener(mdMinMediaCheck);*/
    },
    finalize() {
    },
};
