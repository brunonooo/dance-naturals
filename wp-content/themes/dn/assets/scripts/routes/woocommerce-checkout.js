'use strict';

export default {
    init() {
        const $invalidVariations = $('.invalid-variations');
        if ($invalidVariations.length) {
            $(document).on('click', '#place_order', function (e) {
                e.preventDefault();
                window.alert(window.gc.labels.variations_alert);
                return;
            })
        }
    },
    finalize() {
    },
};
