'use strict';

import Maps from '../../nooo_modules/maps/man-maps';
import mapStyles from '../../nooo_modules/maps/map-style.json';

export default {
    init() {

        function initAnimations (){

            //INIT MAPS

            if(window.mapsManager.googleApiReady) {

                new Maps({
                    selector    : '.map-canvas',
                    center : {
                        lat     : 45.486596,
                        lng     : 11.988424,
                    },
                    forceCenter: false,
                    forceZoom: false,
                    mapOptions      : {
                        center              : {
                            lat     : 45.486596,
                            lng     : 11.988424,
                        },
                        mapTypeId           : window.google.maps.MapTypeId.ROADMAP,
                        scrollwheel         : false,
                        gestureHandling     : 'cooperative',
                        maxZoom             : 10,
                        mapTypeControl      : false,
                        streetViewControl   : false,
                        rotateControl       : false,
                        styles: mapStyles,
                    },
                    markers     : {
                        '.marker' : {
                            icon  : '/assets/vectors/map-marker.svg',
                            size  : {
                                w : 12,
                                h : 12,
                            },
                        },
                    },
                });

                let provinceFilter = document.getElementById('resellers-filter'),
                    sellersInfo = document.getElementById('resellers-info');

                $(sellersInfo.querySelectorAll('li')).hide();

                let initProvinceFilter = (filter, toBeFilteredWrapper) => {
                    filter.addEventListener('change', () => {
                        let selected = filter.value;
                        let filtered = toBeFilteredWrapper.querySelectorAll('[data-province="'+selected+'"]');
                        $(sellersInfo.querySelectorAll('li')).hide();
                        $(filtered).show();
                    });
                };

                let lookForDuplicates = (filter, provName) => {
                    let options = Array.from(filter.querySelectorAll('options'));

                    options.forEach((option) => {
                        if(provName === option.value ){
                            return true;
                        }
                    });

                    return false;
                };

                let lookForProvince = (list, item, filter) => {
                    for ( var k = 0; k < list.length; ++k ){
                        let component = list[k];
                        let province = component.types.indexOf('administrative_area_level_2');
                        if(province >= 0) {
                            let provName = component.long_name;
                            item.setAttribute('data-province', provName);
                            let isDuplicate = lookForDuplicates(filter, provName);

                            if (!isDuplicate) {
                                let option = document.createElement('option');
                                option.value = provName;
                                option.textContent = provName;

                                filter.appendChild(option);

                                return true;
                            }
                        }
                    }
                    return false;
                };

                let geocode = (selector, filter) => {
                    let items = Array.from(document.querySelectorAll(selector));

                    if (items.length) {
                        let geocoder = new window.google.maps.Geocoder();
                        items.forEach((item) => {
                            let lat = item.getAttribute('data-lat'),
                                lng = item.getAttribute('data-lng'),
                                latLng = new window.google.maps.LatLng(lat, lng);

                            geocoder.geocode( { location: latLng}, function(results,status) {
                                if (status == window.google.maps.GeocoderStatus.OK) {
                                    for ( var i = 0; i < results.length; ++i ){
                                        let obj = results[i];
                                        let components = obj.address_components;
                                        let found = lookForProvince(components, item, filter);

                                        // SKIP OTHER RESULTS IF FOUND A PROVINCE
                                        if(found) {
                                            break;
                                        }
                                    }

                                    initProvinceFilter(provinceFilter, sellersInfo);
                                } else {
                                    alert("Problema nella ricerca dell'indirizzo: " + status);
                                }
                            });
                        });
                    }
                };

                geocode('.reseller-info__single', provinceFilter);
            }
        }

        window.addEventListener('pageloadend', function(){
            initAnimations();
        });
    },
    finalize() {
    },
};
