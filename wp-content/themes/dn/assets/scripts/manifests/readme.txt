!###-- messages-config.json --###!
Add messages matches for frontend removal
BE AS SPECIFIC AS POSSIBLE!!! And leave empty string on a language if it is not translated

message structure example:
{
      "it" : "message specific part of the content in italian",
      "en" : "message specific part of the content in english"
},