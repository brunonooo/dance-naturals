/**
 * NEWSLETTER POPUP
 */

export default class Popup{

    constructor(el) {
        this.el = $(el);
    }

    launch(){
        if(!this.checkCookie('diemmenewsletter')){
            let popup = this.el;

            popup.modal();
            this.setCookie('diemmenewsletter', 'seen', 7 );

            popup.on('shown.bs.modal', function(){

                $('body').addClass('scroll-block');

                if( window.gc.fpEnabled ){
                    $.fn.fullpage.setAllowScrolling(false);
                    $.fn.fullpage.setKeyboardScrolling(false);
                }

                history.pushState({ popup: 'open' }, "subscribe popup", "#subscribe");
            });

            window.onpopstate = function(){
                if(!history.state){
                    popup.modal('hide');
                }
            };

            popup.on('hidden.bs.modal', function(){
                $('body').removeClass('scroll-block');

                if( window.gc.fpEnabled ){
                    $.fn.fullpage.setAllowScrolling(true);
                    $.fn.fullpage.setKeyboardScrolling(true);
                }

                history.replaceState({ popup: 'closed' }, "closed popup", "#benvenuto");

            });
        } else {
            return;
        }
    }

    setCookie (cname, cvalue, exdays){
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    readCookie (cname){
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');

        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    }

    checkCookie(cname) {
        var cookie = this.readCookie(cname);

        if (cookie != "") {
            return true;
        }
    }
}