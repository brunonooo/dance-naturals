import H from '../man-helpers';

export default (function (){
    let _selector, _container, _scroller, _scrollerWidth, _overflow;

    function _calcOverflow(){

        if(_scroller.hasChildNodes()){

            var c = 0;
            let children = Array.prototype.slice.call(_scroller.children);
            children.forEach(function(n){
                c += (n.nodeType === 1 ? _outerWidth(n) : 0);
            });

            return c  - _scrollerWidth;
        }
    }

    function _outerWidth(el) {
        var width = el.offsetWidth;
        var style = el.currentStyle || getComputedStyle(el);

        width += parseInt(style.marginLeft) + parseInt(style.marginRight);
        return width;
    }


    function _onResize(){
        _scrollerWidth = _scroller.clientWidth;
        _overflow = _calcOverflow();
    }

    function _onPan(e){
        if(e.target.scrollLeft > 3){
            H.debounce(H.extraClass(_container, 'scrolling'), 50);
        }else{
            H.debounce(H.deleteClass(_container, 'scrolling'), 50)
        }

        if(e.target.scrollLeft > _overflow  - 3 ){
            H.debounce(H.deleteClass(_container, 'scrolling'), 50)
            H.debounce(H.extraClass(_container, 'scrolled'), 50)
        }else{
            H.debounce(H.deleteClass(_container, 'scrolled'), 50)
        }
    }

    function _init(cSelector){
        _selector = cSelector || '.overflowing-bar';

        _container = document.querySelector(_selector);

        if(_container){
            _scroller = _container.querySelector('.overflowing-bar--inner');
            _scrollerWidth = _scroller.clientWidth;
            _overflow = _calcOverflow();

            _scroller.addEventListener('scroll', _onPan);
            _scroller.addEventListener('resize', _onResize);
        }else{
            console.log('You\'re doing it wrong! There is no element matching the specified selector in the page!');
        }
    }

    return {
        init: _init,
    }

})();