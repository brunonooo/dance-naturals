import H from '../nooo-helpers.js'
import '../polyfills/closest';

/**
 *
 * IMAGE BLACKMAGIC
 *
 */
export default (function(){

    let instance;

    function init(options) {

        let wHeight,
            ticking;

        let wScroll = getScrollPos();

        let opt = {
            wrapper: '.img-wrapper',
            target: '[data-magic-src]',
            targetOnScroll: '[data-lazy-onscroll]',
            dataSource: 'data-magic-src',
            placeholderSize: 50,
            smoothScrollBar: null,
            threshold: 1,
            nodesList: [],
        };

        function getScrollPos () {
            return window.scrollY || window.pageYOffset
        }

        function getOffset (node) {
            return node.getBoundingClientRect().top + wScroll;
        }

        function inViewport (node) {
            const viewTop = wScroll;
            const viewBot = viewTop + wHeight;

            const nodeTop = getOffset(node);
            const nodeBot = nodeTop + node.offsetHeight;

            const offset = (opt.threshold / 100) * wHeight;

            return (nodeBot >= viewTop - offset) && (nodeTop <= viewBot + offset);
        }

        let updateScroll = H.throttle(() => {
            if(opt.smoothScrollBar){
                wScroll = window.gc.sb.scrollTop
            } else {
                wScroll = getScrollPos();
            }
            requestFrame(check);
        }, 250);

        function requestFrame (callback) {
            if (!ticking) {
                window.requestAnimationFrame(callback);
                ticking = true
            }
        }

        function swapSrc(node){

            let canvas = node.querySelector(opt.target),
                img = node.querySelector('.lazy-image');

            if(canvas){

                // GET LAZY URL
                let lazyUrl = canvas.getAttribute(opt.dataSource),
                    lazySrcSet = canvas.getAttribute(opt.dataSource+'set');

                img.src = lazyUrl;
                img.srcset = lazySrcSet;

                img.onload = function() {
                    canvas.removeAttribute(opt.dataSource);
                    img.setAttribute('data-load-ready','true');
                    autoUpdate();
                };
            }
        }

        function check(){
            wHeight = window.innerHeight;

            opt.nodesList.forEach(function(node){
                if (node.querySelector('[data-lazy-onscroll]')) {
                    let visible = opt.smoothScrollBar ? opt.smoothScrollBar.isVisible(node) : inViewport(node);

                    if (visible) {
                        swapSrc(node);
                    }
                }
            });

            ticking = false;
            return this;
        }

        function magicLoadInto(context){
            let lazyContext = context;

            if ( typeof lazyContext === 'string' ){
                lazyContext = document.querySelector(context);
            }

            let lazyNodes = lazyContext.querySelectorAll(opt.wrapper);

            if (lazyNodes.length) {
                Array.prototype.forEach.call(lazyNodes, function (el) {
                    swapSrc(el);
                });
            }

            ticking = false;
            return this
        }

        function magicLoad(el){
            let lazyNode = el;

            if ( typeof lazyContext === 'string' ){
                lazyNode = document.querySelectorAll(el);
            }

            let node = lazyNode.closest(opt.wrapper);

            if (node) {
                swapSrc(node);
            }

            ticking = false;
            return this
        }

        function mergeOptions(options){
            if(options){
                // Merge default options and passed options
                opt = Object.assign(opt, options);
            }
        }

        function optionsUpdate(options){
            if(options){
                // Merge default options and passed options
                opt = Object.assign(opt, options);
                autoUpdate();
            }
        }

        function autoUpdate(){

            let wrappers = document.querySelectorAll(opt.wrapper);
            opt.nodesList = [];

            Array.prototype.forEach.call(wrappers, function(el){

                if(el.querySelector(opt.target)){
                    opt.nodesList.push(el);
                }
            });

        }

        function forceUpdate(){
            autoUpdate();
            getImagesInfos(opt.nodesList);
            updateScroll();
        }

        function createPlaceholder(imgData){

            let img = new Image();

            img.src = imgData.placeholder_src;
            imgData.placeholder.setAttribute(opt.dataSource, imgData.src);
            imgData.placeholder.setAttribute('data-magic-srcset', imgData.srcset);

            /*img.onload = function(){
                boxBlurImage(img, imgData.canvas, 8, false, 2);
            };*/
        }

        function getImagesInfos(nodesList){
            Array.prototype.forEach.call(nodesList, function(el){

                let dataHolder = el.querySelector(opt.target),
                    ph = el.querySelector('.lazy-placeholder');

                // GET LAZY URL
                let lazyUrl = dataHolder.getAttribute(opt.dataSource);

                // REMOVE ATTRIBUTE TO REMOVE FROM QUERY LIST
                dataHolder.removeAttribute(opt.dataSource);

                if(lazyUrl){

                    let imgData = {
                        src:        lazyUrl,
                        srcset:     dataHolder.getAttribute('data-magic-srcset'),
                        sizes:      dataHolder.getAttribute('data-magic-sizes'),
                        placeholder:     ph,
                        placeholder_src: ph.src,
                        srcsetTree: [],
                        maxSize:    null,
                    };

                    createPlaceholder(imgData);
                }
            });
        }

        /**
         * ACTUAL INITIALIZATION CODE
         */
        mergeOptions(options);
        autoUpdate();
        getImagesInfos(opt.nodesList);

        if(window.gc.sb){
            window.gc.sb.addListener(() => {
                updateScroll();
            })
        } else {
            document.addEventListener('scroll', updateScroll);
        }
        updateScroll();

        // IF DESKTOP START LAZY LOAD EVERY 250MS TO IMPROVE SCROLL PERFORMANCE
        if (!window.gc.isMobile) {
            let interval = setInterval(function(){
                if (opt.nodesList[0]){
                    if (opt.nodesList[0].querySelector('[data-lazy-onscroll]')) {
                        swapSrc(opt.nodesList[0]);
                    }
                } else {
                    clearInterval(interval);
                    console.log(
                        '%c%s',
                        `display: block;
                     background-color: #33b679;
                     color: #f3f3f3;
                     line-height: 2;
                     text-align: left;`,
                        'All images loaded - Manuel'
                    );
                }
            }, 250);
        }

        return {
            lazyLoadInto: magicLoadInto,
            lazyLoad: magicLoad,
            refresh: forceUpdate,
            options: optionsUpdate,
        }
    }

// API
    return {
        init:  function (options) {
            if (!instance) {
                instance = init(options);
            } else {
                console.log(
                    '%c%s',
                    `display: block;
                     background-color: rgba(255, 206, 109, .8 ); 
                     color: #D88700; 
                     line-height: 2; 
                     text-align: left;`,
                    'Hey, no cheating! Only one imageManager per page allowed my friend!\n - Manuel'
                );
            }

            return instance;
        },
    };

})();
