/**
 *
 * Simulated fullheight link
 *
 */

export default class SimLink {
    constructor(options) {
        const def = {
            router : 'a',
            target : '.simulated-link-target',
        };

        this.opt = options ? {...def, ...options} : def;
        this.init();
    }

    simulateLink = (e) => {
        e.stopPropagation();
        e.preventDefault();

        let ref = this;

        let target = e.currentTarget.querySelector(ref.opt.router),
            targetBlank = target.getAttribute('target') == '_blank',
            route = target.href;

        if (targetBlank || e.metaKey) {
            window.open(route, '_blank');
        } else {
            window.location = route;
        }
    };

    checkLinks = () => {
        let ref = this;

        $(ref.opt.target).each(function(){
            let $link = $(this).find('a');

            if( $link.length > 1 ) {
                /* eslint-disable */
                console.log(
                    '%c%s',
                    `display: block;
                     background-color: rgba(255, 206, 109, .8 ); 
                     color: #D88700; 
                     line-height: 2; 
                     text-align: left;`,
                    'Hey! you should check your simulated links mate, because you\'ve got multiple links inside upcoming selector. This could lead to various problems and links might not work as expected so... Check your \'a\'s and thank me later!\n - Manuel'
                );
                console.log(this);
                /* eslint-enable */
            }
        });
    };

    init = () => {
        let ref = this;
        this.checkLinks();

        $('body').on( 'click', ref.opt.target, ref.simulateLink);
    };

    destroy = () => {
        let ref = this;
        $('body').off( 'click', ref.opt.target, ref.simulateLink);
    };

    refresh = () => {
        this.destroy();
        this.init();
    };
}
