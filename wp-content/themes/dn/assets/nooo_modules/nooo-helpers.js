var h = {
    $body : $('body'),
    $html : $(document.documentElement),
    isHome :  document.querySelector('.home'),
    isBlog: document.querySelector('.blog'),
    header: document.querySelector('.site-header'),
    headersHeight: $('.site-header').innerHeight(),
    wHeight: $(window).height(),
    wWidth: $(window).width(),

    scrollPos() {
        return window.scrollY || window.pageYOffseth
    },
    extraClass(el, className){

        let classList = className.split(' ');

        classList.forEach( (singleClass) => {
            if (el.classList){
                el.classList.add(singleClass);
            }else{
                el.className += ' ' + singleClass;
            }
        })
    },
    deleteClass(el, className){
        let classList = className.split(' ');

        classList.forEach((singleClass) => {
            if (el.classList)
                el.classList.remove(singleClass);
            else
                el.className = el.singleClass.replace(new RegExp('(^|\\b)' + singleClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        });
    },
    debounce( func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;

            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };

            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    },
    throttle(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            if ( !timeout ) timeout = setTimeout( later, wait );
            if (callNow) func.apply(context, args);
        };
    },
    isVertical () {
       return window.innerHeight > window.innerWidth
    },
    checkOrientation () {
        function setVertical () {
            this.deleteClass(document.documentElement, 'horizontal');
            this.extraClass(document.documentElement, 'vertical');
        }

        function setHorizontal () {
            this.deleteClass(document.documentElement, 'vertical');
            this.extraClass(document.documentElement, 'horizontal');
        }

        this.isVertical() ? setVertical.call(this) : setHorizontal.call(this);
    },
};

h.headersHeight =  h.header.clientHeight;

window.addEventListener('resize', function(){
    h.headersHeight = h.header.clientHeight;
    h.wHeight = $(window).height();
});

export default h;
