
import ua from '../man-hound';
import mixitup from 'mixitup';
import OB from '../overflowing_bar/man-overflowing-bar';

let filterGrid = function(){

    OB.init();

    let Grid = {
        $el: $('#filtered-grid'),
        bigView : true,
        toggleView : () => {
            if(Grid.bigView){
                Grid.$el.addClass('filtered-grid__small-view');
                Grid.bigView = false;
            }else {
                Grid.$el.removeClass('filtered-grid__small-view');
                Grid.bigView = true;
            }
        },
    };

    $('.grid-control').on('click', function(){

        //IF ACTIVE BAIL EARLY
        if($(this).hasClass('active')){
            return;
        }else{
            Grid.toggleView();
            $('.grid-control').removeClass('active');
            $(this).addClass('active');
        }
    });

    mixitup(Grid.$el, {
        selectors: {
            target: '.filtered-grid--item',
        },
        animation: {
            duration: 400,
            effectsIn: 'fade scale(.8)',
            effectsOut: 'fade scale(.8)',
            perspectiveDistance: '500px',
        },
        load: {
            filter: 'all',
        },
    });

    let gridTouchHandler = function(){
        let $tut = $('.touch-tutorial')

        // BAIL EARLY IF NO TUT
        if(!$tut.length){
            return;
        }

        $tut.addClass('in');

        $('.touch-tutorial, #dispatch-tut').click(function(){
            $tut.removeClass('in');
        });
    };

    if(ua.isMobile){
        gridTouchHandler();
    }
}

export default filterGrid;