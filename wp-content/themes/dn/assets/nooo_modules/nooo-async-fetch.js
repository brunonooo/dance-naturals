import $ from 'jquery';
import merge from 'deepmerge';

export default class AsyncManager {
    constructor(_trigger, _selector, _opt = {}) {
        let def = {
            perPage: 1,
            offset: 0,
            action: 'load_more_posts',
            callback: this.renderNewCards,
        };

        this.options = merge(def, _opt);
        this.trigger = _trigger;
        this.selector = _selector;
        this.options.offset = document.querySelectorAll(_selector).length + this.options.offset;
        this.triggersList = document.querySelectorAll(_trigger);

        this.setListeners();
    }

    fetchPosts = (e) => {
        const ppp = this.options.perPage.toString(),
            offset = this.options.offset.toString(),
            action = this.options.action,
            callback = this.options.callback,
            inst = this;

        this.deactivateTrigger(e);

        /* eslint-disable no-undef */
        $.ajax({
            url: ajax.ajaxUrl,
            type: 'post',
            data: {
                action: action,
                posts_per_page: ppp,
                offset: offset,
            },
            error: (error) => {
                alert('Ops, something went wrong!');
                console.log('Ups, something went wrong with the request: \n' + error );
            },
            success : (response) => {

                const data = JSON.parse(response);
                //SET NEW OFFSET
                inst.options.offset += inst.options.perPage;
                console.log(data);

                // IF WE REACHED LAST POST
                if(data.reached_max){ inst.setReachedMax(e) }

                //RENDER WHAT WE HAVE
                callback(data.html);

                //REACTIVATE TRIGGER
                inst.activateTrigger(e);
            },
        });
        /* eslint-enable no-undef */
    };

    setListeners = () => {
        const inst = this;

        this.triggersList.forEach((el) => {
            el.addEventListener('click', inst.fetchPosts.bind(inst));
        });
    };

    deactivateTrigger = (e) => {
        e.target.setAttribute('disabled', 'disabled');
    };

    activateTrigger = (e) => {
        e.target.removeAttribute('disabled');
    };

    setReachedMax = (e) => {
        $(e.target).addClass('reached-max');
    };
}