import merge from 'deepmerge';

export default class ThreeD {
    constructor(options){
        let ref = this;
        let def = {
            context : '[data-threed-context]',
            platform : '[data-threed-platform]',
            layers :  '[data-threed-layer]',
            inverted : false,
            scaleLayers : true,
            relativeScroll : false,
            smoothReset : {
                active : true,
                speed : 800,
                timeout : null,
                needsReset : true,
            },
            mobile: {
                breakpoint : 767,
                reductionGlobal: 1000,
                reductionLayers : 9,
                reductionStrength : 6,
            },
        };

        this.opt = options ? merge(def, options) : def;

        // CACHE TARGET AND LAYERS ELEMENTS IN THE PAGE
        this.$targets = $(this.opt.context).find(this.opt.platform);
        this.$layers = this.$targets.find(this.opt.layers);

        // CACHING WINDOW SIZE AND RESETTING ON RESIZE
        this.wSize = this.getWindowSize();

        $(window).on('resize', function(){
            this.wSize = ref.getWindowSize();
        });

        // MOBILE BEHAVIOUR
        if ( window.DeviceOrientationEvent && this.wSize.w < this.opt.mobile.breakpoint ) {
            window.addEventListener('deviceorientation', this.orientationMovement, {
                passive: true,
            });
        } else {
            this.moveCards3D();
        }
    }

    getWindowSize = () => {
        return {
            w : window.innerWidth,
            h : window.innerHeight,
        }
    };

    calcTargetData = (target) => {
        let data = {
            $el : $(target),
        };

        data.w = data.$el.outerWidth();
        data.h = data.$el.outerHeight();
        data.isHorizontal = window.Math.max(data.w, data.h) === data.w;

        if(data.isHorizontal) {
            data.ratioOffset =  data.h / data.w * 5; // (10% / 2)
        } else {
            data.ratioOffset =  data.w / data.h * 5; // (10% / 2)
        }

        data.offset = this.opt.relativeScroll ? data.$el.offset() : data.$el.get(0).getBoundingClientRect();

        return data;

    };

    moveCards3D = () => {
        // BAIL EARLY IF NO TARGETS
        if(!this.$targets.length){
            return;
        }

        let ref = this;

        this.$targets.on('mousemove', function(e) {
            let target = ref.calcTargetData(this);

            let $layers = target.$el.find(ref.opt.layers);

            if( ref.opt.smoothReset.active && !ref.opt.smoothReset.timeout && ref.opt.smoothReset.needsReset){
                ref.smoothResetReset($layers);
            }

            // CALCULATE MOVEMENTS
            let moveX = .5 - (e.clientX - target.offset.left) / target.w,
                moveY = .5 - (e.clientY - target.offset.top) / target.h,
                strength =  target.$el.data('threed-strength') ? target.$el.data('threed-strength') : 1;

            // APPLY RATIO OFFSET
            if(target.isHorizontal) {
                moveY = moveY * target.ratioOffset;
            } else {
                moveX = moveX * target.ratioOffset;
            }

            // INVERT DIRECTION
            strength = ref.opt.inverted ? window.Math.abs(strength) : -window.Math.abs(strength);

            let translate = 'translate3d(0 ,' + -moveX * strength+ 'px, 0) rotateX('+ -moveY * strength +'deg) rotateY('+ moveX * strength +'deg)';

            // APPLY TRANFORM
            target.$el.css('transform', translate);

            // TRANFORM SUBLAYERS
            $layers.each(function() {
                let $this = $(this),
                    strengthLayer = $this.data('threed-strength') || 1;

                // INVERT DIRECTION
                strengthLayer = ref.opt.inverted ? -window.Math.abs(strengthLayer) : window.Math.abs(strengthLayer);

                let scaleCalc = ref.opt.inverted ? 1 - strengthLayer / 100 : 1 + strengthLayer / 100,
                    scale = 'scale('+ scaleCalc + ')';

                let translateLayer = 'translate3d('+ moveX * strengthLayer +'px,'+ moveY * strengthLayer +'px, 0)' + scale;
                // APPLY TRANFORM
                $this.css('transform', translateLayer);
            });
        });

        if(ref.opt.smoothReset.active) {
            ref.smoothResetInit();

            this.$targets.on('mouseleave', function () {
                ref.smoothResetInit();
            });
        }
    };

    smoothResetInit = () => {

        // GO BACK TO PLACE SMOOOTHLY
        this.$targets.addClass('smooth-reset');
        this.$layers.addClass('smooth-reset');
        this.opt.smoothReset.needsReset = true;

        this.$targets.css('transform', 'translate3d(0,0,0)');
        this.$layers.css('transform', 'translate3d(0,0,0)');
    };

    smoothResetReset = ($layers) => {
        let ref = this;

        this.opt.smoothReset.needsReset = false;

        this.opt.smoothReset.timeout = window.setTimeout(function(){
            ref.$targets.removeClass('smooth-reset');

            if($layers.length){
                $layers.removeClass('smooth-reset');
            }

            ref.opt.smoothReset.timeout = null;
        }, ref.opt.smoothReset.speed);
    };

    // DEVICE ORIENTATION BEHAVIOUR
    orientationMovement = (e) => {
        let ref = this;

        /*let alpha = e.alpha;*/
        let beta = e.beta - 45;
        let gamma = e.gamma / this.opt.mobile.reductionStrength;

        // CEILING TO BLOCK EFFECT IN EXTREME ORIENTATIONS
        beta = e.beta > 180 ? 40 : beta;
        beta = e.beta < 0 ? - 90 : beta;
        beta = e.beta > 90 ? 90 : beta;
        gamma = e.gamma < - 40 ? - 40 / this.opt.mobile.reductionStrength : gamma ;
        gamma = e.gamma > 40 ? 40 / this.opt.mobile.reductionStrength : gamma ;

        let strength = this.$targets.data("threed-strength") / 3;

        strength = ref.opt.inverted ? window.Math.abs(strength) : -window.Math.abs(strength);

        let transform = "translateY(" + gamma * strength / this.opt.mobile.reductionGlobal + "px) rotateX(" + beta / this.opt.mobile.reductionStrength * strength + "deg) rotateY(" + gamma *  strength + "deg)";

        this.$targets.css('transform', transform);

        this.$layers.each(function() {
            let $layer = $(this),
                strengthLayer = $layer.data('threed-strength') || 0,
                transformLayer = "translateX(" + (gamma * strengthLayer/ ref.opt.mobile.reductionLayers) + "px) translateY(" + ((beta/ ref.opt.mobile.reductionStrength) * strengthLayer / ref.opt.mobile.reductionLayers) + "px)";

            $layer.css("transform", transformLayer);
        });
    }
}
