import {ScrollbarPlugin} from 'smooth-scrollbar';

export default class BlockPlugin extends ScrollbarPlugin {
    static pluginName = 'block';

    static defaultOptions = {
        open: false,
    };

    transformDelta(delta) {
        return this.options.open ? { x: 0, y: 0 } : delta;
    }
}