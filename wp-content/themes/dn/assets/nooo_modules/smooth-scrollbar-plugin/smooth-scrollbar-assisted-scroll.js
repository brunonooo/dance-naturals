import { ScrollbarPlugin } from 'smooth-scrollbar';

export default class ScrollEndPlugin extends ScrollbarPlugin {
    static pluginName = 'state';

    onInit () {
        this.scrollEnd = new Event('sb.scrollEnd');
    }

    onRender(remainMomentum) {
        if ( remainMomentum.y < 10 ) {
            window.dispatchEvent(this.scrollEnd);
        }
    }
}
