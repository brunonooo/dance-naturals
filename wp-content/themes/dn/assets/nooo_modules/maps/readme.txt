ES6 code, babel needed!

MORE CODE NEEDED TO MAKE MAPS WORK:
**/ Wordpress enqueue script
wp_enqueue_script('gmaps', "https://maps.googleapis.com/maps/api/js?key=YOUR-API-KEY-HERE&callback=mapsManager.mapsCheck, array(), null, true);
-- (Optional and not needed if you are using my helpers.php script) Add this to your functions.php to make it asynchronous:

///CODE:
function add_async_attribute($tag, $handle) {
    $scripts_to_async = array('gmaps');

    foreach($scripts_to_async as $async_script) {
        if ($async_script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }
    return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
///ENDCODE;

**/ HEAD CODE
-- Don't forget to add this to your head

////CODE:
<script>
window.mapsManager = {
    googleApiReady: false,
    initializerReady: false,
    mapsCheck : function() {
        window.mapsManager.googleApiReady = true;

        if(window.mapsManager.initializerReady && typeof window.mapsManager.map !== 'undefined'){
            console.log('initmap from header');
            window.mapsManager.map.init();
        }
    }
};
</script>
///ENDCODE;

**/ MAP MARKUP
- Having multiple markers is fine

////CODE:
<div class="map-canvas">
        <div class="marker" data-lat="YOUR-LATITUDE" data-lng="YOUR-LONGITUDE">
            <div class="marker-inner">
                YOUR INFO WINDOW CONTENT
            </div>
        </div>
</div>
///ENDCODE;

**/ JS INIT
import Maps from '../../nooo_modules/maps/nooo-maps';

 if(mapsManager.googleApiReady) {
     new Maps({
      selector    : '.map-canvas',
      center : {
          lat     : -34.397,
          lng     : 150.644,
      },
      forceCenter: false,
      zoom            : 10,
      forceZoom: false,
      mapOptions      : {
          center              : this.center,
          zoom                : 10,
          mapTypeId           : google.maps.MapTypeId.ROADMAP,
          scrollwheel         : false,
          gestureHandling     : 'cooperative',
          maxZoom             : 10,
          mapTypeControl      : false,
          streetViewControl   : false,
          rotateControl       : false,
      },
      markers     : {
          '.marker' : {
              icon  : '/assets/vectors/map-markers/custom-marker.svg',
              size  : {
                  w : 25,
                  h : 25
              },
          }
      });
}

YOU CAN THANK ME LATER,
Manuel