import merge from 'deepmerge';

export default class Maps {
    constructor(options) {
        const def = {
            selector    : '.map-canvas',
            center : {
                lat     : -34.397,
                lng     : 150.644,
            },
            forceCenter: false,
            zoom            : 10,
            forceZoom: false,
            mapOptions      : {
                center              : {
                    lat     : -34.397,
                    lng     : 150.644,
                },
                zoom                : 10,
                mapTypeId           : window.google.maps.MapTypeId.ROADMAP,
                scrollwheel         : false,
                gestureHandling     : 'cooperative',
                maxZoom             : 10,
                mapTypeControl      : false,
                streetViewControl   : false,
                rotateControl       : false,
            },
            onMapLoad  : null,
        };

        this.opt = options ? merge(def, options) : def;
    }

    /**
     *  center_map
     *  This function will center the map, showing all markers attached to this map
     */
    center_map = ( map ) => {
        let ref = this;
        let bounds = new window.google.maps.LatLngBounds();
        let listOfCoordinates = map.markers;

        // NO MARKERS?
        if(!map.markers) {
            map.setCenter(ref.opt.center);
            return;
        }

        // ONLY 1 MARKER?
        if( listOfCoordinates.length == 1 ) {
            let center = this.opt.forceCenter ? this.opt.center : listOfCoordinates[0].getPosition();
            // SET CENTER OF MAP
            map.setCenter( center );
            map.setZoom( this.opt.zoom );

        } else {

            // LOOP THROUGH ALL COORDINATES AND CREATE BOUNDS
            for( let i = 0; i < listOfCoordinates.length; ++i ){

                // NORMALIZING POSITION DATA
                let thisCoords =  listOfCoordinates[i].getPosition();

                bounds.extend( thisCoords );
            }
        }
    };

    setup_markers_options = (marker, args ) => {
        let lat =  marker.getAttribute('data-lat') ?  marker.getAttribute('data-lat') : this.opt.center.lat,
            lng =  marker.getAttribute('data-lng') ?  marker.getAttribute('data-lng') : this.opt.center.lng,
            latlng = new window.google.maps.LatLng( lat, lng );


        let size = args.markersGroup.size ? args.markersGroup.size : { w: 25, h : 25 };
        // ICON OPTIONS
        let iconOptions = {
            url         : window.gc.themePath + args.markersGroup.icon,
            size        : new window.google.maps.Size(size.w, size.h),
            scaledSize  : new window.google.maps.Size(size.w, size.h),  // makes SVG icons work in IE
        };

        return {
            position    : latlng,
            map         : args.map,
            optimized   : false,
            icon        : iconOptions,
        }
    };

    /*
     *  add_marker
     *  This function will add a marker to the selected Google Map
     */
    add_marker = ( args ) => {
        let ref = this;
        let markersInGroup = args.markersGroup.ref;

        markersInGroup.forEach( function(marker){
            let markerOptions =  ref.setup_markers_options(marker, args);

            // CREATE MARKER
            let mrk = new window.google.maps.Marker(markerOptions);

            // ADD TO MAP REFERENCE ARRAY
            args.map.markers.push( mrk );

            // ADD REFERENCE TO LAST OPENED INFO WINDOW
            args.map.lastOpenedIw = '';

            // IF MARKER CONTAINS HTML, ADD IT TO AN INFOWINDOW
            let infoContent = marker.innerHTML;

            if( infoContent  ) {

                // CREATE INFO WINDOW
                let infowindow = new window.google.maps.InfoWindow({
                    content     : infoContent,
                });

                // SHOW INFO WINDOW WHEN MARKER IS CLICKED
                window.google.maps.event.addListener(mrk, 'click', function() {

                    // CLOSE OPENED INFOWINDOWS
                    if( args.map.lastOpenedIw ){
                        args.map.lastOpenedIw.close();
                    }

                    infowindow.open( args.map, mrk );

                    // SAVE REFERENCE TO CURRENTLY OPENED INFOWINDOW
                    args.map.lastOpenedIw = infowindow;
                });
            }
        });
    };

    /*
     *  new_map
     *  This function will render a Google Map onto the selected jQuery element
     */
    new_map = ( el ) => {
        let ref = this;

        if(this.opt.markers){
            // STORE MARKERS NODES REFERENCE IN EACH GROUP
            Object.keys(ref.opt.markers).forEach(function(key) {
                ref.opt.markers[key].ref = Array.from(el.querySelectorAll(key));
            });
        }

        // CREATE MAP
        let map = new window.google.maps.Map( el, ref.opt.mapOptions);

        if(this.opt.markers) {
            // ADD A MARKERS REFERENCE
            map.markers = [];

            // ADD MARKERS
            Object.keys(this.opt.markers).forEach(function (key) {
                ref.add_marker({
                    markersGroup: ref.opt.markers[key],
                    map: map,
                });
            });
        }

        ref.center_map( map );

        return map;
    };


    init = () => {
        let ref = this;
        let mapCanvases = Array.from( document.querySelectorAll( ref.opt.selector));

        let maps = mapCanvases.map(function(map){
            return ref.new_map(map);
        });

        // STORE MAPS REFERENCES IN GLOBAL CONTEXT
        window.gc.maps = maps;

        if(this.opt.onMapLoad){
            this.opt.onMapLoad();
        }
    };
}
