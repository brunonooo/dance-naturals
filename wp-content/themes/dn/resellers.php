<?php
/**
 * Template Name: Rivenditori
 */

$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['resellers'] = Timber::get_posts(array(
	'post_type' => 'reseller',
	'posts_per_page' => -1
));

Timber::render('resellers.twig' , $context );
